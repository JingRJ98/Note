# 路由
@/app文件夹下的文件夹名自动约定为路由
例如创建一个dashboard文件夹`@/app/dashboard`, 则路由为/dashboard, 大小写敏感
实测浏览器url内容会为全小写(不包括params参数) 所以文件夹必须是小写

/路由展示的内容为 @/app/page.tsx组件
同理 每一级路由都显示该文件夹下的page.tsx/jsx 组件
必须是全小写的page, tsx和jsx无所谓

文件中嵌套文件夹 对应多级路由
@/app/dashboard/customers 对应路由 /dashboard/customers
展示的组件则为@/app/dashboard/customers/page.tsx

## 路由跳转
import Link from 'next/link'
Link组件来切换路由, 相比a标签跳转, 不会引起页面的刷新
为了改善导航体验，Next.js 自动按路线段对应用程序进行代码分割。
这与传统的 React SPA不同，浏览器在初始加载时加载所有应用程序代码。
next会自动在后台 预取 对应的路由, 并提前加载页面, 这使得导航页面的速度大大加快
原理: https://nextjs.org/docs/app/building-your-application/routing/linking-and-navigating#how-routing-and-navigation-works

## 当前路由
获取当前路由只能在客户端组件 需要声明 'use client'
```js
'use client'

import { usePathname } from 'next/navigation'
// const pathname = usePathname()
const pathname = usePathname()
```

## 忽略路由
()包裹的文件夹不会包含在url路径中
此/dashboard/(overview)/page.tsx 的路由还是 /dashboard

## 动态路由
[]包裹的文件夹会使用方括号中的值来动态创建路由
例如@/app/dashboard/invoices/[id]/edit的路由为 /dashboard/invoices/314rdwx8342rwdeq/edit


## pages组件默认props
searchParams 可以读取到url后面的params参数 是对象的形式


# 布局
在@/app/文件夹下 创建layout.tsx, layout必须全小写
layout.tsx文件能自动访问到本文件夹的页面组件和子文件夹的组件


# 静态渲染和动态渲染
组件不依赖于数据，也不针对用户进行个性化，因此它可以是静态的。

组件依赖于经常更改的数据，并将针对用户进行个性化设置，因此它们可以是动态的。

# 错误处理
路由文件中添加error.tsx来处理异常报错
添加not-found.tsx来处理404页面