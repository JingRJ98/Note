# 数据库分类
数据库主要分为两种
关系型数据库（RDBMS）
MySQL SQL Server……
关系型数据库中全是表


非关系型数据库（Not Only SQL）
MongoDB redis

# 简介
是一个基于分布式文件存储的数据库

数据库是按照数据结构来组织存储和管理数据的应用程序

主要作用就是管理数据,实现数据的crud

相对于纯文件管理数据,数据库管理数据的优势:
- 速度更快
- 扩展性更强
- 安全性更强

# why mongoDB
语法接近js, 容易上手, 学习成本低

# 核心概念
## 数据库（database）
仓库，中可以存放集合
仓库不需要手动创建

## 集合（collection）
数据库里面可以包含很多集合

## 文档（document）
文档是数据库中的最小单位，我们存储和操作的都是文档

# 安装

安装并运行mongoDB

1. 插入环境变量
`vim ~/.bash_profile`
`export PATH=${PATH}:/usr/local/mongodb/bin` 
`source ~/.bash_profile`
2. 运行
`mongod --dbpath /usr/local/mongodb/data/db` 
如果出现没有mongod命令 执行第一步
3. 另一个窗口打开
`mongo`


# 命令
## 数据库命令
- 显示所有数据库`show dbs`
- 切换到指定的数据库,数据库不存在自动创建 `use name`
- 显示当前所在的数据库 `db`
- 删库
`use <想删的数据库名称>`
`db.dropDatabase()`

## 集合命令
- 创建集合`db.createCollection('users')`
- 显示所有集合`show collections`
- 删除某个集合`db.集合名.drop()`
- 重命名集合`db.集合名.renameCollection('新名字')`

## 文档命令
- 插入文档`db.集合名.insert(对象)`
- 查询文档`db.集合名.find(查询条件)` 查询条件为空时显示全部文档
- 更新文档`db.集合名.update(查询条件, 新的文档)`
  如果需要保留没有发生修改的属性 `db.集合名.update(查询条件, {$set: 新对象})`
- 删除文档`db.集合名.remove(查询条件)` 查询条件为空时删除全部文档

# mongoose
对象文档模型库 http://www.mongoose.net
作用: 方便使用代码自动操作数据库

## 连接数据库
```js
// 链接数据库
const mongoose = require('mongoose')

// 协议名称://ip地址:端口/数据库名称
mongoose.connect('mongodb://127.0.0.1:27017/bili')

// 设置回调
// once表示回调只执行一次 
mongoose.connection.once('open', () => {
  console.log('连接成功');
  // 创建文档的结构对象
  const bookSchema = new mongoose.Schema({
    // 约束集合中文档的属性和值类型
    // 类似ts中的interface
    name:String,
    author: String,
    price: Number,
  })
  // 创建模型对象
  // model接受两个参数,第一个集合名称,第二个文档的结构(上面定义的'类型')
  const bookModel = mongoose.model('books', bookSchema)
  // 新增数据
  bookModel.create({
    name: '西游记',
    author: '吴承恩',
    price: 20
  }).then((data) => {
    console.log(data)
  })
})

mongoose.connection.on('error', () => {
  console.log('连接失败');
})

mongoose.connection.on('close', () => {
  console.log('连接关闭');
})

// setTimeout(() => {
//   // 关闭连接
//   mongoose.disconnect()
// }, 2000)
```

## 文档结构对象可以选的类型值
String 
Number
Boolean
Array
Date 日期
Buffer
Mixed 任意类型 需要使用mongoose.Schema.Types.Mixed指定
ObjectId 对象id 需要使用mongoose.Schema.Types.ObjectId 指定
Decimal128 高精度数字 需要使用 mongoose.Schema.Types.Decimal128 指定

当对象值和设置的文档结构类型不一致的时候会创建失败


## 字段值校验
有点类似表单的校验
校验字段是否必选, 默认值, 枚举值等等
```js
  const bookSchema = new mongoose.Schema({
    // 约束集合中文档的属性和值类型
    name:{
      type: String,
      required: true, // 该属性必须不为空
      default: '匿名'
    },
    gender: {
      type: String,
      enum: ['男', '女'], // 所选的值必须在给定的枚举中,否则报错
    },
    username: {
      type: String,
      unique: true, // 表示username这个值必须是独一无二的
      // unique需要重建集合才能起作用
    }
    author: String,
    price: Number,
  })
```

## 创建一个小说数据库
```js
//1. 安装 mongoose
//2. 导入 mongoose
const mongoose = require('mongoose');

//设置 strictQuery 为 true
mongoose.set('strictQuery', true);

//3. 连接 mongodb 服务                        数据库的名称
mongoose.connect('mongodb://127.0.0.1:27017/bilibili');

//4. 设置回调
// 设置连接成功的回调  once  事件回调函数只执行一次
mongoose.connection.once('open', () => {
  //5. 创建文档的结构对象
  //设置集合中文档的属性以及属性值的类型
  let BookSchema = new mongoose.Schema({
    name: String,
    author: String,
    price: Number,
    is_hot: Boolean
  });

  //6. 创建模型对象  对文档操作的封装对象    mongoose 会使用集合名称的复数, 创建集合
  let BookModel = mongoose.model('novel', BookSchema);

  //7. 新增数据
  BookModel.insertMany([{
    name: '西游记',
    author: '吴承恩',
    price: 19.9,
    is_hot: true
  }, {
    name: '红楼梦',
    author: '曹雪芹',
    price: 29.9,
    is_hot: true
  }, {
    name: '三国演义',
    author: '罗贯中',
    price: 25.9,
    is_hot: true
  }, {
    name: '水浒传',
    author: '施耐庵',
    price: 20.9,
    is_hot: true
  }, {
    name: '活着',
    author: '余华',
    price: 19.9,
    is_hot: true
  }, {
    name: '狂飙',
    author: '徐纪周',
    price: 68,
    is_hot: true
  }, {
    name: '大魏能臣',
    author: '黑男爵',
    price: 9.9,
    is_hot: false
  },
  {
    name: '知北游',
    author: '洛水',
    price: 59,
    is_hot: false
  },{
    name: '道君',
    author: '跃千愁',
    price: 59,
    is_hot: false
  },{
    name: '七煞碑',
    author: '游泳的猫',
    price: 29,
    is_hot: false
  },{
    name: '独游',
    author: '酒精过敏',
    price: 15,
    is_hot: false
  },{
    name: '大泼猴',
    author: '甲鱼不是龟',
    price: 26,
    is_hot: false
  },
  {
    name: '黑暗王者',
    author: '古羲',
    price: 39,
    is_hot: false
  },
  {
    name: '不二大道',
    author: '文刀手予',
    price: 89,
    is_hot: false
  },
  {
    name: '大泼猴',
    author: '甲鱼不是龟',
    price: 59,
    is_hot: false
  },
  {
    name: '长安的荔枝',
    author: '马伯庸',
    price: 45,
    is_hot: true
  },
  {
    name: '命运',
    author: '蔡崇达',
    price: 59.8,
    is_hot: true
  },
  {
    name: '如雪如山',
    author: '张天翼',
    price: 58,
    is_hot: true
  },
  {
    name: '三体',
    author: '刘慈欣',
    price: 23,
    is_hot: true
  },
  {
    name: '秋园',
    author: '杨本芬',
    price: 38,
    is_hot: true
  },
  {
    name: '百年孤独',
    author: '范晔',
    price: 39.5,
    is_hot: true
  },
  {
    name: '在细雨中呼喊',
    author: '余华',
    price: 25,
    is_hot: true
  },]).then(data => {
    console.log(data);
    //8. 关闭数据库连接 (项目运行过程中, 不会添加该代码)
    mongoose.disconnect();
  }, err => {
    // 判断是否有错误
    if (err) {
      console.log(err);
      return;
    }
  });

});

// 设置连接错误的回调
mongoose.connection.on('error', () => {
  console.log('连接失败');
});

//设置连接关闭的回调
mongoose.connection.on('close', () => {
  console.log('连接关闭');
});
```
## 删除文档
- 删除单条数据
```js
mongoose.connection.once('open', () => {
  let BookSchema = new mongoose.Schema({
    name: String,
    author: String,
    price: Number,
    is_hot: Boolean
  });

  let BookModel = mongoose.model('novel', BookSchema);

  BookModel.deleteOne({
    _id: '650ed2fe0aacc5b5ce22c62e'
  }).then(console.log, e => {
    console.log('删除失败')
    return 
  })
});

mongoose.connection.on('error', () => console.log('连接失败'));
mongoose.connection.on('close', () => console.log('连接关闭'));
```
- 删除多条数据
```js
// 批量删除is_hot为false的数据
BookModel.deleteMany({is_hot: false}).then(console.log, e => {
  console.logh('删除失败')
  return 
})
```

## 更新
- 更新单条
`BookModel.updateOne({name: '红楼梦'}, {price: 10}).then(console.log, console.error)`
- 更新多条
`BookModel.updateMany({author: '余华'}, {is_hot: true}).then(console.log, console.error)`

## 读取
`BookModel.findOne({name: '狂飙'}).then(console.log, console.error)`
`BookModel.findById('afdwsf324rfedserr3ewd').then(console.log, console.error)`
- 批量获取
`BookModel.find({author: '余华'}).then(console.log, console.error)`

# mongoose条件控制
## 运算符
大于 $gt
小于 $lt
大于等于 $gte
小于等于 $lte
不等于 $ne
例: 查找价格小于20的图书
`BookModel.find({price: {$lt: 20}}, (err, data) => {})`

## 逻辑运算
`$or`
`$and`
```js
db.students.find({$or: [{age: 18}, {age: 20}]})
// 查询年龄 >= 18 且 年龄 <= 20
db.students.find({$and: [{age: {$gte: 18}}, {age: {$lte: 20}}]})
```

## 正则
```js
db.students.find({name: /jrj/ig}).then(() => {}, () => {})
```
## 个性化读取
筛选特定的属性
```js
// 只要名称和作者属性
// select设置的对象的属性值为1表示要 0表示不要
BookModel.find().select({name: 1, author: 1}).exec().then
```

数据排序
```js
// 按照价格排序 1为升序  -1为倒序
BookModel.find().sort({price: 1}).exec().then()
```

数据截取
skip跳过  limit限定

```js
// 取出价格最高的前三本
BookModel.find().sort({price: -1}).limit(3).exec().then()
// 取出价格排名4-6本书
// 即 跳过三本 取三本书
BookModel.find().sort({price: -1}).skip(3).limit(3).exec().then()
```



