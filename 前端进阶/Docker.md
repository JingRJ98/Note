# 简介
[官网](https://www.docker.com)
[仓库](https://hub.docker.com)

代码 + 配置 + 环境打包为一整个镜像文件

系统平滑移植, 除了代码之外的所有环境和依赖都打包搬走

解决了运行环境和配置问题的软件容器 方柏霓做持续集成并有助于整体发布的容器虚拟化技术

## 容器和虚拟机的区别
虚拟机: 资源占用多 操作繁琐
容器虚拟化: linux容器没必要模拟一整个操作系统,而是对进程进行隔离, 有了容器 就可以讲软件运行所需的所有资源打包到一个隔离的容器中
容器与虚拟机不同, 不需要捆绑一整套操作系统,只需要软件工作许绕的库 资源和设置, 系统因此变得高效轻量并保证部署在任何环境中的软件都可以始终如一的运行

传统虚拟机需要虚拟出一套硬件(网卡 硬盘)在其上再运行一个完整的操作系统,在该系统上再运行所需的应用进程
docker是在操作系统上实现虚拟化 直接复用本地的操作系统,没有自己的内核也没有模拟硬件

每个容器之间互相隔离 每个容器都有自己的文件系统 容器之间进程不会互相影响,能区分计算资源

## docker的作用
docker出现后催生了devOps 开发运维于一体

coder => programmer => software engineer => devOps engineer

更快速的应用交付和部署

更快捷的升级和扩缩容

更简单的系统运维
应用容器化运行后,生产环境运行的应用可与开发 测试环境的应用高度一致 容器会将应用程序相关的环境和状态完全封装起来,不会因为底层的基础架构和操作系统的不一致给应用带来影响

更高效的计算资源利用
docker是内核级虚拟化 一个物理机上可以跑很多个容器


## docker三大元素
镜像
类似于类

容器
类似类new出来的实例, 一个镜像可以创建很多个容器
可以把容器看成一个简易的linux环境 (包含root用户权限 进程空间 用户空间和网络空间) 和运行在其中的应用程序

仓库(线上/本地 类似github)
存放镜像的地方


## docker架构
是一个C/S模式架构,后端是松耦合架构 众多模块各司其职

1. 用户使用client与docker daemon进行通信 发送请求给后者
2. docker daemon作为docker架构中的 主体 首先提供sevrer的功能使其可以接受client的请求
3. docker引擎执行docker内部的一系列工作  每一项工作都是以一个job的形式存在
4. jog的运行过程中  当需要容器镜像时  则从docker仓库中下载镜像 并通过镜像管理驱动将下载的驱动以graph的形式存储
5. 当需要为docker创建网络环境时 通过网络管理驱动创建并配置容器网络环境
6. 当需要限制容器运行资源或执行用户指令等操作时 通过execdriver来完成
7. libcontainer是一项独立的容器管理包 network driver以及exec driver都是通过Libcontainer来实现具体对容器进行的操作

# 常用命令

# 基础命令
systemctl start docker
systemctl restart docker
systemctl stop docker
systemctl status docker
systemctl enable docker 开机启动, 如果不配置每次都需要systemctl start docker
docker info 查看docker 信息
docker --help


# 镜像类命令
docker images    显示本地所有的镜像
docker images -a 显示本地所有的镜像
docker images -q 显示本地所有的镜像id

docker search <镜像名> 查找指定名称线上所有的镜像
docker search <镜像名> --limit 5 查找指定名称线上镜像 限制前五个
docker pull <镜像名>   下载镜像 默认最新版本
docker pull <镜像名>:<TAG>  下载指定版本的镜像 

docker system df 查看镜像/容器/数据卷所占磁盘空间
docker rmi <镜像id>    删除某个镜像
docker rmi -f <镜像id> 强制删除


# 容器类命令
## 运行容器
docker run <option>
### option 
--name 指定容器名字
-d 后台运行容器
-i 以交互模式运行容器 通常与-t一起用 
-t 为容器重新分配一个伪终端

注意docker run -it启动的前台交互式容器 一旦ctrl + c关闭进程 会直接关闭容器



-P随机端口
-p指定端口

### 例子
docker run -it ubuntu /bin/bash
返回一个新的终端 在操作ubuntu的实例容器

可以生成多个ubuntu容器
`docker run -it --name=myUbuntu ubuntu bash`此时又会创建一个ubuntu容器


## 查看容器
docker ps 列出所有正在运行的docker容器
docker ps -a 列出正在运行的和之前曾经运行过的
docker ps -l 列出最近创建的容器
docker ps -n <number> 列出最近创建的n个容器

## 进入容器
docker exec -it <container_id> bash 

## 退出/启动容器
exit 退出 容器不会停止

docker start <容器ID/容器名> 启动容器
docker restart <容器ID/容器名> 重启容器
docker stop <容器ID/容器名> 停止容器
docker kill <容器ID/容器名> 强制停止容器
docker rm <容器ID> 容器停止后才能删除
docker rm -f <容器ID> 强制删除容器哦

删除多个, 借助shell参数
docker rm -f $(docker ps -a -q)

docker container prune 删除所有没在运行的容器

## 其他
docker logs <容器ID> 查看容器日志
docker inspect <容器ID> 查看容器内部信息

docker exec -it <容器ID> bash 进入正在运行的容器 并以命令行交互

docker cp <容器ID>:容器内路径 目标路径 将容器内的文件拷贝到容器外

docker export <容器ID> > <导出文件名.tar> 导出容器内的内容流作为一个tar归档文件

cat abct.tar | docker import - root/ubuntu:0.0.1 从tar包中导入一个新的docker镜像, 并且该镜像是可以创建容器的, 创建的容器和之前创建归档tar包的容器是一样的 , 容器 => tar => 新镜像 => 新容器


# 数据存储
容器运行的时候可以指定挂在本机的路径
vloumn path和 container path
container path要是镜像能访问的上下文
