# Babel
## 概述

### babel能做什么
我们平时主要用 babel 来做 3 种事情：

1. 转译 esnext、typescript、flow 等 到目标环境支持的js
这个是最常用的功能，用来把代码中的 esnext 的新的语法、typescript 和 flow 的语法转成基于目标环境支持的语法的实现。并且还可以把目标环境不支持的 api 进行 polyfill。

babel7 提供了 @babel/preset-env 的包，可以指定目标 env 来按需转换，转换更加的精准，产物更小。

2. 一些特定用途的代码转换
babel 是一个转译器，暴露了很多 api，用这些 api 可以完成代码到 AST 的解析、转换、以及目标代码的生成。

开发者可以用它来完成一些特定用途的转换，比如函数插桩（函数中自动插入一些代码，例如埋点代码）、自动国际化等。

流行的小程序转译工具 taro，就是基于 babel 的 api 来实现的。

3. 代码的静态分析
对代码进行 parse 之后，会生成 AST，通过 AST 能够理解代码结构，除了转换 AST 再打印成目标代码之外，也同样可以用于分析代码的信息，进行一些静态检查。

linter 工具就是分析 AST 的结构，对代码规范进行检查。

api 文档自动生成工具，可以提取源码中的注释，然后生成文档。

type checker 会根据从 AST 中提取的或者推导的类型信息，对 AST 进行类型是否一致的检查，从而减少运行时因类型导致的错误。

压缩混淆工具，这个也是分析代码结构，进行删除死代码、变量名混淆、常量折叠等各种编译优化，生成体积更小、性能更优的代码。



### babel是什么
是一个工具链(微内核架构) 主要用于旧浏览器或者环节中将es6+代码转换为向后兼容版本的js版本

包括: 语法转换 源代码转换 polyfill实现目标环节缺少的功能

babel核心只包含很简单的功能 如果要实现更丰富的功能 需要下载各种对应的插件
比如转换箭头函数的插件 转换块级作用域的插件

@babel/preset-env 插件已经集成了一般常用的插件

### babel工作过程
1. parse：通过 parser 把源码转成抽象语法树（AST）
2. transform 遍历AST 调用各种transform插件对AST进行增删改
3. generate 把转换后的AST打印成目标代码 并生成sourcemap


## parse
把源码字符串转换成机器能理解的AST
这个过程分为词法分析 语法分析
### 词法分析
把代码中字符串分割为最小不可以分割的单词
每个单词在AST中会生成一个节点
```js
const s = 'jrj'
const fun = s =>  console.log(s)
fun(s)
```
提取源代码中的每个词 标注每个词的type 和value value就是词本身
例如`const`是Keyword
例如`fun`是Identifier
例如`=`是Punctuator 标点
例如`fun`是Identifier

# 尝试写一个babel插件

babel插件寻找节点, 增删改查节点的过程很像dom里面, 找到对应的节点 然后进行增删改查新的节点

babel插件的形式
第一种是一个函数返回一个对象的格式，对象里有 visitor、pre、post、inherits、manipulateOptions 等属性。
```js
module.exports = function (api, options, dirname) {
  return {
    visitor: {
      CallExpression(path) {
        // 对源代码中每个函数调用表达式进行判断
        const call = path.get('callee')
        const obj = call.get('object')

        if(obj.node.name === 'console' && call){
          path.remove()
        }
      }
    }
  }
}
```
第二种是不需要参数时间 直接返回一个对象, 包含上面的属性

## 编写插件流程
定义一个.babelrc文件用于配置插件
需要在`.babelrc`中进行插件的引入
```js
{
  "plugins": ["./test_x/plugin"]
}
```

插件代码
```js
// ./test_x/plugin.js
module.export = function() {
  return {
    vistor: {...}
  }
}
```

package.json中新增运行babel的命令
```js
{
  "scripts": {
    "example_x": "babel ./test_x/source.js --out-file ./test_x/build.js "
  },
}
```

原始文件路径 `./test_x/source.js`
转译后的文件路径 `./test_x/build.js`

## 本项目实现的插件

### test_1
修改变量的值

### test_2
删除代码中的console

### test_3
将?.可选链式运算符降级编译

`const val = a?.b`
转换成
`const val = a === null || a === void 0 ? void 0 : a.b`

### test_4
封装console.log()函数
实现babel插件:普通的console.log()函数中插入当前的文件名和行列号, 便于快速定位
console.log(1)
转译为
console.log('文件名 (行号 列号): ',1)