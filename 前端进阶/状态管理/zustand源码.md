# [zustand](https://docs.pmnd.rs/zustand/getting-started/introduction) 简介

一个小型, 快速的状态管理库

## 使用
创建store
方式1
```js
import { create } from 'zustand'

const useStore = create((set) => ({
  bears: 0,
  increasePopulation: () => set((state) => ({ bears: state.bears + 1 })),
  removeAllBears: () => set({ bears: 0 }),
}))
```
方式2 (ts)
```js
import { create } from 'zustand'

// 状态类型
type State = {
  count: number
}

// 操作类型
type Actions = {
  increment: (qty: number) => void
  decrement: (qty: number) => void
}

// 状态和操作合并为一个类型一个
const useCountStore = create<State & Actions>((set) => ({
  count: 0,
  increment: (qty: number) => set((state) => ({ count: state.count + qty })),
  decrement: (qty: number) => set((state) => ({ count: state.count - qty })),
}))
```
方式3
```js
import { create } from 'zustand'

type State = {
  count: number
}

type Actions = {
  increment: (qty: number) => void
  decrement: (qty: number) => void
}

type Action = {
  type: keyof Actions
  qty: number
}

const countReducer = (state: State, action: Action) => {
  switch (action.type) {
    case 'increment':
      return { count: state.count + action.qty }
    case 'decrement':
      return { count: state.count - action.qty }
    default:
      return state
  }
}

const useCountStore = create<State & Actions>((set) => ({
  count: 0,
  dispatch: (action: Action) => set((state) => countReducer(state, action)),
}))
```

组件中使用
```js
// src/store/First
import { create } from 'zustand'

type State = {
  value: number
}

type Actions = {
  setValue: (v: State['value']) => void
  increment: () => void;
  decrement: () => void;
  increOdd: () => void;
}

export const useCountStore = create<State & Actions>((set) => ({
  value: 0,
  setValue: (v: number) => set({value: v}),
  increment: () => set(s => ({value: s.value + 1})),
  decrement: () => set(s => ({value: s.value - 1})),
  increOdd: () => set(s => {
    if(s.value & 1){
      return {
        value: s.value + 1
      }
    }
    return s
  }),
}))
```

```js
// src/pages/First
import { FC, memo, useEffect } from 'react';
import { Button } from 'antd'

import { useCountStore } from '@/store/First'

interface FirstProps {
  initValue: number
}

const First: FC<FirstProps> = memo(({ initValue }) => {
  const value = useCountStore(s => s.value)
  const setValue = useCountStore(s => s.setValue)
  const increment = useCountStore(s => s.increment)
  const increOdd = useCountStore(s => s.increOdd)
  const decrement = useCountStore(s => s.decrement)

  useEffect(() => {
    setValue(initValue)
  }, [initValue, setValue])
  
  return (
    <>
      <div>value: {value}</div>
      <Button type='primary' style={{marginLeft: '10px'}} onClick={() => increment()}>增加1</Button>
      <Button type='primary' style={{marginLeft: '10px'}} onClick={() => decrement()}>减少1</Button>
      <Button type='primary' style={{marginLeft: '10px'}} onClick={() => increOdd()}>奇数加1</Button>
    </>
  )
});
export default First;
```

## 引入中间件
```jsx
import { create } from "zustand";
import { persist } from "zustand/middleware";
import { immer } from "zustand/middleware/immer";

export interface SubtitleStyling {
  /**
   * Text color of subtitles, hex string
   */
  color: string;

  /**
   * size percentage, ranges between 0.01 and 2
   */
  size: number;

  /**
   * background opacity, ranges between 0 and 1
   */
  backgroundOpacity: number;
}

export interface SubtitleStore {
  lastSync: {
    lastSelectedLanguage: string | null;
  };
  enabled: boolean;
  lastSelectedLanguage: string | null;
  styling: SubtitleStyling;
  overrideCasing: boolean;
  delay: number;
  updateStyling(newStyling: Partial<SubtitleStyling>): void;
  setLanguage(language: string | null): void;
  setCustomSubs(): void;
  setOverrideCasing(enabled: boolean): void;
  setDelay(delay: number): void;
  importSubtitleLanguage(lang: string | null): void;
  resetSubtitleSpecificSettings(): void;
}

export const useSubtitleStore = create(
  persist(
    immer<SubtitleStore>((set) => ({
      enabled: false,
      lastSync: {
        lastSelectedLanguage: null,
      },
      lastSelectedLanguage: null,
      overrideCasing: false,
      delay: 0,
      styling: {
        color: "#ffffff",
        backgroundOpacity: 0.5,
        size: 1,
      },
      resetSubtitleSpecificSettings() {
        set((s) => {
          s.delay = 0;
          s.overrideCasing = false;
        });
      },
      updateStyling(newStyling) {
        set((s) => {
          if (newStyling.backgroundOpacity !== undefined)
            s.styling.backgroundOpacity = newStyling.backgroundOpacity;
          if (newStyling.color !== undefined)
            s.styling.color = newStyling.color.toLowerCase();
          if (newStyling.size !== undefined)
            s.styling.size = Math.min(2, Math.max(0.01, newStyling.size));
        });
      },
      setLanguage(lang) {
        set((s) => {
          s.enabled = !!lang;
          if (lang) s.lastSelectedLanguage = lang;
        });
      },
      setCustomSubs() {
        set((s) => {
          s.enabled = true;
          s.lastSelectedLanguage = null;
        });
      },
      setOverrideCasing(enabled) {
        set((s) => {
          s.overrideCasing = enabled;
        });
      },
      setDelay(delay) {
        set((s) => {
          s.delay = Math.max(Math.min(500, delay), -500);
        });
      },
      importSubtitleLanguage(lang) {
        set((s) => {
          s.lastSelectedLanguage = lang;
          s.lastSync.lastSelectedLanguage = lang;
        });
      },
    })),
    {
      name: "__MW::subtitles",
    },
  ),
);
```
## 合并多个slice
```js
import { StateCreator } from "zustand";

import { CastingSlice } from "@/stores/player/slices/casting";
import { DisplaySlice } from "@/stores/player/slices/display";
import { InterfaceSlice } from "@/stores/player/slices/interface";
import { PlayingSlice } from "@/stores/player/slices/playing";
import { ProgressSlice } from "@/stores/player/slices/progress";
import { SourceSlice } from "@/stores/player/slices/source";
import { ThumbnailSlice } from "@/stores/player/slices/thumbnails";

export type AllSlices = InterfaceSlice &
  PlayingSlice &
  ProgressSlice &
  SourceSlice &
  DisplaySlice &
  CastingSlice &
  ThumbnailSlice;
export type MakeSlice<Slice> = StateCreator<
  AllSlices,
  [["zustand/immer", never]],
  [],
  Slice
>;

import { create } from "zustand";
import { immer } from "zustand/middleware/immer";

import { createCastingSlice } from "@/stores/player/slices/casting";
import { createDisplaySlice } from "@/stores/player/slices/display";
import { createInterfaceSlice } from "@/stores/player/slices/interface";
import { createPlayingSlice } from "@/stores/player/slices/playing";
import { createProgressSlice } from "@/stores/player/slices/progress";
import { createSourceSlice } from "@/stores/player/slices/source";
import { createThumbnailSlice } from "@/stores/player/slices/thumbnails";
import { AllSlices } from "@/stores/player/slices/types";

export const usePlayerStore = create(
  immer<AllSlices>((...a) => ({
    ...createInterfaceSlice(...a),
    ...createProgressSlice(...a),
    ...createPlayingSlice(...a),
    ...createSourceSlice(...a),
    ...createDisplaySlice(...a),
    ...createCastingSlice(...a),
    ...createThumbnailSlice(...a),
  })),
);

// 单个slice
import { MakeSlice } from "@/stores/player/slices/types";

export interface PlayingSlice {
  mediaPlaying: {
    isPlaying: boolean;
    isPaused: boolean;
    isSeeking: boolean; // seeking with progress bar
    isDragSeeking: boolean; // is seeking for our custom progress bar
    isLoading: boolean; // buffering or not
    hasPlayedOnce: boolean; // has the video played at all?
    volume: number;
    playbackRate: number;
  };
  play(): void;
  pause(): void;
}

export const createPlayingSlice: MakeSlice<PlayingSlice> = (set) => ({
  mediaPlaying: {
    isPlaying: false,
    isPaused: true,
    isLoading: false,
    isSeeking: false,
    isDragSeeking: false,
    hasPlayedOnce: false,
    volume: 1,
    playbackRate: 1,
  },
  play() {
    set((state) => {
      state.mediaPlaying.isPlaying = true;
      state.mediaPlaying.isPaused = false;
    });
  },
  pause() {
    set((state) => {
      state.mediaPlaying.isPlaying = false;
      state.mediaPlaying.isPaused = false;
    });
  },
});
```












------ 





# 状态管理库的比较
## 一、结构（Structure）  ：一般有三种构造方式来组织状态数据的存放。

- global：  全局式，也被称作单一数据源，将所有的数据放到一个大对象中，关键词：combineReducers();
- multiple stores：  多数据源模式，将状态存放到多个数据源中，可在多个地方使用消费，关键词：useStore()；
- atomic state：  原子状态，与创建对象形式的存储不同，针对每一个变量可以是响应式的，通过原子派生的方式来适应复杂的开发场景，关键词：atom()；


## 二、读取操作（get/read）  ：约定如何从 Store 中读取需要使用的数据，可以是直接读取源数据(getter)，也可以是读取派生数据(selector)。

- getter：  使用时直接是 xxx.name，一般可搭配 getter 方法做些额外修饰，实现类似 computed 的效果，多用在 object 或者 class 中；
- selector function：派生数据，一般使用方式如下： useSelector(state => state.counter)；由于每次派生出来的结果是一个带逻辑的函数，可以搭配 useMemo，shallow 等方案做些性能优化；

## 三、更新数据（set/write）  ：此部分的操作会决定你如何去更新数据状态并最终反应到视图中，同时也会影响你如何封装操作逻辑，此外还需要感知是同步操作还是异步操作，一般有三种方式：

- setter：  此类操作方式会尽量贴近 React.useState 里的用法，const [counter, setCounter]=useState(0)，支持 setCounter(newValue)和 setCounter(prev=>prev+1)两种操作了
- dispatch：  旨在将操作动作和实际数据相分离，dispatch 的触发函数中不需要包含实际处理逻辑，只需要触发类型(type)和更新值(payload)；例如：dispatch({type:'decrement', value: 'newVal'})
- reactive：  响应式更新，此类操作最简单直接，只需要确保更新动作被包装在 action 函数下，更新操作为：action(counterStore.decrement())即可完成更新动作。
- 不可变数据：  考虑到数据操作的副作用，一般鼓励使用不可变数据结构来实现数据更新，通常简单的方式可以用 Object.assign()，{...}展开符，复杂的结构可用 immutable.js，immer 等来配套使用，实际项目开发中应避免数据结构嵌套过深。

## useContext
context 适合作为一次性共享消费的处理场景，数据结构应到越简单越好，例如：国际化，主题，注册等这类全局变化且需要强制更新的操作。context 的数据管理方案应当作为应用程序数据的一部分而非全局状态管理的通用解决方案。