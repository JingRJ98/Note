# 简介
Mobx是响应式状态管理库，无关任何前端框架。现在已经发布到Mobx6了。在Mobx5之前，响应式原理是基于Object.defineProperty的，可以向下兼容到ES5浏览器，而从Mobx5开始使用proxy特性支持响应式，最低支持ES6浏览器。
相较于Redux，Mobx只强调下面三个概念

State(状态)
Actions(动作)
Derivations(派生)


在任何事件中调用action，修改state，如果这个state是响应式的，那么会通知基于这个state派生的计算值，或者触发派生的副作用。


其中派生属性可以分为两种情况

计算值（computeds）：类似于React的useMemo方法和Vue的computer方法
副作用（reactions）：类似于React的useEffect方法和Vue的WatchEffect方法

# 创建store

## 1. class写法
```ts
import { makeObservable, observable, action, runInAction } from "mobx";

class MyStore {
    let tableList = [];
    let condition = {
        current: 1,
        pageSize: 10,
        // ...other
    };

    constructor(title) {
        makeObservable(this, {
            tableList: observable,
            condition: observable,
            getTableList: action,
            onConditionChangez: action
        })

        // 设置 reaction，当 condition 变化时自动请求 tableList 数据
        reaction(
            () => this.condition, // 监听的表达式
            (condition) => this.getTableList(condition) // 触发的副作用
        )
        // 初始化时默认请求列表数据
        this.getTableList(this.condition);
    }
    // 计算属性
    get getInBeijing() {
        return this.tableList.map(item => item.address === "beijing");
    }
    getTableList (condition) {
        fetch("/v1/table/list", {
            method: "post",
            data: condition
        }).then((res) => {
            runInAction(() => {
                this.tableList = res.data.list;
            })
        })
    }
    onConditionChange (condition) {
        this.condition = condition;
    }
}

```

## 2. 函数式写法
使用接口创建对象的写法通常和组件结合在一起使用，虽然灵活，但是由于太过零散，不易于扩展。
```js
import { observable, action } from "mobx"

funtion App () {
    const myStore = observable({
        tableList: [],
        condition: {
            pageSize: 10,
            current: 1,
            // ...other
        },
        // 计算属性
        get getInBeijing() {
            return this.tableList.map(item => item.address === "beijing");
        }
    })
    // 副作用
    reaction(
        () => myStore.condition,
        () => getTableList()
    );

    const getTableList = action(() => {
        fetch("/v1/table/list", {
            method: "post",
            data: myStore.condition
        }).then((res) => {
            runInAction(() => {
                myStore.tableList = res.data.list;
            })
        })
    })

    const onConditionChange = action(myStore) => {
        myStore.condition = condition
    }

    useEffect(() => {
        myStore.getTableList(myStore.condition);
    }, [])

    return <>
        <Condition condition={myState.condtion} onChange={onConditionChange} />
        <Table data={myState.tableList} onPaigionChange={onConditionChange} />
    </>
}
```

# 在react中应用
在React中使用Mobx，通常有两个包：mobx-react、mobx-react-lite

mobx-react：提供类组件和hook组件的一些方法
mobx-react-lite：仅仅提供hook组件的一些方法


# Mobx 6.x
在Mobx 6.x或更高版本中，推荐使用 makeObservable 和 makeAutoObservable 将普通 JavaScript 对象或类转换为可观察对象的函数。它们可以自动地将类的属性声明为可观察状态
## 1.  makeObservable
makeObservable 是 Mobx 中用来将普通对象转换成可观察对象的函数。需要手动定义每个属性和方法的可观察类型。

使用
makeObservable(target, annotations?, options?)

1. target: 要转换成可观察对象的目标对象，一般是this。
2. annotations: 一个对象，用来指定哪些属性需要被转换成可观察属性，以及它们的行为和类型。
3. options：配置对象，它可以设置一些选项来影响注解的行为。有两个常用的选项：

autoBind：这个选项默认为 false，表示不自动绑定 action 方法的 this 指向。如果设置为 true，表示使 action 方法始终拥有正确的 this 指向，而不需要使用 bind 或箭头函数来绑定。
proxy：这个选项默认为 true，表示使用 Proxy 包装来创建 observable 属性。如果设置为 false，表示禁用 Proxy 包装，使用 defineProperty 来创建 observable 属性。这样可以提高兼容性，但是会失去一些特性，例如动态添加或删除属性。
name: <string>： 为对象提供一个在错误消息和反射 API 中打印的调试名称。

## 2. makeAutoObservable
makeAutoObservable 用于自动将对象的属性和方法标记为可观察的。这使得当这些属性或方法发生变化时，MobX 可以追踪并触发相关的副作用（如重新渲染 UI）。

makeAutoObservable(target, overrides?, options?)


target： 要转化的目标对象，通常是 this。
overrides： 覆盖默认的推断规则，可以指定某些属性或方法的注解，或者用 false 排除它们。
options： 配置对象，它可以设置一些选项来影响注解的行为。有以下常用的选项：

autoBind：这个选项默认为 true，表示不自动绑定 action 方法的 this 指向。如果设置为 true，表示使 action 方法始终拥有正确的 this 指向，而不需要使用 bind 或箭头函数来绑定。
proxy：这个选项默认为 false，表示使用 Proxy 包装来创建 observable 属性。如果设置为 false，表示禁用 Proxy 包装，使用 defineProperty 来创建 observable 属性。这样可以提高兼容性，但是会失去一些特性，例如动态添加或删除属性。
name: <string>： 为对象提供一个在错误消息和反射 API 中打印的调试名称。

## 比较 makeAutoObservable 和 makeObservable
makeAutoObservable 就像是加强版的 makeObservable，在默认情况下它将推断所有的属性。你仍然可以使用 overrides 重写某些注解的默认行为。 具体来说，false 可用于从自动处理中排除一个属性或方法。 查看上面的代码分页获取示例。 与使用 makeObservable 相比，makeAutoObservable 函数更紧凑，也更容易维护，因为新成员不需要显式地提及。 然而，makeAutoObservable 不能被用于带有 super 的类或 子类。

推断规则：

所有 自有 属性都成为 observable。
所有 getters 都成为 computed。
所有 setters 都成为 action。
所有 prototype 中的 functions 都成为 autoAction。
所有 prototype 中的 generator functions 都成为 flow。（需要注意，generators 函数在某些编译器配置中无法被检测到，如果 flow 没有正常运行，请务必明确地指定 flow 注解。）
在 overrides 参数中标记为 false 的成员将不会被添加注解。例如，将其用于像标识符这样的只读字段

## action
无法观察到中间值
```ts
import { makeObservable, observable, action } from "mobx"

class Doubler {
    value = 0

    constructor(value) {
        makeObservable(this, {
            value: observable,
            increment: action
        })
    }

    increment() {
        // 观察者不会看到中间状态.
        this.value++
        this.value++
    }
}
```
# mobx集成react


## 尽量拆更多子组件
不好
```ts
const MyComponent = observer(({ todos, user }) => (
    <div>
        {user.name}
        <ul>
            {todos.map(todo => (
                <TodoView todo={todo} key={todo.id} />
            ))}
        </ul>
    </div>
))

```
在上面的示例中，当 user.name 改变时，React 会不必要地协调所有的 TodoView 组件。尽管 TodoView 组件不会重新渲染，但是协调的过程本身是非常昂贵的。




好
```ts
const MyComponent = observer(({ todos, user }) => (
    <div>
        {user.name}
        <TodosView todos={todos} />
    </div>
))

const TodosView = observer(({ todos }) => (
    <ul>
        {todos.map(todo => (
            <TodoView todo={todo} key={todo.id} />
        ))}
    </ul>
))
```

## 只有需要的组件才用被观察的变量
不好的
组件的所有者也必须重新渲染, 因为在DisplayName的父组件中也用到了person.name
```tsx
<DisplayName name={person.name} />
```


好
```tsx
<DisplayName person={person} />
```
