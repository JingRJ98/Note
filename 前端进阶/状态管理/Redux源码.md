# 一步一步实现简单的状态管理
[参考实现简单的redux](https://github.com/brickspert/blog/issues/22)
## 1. 发布订阅
```js
const state = {
  c: 1
}

let listeners = []

function subscribe(l) {
  listeners.push(l)
  return function unsubscribe(){
    const idx = listeners.indexOf(l)
    listeners.splice(idx, 1)
  }
}

// 把state改成传入的v
const changeState = (v) => {
  state.c = v
  for(const l of listeners){
    l(v)
  }
}

// 执行订阅
subscribe((v) => {
  console.log('修改的值是 :>> ', v);
})

changeState(1)
changeState(2)
changeState(3)

// 修改的值是 :>>  1
// 修改的值是 :>>  2
// 修改的值是 :>>  3
```
## 2. 封装公共代码
```js
const createStore = (initialState) => {
  let state = initialState
  const listeners = []

  // 订阅
  const subscribe = (l) => {
    listeners.push(l)
  }

  const changeState = (newState) => {
    state = newState
    // 发布
    for(const f of listeners){
      // 注意这里不传参
      f()
    }
  }

  const getState = () => {
    return state
  }

  return {
    subscribe,
    changeState,
    getState
  }
}


//////////////// 使用 ////////////////
const initialState = {
  book: {
    name: '',
    desc: ''
  },
  score: 0
}

const store = createStore(initialState)

// 某处要使用book的数据(即订阅了数据源中的book相关数据)
store.subscribe(() => {
  const {book: {name, desc}} = store.getState()
  // 打印意味着使用
  console.log('book.name :>> ', name);
  console.log('book.desc :>> ', desc);
})

// 某处要使用score的数据(即订阅了数据源中的score相关数据)
store.subscribe(() => {
  const {score} = store.getState()
  // 打印意味着使用
  console.log('score :>> ', score);
})


const changeScore = (score) => {
  store.changeState({
    ...store.getState(),
    // 只改分数
    score
  })
}

const changeBook = (book) => {
  store.changeState({
    ...store.getState(),
    // 只改分数
    book
  })
}
changeScore(100)
changeBook({
  name: '三国演义',
  desc: '魏蜀吴'
})

console.log('store.getState() :>> ', store.getState()); // { book: { name: '三国演义', desc: '魏蜀吴' }, score: 100 }
```


## 3. 代码健壮性考虑
因为store中的内容会被随意改变, 例如上面执行`changeBook('假数据')`, store也被改了
需要对修改数据加以一定的规范
目前changeState会把收到的任何东西覆盖掉store里面的state, 比较危险
定义一个函数, 这个函数功能:
1. 会将传入的数据转换成规范的state并返回
2. 这个函数只会按照自己 几种特定方式改state, 其他情况不改(返回上一次的state)
```js
/**
 * 改分数数据的正规流程
 * @param {any} state 上一次的state
 * @param {{type: string; data: any}} action 修改动作
 */
const myScoreChange = (state, action) => {
  switch (action.type) {
    // 加分
    case 'INCREMENT':
      return {
        ...state,
        score: action.data + state.score
      }
    // 减分
    case 'MINUS':
      return {
        ...state,
        score: action.data + state.score
      }
    // 其他所有不规范的修改方式都不作修改
    default: return state
  }
}
```

然后改造一下createStore 
这里myScoreChange直接放在store中的操作需要改为从参数中传递进去
只关注分数
```js
const createStore = (myScoreChange, initialState) => {
  let state = initialState
  const listeners = []

  // 订阅
  const subscribe = (l) => {
    listeners.push(l)
  }

  const changeState = (action) => {
    state = myScoreChange(state, action)
    // 发布
    for (const f of listeners) {
      // 注意这里不传参
      f()
    }
  }

  const getState = () => {
    return state
  }

  return {
    subscribe,
    changeState,
    getState
  }
}

/**
 * 改分数数据的正规流程
 * @param {any} state 上一次的state
 * @param {{type: string; data: any}} action 修改动作
 */
const myScoreChange = (state, action) => {
  switch (action.type) {
    // 加分
    case 'INCREMENT':
      return {
        ...state,
        score: action.data + state.score
      }
    // 减分
    case 'MINUS':
      return {
        ...state,
        score: action.data + state.score
      }
    // 其他所有不规范的修改方式都不作修改
    default: return state
  }
}


//////////////// 使用 ////////////////
const initialState = {
  book: {
    name: '',
    desc: ''
  },
  score: 0
}

const store = createStore(initialState)

// 某处要使用score的数据(即订阅了数据源中的score相关数据)
store.subscribe(() => {
  const { score } = store.getState()
  // 打印意味着使用
  console.log('score :>> ', score);
})


const changeScore = (action) => {
  store.changeState(action)
}

changeScore({type: 'INCREMENT', data: 60})
// 无效操作
changeScore('abc')
changeScore(100)
console.log('store.getState() :>> ', store.getState()); // { book: { name: '', desc: '' }, score: 60 }
```

## 4.优化
首先改名字, 看起来比较有b格(贴近redux)
myScoreChange 这种函数就改为scoreReducer
changeState 改为dispatch

### state结构
为了区分多个数据, state中的每个数据都加一层{}
```js
const initialState = {
  book: {
    name: '',
    desc: ''
  },
  studentScore: {
    score: 0
  }
}
```

### combinReducer函数
注意到上面插入了scoreReducer在createStore 参数中, 首先考虑多个数据需要被改时怎么弄?
例如同时又bookReducer, scoreReducer, 不可能吧所有reducer函数都在createStore的时候通过参数穿进去
所以需要一个函数, 能集合多个reducer, 同时返回唯一的一个reducer函数 供createStore使用
```js
/**
 * 整合多个reducer
 * @param {object} reducers key是变量对应的名字, value是reducer函数
 */
const combinReducer = (reducers) => {
  const reducerKeys = Object.keys(reducers)

  // 还记得reducer函数的特点吗, 接受旧state, 和代表修改操作的action对象
  return (state = {}, action) => {
    const nextState = {}
    for(const key of reducerKeys) {
      const reducer = reducers[key]
      // store中这个key之前对应的数据
      const previousKeyState = state[key]
      // store中这个key对应的数据经过reducer函数生成的新结果
      // 注意只有一个action
      const nextKeyState = reducer(previousKeyState, action)
      nextState[key] = nextKeyState
    }

    return nextState
  } 
}
```

### 整体
```js
const createStore = (reducer, initialState) => {
  let state = initialState
  const listeners = []

  // 订阅
  const subscribe = (l) => {
    listeners.push(l)
  }

  const dispatch = (action) => {
    state = reducer(state, action)
    // 发布
    for (const f of listeners) {
      // 注意这里不传参
      f()
    }
  }

  const getState = () => {
    return state
  }

  return {
    subscribe,
    dispatch,
    getState
  }
}


/**
 * 整合多个reducer
 * @param {object} reducers key是变量对应的名字, value是reducer函数
 * @return {function} 一个唯一的reducer函数
 */
const combinReducer = (reducers) => {
  const reducerKeys = Object.keys(reducers)

  // 还记得reducer函数的特点吗, 接受旧state, 和代表修改操作的action对象
  return (state = {}, action) => {
    const nextState = {}
    for (const key of reducerKeys) {
      const reducer = reducers[key]
      // store中这个key之前对应的数据
      const previousKeyState = state[key]
      // store中这个key对应的数据经过reducer函数生成的新结果
      // 注意只有一个action
      const nextKeyState = reducer(previousKeyState, action)
      nextState[key] = nextKeyState
    }

    return nextState
  }
}



// 改分数(接受自己这部分state, 返回自己这部分的state)
const scoreReducer = (state, action) => {
  switch (action.type) {
    // 加分
    case 'INCREMENT':
      return {
        ...state,
        score: action.data + state.score
      }
    // 减分
    case 'MINUS':
      return {
        ...state,
        score: action.data + state.score
      }
    // 其他所有不规范的修改方式都不作修改
    default: return state
  }
}

// 改book(接受自己这部分state, 返回自己这部分的state)
const bookReducer = (state, action) => {
  switch (action.type) {
    // 改名字
    case 'NAME':
      return {
        ...state,
        name: action.data,
      }
    // 改描述
    case 'DESC':
      return {
        ...state,
        desc: action.desc
      }
    // 其他所有不规范的修改方式都不作修改
    default: return state
  }
}


//////////////// 使用 ////////////////
const initialState = {
  book: {
    name: '',
    desc: ''
  },
  studentScore: {
    score: 0
  }
}

const store = createStore(combinReducer({
  studentScore: scoreReducer, 
  book: bookReducer,
}) ,initialState)

// 某处要使用book的数据(即订阅了数据源中的book相关数据)
store.subscribe(() => {
  const { book: { name, desc } } = store.getState()
  // 打印意味着使用
  console.log('book.name :>> ', name);
  console.log('book.desc :>> ', desc);
})

// 某处要使用score的数据(即订阅了数据源中的score相关数据)
store.subscribe(() => {
  const { studentScore: {score} } = store.getState()
  // 打印意味着使用
  console.log('score :>> ', score);
})


const changeIncreScore = (v) => {
  store.dispatch({type: 'INCREMENT', data: v})
}

const changeBookName = (name) => {
  store.dispatch({type:'NAME', data: name })
}

changeIncreScore(100)
changeBookName('三国演义')

console.log('store.getState() :>> ', store.getState()); 
// { studentScore: { score: 100 }, book: { name: '三国演义', desc: '' } }
```

## 5. 进一步优化
现在所有的state写在一起,需要形式上将他们拆分, 现实中可能是在多个文件中的reducer和各自的state
注意: 只是形式上!!!, redux是单一数据源的, 也就是所有拆开的state还会合在一个对象中
redux三大原则之一: 单一数据源
整个应用的 全局 state 被储存在一棵 object tree 中，并且这个 object tree 只存在于唯一一个 store 中。

### 拆开state
好处是数据清晰, 且去掉了第四步中为了区分多个数据给每份数据自己包的{}
```js
// 各个模块自己的文件 这里以book为例

const initialState = {
  name: '',
  desc: ''
}

const reducer = (state, action) => {
  // 如果没有初始值, 就给初始值, 这里是为了将多个state合在一个对象中时, 能够直接收集到initialState
  // 仔细想想, 这步需要结合createStore里面的dispatch({ type: Symbol() })部分理解
  // 为了把初始state抛出去
  if(!state){
    state = initialState1
  }
  switch (action.type) {
    // 改名字
    case 'NAME':
      return {
        ...state,
        name: action.data,
      }
    // 改描述
    case 'DESC':
      return {
        ...state,
        desc: action.desc
      }
    // 其他所有不规范的修改方式都不作修改
    default: return state
  }
}
```

### 整体
```js
const createStore = (reducer, initialState) => {
  let state = initialState
  const listeners = []

  // 订阅
  const subscribe = (l) => {
    listeners.push(l)
  }

  const dispatch = (action) => {
    state = reducer(state, action)
    // 发布
    for (const f of listeners) {
      // 注意这里不传参
      f()
    }
  }

  const getState = () => {
    return state
  }

  // 这里的作用是执行createStore()的时候 可以不传initialState了
  // 此时redeucer函数的state就是undefined 默认为空{}, 然后获取各个子satte时就会返回undefined
  // 然后就会触发各个子reducer函数里面的 if(!state) state = initialState
  // 于是就收集到了所有的子state, 并且按照不同的key放在了同一个state里面
  dispatch({ type: Symbol() })

  return {
    subscribe,
    dispatch,
    getState
  }
}


/**
 * 整合多个reducer
 * @param {object} reducers key是变量对应的名字, value是reducer函数
 * @return {function} 一个唯一的reducer函数
 */
const combinReducer = (reducers) => {
  const reducerKeys = Object.keys(reducers)

  // 还记得reducer函数的特点吗, 接受旧state, 和代表修改操作的action对象
  return (state = {}, action) => {
    const nextState = {}
    for (const key of reducerKeys) {
      const reducer = reducers[key]
      // store中这个key之前对应的数据
      const previousKeyState = state[key]
      // store中这个key对应的数据经过reducer函数生成的新结果
      // 注意只有一个action
      const nextKeyState = reducer(previousKeyState, action)
      nextState[key] = nextKeyState
    }

    return nextState
  }
}

//////////////// 使用 ////////////////
///////////// book文件 /////////////
let initialState1 = {
  name: '',
  desc: ''
}

// 改book(接受自己这部分state, 返回自己这部分的state)
const bookReducer = (state, action) => {
  // 如果没有初始值, 就给初始值, 这里是为了将多个state合在一个对象中时, 能够直接收集到initialState
  // 仔细想想, 这步需要结合createStore里面的dispatch({ type: Symbol() })部分理解
  if(!state){
    state = initialState1
  }
  switch (action.type) {
    // 改名字
    case 'NAME':
      return {
        ...state,
        name: action.data,
      }
    // 改描述
    case 'DESC':
      return {
        ...state,
        desc: action.desc
      }
    // 其他所有不规范的修改方式都不作修改
    default: return state
  }
}


///////////// score文件 /////////////
let initialState = {
  score: 0
}

// 改分数(接受自己这部分state, 返回自己这部分的state)
const scoreReducer = (state, action) => {
  // 如果没有初始值, 就给初始值, 这里是为了将多个state合在一个对象中时, 能够直接收集到initialState
  // 仔细想想, 这步需要结合createStore里面的dispatch({ type: Symbol() })部分理解
  if(!state){
    state = initialState
  }

  switch (action.type) {
    // 加分
    case 'INCREMENT':
      return {
        ...state,
        score: action.data + state.score
      }
    // 减分
    case 'MINUS':
      return {
        ...state,
        score: action.data + state.score
      }
    // 其他所有不规范的修改方式都不作修改
    default: return state
  }
}


// 注意已经不要传一个initialState了, 内部流程会自动收集所有子state
const store = createStore(combinReducer({
  studentScore: scoreReducer, 
  book: bookReducer,
}))

// 某处要使用book的数据(即订阅了数据源中的book相关数据)
store.subscribe(() => {
  const { book: { name, desc } } = store.getState()
  // 打印意味着使用
  console.log('book.name :>> ', name);
  console.log('book.desc :>> ', desc);
})

// 某处要使用score的数据(即订阅了数据源中的score相关数据)
store.subscribe(() => {
  const { studentScore: {score} } = store.getState()
  // 打印意味着使用
  console.log('score :>> ', score);
})


const changeIncreScore = (v) => {
  store.dispatch({type: 'INCREMENT', data: v})
}

const changeBookName = (name) => {
  store.dispatch({type:'NAME', data: name })
}

changeIncreScore(100)
changeBookName('三国演义')

console.log('store.getState() :>> ', store.getState()); 
// { studentScore: { score: 100 }, book: { name: '三国演义', desc: '' } }
```

## 6. 中间件
一言以蔽之, 中间件是对dispatch的重写, 用于增强dispatch的功能

### 6.1 一个中间件直接改createStore中的dispatch
由于dispatch函数执行就会更新state, 所以如果需要扩展中间件, 就需要改写dispatch

中间件的作用是记录修改状态前后的状态
```js
const createStore = (reducer, initialState) => {
  let state = initialState
  const listeners = []

  // 订阅
  const subscribe = (l) => {
    listeners.push(l)
  }

  // 日志需求
  const dispatch = (action) => {
    console.log(state) // 打印改之前的state
    state = reducer(state, action)
    console.log(state) // 打印改之后的state
    // 发布
    for (const f of listeners) {
      // 注意这里不传参
      f()
    }
  }

  const getState = () => {
    return state
  }

  // 这里的作用是执行createStore()的时候 可以不传initialState了
  // 此时redeucer函数的state就是undefined 默认为空{}, 然后获取各个子satte时就会返回undefined
  // 然后就会触发各个子reducer函数里面的 if(!state) state = initialState
  // 于是就收集到了所有的子state, 并且按照不同的key放在了同一个state里面
  dispatch({ type: Symbol() })

  return {
    subscribe,
    dispatch,
    getState
  }
}
```

更好的写法
(仔细想想)
```js
const store = create(reducer)
const next = store.dispatch

store.dispatch = (action) => {
  console.log(store.getState()) // 打印改之前的state
  next(action)
  console.log(store.getState()) // 打印改之后的state
}
```

### 6.2多个中间件
在6.1一个中间件的基础上添加一个中间件
```js
const store = create(reducer)
const originDispatch = store.dispatch

// 中间件的作用是记录修改状态前后的状态
const next1 =  (action) => {
  console.log(store.getState()) // 打印改之前的state
  originDispatch(action)
  console.log(store.getState()) // 打印改之后的state
}

// 中间件的作用是改状态时出错的catch处理
const next2 =  (action) => {
  try {
    next1(action)
  } catch(err) {
    console.log(err) // 打印错误
  }
}

store.dispatch = next2
```

可以看到如果有多个中间件需要一层一层的包裹, 有没有更灵活的写法?

### 6.3 更灵活的中间件写法
注意6.2里面的next2中写死了next1, next1中写死了originDispatch
需要将写死的部分作为参数传递, 同时自己返回一个dispatch函数给别的中间件使用
即自己有个接入, 同时return dispatch作为别的接入
想象小时候的多节铅笔
just像  >--> >--> >-->, 可以合成一个巨大的 >------------------->
```js
// 中间件的作用是记录修改状态前后的状态
// 入参是dispatch 出参也是一个dispatch, 还记得dipatch的特点吗? 接收action 根据action改state
// 第一个箭头后面的部分是dispatch
const next1 = (next) => (action) => {
  console.log(store.getState()) // 打印改之前的state
  originDispatch(action)
  console.log(store.getState()) // 打印改之后的state
}

// 中间件的作用是改状态时出错的catch处理
// 第一个箭头后面的部分是dispatch
const next2 = (next) => (action) => {
  try {
    next1(action)
  } catch(err) {
    console.log(err) // 打印错误
  }
}

const store = create(reducer)
const originDispatch = store.dispatch

store.dispatch = next2(next1(action))
```
### 6.4 还有问题
注意next1里面有store, next2中没有, 如果我们想讲中间件函数脱离store怎么解决?
通过参数传入! 这就是为什么有三层箭头函数的原因!
```js
/////////// 单独的中间件函数 ///////////////
// 中间件的作用是记录修改状态前后的状态
// 入参是dispatch 出参也是一个dispatch, 还记得dipatch的特点吗? 接收action 根据action改state
// 第一个箭头后面的部分是dispatch
const next1 = (store) => (next) => (action) => {
  console.log(store.getState()) // 打印改之前的state
  originDispatch(action)
  console.log(store.getState()) // 打印改之后的state
}

/////////// 单独的中间件函数 ///////////////
// 中间件的作用是改状态时出错的catch处理
// 第一个箭头后面的部分是dispatch
const next2 = (store) => (next) => (action) => {
  try {
    next1(action)
  } catch(err) {
    console.log(err) // 打印错误
  }
}


/////////// 真正的store ///////////////
const store = create(reducer)
const originDispatch = store.dispatch

const realNext1 = next1(store)
const realNext2 = next2(store)
// 这里谁在里面谁在外面不重要 都可以换
store.dispatch = realNext2(realNext1(action))
```

### 6.5 尽善尽美
尽管经过以上步骤 中间件的原理已经差不多了 但是还可以尽善尽美
```js
// 引入多个中间件
import loggerMiddleware from './middlewares/loggerMiddleware';
import exceptionMiddleware from './middlewares/exceptionMiddleware';
import timeMiddleware from './middlewares/timeMiddleware';

...

const store = createStore(reducer);
const next = store.dispatch;

// 得到多个realNext
const logger = loggerMiddleware(store);
const exception = exceptionMiddleware(store);
const time = timeMiddleware(store);
// 套在一起
store.dispatch = exception(time(logger(next)));
```
实际上这里用户还需要写很多模版代码, 可以把这部分封装起来
期待一个applyMiddleware函数, 参数是所有的中间件组成的数组 顺序不重要
该函数的核心作用是将数组元素变成嵌套包裹的元素
更多的: 不能总是改store.dispatch, 不如让applyMiddleware返回一个函数, 该函数接收老createStore, 返回新createStore

```js
const applyMiddleware = (...middlewares) => {
  // 返回一个函数, 该函数接收老createStore, 返回新createStore
  return function rewriteCreateStoreFunc(oldCreateStore){
    return function newCreateStore(reducer){
      const store = oldCreateStore(reducer)
      let originDispatch = store.dispatch
      const newDispatchArr = middlewares.map(f => f(store))
      // 写法1
      newDispatchArr.forEach(f => originDispatch = f(originDispatch))
      // 写法2 因为这个数组中每个元素都是接收函数 返回函数
      originDispatch = newDispatchArr.reduce((pre, c) => c(pre), originDispatch)
      store.dispatch = originDispatch

      return store
    }
  }
}
```

注意点
最里面的中间件拿到了store是oldCreateStore创建出来的完整的store, 形如
```js
{
  subscribe,
  changeState,
  getState
}
```

其实只要返回形如
```js
{
  getState
}
```
就可以了

这样是最小开放原则, 即中间件里面用store只是为了得到数据, 不允许改数据, 也不允许改store的方法, 虽然中间件也不会改
*想象一下, 你是一家公司的开发, 被裁员了, 于是你为了报复, 写了一个邪恶中间件, 里面有改store的方法, 会偶然引起bug, 然后将这个中间件打包成一个包放在npm上, 在项目中引用, 不仔细看中间件的源码很难检查出来*


为了只暴露getState 可以这样做
```js
const store = {
  getState: oldCreateStore(reducer).getState
}

```


### 6.6 尾声
如果用户不需要中间件, 那么用户直接使用原始的createStore, 如果用户需要中间件, 那么就要用applyMiddleWare来创建这个createStore
等于说现在有两种createStore, 有没有什么办法可以不考虑用不用, 只提供一种createStore呢? 通过参数传进去来判断有没有使用中间件

两种
```js
/*没有中间件的 createStore 直接引入使用*/
import { createStore } from './redux';
const store = createStore(reducer, initState);

/*有中间件的 createStore 需要创建使用*/
const rewriteCreateStoreFunc = applyMiddleware(exceptionMiddleware, timeMiddleware, loggerMiddleware);
const newCreateStore = rewriteCreateStoreFunc(createStore);
const store = newCreateStore(reducer, initState);
```

优雅的方式
```js
const createStore = (reducer, initState, rewriteCreateStoreFunc) => {
  /*如果有 rewriteCreateStoreFunc，那就采用新的 createStore */
  if(rewriteCreateStoreFunc){
    // 把自己这个函数作为参数传进去 两级反转
    const newCreateStore =  rewriteCreateStoreFunc(createStore);
    return newCreateStore(reducer, initState);
  }
  /*否则按照没有中间件的流程走*/
  // 创建store
  // 订阅(支持退订)
  // 创建dispatch
  // 创建state的get函数
  // return store
}
```






# 源码拆解

_除了传参类型错误和边界情况之后的redux源码解析_

```js
const randomString = () => Math.random().toString(36).substring(7).split('').join('.')
/**
 * 组合多个函数
 * compost(f, g, h) 返回一个函数 (...args) => f(g(h(...args)))
 *
 * @param {...Function} funcs 
 * @returns {Function} 
 */
function compose(...funcs) {
  if (funcs.length === 0) {
    return (arg) => arg
  }

  if (funcs.length === 1) {
    return funcs[0]
  }

  return funcs.reduce((a, b) => (...args) => a(b(...args)))
}

// 三种内置actionType:  init replace UNKNOWN_ACTION
const ActionTypes = {
  // 一个完全随机的type, 确保不匹配任何type
  INIT: `@@redux/INIT${randomString()}`,
  REPLACE: `@@redux/REPLACE${randomString()}`,
  // 用于测试 reducer 函数内部有没有对未知的 type 进行判断
  PROBE_UNKNOWN_ACTION: () => `@@redux/PROBE_UNKNOWN_ACTION${randomString()}`,
}

/**
 * 创建一个store 维护state
 * *已经不推荐使用这个方式, 更推荐@reduxjs/toolkit的configureStore
 * 唯一更改数据的方式是通过dispatch
 * 必须保证单一store
 * 
 * @param {(state, action: {type: string, other: any}) => newState} reducer 一个函数,接收当前state和更改state的action. 返回新state
 * @param {state} preloadedState 初始值
 * @param {function} enhancer 用来重写createStore的函数 redux官方的使用中间件增强的函数是applyMiddleware
 * @returns 一个Redux存储，允许读取状态、调度操作和订阅更改
 */
function createStore(reducer, preloadedState, enhancer) {
  // 如果第二个参数是一个object，我们认为他是 initState，如果是 function，我们就认为他是 enhancer
  // 即中间的参数可以省略
  if (typeof preloadedState === 'function' && typeof enhancer === 'undefined') {
    enhancer = preloadedState
    preloadedState = undefined
  }

  // 如果指定了利用中间件增强createStore enhancer(createStore)返回一个新的createStore
  // 实际用法:
  // const enhancer = applyMiddleware(middleware1, middleware2, middleware3);
  // const store = createStore(reducer, initState, enhancer);
  if (typeof enhancer !== 'undefined') return enhancer(createStore)(reducer, preloadedState)

  let currentReducer = reducer
  let currentState = preloadedState
  let currentListeners = []
  let nextListeners = currentListeners

  /**
   * This makes a shallow copy of currentListeners so we can use
   * nextListeners as a temporary list while dispatching.
   *
   * This prevents any bugs around consumers calling
   * subscribe/unsubscribe in the middle of a dispatch.
   */
  function ensureCanMutateNextListeners() {
    if (nextListeners === currentListeners) {
      nextListeners = currentListeners.slice()
    }
  }

  // 只是返回state 相当于一个get
  function getState() {
    return currentState
  }

  /**
   * 订阅状态更新
   * @param {Function} listener 
   * @returns {Function} 订阅对应的退订函数
   */
  function subscribe(listener) {
    ensureCanMutateNextListeners()
    nextListeners.push(listener)

    // 实际用法: 
    // const unsubscribe = store.subscribe(() => {
    //   let state = store.getState();
    //   console.log(state.counter.count);
    // });
    // /*退订*/
    // unsubscribe();
    return function unsubscribe() {
      ensureCanMutateNextListeners()
      const index = nextListeners.indexOf(listener)
      nextListeners.splice(index, 1)
      currentListeners = null
    }
  }

  /**
   * 改变状态的函数
   * action 必须 包含type,否则报错,用于帮助reducer函数来根据type执行不同的操作
   * @param {{type: string; other: any}} action 
   * @returns 
   */
  function dispatch(action) {
    currentState = currentReducer(currentState, action)

    const listeners = (currentListeners = nextListeners)
    for (let i = 0; i < listeners.length; i++) {
      const listener = listeners[i]
      listener()
    }

    return action
  }

  /**
   * 替换当前的reducer
   * reducer 拆分后，和state是一一对应的。希望在做按需加载的时候reducer也可以跟着组件在必要的时候再加载，然后用新的 reducer 替换老的 reducer
   *
   * @param {Function} nextReducer The reducer for the store to use instead.
   * @returns {void}
   */
  function replaceReducer(nextReducer) {
    currentReducer = nextReducer

    // 这个动作的效果与dispatch({type: ActionTypes.INIT})类似 
    // 触发state = currentReducer(currentState, action)
    // 新来的 reducer 把自己的默认状态放到 state 树上去
    dispatch({ type: ActionTypes.REPLACE })
  }

  // dispatch一个不匹配任何type的 action，来触发state = reducer(currentState, { type: ActionTypes.INIT })
  // 因为 action.type 不匹配任何操作，每个子 reducer 执行兜底的return, 返回initState
  dispatch({ type: ActionTypes.INIT })

  return {
    dispatch,
    subscribe,
    getState,
    replaceReducer,
  }
}

/**
 * reducer 的拆分和合并
 * reducer 接收老的 state和action，返回新的 state, 如果所有的reducer函数逻辑写在一起会十分复杂,所以会根据state拆分不同的reducer
 * 然后通过一个函数来进行reducer整合, 将值不同的reducer函数转换成一个对象,变成一个reducer函数
 *
 * @param {Object} reducers 不同的reducer函数
 * {
 *   demo1: demo1Reducer
 *   demo2: demo2Reducer
 * }
 *
 * @returns {Function} 一个reducer函数 这个函数会针对每个业务的各自reducer,然后生成不同业务对应自己的state
 * {
 *   demo1: demo1State
 *   demo2: demo2State
 * }
 */
function combineReducers(reducers) {
  const reducerKeys = Object.keys(reducers)
  const finalReducers = {}
  for (let i = 0; i < reducerKeys.length; i++) {
    const key = reducerKeys[i]

    if (typeof reducers[key] === 'function') {
      finalReducers[key] = reducers[key]
    }
  }
  const finalReducerKeys = Object.keys(finalReducers)

  // This is used to make sure we don't warn about the same
  // keys multiple times.
  let unexpectedKeyCache
  if (process.env.NODE_ENV !== 'production') {
    unexpectedKeyCache = {}
  }

  // 返回合并后的新的reducer函数
  return function combination(state = {}, action) {
    let hasChanged = false
    const nextState = {}
    // 遍历执行所有的reducers
    for (let i = 0; i < finalReducerKeys.length; i++) {
      const key = finalReducerKeys[i]
      const reducer = finalReducers[key]
      const previousStateForKey = state[key]
      const nextStateForKey = reducer(previousStateForKey, action)
      nextState[key] = nextStateForKey
      // 注意这个或运算, 如果所有的nextStateForKey !== previousStateForKey 都为false, hasChanged则一直是false
      hasChanged = hasChanged || nextStateForKey !== previousStateForKey
    }
    hasChanged = hasChanged || finalReducerKeys.length !== Object.keys(state).length
    return hasChanged ? nextState : state
  }
}

function bindActionCreator(actionCreator, dispatch) {
  // 通过闭包隐藏actionCreator和 dispatch
  return function () {
    return dispatch(actionCreator.apply(this, arguments))
  }
}

/**
 * bindActionCreators 我们很少很少用到，一般只有在 react-redux 的 connect 实现中用到。
 * 通过闭包，把 dispatch 和 actionCreator 隐藏起来，让其他地方感知不到 redux 的存在。
 */
function bindActionCreators(actionCreators, dispatch) {
  if (typeof actionCreators === 'function') {
    return bindActionCreator(actionCreators, dispatch)
  }

  const boundActionCreators = {}
  for (const key in actionCreators) {
    const actionCreator = actionCreators[key]
    if (typeof actionCreator === 'function') {
      boundActionCreators[key] = bindActionCreator(actionCreator, dispatch)
    }
  }
  return boundActionCreators
}

/**
 * 多中间件合作
 * 中间件是对 dispatch 的扩展，或者说重写，增强 dispatch 的功能
 * @param {...Function} middlewares 要应用的中间件链
 * @returns {Function} 一个增强的createStore函数
 */
function applyMiddleware(...middlewares) {
  // 返回的还是createStore函数,只不过里面的dispatch函数被强化了
  return (createStore) => (...args) => {
    // ...args 就是原来createStore函数里的那三个或者两个参数
    // 生成原来的store
    const store = createStore(...args)
    let dispatch = () => {
      throw new Error(
        'Dispatching while constructing your middleware is not allowed. ' +
        'Other middleware would not be applied to this dispatch.'
      )
    }

    const middlewareAPI = {
      getState: store.getState,
      dispatch: (...args) => dispatch(...args),
    }
    // 给每个 middleware 传下store, 但是不能给中间件完整的store, 否则中间件甚至可以修改 subscribe 方法
    // 按照最小开放策略，所以开放一个最小的权限,即只有getState
    // chain函数数组
    const chain = middlewares.map((middleware) => middleware(middlewareAPI))
    // compose将函数数组转为f(g(h()))模式
    dispatch = compose(...chain)(store.dispatch)

    return {
      ...store,
      // 中间件只应用于dispatch, 原createStore返回的dispatch被用中间件强化了
      dispatch,
    }
  }
}
```

# redux核心哲学
三大原则
## 单一数据源
整个应用的状态尽管形式上可能是分开的, 但是是存在同一个object中的
整个应用的 全局 state 被储存在一棵 object tree 中，并且这个 object tree 只存在于唯一一个 store 中。

这条原则简化了同构应用的开发，因为在服务端的 state 可以序列化并注入到客户端，不需要做其他的一些事情。一个单一数据源 state tree 也简化了应用的调试和和监控；它也让你在开发中能将应用数据持久化到本地，从而加速开发周期。
此外，有一些功能以前很难实现，比如“撤销/重做”，在单一数据源的原则下，使用 Redux 实现将非常容易。


## State 是只读的
唯一改变 state 的方法就是触发 action，action 是一个用于描述已发生事件的**普通对象**。

这条原则确保了视图行为和网络请求回调都不能直接修改 state，相反它们只能表达出想要修改 state 的意图。
因为所有的修改都被集中化处理，且严格按照顺序一个接一个地执行，因此不用担心竞态条件（race condition）的出现。 
Action 就是普通对象而已，因此它们可以被日志打印、序列化、储存、后期调试或测试时回放出来。

## 使用纯函数(reducer)来执行修改
为了描述 action 如何改变 state tree，你需要编写纯的 reducers。

`Reducer 是纯函数，它接收之前的 state 和 action，并返回新的 state。`
记住，一定要返回一个新的对象，而不是修改之前的 state。你一开始可以只有单个 reducer，但随着应用复杂度的增长，你可以把大的 reducer 划分为一个个小的 reducers，分别管理着 state tree 的不同部分。由于 reducer 只是函数，你可以控制它们被调用的顺序，传入附加数据.

# 中间件

-------------------------------------------------------

# redux toolkit 实践

## immer
Immer  是 mobx 的作者写的一个 immutable 库，核心实现是利用 ES6 的 proxy，几乎以最小的成本实现了 js 的不可变数据结构，简单易用、体量小巧、设计巧妙，满足了我们对 JS 不可变数据结构的需求。

### 基本概念
currentState：被操作对象的最初状态
draftState: 根据currentState生成的草稿、是currentState的代理、对draftState所有的修改都被记录并用于生成nextState。在此过程中，currentState不受影响
nextState: 根据draftState生成的最终状态
produce: 用于生成nextState或者producer的函数
Producer: 通过produce生成，用于生产nextState,每次执行相同的操作
recipe:用于操作draftState的函数

# @reduxjs/toolkit用法
定义
`src/stores/demoSclice.ts`
```ts
import { createSlice, PayloadAction } from '@reduxjs/toolkit'

const initialState = {
  list: [
    { id: '1', title: 'First Post!', content: 'Hello!' },
    { id: '2', title: 'Second Post', content: 'More text' }
  ]
}

const demoSlice = createSlice({
  name: 'demo',
  initialState,
  reducers: {
    setList(state, action: PayloadAction<any[]>) {
      state.list = action.payload
    } 
  }
})

const {actions, reducer} = demoSlice

// 可以直接使用这个函数进行dispatch
export const {setList} = actions
export default reducer
```

`src/stores/index.ts`
```ts
import demoReducer from 'demoSlice'

export const rootReducer = {
  demo: demoReducer
}
```

使用
`src/Component`
```ts
// 使用数据
import { useSelector } from 'react-redux'

const App = () => {
  const {list} = useSelector(state => state.posts)
}
```

```ts
// 更改数据
import { useDispatch } from 'react-redux'
// 修改数据的函数
import {setList} from 'src/stores/demoSlice'

const App = () => {
  const dispatch = useDispatch()

  //////
  dispatch(setList(someData))
  //////

}
```

根组件配置
`App.tsx`
```ts
import { Provider } from 'react-redux'
import store from './store'
import App from './App'

// As of React 18
const root = ReactDOM.createRoot(document.getElementById('root'))
root.render(
  <Provider store={store}>
    <App />
  </Provider>
)
```