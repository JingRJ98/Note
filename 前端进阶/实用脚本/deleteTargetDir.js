/**
 * 递归删除当前路径及子路径中所有名为 dirName 的文件夹, 和文件夹中所有的文件
 */

const fs = require('fs');
const path = require('path');

// 获取当前工作目录
const currentDir = process.cwd();

// 递归删除文件夹及其内容的函数
function deleteFolderRecursively(targetDir) {
    if (fs.existsSync(targetDir)) {
        fs.readdirSync(targetDir).forEach(file => {
            const filePath = path.join(targetDir, file);

            // 检查是否是文件夹
            if (fs.lstatSync(filePath).isDirectory()) {
                // 递归删除子文件夹
                deleteFolderRecursively(filePath);
            } else {
                // 删除文件
                fs.unlinkSync(filePath);
            }
        });

        // 删除空文件夹
        fs.rmdirSync(targetDir);
    }
}

// 检查当前目录及其子目录中的指定文件夹名字的文件夹
function searchAndDeleteTestFolders(dir, dirName) {
    if (fs.existsSync(dir)) {
        fs.readdirSync(dir).forEach(item => {
            const itemPath = path.join(dir, item);

            if (item === dirName && fs.lstatSync(itemPath).isDirectory()) {
                console.log(`Deleting folder: ${itemPath}`);
                deleteFolderRecursively(itemPath);
            } else if (fs.lstatSync(itemPath).isDirectory()) {
                // 递归搜索子目录
                searchAndDeleteTestFolders(itemPath, dirName);
            }
        });
    }
}

///////////////////////////////////////////////
///////////输入要删除的目标文件夹的名称/////////////
const dirName = '__test'
///////////////////////////////////////////////
///////////////////////////////////////////////

searchAndDeleteTestFolders(currentDir, dirName);
console.log(`所有 ${dirName} 文件夹和他们的内容已经被删除`);

