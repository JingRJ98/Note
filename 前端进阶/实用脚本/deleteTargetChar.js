const fs = require('fs');
const path = require('path');

const handle = (p, char) => {
  fs.readdir(p, (e, data) => {
    if (e) {
      console.log('读取文件失败', e);
      return
    }
    data.forEach(f => {
      // 递归拼接新路径
      const filedir = path.join(p, f);
      fs.stat(filedir, (err, stat) => {
        if (err) {
          console.log('加载文件失败', err)
          return
        }
        if (stat.isFile()) {
          // 判断文件名是否包含指定字符串
          if (f.includes(char)) {
            fs.unlinkSync(filedir)
          }
        } else if (f !== 'node_modules') {
          handle(filedir, char)
        }
      })
    })
  })
}

const char = '.en-US.md'
handle(path.resolve(__dirname), char)
console.log(`所有名称中包含 ${char} 的文件已经被删除`)
