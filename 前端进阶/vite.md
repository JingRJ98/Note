# 什么是构建工具
企业级项目里面都可能具备哪些功能
- ts 需要tsc将ts文件转换为js代码
- react 安装react-complier将jsx/tsx等文件转换为render函数
- css预处理器支持
- 语法降级 es6+ => es5 不是构建工具做的 构建工具集成对应的处理工具来自动化处理
- 体积优化
- 模块化

构建工具就是集成以上等功能的工具

# vite和webpack优劣
TODO:

# vite预加载
在处理的过程中如果看到了非绝对路径或者非相对路径的引入, 会尝试开启路径补全
例如`import _ from 'loadash'`
会被处理成`import __vite__cjsImport0_lodash from "/node_modules/.vite/deps/lodash.js?v=fe9f707f"`
找寻依赖的过程是自当前目录自动向上查找的过程,直到搜寻到根目录或者搜寻到对应的依赖为止
当没有在项目的node_module中没有lodash,那么会从电脑的全局目录中查找

## 依赖预构建
vite会找到对应的依赖,然后调用esbuild将其他规范的代码库转换为esbuild规范,放在node_module/.vite/deps下
它在开发过程中，会将依赖项提前编译成小的、独立的文件。当你请求一个特定的依赖项时，Vite 可以快速地提供它，而不必等待整个捆绑文件的构建。这减少了构建时间和加载时间，使开发过程更加快速和高效。这个特性特别有助于大型项目，因为你不必等待整个捆绑文件重新构建，只需重新构建修改的部分依赖项即可。

总之，Vite 的依赖预构建是一种优化前端开发和构建性能的方法，它通过提前编译和按需提供依赖项来加速开发和减少构建时间。这对于现代的前端开发非常有用，尤其是在大型项目中
解决了
1. 不同的第三方库会有不同的导出格式(es6, commonjs)
2. 对路径的处理上同意使用/node_modules/.vite/deps
3. 解决网络多包传输问题 (最重要的)

```js
export default {
  optimizeDeps: {
    exclude: ["lodash-es"], // 遇到这个依赖的时候,不进行依赖预构建
  }
}
// 此时vite没有对lodash-es进行预构建, 网络传输了所有的loadsh-es中的包, 因为lodash-es中每个工具函数单独export的
```

## 配置文件处理细节 vite.config.js
1. vite配置文件的语法提示
相较于直接export default {}
使用defineConfig, 该函数的入参有ts类型提示
```js
import { defineConfig } from 'vite'

export default defineConfig({
  // 这里会有代码提示
  // optimizeDeps:
})
```
方式2
```js
/** @type import("vite").UserConfig */
const config = {
  // 这里也出现了提示
  // optimizeDeps
}
```
2. 关于环境的处理
使用webpack的时候需要配置webpack.dev.config 和webpack.prop.config
使用vite的时候
```js
import { defineConfig  } from 'vite'
import viteBaseConfig from './vite.base.config'
import viteDevConfig from './vite.dev.config'
import viteProdConfig from './vite.prod.config'

// 策略模式
const envResolver = {
  "build": () => ({...viteBaseConfig, ...viteProdConfig}),// 生产环境
  "serve": () => ({...viteBaseConfig, ...viteDevConfig}) // 开发环境
}

export default defineConfig(({ cmd }) => {
  return envResolver[cmd]()
})
```

## 环境变量配置
1. 开发环境
2. 生产环境
3. 测试环境
4. 预发布环境
5. 灰度环境
...

不同的环境变量对应不同的baseUrl, 或者APP_key
vite内置dotenv第三方库,会自动读取根目录下的.end文件,将其注入到process变量下, 但是考虑到和其他配置的一些冲突问题,不会直接首先注入


### vite服务端使用lodeEnv函数来获取env变量
所谓的客户端就是vite.config.ts文件中
这些文件的执行打印会在终端中打印
```js
import { defineConfig , loadEnv } from 'vite'

// 当运行vite --mode development的时候
// development就会作为这里的mode
// 也可以指定其他的开发环境的命名例如 "dev": "vite --mode dev" 此时mode变为dev
export default defineConfig(({ cmd, mode }) => {
  // vite获取环境变量
  const env = loadEnv(mode, process.cwd(), '')
  console.log('env :>> ', env);
})
```

.env所有环境需要用到的环境变量
.env.development vite默认将开发环境命名为development
.env.production vite默认将开发环境命名为production

执行命令可以添加 --mode <envName>

执行loadEnv函数时
1. 直接找到.env文件 解析其中的环境变量 放进一个对象里面
2. 将mode进行文件名拼接,例如找到.env.dev文件 并解析其中的变量, 覆盖前面的变量

`pnpm run dev --mode prod`
此时会先查找.env文件并解析其中的变量, 然后会解析.env.prod文件,并用其中的变量覆盖前面的同名变量

### 客户端使用import.meta.env来获取环境变量
所谓的客户端就是项目的实际代码中
这些文件的执行打印会在浏览器中打印
vite首先做了一层拦截,防止环境变量中存在隐私性的变量暴露,例如key等
如果环境变量不是VITE开头的, 客户端里面无法直接获取, 浏览器无法打印出来

```js
// .env.dev
APP_KEY=1
VITE_APP_KEY=1

// APP
console.log(import.meta.env)
// 浏览器只能打印出VITE_APP_KEY
```

如果实在不想加VITE前缀,可以使用envPrefix配置
```js
import { defineConfig , loadEnv } from 'vite'

export default defineConfig({
  envPrefix: 'ENV_'// 环境变量的前缀
})
```

# vite中处理css
将css文件转换为js文件脚(方便热更新或者css模块化) 同时设置content-type为js, 让浏览器以js的形式来加载css

## css模块化的原理
.module.css文件会自动添加hash后缀, 确保不重复