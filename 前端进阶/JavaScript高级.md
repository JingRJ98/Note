# 代理与反射
new Proxy构造函数
接受两个参数 目标对象和处理函数对象

```js
const obj = {
  id: 1
}

const proxy = new Proxy(obj, {})

console.log('proxy.id', proxy.id) // 1
console.log('obj.id', obj.id) // 1
```

修改代理对象的属性,原对象也会改
修改原对象的属性,代理对象的属性也会修改

## 捕获器
捕获器可以用于修改基本方法的行为
一个处理函数的对象可以包含任意个捕获器
```js
const obj = {
  foo: 'bar'
}

const proxy = new Proxy(obj, {
  // 定义一个捕获器
  // 属性读取操作的捕捉器
  // 当对象执行属性读取操作的时候,会捕捉这个行为,然后返回捕获器里的逻辑,返回值也是捕获器里面的返回值
  get() {
    return 'Hello Proxy';
  }
})

console.log(proxy.foo)
// 执行get, 返回Hello Porxy
// 不返回 'bar'
```

所有的捕捉器是可选的。如果没有定义某个捕捉器，那么就会保留源对象的默认行为

## 捕获器的参数和反射
所有的捕获器都可以访问相应的参数

get()捕获器可以拿到目标对象,要查询的属性和代理对象三个参数
```js
const obj = {
  foo: 'bar'
}

const proxy = new Proxy(obj, {
  // 定义一个捕获器
  // 属性读取操作的捕捉器
  // 当对象执行属性读取操作的时候,会捕捉这个行为,然后返回捕获器里的逻辑,返回值也是捕获器里面的返回值
  get(targetObj, property, receiver) {
    console.log(targetObj); // {foo: 'bar'}
    console.log(targetObj === obj); // true
    console.log(property); // foo
    console.log(receiver); // {foo: 'bar'}

    // return target[property] 就和原来的行为一样了
    return 'Hello Proxy';
  }
})

proxy.foo
```

## Reflect
全局对象Reflect身上包含了对象的方法
```js
const obj = {a: 1}
Reflect.set(obj, 'a', 2)

Reflect.has(obj, 'a')
```

## 代理的作用
保护原有对象
将访问原有对象变成访问代理对象
比如在get或者set操作的时候在捕获器里面进行一些拦截
函数也可以是一种目标对象,可以借助代理进行函数的参数验证

# 函数尾调用优化
es6新增的内存管理优化机制让js引擎在满足条件时可以重用栈帧
尾调用: 外部函数的返回值是一个内部函数的返回值
```js
function outer() {
  return inner()
}
```

尾调用之前: outer推一个栈帧, 执行到inner,再推一个栈帧

尾调用优化: 执行到inner的时候销毁outer的栈帧,因为inner函数的返回值也是outer函数的返回值,没必要保留两个栈帧

尾调用优化的条件
1. 严格模式
2. 外部函数的返回值就是内部函数的返回值
3. 内部函数没有引用外部函数的作用域的自由变量(即没有产生闭包)

+ 例 斐波那契数列
```js
function fib(n) {
  if(n < 2) {
    return n
  }
  return fib(n - 1) + fib(n - 2)
}
// 不符合尾调用优化 因为返回值里面有一个加号 运算
// fib(1000) 执行不了
```

改成尾调用优化
```js
function fib(a, b, n) {
  if(n === 0)return a
  return fib(b, a + b, n - 1)
}
```

# web worker

## 优势

独立运行：Web Worker在一个独立的线程中运行，与主线程互不干扰。这可以避免主线程因执行耗时操作而卡顿，从而提高页面的响应速度和用户体验。
隔离机制：Web Worker无法直接访问或操作DOM元素。这种隔离机制可以防止潜在的页面修改和恶意攻击，确保网页的安全性和稳定性。
多线程异步执行： Web Worker允许您在标签页进程中创建多个独立线程，每个线程都可以在不同的内核上执行，因此可以实现并行计算。


## 实际应用场景

1. 计算密集型任务：例如，数据分析、图像处理、加密算法等。这些任务通常耗时较长且对 CPU性能要求较高。通过将这些任务委托给Web Worker，可以避免阻塞主线程，从而保持应用的响应性。
2. 异步操作：当需要执行一些异步操作时，例如从服务器获取数据，可以使用Web Worker来避免因为等待异步操作完成而导致的主线程卡顿。
4. 人工智能：Web Worker可以用于处理机器学习模型的训练或推断时的并行计算，从而提高模型的训练速度和响应性。


## 局限性
- 兼容性问题：尽管HTML5已经普及，但Web Worker在大多数厂商的浏览器中仅在最新的大版本中支持，在旧版本中支持并不好。这导致开发者在实际项目中可能会放弃使用Web Worker。
- 资源限制：无法直接操作dom元素、同源策略、无法读取本地文件等等。
- 编程复杂性：使用Web Worker需要处理线程间的通信，这增加了编程的复杂性。对于简单的任务使用Web Worker可能并不划算。
- 安全性问题：因Web Worker是在后台运行的多线程，所以更隐蔽，由此也带来了新的安全问题：如恶意脚本的注入以利用Web Worker大规模执行多线程攻击来放大攻击效果、恶意创建大量web worker并让它们执行高负载的任务而导致浏览器崩溃、importScripts没有跨域限制可能会加载不受信任的脚本从而导致安全漏洞等等。

## 通信效率问题

- 无法共享内存： 与传统多线程编程不同，Web Worker不能直接共享内存，主页面与worker之间的数据传递的是通过拷贝而不是共享来完成的。因此在如大文件传输场景下可能会消耗大量内存和处理时间。
- 数据序列化问题： 当主线程与Web Worker之间在数据交换时需要对数据进行序列化和反序列化，序列化会阻塞发送方，而反序列化会阻塞接收方。因此即使是小型数据，也需要经过这一过程。这可能在频繁通信的情况下积累成为性能瓶颈。
- postMessage：postMessage方法本身并不是导致通信效率低下的主要原因，而是由于如Worker线程需要频繁地向主线程发送大量消息，或者消息体积较大等其他因素造成。这可能会导致主线程处理消息的速度跟不上Worker线程发送消息的速度，从而引起通信拥塞和性能问题。


## 项目中使用
### 例1: 加载dicom

```js
// dicom.worker.js
self.onmessage(
  fetch(xxxx).then(
    data => {
      data.arrayBuffer().then(d => {
        xxLoader.load(d)
      })
    }
  )
)

xxLoader.onload = d => {
  self.postmessage(d)
}
```

```js
// 主线程dicom.js
// 从分线程的文件里面引入
import DicomWorker from '@util/dicom.worker.js'
const worker = new DicomWorker()

worker.onmessage = event => {
  // 把下载到的东西发送出去
  fireEvent('自定义的事件', event.xxx)
}
```

### 2
```js
// worker.js
self.onmessage = function(event) {
  // 处理从主线程传递过来的数据
  const data = event.data;
  // 执行一些计算密集的任务
  const result = performSomeHeavyTask(data);
  // 将结果发送回主线程
  postMessage(result);
};
```
```js
// App
import React, { Component } from 'react';

class MyComponent extends Component {
  componentDidMount() {
    // 创建一个新的Web Worker实例
    const worker = new Worker('worker.js');

    // 向Web Worker发送数据
    worker.postMessage(someData);

    // 设置接收Web Worker的响应
    worker.onmessage = (event) => {
      // 处理来自Web Worker的响应数据
      const result = event.data;
      // 更新React组件的状态或执行其他操作
      this.setState({ result });
    };
  }

  componentWillUnmount() {
    // 在组件卸载时记得终止Web Worker
    worker.terminate();
  }

  render() {
    // 渲染组件
  }
}

export default MyComponent;
```

## 注意
在项目中直接引入worker文件路径会失败
需要使用 worler-loader插件 将.worker.ts文件转换为worker
```js
import Worker from "./file.worker.js";

const worker = new Worker();

worker.postMessage({ a: 1 });
worker.onmessage = function (event) {};

worker.addEventListener("message", function (event) {});
```

为了让Worlker可以直接被new调用, worker-loader插件将.worker.ts的文件转换了一个可以构造的函数
```js
export default class WebWorker {
  constructor(worker: WebWorker) {
    let code = worker.toString();
    code = code.substring(code.indexOf('{') + 1, code.lastIndexOf('}'));

    const blob = new Blob([code],{type: 'text/javascript'});

    return new Worker(URL.createObjectURL(blob));
  }
}
```

```js
import WebWorker from "./work";

export const MyWorker = () => {
  function workerCode(this: any){

    this.onmessage = function (e:MessageEvent){
      const {a,b} = e.data;
      this.postMessage({sum: a + b});
    }

  }
  const myWorker = new WebWorker(workerCode);
  return myWorker;
}
```
```js
const worker = MyWorker();

worker.postMessage({ a:1, b:2 });

worker.onmessage = (m:any) =>{
  console.log("msg from worker: ",m.data); // 3
};
```

这种方式会导致ts报错
可添加全局定义
```ts
declare module '*.worker.ts' {
  class WebpackWorker extends Worker {
    constructor();
  }
  export default WebpackWorker;
}
```

# 二进制文件处理
Blob、File、FileReader、ArrayBuffer、Base64、URL.createObjectURL()

## Blob
Blob 对象表示一个不可变、原始数据的类文件对象。它的数据可以按文本（text()方法）或二进制（arrayBuffer()方法）的格式进行读取，也可以转换成 ReadableStream （stream()方法）可读流来用于数据操作。Blob 提供了一种高效的方式来操作数据文件，而不需要将数据全部加载到内存中(比如流式读取、文件切片slice()方法)，这在处理大型文件或二进制数据时非常有用。
Blob 表示的不一定是 JavaScript 原生格式的数据，它还可以用来存储文件、图片、音频、视频、甚至是纯文本等各种类型的数据。

### 创建Blob对象
可以使用 new Blob() 构造函数来创建一个 Blob 对象：
```js
new Blob(blobParts)
new Blob(blobParts, options)
```

blobParts (可选)：一个可迭代对象，比如 Array，包含 ArrayBuffer、TypedArray、DataView、Blob、字符串或者任意这些元素的混合，这些元素将会被放入 Blob 中。
options (可选)：可以设置 type （MIME 类型）和 endings （用于表示换行符）。
```js
const blob1 = new Blob(["Hello, world!"], { type: "text/plain" });

const blob2 = new Blob(['<q id="a"><span id="b">hey!</span></q>'], { type: "text/html" });
```

### 属性和方法
```js
console.log(blob.size); // 输出 Blob 的大小
console.log(blob.type); // 输出 Blob 的 MIME 类型
```

slice() 方法：可以创建一个新的 Blob 对象，它是原 Blob 的一个子集。
```js
// slice([start], [end], [contentType])

// 该方法用于从 Blob 中提取一部分数据，并返回一个新的 Blob 对象。参数 start 和 end 表示提取的字节范围，contentType 设置提取部分的 MIME 类型。

const blob = new Blob(["Hello, world!"], { type: "text/plain" });

const partialBlob = blob.slice(0, 5);

```

text() 方法：将 Blob 中的数据读取为文本。

```js
blob.text().then((text) => {
  console.log(text); // 输出 "Hello, world!"
});
```

arrayBuffer() 方法：将 Blob 中的数据读取为 ArrayBuffer。
```js
const blob = new Blob(["Hello, world!"], { type: "text/plain" });

blob.arrayBuffer().then((buffer) => {
  console.log(buffer);
});

```

stream() 方法：返回一个 ReadableStream，允许你以流的形式读取 Blob 的数据。
适合处理大文件
```js
const blob = new Blob(["Hello, world! This is a test Blob."], { type: 'text/plain' });

// 获取 Blob 的可读流
const readableStream = blob.stream();

// 创建一个默认的文本解码器
const reader = readableStream.getReader();

// 用于读取流并输出到控制台
function readStream() {
  reader.read().then(({ done, value }) => {
    if (done) {
      console.log('Stream complete');
      return;
    }
    // 将 Uint8Array 转换为字符串并输出
    console.log(new TextDecoder("utf-8").decode(value));
    // 继续读取下一个块
    return reader.read().then(processText);
  }).catch(err => {
    console.error('Stream failed:', err);
  });
}

readStream();
```
一个更复杂的场景
将一个流的数据写入另一个流
```js
const blob = new Blob(["Hello, world! This is a test Blob."], { type: 'text/plain' });

// 使用 Blob.stream() 方法获取一个可读流
const readableStream = blob.stream();

// 创建一个新的 Response 对象，以便使用 Response.body 作为可读流
const response = new Response(readableStream);

// 使用 TextDecoderStream 将二进制流转换为字符串
const textDecoderStream = new TextDecoderStream();
readableStream.pipeTo(textDecoderStream.writable);

// 获取解码后的可读流
const decodedStream = textDecoderStream.readable;

// 创建一个可写流目标，通常是显示在页面上或传输到服务器
const writableStream = new WritableStream({
  write(chunk) {
    console.log(chunk); // 在控制台输出解码后的文本
    // 这里你可以将数据写入到某个地方，比如更新网页内容或上传到服务器
  },
  close() {
    console.log('Stream complete');
  }
});

// 将解码后的流数据写入到可写流
decodedStream.pipeTo(writableStream).catch(err => {
  console.error('Stream failed:', err);
});

```

### 使用场景
1. 生成文件下载
```js
/**
 * 下载文件
 * @param {Blob} blob   blob数据
 * @param {string} name 下载的文件名
 * @param {string} type 下载的文件后缀
 */
function _download(blob: Blob, name: string, type: string): void {
  const aLink = document.createElement('a');
  aLink.href = window.URL.createObjectURL(blob);
  aLink.download = `${name}.${ type }`;
  aLink.click();
  URL.revokeObjectURL(url); // 释放 URL 对象
}
```

2. 文件上传
Blob 常用于上传文件到服务器，尤其是在使用 FormData API 时
```js
const formData = new FormData();
// 其实 File 对象就是继承了 Blob 对象，只不过加上了一些文件信息。
formData.append("file", blob, "example.txt");
```

3. 图像处理
通过 FileReader API 可以将 Blob 对象读取为不同的数据格式。
```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Document</title>
  </head>
  <body>
    <input type="file" id="fileInput" accept="image/*" />

    <div id="imageContainer"></div>
    <script>
      const fileInput = document.getElementById("fileInput");
      const imageContainer = document.getElementById("imageContainer");
      fileInput.addEventListener("change", function (event) {
        // File 对象继承了 Blob 对象
        const file = event.target.files[0];

        if (file && file.type.startsWith("image/")) {
          const reader = new FileReader();

          reader.onload = function (e) {
            const img = document.createElement("img");
            img.src = e.target.result;
            img.style.maxWidth = "500px";
            img.style.margin = "10px";
            imageContainer.innerHTML = "";
            imageContainer.appendChild(img);
          };
          // 转成 base64
          reader.readAsDataURL(file);
        } else {
          alert("请选择一个有效的图片文件。");
        }
      });
    </script>
  </body>
</html>
```

## File
File 接口基于 Blob，继承了 blob 的功能并将其扩展以支持用户系统上的文件。


File 是 JavaScript 中代表文件的数据结构，它继承自 Blob 对象，包含文件的元数据（如文件名、文件大小、类型等）。
Blob 是纯粹的二进制数据，它可以存储任何类型的数据，但不具有文件的元数据（如文件名、最后修改时间等）。
File 是 Blob 的子类，除了继承 Blob 的所有属性和方法之外，还包含文件的元数据。
你可以将 File 对象看作是带有文件信息的 Blob。Blob 更加通用，而 File 更专注于与文件系统的交互。

File 对象通常有三种方式获取：

用户使用<input type="file"> 元素选择文件返回的 FileList 对象中获取（即files 属性）
从拖放操作返回的 DataTransfer 对象中获取。
可以使用 JavaScript 构造函数手动创建。
```html
<input type="file" id="fileInput" />

<script>
  // 获取用户上传的
  document.getElementById("fileInput").addEventListener("change", (event) => {
    const file = event.target.files[0];
    console.log("文件名:", file.name);
    console.log("文件类型:", file.type);
    console.log("文件大小:", file.size);
  });

  // 手动创建 File
  const file = new File(["Hello, world!"], "hello-world.txt", {
    type: "text/plain",
  });
  console.log(file);
</script>
```

### 属性和方法
```js
const blob = file.slice(0, 1024); // 获取文件的前 1024 个字节

file.text().then((text) => {
  console.log(text); // 输出文件的文本内容
});

file.arrayBuffer().then((buffer) => {
  console.log(buffer); // 输出文件的 ArrayBuffer
});

const stream = file.stream();

// File 对象上的一些常见属性
file.type
file.size
file.name
file.lastModified
```

### FileReader

readAsArrayBuffer()开始读取指定的 Blob 或 File 对象，并将文件内容读为 ArrayBuffer。

readAsBinaryString()开始读取指定的 Blob 或 File 对象，并将文件内容读为二进制字符串。

readAsDataURL()开始读取指定的 Blob 或 File 对象，并将文件内容读为 Data URL（Base64 编码的字符串）。

readAsText()开始读取指定的 Blob 或 File 对象，并将文件内容读为文本字符串，默认使用 UTF-8 编码。

abort()中止读取操作。在返回时，readyState 属性为 DONE。



除了属性和方法，FileReader 还会触发一些事件，可以监听这些事件来处理读取过程中的不同阶段


onloadstart在读取操作开始时触发。

onprogress在读取数据块时周期性地触发。

onload在读取操作成功完成时触发。

onabort在读取操作被中止时触发。

onerror在读取操作出错时触发。

onloadend在读取操作完成时触发，无论成功或失败（包括被中止）

## URL.createObjectURL()
URL 接口的提供了createObjectURL() 静态方法用于生成临时 URL ，它可以用来表示一个 File、Blob, 允许开发者通过 URL 引用本地的文件或数据，而不需要将其上传到服务器。

这个 URL 生命周期与其创建时所在窗口的 document 绑定在一起，浏览器会在卸载文档时自动释放对象 URL，然而，为了优化性能和内存使用，如果在安全时间内可以明确卸载，就应该卸载，这时需手动调用revokeObjectURL()。
### 使用场景

1. 预览文件：用户上传文件后，可以使用 createObjectURL() 生成一个 URL 来在浏览器中预览图像、视频或音频。
2. 动态生成内容：在不需要持久化存储的情况下，使用 Blob 对象动态生成内容并通过 URL 引用。