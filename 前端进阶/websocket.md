# HTTP和websocket
例如购物网站 点击商品 跳转到详情页
就是前端发起一次HTTP请求到服务器 网站返回一次HTTP响应 展现详情信息
这种由客户端主动请求服务器响应的方式已经满足大部分网站应用场景
但是这种方式服务器不会主动给客户端发送信息

# 如何让服务端主动发消息给客户端?
+ 客户端 自动连续 给服务端发送请求(浪费资源 请求间隔时间长时效性差)
+ 长轮询 将客户端等待服务端响应时间拉长 减少请求次数 -百度网盘扫码登录
  如何实现长轮询?
  我们的项目中扫码使用的是什么?
+ websocket -即时聊天 网页游戏等

# 简单工作原理
为了兼容浏览网页的时候从购物网站点到网页游戏中 两种不同的请求响应场景
都会首先由客户端向服务端发起一次HTTP请求

## 请求
请求里带有特殊的HTTP header
```
Connection: Upgrade
Upgrade: websocket
Sec-WebSocket-Key: G4334F14UYITVY312RGDEQ
```

## 响应
携带101状态码
101 (切换协议) 请求者已要求服务器切换协议，服务器已确认并准备切换
如果服务器支持升级websocket 就会升级 同时间根据hash转换成另一个字符串放在响应头的sec-ws-accept里面

## 数据包
opcode: 表示数据格式
payload: 存放想要传输的数据长度




# nodejs-websocket
[nodejs-websocket](https://github.com/sitegui/nodejs-websocket)

可以使用nodejs的方式创建websocket


