# NestJS
nest是一个js语言的后端框架
类似java中的 spring boot 或者go中的 gin


# nest cli
    ┌───────────────┬─────────────┬──────────────────────────────────────────────┐
    │ name          │ alias       │ description                                  │
    │ application   │ application │ Generate a new application workspace         │
    │ class         │ cl          │ Generate a new class                         │
    │ configuration │ config      │ Generate a CLI configuration file            │
    │ controller    │ co          │ Generate a controller declaration            │
    │ decorator     │ d           │ Generate a custom decorator                  │
    │ filter        │ f           │ Generate a filter declaration                │
    │ gateway       │ ga          │ Generate a gateway declaration               │
    │ guard         │ gu          │ Generate a guard declaration                 │
    │ interceptor   │ itc         │ Generate an interceptor declaration          │
    │ interface     │ itf         │ Generate an interface                        │
    │ library       │ lib         │ Generate a new library within a monorepo     │
    │ middleware    │ mi          │ Generate a middleware declaration            │
    │ module        │ mo          │ Generate a module declaration                │
    │ pipe          │ pi          │ Generate a pipe declaration                  │
    │ provider      │ pr          │ Generate a provider declaration              │
    │ resolver      │ r           │ Generate a GraphQL resolver declaration      │
    │ resource      │ res         │ Generate a new CRUD resource                 │
    │ service       │ s           │ Generate a service declaration               │
    │ sub-app       │ app         │ Generate a new application within a monorepo │
    └───────────────┴─────────────┴──────────────────────────────────────────────┘



# 基础概念
## Controller
nest的api
处理路由和解析请求参数
```ts
import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
```

## service
概念
做业务逻辑的具体实现，比如操作数据库等

对应的controller和对应的service在一起
比如bookController和bookService在一起

## Module
nest的api

因为对应的controller和对应的service在一起
所以把他们放在一个模块里面  用于区分

每个模块都会包含 controller、service、module、dto、entities 这些东西

```ts
import { Module } from '@nestjs/common';
import{ UserService }from '。/user.service';
import{UserController}from'./user.controller';

@Module({
    controllers:[UserController],
    providers:[UserService],
})

export class UserModule {}
```


## Inject
nest的api
IoC (依赖注入) 会自动从容器中查找实例来注入
构造器注入和属性注入



# nest中的架构思想和设计模式
## MVC
nest 应用跑起来后，会从 AppModule 开始解析，初始化 IoC 容器，加载所有的 service 到容器里
然后解析 controller 里的路由，接下来就可以接收请求了。

其实这种架构叫做 MVC 模式，也就是 model、view、controller。

请求先经过 Controller，然后调用 Model 层的 Service、Repository 完成业务逻辑，最后返回对应的 View。

## IOC
控制反转 是一种设计思想, 目的是解耦
依赖注入是实现IOC的手段
在Nest中
Provider：真正提供具体功能实现的低层类。 (工人)
Controller：调用低层类来为用户提供服务的高层类。(车间)
Nest框架本身：控制反转容器，对高层类和低层类统一管理，控制相关类的新建与注入，解耦了类之间的依赖。

## AOP
Middleware、Guard、Pipe、Interceptor、Filter 都可以透明的添加某种处理逻辑到某个路由或者全部路由，这就是 AOP 的好处。
`nest g <某个AOP工具名, 例如guard> <自定义名称> --no-spec --flat`
--no-spec表示不生成测试的spec模版
--flat表示展平在当前目录下 不单独生成一个文件夹

### AOP工具调用顺序
1. 首先是中间件机制 middleware
2. 会先调用 Guard，判断是否有权限等, 没权限抛出异常
3. 抛出的 ForbiddenException 会被 ExceptionFilter 处理，返回 403 状态码。
4. 如果有权限，就会调用到拦截器interceptor
5. 最后是使用pipe对参数进行处理
6. 如果有响应拦截器 继续走Interceptor
7. ExceptionFilter处理响应中的异常


## 装饰器
@Module定义模块
@Controller定义controller
@Injectable 定义provider, 可以是任何类

@Inject在controller中可以用来注入
@Optional表示可选

@Global可以把module变成全局module, 这样该module exports的provider就不需要imports module就可以直接注入

filer是用来处理异常的类, @Catch可以指定处理什么类型异常
@UseFilters 可以将处理异常的类应用到controller中的handler上
@UseGuards @UseInterceptors @UsePipes都是类似的用法

## controller中的参数类型
handler的请求方式 @Get @Post @Put @Delete @Patch @Options @Head
handler参数
@Param(<name>, pipe) 取url路径中的参数 /xxx/aaa 中的aaa
@Query(<name>, pipe) 取query参数 /xxx/aaa?num=1 中的num
@Body() 一般用dto里面的class来定义请求体里面的参数

## metadata
handler和class可以通过@SetMetadata指定metadata




# 动态创建module
module中定义static方法用于动态创建module, 支持传入参数
nest约定了三种方法
register

forRoot

forFeature


