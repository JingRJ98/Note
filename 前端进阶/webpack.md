# 简介
静态的模块化打包工具

## 初体验
./src/main.js作为入口
./public/index.html
创建package.json文件
`pnpm init`
安装webpack和webpack-cli
-D局部安装 -g全局安装
`npm i webpack webpack-cli -D`
执行打包(开发 | 生产)
`npx webpack ./src/main.js --mode=development`
`npx webpack ./src/main.js --mode=production`
开发打包结果包含各种注释
生产环境是极致压缩(去除注释,变量名也变得极简)
构建结果默认在./dist中

打包完html引入./dist/main.js就可以在浏览器显示了


# 五个核心概念
## 1. Entry入口
## 2. Output输出
## 3. Loader 处理非js(css,图片等)其他资源
loader是用于特定的模块类型进行转换
```js
const a = module: {
  rules: [
    {
      test: RegExp,
      loader: "css-loader",
    }
  ]
}
```
test用于匹配文件,value是正则

loader属性指定处理的loader(一般不用这个 用use)

use数组,每个元素都是一个useEntry对象
可以指定多个loader,上面那个只能指定一个loader
`use数组里面的loader处理顺序是从后向前的!!!`

## 4. Plugins插件
执行更广泛的任务,比如打包优化 资源管理 环境变量注入等
从打包优化和压缩,到重定义环境变量,扩展webpack的功能
plugins是一个数组,里面每个元素都是一个plugin实例
```bash
{
  plugins: [
    new CleanWebpackPlugin()
  ]
}
```
## 5. Mode模式
+ development开发模式  能让代码本地调试运行的环境
  开发模式不会讲文件打包输出到dist中,而是在内存中的打包 编译
  这就是平时开发的时候dist目录下没有打包的文件的原因
+ production 生产模式  能让代码优化上线运行的环境

## 一些名称相关的placeholders
[ext] 扩展名
[name] 文件名
[hash] 文件的内容hash  使用MD4处理 生成的128位hash值  32个16进制数
[contentHash] 在file-loader中和[hash]值一致
[hash:<number>] 截取hash的长度 默认32位太长了
[path] 文件相当于webpack配置的路径

## webpack配置文件
在根目录下webpack.config.js

```js
// 内置模块 resolve用来拼接绝对路径的方法
const { resolve } = require('path');

module.exports = {
  // 入口起点
  entry: "./src/main.js",
  // 输出
  output: {
    // 输出文件名
    filename: 'build.js',
    // 配置里面的输出路径必须使用绝对路径
    // __dirname 是node的变量,代表当前文件的目录绝对路径
    // 第二个参数表示输出的所有内容的文件目录
    path: resolve(__dirname, 'build'),
    // 自动清空上次打包结果 先删除build目录再重新打包
    // 只有输出的文件名不一样才有用比如带hash,文件名一样自动回覆盖上次的打包结果
    clean: true,
  },
  // loader
  module: {
    // 详细loader配置
    // rules是数组,里面每个元素是一个rule对象
    rules: [
      {
        // 处理css文件loader
        // 检测所有.css文件
        test: /\.css$/,
        // 处理的loader
        // use数组的执行顺序是从后到前的!
        use: [
          // 创建style标签 添加到header中 (此时css代码在style标签中,仍然是和js一个文件)
          // JS、CSS 资源无法并行加载，从而降低页面性能；
          // 资源缓存粒度变大，JS、CSS 任意一种变更都会致使缓存失效。
          // 生产环境中一般使用mini-css-extract-plugin来执行
          // MiniCssExtractPlugin.loader + new MiniCssExtractPlugin()
          'style-loader',
          // 将css文件变成commonjs模块加载到js中
          // 使用loader前先下载
          // npm install --save-dev css-loader
          // 简写: npm i css-loader -D
          'css-loader',
        ],

        // 等同于下面这种,上面更简单
        // use: [
        //   {loader: 'css-loader'},
        //   ...
        // ]
      },
      {
        // 处理less文件loader
        test: /\.less$/,
        // 处理的loader
        use: [
          'style-loader',
          'css-loader',
          // 将less文件编译成css文件
          'less-loader',
        ]
      }
    ]
  },
  // plugins配置
  plugins: [],
  // 模式(开发 | 生产)
  mode: 'development',
}
```

### 使用
plugins: 1.下载  2.引入  3.使用

## 打包html & 图片资源 & icon图标 config
```js
const { resolve } = require('path');
// 用于自动在dist中生成一个html文件并引入打包后的 js
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  module: {
    rules: [
      {
        // 匹配图片
        test: /\.(jpe?g|png|gif|svg)$/,
        loader: 'url-loader',
        options: {
          // base64
          // 优点: 减少请求数量 减轻服务器压力
          // 缺点: 图片体积会更大 文件请求速度会更慢

          // 实际情况中可以自行设置limit数值
          // 不加limit所有图片都base64处理了
          // 图片大小小于8kb,就会被base64处理
          limit: 8 * 1024,

          // 问题: 因为url-loader使用es6解析模块化 而html-loader引入图片是commonjs
          // 解析时会出问题  [object Module]
          // 解决 关闭url-loader的es6模块化  使用commonjs解析
          esModule: false,

          // 给图片重命名
          // 取出文件名字哈希值前10位
          // [ext] 保留源文件的后缀名
          name: '[hash:10][ext]'
        }
      },
      {
        test: /\.html$/,
        // 处理html文件中的img图片
        loader: 'html-loader',
      },
      {
        // 字体图标资源
        test: /\.(ttf|woff2?)$/i,
        type: 'asset/resource',
        generator: {
          // 输出路径
          filename: 'static/media/[name]_[hash:8][ext][query]'
        }
      }
      {
        // 打包其他资源
        // 采用排除法 exclude排除三件套文件
        exclude: /\.(js|css|html)$/,
        loader: 'file-loader',
      }
    ]
  },
  // plugins配置
  plugins: [
    new HtmlWebpackPlugin({
      // 生成一个index.html文件 通过template指定路径下的html模版生成
      template: './src/index.html',
      // 配置生成的html文件标题
      title: 'xxxxxxxx'
    })
  ],
  // 模式
  mode: 'development',
}
```

对于图片资源,可以设置type: 'asset'等 不需要使用loader

## 设置输出路径
js文件直接在output属性中设置
图片资源在loader中的 type: asset 下 generator配置打包的资源输出路径
```js
const path = require('path')

module.exports = {
  // 入口
  entry: './src/main.js',
  // 输出
  output: {
    // 文件的输出路径, 绝对路径
    path: path.resolve(__dirname, 'dist'),
    // 入口文件的输出路径, 放在dist下的js文件夹中
    filename: 'static/js/main.js',
    // 每次打包之前删除之前打包的元素
    clean: true
  },
  // loader
  module: {
    rules: [
      // loader配置
      {
        test: /\.css$/i, // 检测文件
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.less$/i,
        use: [
          'style-loader',
          'css-loader',
          'less-loader',
        ],
      },
      {
        test: /\.s[ac]ss$/i,
        use: [
          'style-loader',
          'css-loader',
          // 将 Sass 编译成 CSS
          'sass-loader',
        ],
      },
      {
        test: /\.(png|jpe?g|gif|webp)$/,
        type: "asset",
        parser: {
          dataUrlCondition: {
            // 小于5kb的图片会转为base64
            // 减少图片请求数量
            // 缺点是体积会变大一点点
            maxSize: 5 * 1024
          }
        },
        generator: {
          filename: 'static/images/[hash][ext][query]'
        }
      },
    ]
  },
  // plugin
  plugins: [],
  // mode
  mode: 'development'
}
```

## 定义常量的plugin
```js
new DefinePlugin({
  // 定义base url
  BASE_URL: '',
})
```

## 配置eslint
```js
module.exports = {
  // 解析选项
  parserOptions: {
    ecmaVersion: 6, // ES 语法版本
    sourceType: "module", // ES 模块化
    ecmaFeatures: { // ES 其他特性
      jsx: true // 如果是 React 项目，就需要开启 jsx 语法
    }
  },
  // 具体检查规则
  rules: {},
  // 继承其他规则
  extends: [],
  // ...
  // 其他规则详见：https://eslint.bootcss.com/docs/user-guide/configuring
};
```
rules 具体规则
"off" 或 0 - 关闭规则
"warn" 或 1 - 开启规则，使用警告级别的错误：warn (不会导致程序退出)
"error" 或 2 - 开启规则，使用错误级别的错误：error (当被触发的时候，程序会退出)

.eslintrc.js
```js
module.exports = {
  // 继承 Eslint 规则
  extends: ["eslint:recommended"],
  env: {
    node: true, // 启用node中全局变量
    browser: true, // 启用浏览器中全局变量
  },
  parserOptions: {
    ecmaVersion: 6,
    sourceType: "module",
  },
  rules: {
    // 使用var运行就会报错
    "no-var": 'error', // 不能使用 var 定义变量
  },
};
```
webpack.config.js
```js
const path = require('path')
// 引入eslint插件
const ESLintPlugin = require('eslint-webpack-plugin')

module.exports = {
  // 入口
  entry: './src/main.js',
  // 输出
  output: {
    // 文件的输出路径, 绝对路径
    path: path.resolve(__dirname, 'dist'),
    // 入口文件的输出路径, 放在dist下的js文件夹中
    filename: 'static/js/main.js',
    clean: true
  },
  // loader
  module: {
    rules: [
      // loader配置
      {
        test: /\.css$/i, // 检测文件
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.less$/i,
        use: [
          'style-loader',
          'css-loader',
          'less-loader',
        ],
      },
      {
        test: /\.s[ac]ss$/i,
        use: [
          'style-loader',
          'css-loader',
          // 将 Sass 编译成 CSS
          'sass-loader',
        ],
      },
      {
        test: /\.(png|jpe?g|gif|webp)$/,
        type: "asset",
        parser: {
          dataUrlCondition: {
            // 小于5kb的图片会转为base64
            // 减少图片请求数量
            // 缺点是体积会变大一点点
            maxSize: 5 * 1024
          }
        },
        generator: {
          filename: 'static/images/[name]_[hash:8][ext][query]'
        }
      },
      {
        test: /\.(ttf|woff2?)$/i,
        type: 'asset/resource',
        generator: {
          filename: 'static/media/[name]_[hash:8][ext][query]'
        }
      }
    ]
  },
  // plugin
  plugins: [
    new ESLintPlugin({
      // 检查哪些文件
      context: path.resolve(__dirname, 'src')
    })
  ],
  // mode
  mode: 'development'
}
```

## 开发时服务
如果修改源代码 是不会自动更新的 必须手动重新打包
*方案一: 在package.json中命令添加watch `build: 'webpack --watch'`*
*方案二: 在webpack.config.js文件第一层添加watch属性 设置为true*

注意 以上两种watch的方案的效率不高 因为每次修改都重新打包全部文件,并且重新生成新的bundle文件,效率不高

方案三: WDS
webpack-dev-server
首先安装
`pnpm i webpack-dev-server -D`

package.json
`"serve": "webpack serve"`

开启之后在localhost: 8080端口自动开启热更新

webpack-dev-server并没有在当前目录(或者指定的目录下生成打包后的 文件)
因为热更新后重新打包的文件操作 相当消耗性能
所以webpack将打包后的文件储存在内存中 每次直接读取 节约性能
使用的是memory-fs/memfs库(webpack自己开发的)
*webpack-dev-server使用的是express开启的服务*

相关配置(不一定准确 以官网最新为准)
```js
module.exports = {
  devServer : {
    // 开发服务器 用来自动化(自动编译 自动打开浏览器 自动刷新浏览器)
    // 特点: 只会在内存中编译打包  不会有任何输出到dist

    // 对于打包后的资源没有什么作用 但是如果打包后的资源 依赖了其他资源
    // 可以用来指定需要从哪个路径下寻找其他资源
    contentBase: resolve(__dirname, 'public'),
    // 启动gzip压缩
    compress: true,
    // 端口号:
    prot: 3000,
    // 是否自动打开浏览器
    open: true,
    // 开启HMR
    hot: true,
  }
}
```

webpack-dev-middleware
可以实现更自由的定制化的HMR服务

## HMR hot module replacement 模块热替换
修改项目中单独某一个模块 只修改对应的模块即可
不需要加载整个页面 可以保留某些应用程序的状态不改变
修改了css, js 会立即在浏览器中表现变化 和在devtools中修改类似

**注意**即使在devServer的配置里面开启了HMR webpack还是会全部重新打包 除非在该模块下指定本模块需要添加HMR

### 框架中的HMR
一般情况下,使用React等框架进行开发的时候 不需要手动设置HMR
react-refresh

### HMR原理
webpack-dev-serve会创建两个服务 提供静态资源的服务 express 和socket服务net.Socket
express server负责提供静态资源的服务(打包后的资源直接被浏览器请求和解析)

net.Socket 是一个socket长连接


# 生产环境搭建
将压缩 优化等操作放在生产环境下处理 因为比较缓慢

## css


### css-loader
作用是将css语法文件解析成webpack可以识别的类js代码, 或者说代码块

### style-loader
将css代码被处理后的模块通过style标签插入到构建的html中

### mini-css-extract-plugin
MiniCssExtractPlugin用于把css文件单独打包成一个css文件 然后通过link标签引入

**对比style-loader**
style-loader会将css-loader解析的语法插入到构建产物的html中
以动态创建style标签的形式插入
这种将 JS、CSS 代码合并进同一个产物文件的方式有几个问题：

1. JS、CSS 资源无法并行加载，从而降低页面性能
2. 资源缓存粒度变大，JS、CSS 任意一种变更都会致使缓存失效
3. 是用js动态创建style标签再插入,可能会造成闪屏现象


### loader顺序
use数组中loader执行的顺序是从后到前的
以上loader的工作原理也说明了为什么是先css-loader, 后style-loader/mini-css-extract-plugin.loader


+ 单独提取css文件
```js
const { resolve } = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin')

module.exports = {
  // 入口起点
  entry: './src/js/index.js',
  // 输出
  output: {
    // 输出文件名
    filename: 'js/build.js',
    // 输出路径
    // __dirname 是node的变量,代表当前文件的目录绝对路径
    path: resolve(__dirname, 'build'),
  },
  // loader
  module: {
    rules: [
      {
        test: /\.css$/,
        // MiniCssExtractPlugin.loader取代'style-loader'
        // 作用是提取js中的 css成单独文件
        use: [MiniCssExtractPlugin.loader, 'css-loader']
      }
    ]
  },
  // plugins配置
  plugins: [
    new HtmlWebpackPlugin({
      template: './src/index.html'
    }),
    new MiniCssExtractPlugin({
      // 对输出的文件重命名
      filename: 'css/built.css'
    })
  ],
  // 模式
  mode: 'development',
}
```

### css兼容处理
postcss-loader

postcss-present-env插件
可以转化一些现代css特性大部分浏览器可以识别的

```js
module: {
  rules: [
    {
      test: /\.css$/,
      // MiniCssExtractPlugin.loader取代'style-loader'
      // 作用是提取js中的 css成单独文件
      use: [
        MiniCssExtractPlugin.loader,
        'css-loader',
        // css兼容性处理
        {
          loader: 'postcss-loader',
          options: {
            ident: 'postcss',
            plugins: () => [
              // postcss插件
              require('postcss-present-env')()
            ]
          }
        }
      ]
    }
  ]
}
```


### css压缩
```js
const OptimizeCssAssetsWebpackPlugin = require('optimize-css-assets-webpack-plugin')
plugins: [
  new OptimizeCssAssetsWebpackPlugin()
]
```

## js兼容性处理
```js
{
  module: {
    rules: [
      /*
        js兼容性处理：babel-loader @babel/core
          1. 基本js兼容性处理 --> @babel/preset-env
            问题：只能转换基本语法，如promise高级语法不能转换
          2. 全部js兼容性处理 --> @babel/polyfill
            问题：我只要解决部分兼容性问题，但是将所有兼容性代码全部引入，体积太大了~
          3. 需要做兼容性处理的就做：按需加载  --> core-js
      */
      {
        test: /\.js$/,
        // 排除/node_modules中的代码 不需要使用babel转译
        exclude: /node_modules/,
        loader: 'babel-loader',
        options: {
          // 预设：指示babel做怎么样的兼容性处理
          presets: [
            [
              '@babel/preset-env',
              {
                // 按需加载
                useBuiltIns: 'usage',
                // 指定core-js版本
                corejs: {
                  version: 3
                },
                // 指定兼容性做到哪个版本浏览器
                targets: {
                  chrome: '60',
                  firefox: '60',
                  ie: '9',
                  safari: '10',
                  edge: '17'
                }
              }
            ]
          ]
        }
      }
    ]
  },
}
```

## 压缩js & html
```js
plugins: [
  new HtmlWebpackPlugin({
    template: './src/index.html',
    // 压缩html代码
    minify: {
      // 移除空格
      collapseWhitespace: true,
      // 移除注释
      removeComments: true
    }
  })
],

// 生产环境下会自动压缩js代码
mode: 'production'
```

## 开发环境和生产环境
可以在项目下创建config文件夹, 定义webpack.dev.js和webpack.prod.js
运行的时候指定config
开发环境`npx webpack serve --config ./config/webpack.dev.js`
生产环境`npx webpack --config ./config/webpack.dev.js`
或者在package.json中定义这个命令
```json
"scripts": {
  "wp": "webpack serve",
  "wp:dev": "webpack serve --config ./config/webpack.dev.js",
  "wp:prod": "webpack --config ./config/webpack.prod.js"
},
```
解释: 开发环境不打包输出到项目根目录(在内存中打包输出), 但是需要有devserver配置(webpack serve)
生产环境不需要dev-server的设置, 但是需要打包输出到项目根目录


## 生产环境配置
MiniCssExtractPlugin用于把css文件单独打包成一个css文件 然后通过link标签引入

**对比style-loader**
style-loader会将css-loader解析的语法插入到构建产物的html中
以动态创建style标签的形式插入
这种将 JS、CSS 代码合并进同一个产物文件的方式有几个问题：

1. JS、CSS 资源无法并行加载，从而降低页面性能
2. 资源缓存粒度变大，JS、CSS 任意一种变更都会致使缓存失效
3. 是用js动态创建style标签再插入,可能会造成闪屏现象

```js
const { resolve } = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCssAssetsWebpackPlugin = require('optimize-css-assets-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

// 定义nodejs环境变量：决定使用browserslist的哪个环境
process.env.NODE_ENV = 'production';

// 复用loader
const commonCssLoader = [
  MiniCssExtractPlugin.loader,
  'css-loader',
  {
    // 还需要在package.json中定义browserslist
    // 解决样式兼容性问题
    loader: 'postcss-loader',
    options: {
      ident: 'postcss',
      plugins: () => [require('postcss-preset-env')()]
    }
  }
];

module.exports = {
  entry: './src/js/index.js',
  output: {
    filename: 'js/built.js',
    path: resolve(__dirname, 'build')
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [...commonCssLoader]
      },
      {
        test: /\.less$/,
        use: [...commonCssLoader, 'less-loader']
      },
      /*
        正常来讲，一个文件只能被一个loader处理。
        当一个文件要被多个loader处理，那么一定要指定loader执行的先后顺序：
          先执行eslint 在执行babel
      */
      {
        // 在package.json中eslintConfig --> airbnb
        test: /\.js$/,
        exclude: /node_modules/,
        // 优先执行
        enforce: 'pre',
        loader: 'eslint-loader',
        options: {
          fix: true
        }
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        options: {
          presets: [
            [
              '@babel/preset-env',
              {
                useBuiltIns: 'usage',
                corejs: {version: 3},
                targets: {
                  chrome: '60',
                  firefox: '50'
                }
              }
            ]
          ]
        }
      },
      {
        test: /\.(jpg|png|gif)/,
        loader: 'url-loader',
        options: {
          limit: 8 * 1024,
          name: '[hash:10][ext]',
          outputPath: 'imgs',
          esModule: false
        }
      },
      {
        test: /\.html$/,
        loader: 'html-loader'
      },
      {
        exclude: /\.(js|css|less|html|jpg|png|gif)/,
        loader: 'file-loader',
        options: {
          outputPath: 'media'
        }
      }
    ]
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: 'css/built.css'
    }),
    new OptimizeCssAssetsWebpackPlugin(),
    new HtmlWebpackPlugin({
      template: './src/index.html',
      minify: {
        collapseWhitespace: true,
        removeComments: true
      }
    })
  ],
  mode: 'production'
};
```

# webpack的模块化

## commonjs模块化
module.exports是一个对象
模块的路径为Key  模块导出的内容放到对象的exports {exports: {}}


## ES module模块化
没看懂
https://www.bilibili.com/video/BV1114y1t72B?p=5&spm_id_from=pageDriver&vd_source=a457835049742948229c603fe72cfe1e

## commonjs加载ES module
支持commonjs和ES Module混用


## ES module 加载commonjs



# 性能优化
## 开发环境性能优化

* 优化打包构建速度
  * HMR
* 优化代码调试
  * source-map

### HMR
```js
/*
  HMR: hot module replacement 热模块替换 / 模块热替换
  作用：一个模块发生变化，只会重新打包这一个模块（而不是打包所有模块）极大提升构建速度

  样式文件：可以使用HMR功能：因为style-loader内部实现了~

  js文件：默认不能使用HMR功能 --> 需要修改js代码，添加支持HMR功能的代码
    注意：HMR功能对js的处理，只能处理非入口js文件的其他文件。

  html文件: 默认不能使用HMR功能.开启HMR会导致问题：html文件不能热更新了~
    解决：修改entry入口，添加html文件引入
    entry: ['./src/js/index.js', './src/index.html'],
    * 其实html不用做HMR功能,因为只有一个html文件 修改了一定会热更新
*/
const { resolve } = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  entry: ['./src/js/index.js', './src/index.html'],
  output: {
    filename: 'js/built.js',
    path: resolve(__dirname, 'build')
  },
  module: {
    rules: [
      // loader的配置
      {
        // 处理less资源
        test: /\.less$/,
        use: ['style-loader', 'css-loader', 'less-loader']
      },
      {
        // 处理css资源
        test: /\.css$/,
        use: ['style-loader', 'css-loader']
      },
      {
        // 处理图片资源
        test: /\.(jpg|png|gif)$/,
        loader: 'url-loader',
        options: {
          limit: 8 * 1024,
          name: '[hash:10][ext]',
          // 关闭es6模块化
          esModule: false,
          outputPath: 'imgs'
        }
      },
      {
        // 处理html中img资源
        test: /\.html$/,
        loader: 'html-loader'
      },
      {
        // 处理其他资源
        exclude: /\.(html|js|css|less|jpg|png|gif)/,
        loader: 'file-loader',
        options: {
          name: '[hash:10][ext]',
          outputPath: 'media'
        }
      }
    ]
  },
  plugins: [
    // plugins的配置
    new HtmlWebpackPlugin({
      template: './src/index.html'
    })
  ],
  mode: 'development',
  devServer: {
    contentBase: resolve(__dirname, 'public'),
    compress: true,
    port: 3000,
    open: true,
    // 开启HMR功能
    // 当修改了webpack配置，新配置要想生效，必须重新webpack服务
    hot: true
  }
};



  /* js文件HMR需要在代码中添加判断 */
  if (module.hot) {
    // 一旦 module.hot 为true，说明开启了HMR功能。 --> 让HMR功能代码生效
    module.hot.accept('./print.js', function() {
      // 方法会监听 print.js 文件的变化，一旦发生变化，其他模块不会重新打包构建。
      // 会执行后面的回调函数
      print();
    });
  }
```


### source-map
`devtool: 'eval-source-map'`
```js
module.exports = {
  entry: "./src/main.js",
  output: {},
  module: {
    rules: [
      // loader的配置
      {
        test: /\.less$/,
        use: ['style-loader', 'css-loader', 'less-loader']
      },
      ......
    ]
  },
  plugins: [],
  mode: 'development',
  devtool: 'eval-source-map'
};
```
+ source-map: 一种 提供源代码到构建后代码映射 技术 （如果构建后代码出错了，通过映射可以追踪源代码错误）

+ `devtool: [inline- |hidden- |eval-][nosources-][cheap-[module-]]source-map,`
  source-map：外部
    错误代码准确信息 和 源代码的错误位置
  inline-source-map：内联
    只生成一个内联source-map
    错误代码准确信息 和 源代码的错误位置
  hidden-source-map：外部
    错误代码错误原因，但是没有错误位置
    不能追踪源代码错误，只能提示到构建后代码的错误位置
    (隐藏源代码)
  eval-source-map：内联
    每一个文件都生成对应的source-map，都在eval
    错误代码准确信息 和 源代码的错误位置
  nosources-source-map：外部
    错误代码准确信息, 和 源代码的错误位置,
    但是没有任何源代码信息!(隐藏源代码)
  cheap-source-map：外部
    错误代码准确信息 和 源代码的 粗略 错误位置
    只能精确的行
  cheap-module-source-map：外部
    错误代码准确信息 和 源代码的错误位置
    module会将loader的source map加入

+ 内联 和 外部的区别：1. 外部生成了文件，内联没有 2. 内联构建速度更快
+ 开发环境：速度快，调试更友好
  速度快(eval>inline>cheap>...)
  更快:  eval-cheap-souce-map
  其次快:  eval-source-map

  调试更友好
  最友好: souce-map
  其次友好:  cheap-module-souce-map
  其次友好:  cheap-souce-map

  权衡速度快和调试快 选择: eval-source-map  / eval-cheap-module-souce-map


+ 生产环境：源代码要不要隐藏? 调试要不要更友好
  内联会让代码体积变大，所以在生产环境不用内联!
  nosources-source-map 全部隐藏
  hidden-source-map 只隐藏源代码，会提示构建后代码错误信息

  权衡后选择 source-map / cheap-module-souce-map


## 生产环境性能优化
* 优化打包构建速度
  * oneOf
  * babel缓存
  * 多进程打包
  * externals
  * dll
* 优化代码运行的性能
  * 缓存(hash-chunkhash-contenthash)
  * tree shaking
  * code split
  * 懒加载/预加载
  * pwa

### oneOf
主要作用于loader
相当于在loader的循环中间添加break 执行完退出
正常来讲，一个文件只能被一个loader处理。
当一个文件要被多个loader处理，那么一定要指定loader执行的先后顺序：
  先执行eslint 在执行babel
```js
module.exports = {
  entry: "./src/main.js",
  output: {},
  module: {
    rules: [
      {
        // 在package.json中eslintConfig --> airbnb
        test: /\.js$/,
        exclude: /node_modules/,
        // 优先执行
        enforce: 'pre',
        loader: 'eslint-loader',
        options: {
          fix: true
        }
      },
      {
        // 以下loader只会匹配一个 类似switch
        // 注意：不能有两个配置处理同一种类型文件
        oneOf: [
          {
            test:,
            use:
          },
          {
            test: ,
            exclude: ,
            loader: ,
            options: {}
          },
          { test: ,loader: },
          { exclude:,loader: }
        ]
      }
    ]
  },
  plugins: [],
  mode: 'production'
};
```

### 缓存
babel缓存
  cacheDirectory: true
  --> 让第二次打包构建速度更快

文件资源缓存
  hash: 每次wepack构建时会生成一个唯一的hash值。
    问题: 因为js和css同时使用一个hash值。
      如果重新打包，会导致所有缓存失效。（可能我却只改动一个文件）
  chunkhash：根据chunk生成的hash值。如果打包来源于同一个chunk，那么hash值就一样
    问题: js和css的hash值还是一样的
      因为css是在js中被引入的，所以同属于一个chunk
  contenthash: 根据文件的内容生成hash值。不同文件hash值一定不一样
  --> 让代码上线运行缓存更好使用
```js
const { resolve } = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCssAssetsWebpackPlugin = require('optimize-css-assets-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ESLintWebpackPlugin = require("eslint-webpack-plugin");

// 定义nodejs环境变量：决定使用browserslist的哪个环境
process.env.NODE_ENV = 'production';

// 复用loader
const commonCssLoader = [
  MiniCssExtractPlugin.loader,
  'css-loader',
  {
    // 还需要在package.json中定义browserslist
    loader: 'postcss-loader',
    options: {
      ident: 'postcss',
      plugins: () => [require('postcss-preset-env')()]
    }
  }
];

module.exports = {
  entry: './src/js/index.js',
  output: {
    // 在文件名后面添加哈希值  当哈希值发生变化的时候不适用缓存
    filename: 'js/built.[contenthash:10].js',
    path: resolve(__dirname, 'build')
  },
  module: {
    rules: [
      {
        // 在package.json中eslintConfig --> airbnb
        test: /\.js$/,
        exclude: /node_modules/,
        // 优先执行
        enforce: 'pre',
        loader: 'eslint-loader',
        options: {
          fix: true
        }
      },
      {
        // 以下loader只会匹配一个
        // 注意：不能有两个配置处理同一种类型文件
        oneOf: [
          {
            test: /\.css$/,
            use: [...commonCssLoader]
          },
          {
            test: /\.less$/,
            use: [...commonCssLoader, 'less-loader']
          },
          /*
            正常来讲，一个文件只能被一个loader处理。
            当一个文件要被多个loader处理，那么一定要指定loader执行的先后顺序：
              先执行eslint 在执行babel
          */
          {
            test: /\.js$/,
            exclude: /node_modules/,
            loader: 'babel-loader',
            options: {
              presets: [
                [
                  '@babel/preset-env',
                  {
                    useBuiltIns: 'usage',
                    corejs: { version: 3 },
                    targets: {
                      chrome: '60',
                      firefox: '50'
                    }
                  }
                ]
              ],
---------->   // 开启babel缓存
---------->   // 第二次构建时，会读取之前的缓存
              cacheDirectory: true,
              cacheCompression: false, // 缓存文件不要压缩
            }
          },
          {
            test: /\.(jpg|png|gif)/,
            loader: 'url-loader',
            options: {
              limit: 8 * 1024,
              name: '[hash:10][ext]',
              outputPath: 'imgs',
              esModule: false
            }
          },
          {
            test: /\.html$/,
            loader: 'html-loader'
          },
          {
            exclude: /\.(js|css|less|html|jpg|png|gif)/,
            loader: 'file-loader',
            options: {
              outputPath: 'media'
            }
          }
        ]
      }
    ]
  },
  plugins: [
     new ESLintWebpackPlugin({
      // 指定检查文件的根目录
      context: path.resolve(__dirname, "../src"),
      exclude: "node_modules", // 默认值
      cache: true, // 开启缓存
      // 缓存目录
      cacheLocation: path.resolve(
        __dirname,
        "../node_modules/.cache/.eslintcache"
      ),
    }),
    new MiniCssExtractPlugin({
      filename: 'css/built.[contenthash:10].css'
    }),
    new OptimizeCssAssetsWebpackPlugin(),
    new HtmlWebpackPlugin({
      template: './src/index.html',
      minify: {
        collapseWhitespace: true,
        removeComments: true
      }
    })
  ],
  mode: 'production',
  devtool: 'source-map'
};
```


### tree shaking
摇树
为了去除应用程序中没有使用的代码
例如引入第三方库中没有使用的

```js
const { resolve } = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCssAssetsWebpackPlugin = require('optimize-css-assets-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

/*
  tree shaking：去除无用代码
    前提：1. 必须使用ES6模块化  2. 开启production环境
    作用: 减少代码体积

    在package.json中配置
      "sideEffects": false 所有代码都没有副作用（都可以进行tree shaking）
        问题：可能会把css / @babel/polyfill （副作用）文件干掉
      "sideEffects": ["*.css", "*.less"]
*/

// 定义nodejs环境变量：决定使用browserslist的哪个环境
process.env.NODE_ENV = 'production';

// 复用loader
const commonCssLoader = [
  MiniCssExtractPlugin.loader,
  'css-loader',
  {
    // 还需要在package.json中定义browserslist
    loader: 'postcss-loader',
    options: {
      ident: 'postcss',
      plugins: () => [require('postcss-preset-env')()]
    }
  }
];

module.exports = {
  entry: './src/js/index.js',
  output: {
    filename: 'js/built.[contenthash:10].js',
    path: resolve(__dirname, 'build')
  },
  module: {
    rules: [
      {
        // 在package.json中eslintConfig --> airbnb
        test: /\.js$/,
        exclude: /node_modules/,
        // 优先执行
        enforce: 'pre',
        loader: 'eslint-loader',
        options: {
          fix: true
        }
      },
      {
        // 以下loader只会匹配一个
        // 注意：不能有两个配置处理同一种类型文件
        oneOf: [
          {
            test: /\.css$/,
            use: [...commonCssLoader]
          },
          {
            test: /\.less$/,
            use: [...commonCssLoader, 'less-loader']
          },
          /*
            正常来讲，一个文件只能被一个loader处理。
            当一个文件要被多个loader处理，那么一定要指定loader执行的先后顺序：
              先执行eslint 在执行babel
          */
          {
            test: /\.js$/,
            exclude: /node_modules/,
            loader: 'babel-loader',
            options: {
              presets: [
                [
                  '@babel/preset-env',
                  {
                    useBuiltIns: 'usage',
                    corejs: { version: 3 },
                    targets: {
                      chrome: '60',
                      firefox: '50'
                    }
                  }
                ]
              ],
              // 开启babel缓存
              // 第二次构建时，会读取之前的缓存
              cacheDirectory: true
            }
          },
          {
            test: /\.(jpg|png|gif)/,
            loader: 'url-loader',
            options: {
              limit: 8 * 1024,
              name: '[hash:10][ext]',
              outputPath: 'imgs',
              esModule: false
            }
          },
          {
            test: /\.html$/,
            loader: 'html-loader'
          },
          {
            exclude: /\.(js|css|less|html|jpg|png|gif)/,
            loader: 'file-loader',
            options: {
              outputPath: 'media'
            }
          }
        ]
      }
    ]
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: 'css/built.[contenthash:10].css'
    }),
    new OptimizeCssAssetsWebpackPlugin(),
    new HtmlWebpackPlugin({
      template: './src/index.html',
      minify: {
        collapseWhitespace: true,
        removeComments: true
      }
    })
  ],
  mode: 'production',
  devtool: 'source-map'
};
```


### 代码分割
配置多入口 最终会生成多个文件(和入口一致)

单页面应用 : 单入口
多页面应用 : 多入口
```js
const { resolve } = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  // 单入口
  // entry: './src/js/index.js',
  entry: {
    // 多入口：有一个入口，最终输出就有一个bundle
    index: './src/js/index.js',
    test: './src/js/test.js'
  },
  output: {
    // [name]：取文件名
    filename: 'js/[name].[contenthash:10].js',
    path: resolve(__dirname, 'build')
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: './src/index.html',
      minify: {
        collapseWhitespace: true,
        removeComments: true
      }
    })
  ],
  mode: 'production'
};
```


```js
const { resolve } = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  // 单入口
  // entry: './src/js/index.js',
  entry: {
    index: './src/js/index.js',
    test: './src/js/test.js'
  },
  output: {
    // [name]：取文件名
    filename: 'js/[name].[contenthash:10].js',
    path: resolve(__dirname, 'build')
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: './src/index.html',
      minify: {
        collapseWhitespace: true,
        removeComments: true
      }
    })
  ],
  /*
    1. 可以将node_modules中代码单独打包一个chunk最终输出
    2. 自动分析多入口chunk中，有没有公共的文件。如果有会打包成单独一个chunk
  */
  optimization: {
    splitChunks: {
      chunks: 'all'
    }
  },
  mode: 'production'
};
```



```js
/*
  通过js代码，让某个文件被单独打包成一个chunk
  import动态导入语法：能将某个文件单独打包
*/

// 设置import引入的文件的文件名/* webpackChunkName: '我想要的文件名' */
import(/* webpackChunkName: '我想要的文件名' */'./test')
  .then(({ mul, count }) => {
    // 文件加载成功~
    // eslint-disable-next-line
    console.log(mul(2, 5));
  })
  .catch(() => {
    // eslint-disable-next-line
    console.log('文件加载失败~');
  });
```

### 懒加载
```js
  // 懒加载~：当文件需要使用时才加载~
  // 预加载 prefetch：会在使用之前，提前加载js文件
  // 正常加载可以认为是并行加载（同一时间加载多个文件）
  // 预加载 prefetch：等其他资源加载完毕，浏览器空闲了，再偷偷加载资源
  import(/* webpackChunkName: 'test-lazy', webpackPrefetch: true */'./test')
    .then(({ mul }) => {
      console.log(mul(4, 5));
    });
```

### pwa
PWA: 渐进式网络开发应用程序(离线可访问)
ServiceWorker

`workbox --> workbox-webpack-plugin`
```js
const { resolve } = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCssAssetsWebpackPlugin = require('optimize-css-assets-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const WorkboxWebpackPlugin = require('workbox-webpack-plugin');

/*
  PWA: 渐进式网络开发应用程序(离线可访问)
    workbox --> workbox-webpack-plugin
*/

// 定义nodejs环境变量：决定使用browserslist的哪个环境
process.env.NODE_ENV = 'production';

// 复用loader
const commonCssLoader = [
  MiniCssExtractPlugin.loader,
  'css-loader',
  {
    // 还需要在package.json中定义browserslist
    loader: 'postcss-loader',
    options: {
      ident: 'postcss',
      plugins: () => [require('postcss-preset-env')()]
    }
  }
];

module.exports = {
  entry: './src/js/index.js',
  output: {
    filename: 'js/built.[contenthash:10].js',
    path: resolve(__dirname, 'build')
  },
  module: {
    rules: [
      {
        // 在package.json中eslintConfig --> airbnb
        test: /\.js$/,
        exclude: /node_modules/,
        // 优先执行
        enforce: 'pre',
        loader: 'eslint-loader',
        options: {
          fix: true
        }
      },
      {
        // 以下loader只会匹配一个
        // 注意：不能有两个配置处理同一种类型文件
        oneOf: [
          {
            test: /\.css$/,
            use: [...commonCssLoader]
          },
          {
            test: /\.less$/,
            use: [...commonCssLoader, 'less-loader']
          },
          /*
            正常来讲，一个文件只能被一个loader处理。
            当一个文件要被多个loader处理，那么一定要指定loader执行的先后顺序：
              先执行eslint 在执行babel
          */
          {
            test: /\.js$/,
            exclude: /node_modules/,
            loader: 'babel-loader',
            options: {
              presets: [
                [
                  '@babel/preset-env',
                  {
                    useBuiltIns: 'usage',
                    corejs: { version: 3 },
                    targets: {
                      chrome: '60',
                      firefox: '50'
                    }
                  }
                ]
              ],
              // 开启babel缓存
              // 第二次构建时，会读取之前的缓存
              cacheDirectory: true
            }
          },
          {
            test: /\.(jpg|png|gif)/,
            loader: 'url-loader',
            options: {
              limit: 8 * 1024,
              name: '[hash:10][ext]',
              outputPath: 'imgs',
              esModule: false
            }
          },
          {
            test: /\.html$/,
            loader: 'html-loader'
          },
          {
            exclude: /\.(js|css|less|html|jpg|png|gif)/,
            loader: 'file-loader',
            options: {
              outputPath: 'media'
            }
          }
        ]
      }
    ]
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: 'css/built.[contenthash:10].css'
    }),
    new OptimizeCssAssetsWebpackPlugin(),
    new HtmlWebpackPlugin({
      template: './src/index.html',
      minify: {
        collapseWhitespace: true,
        removeComments: true
      }
    }),
    new WorkboxWebpackPlugin.GenerateSW({
      /*
        1. 帮助serviceworker快速启动
        2. 删除旧的 serviceworker

        生成一个 serviceworker 配置文件~
      */
      clientsClaim: true,
      skipWaiting: true
    })
  ],
  mode: 'production',
  devtool: 'source-map'
};
```

```js
/*
  1. eslint不认识 window、navigator全局变量
    解决：需要修改package.json中eslintConfig配置
      "env": {
        "browser": true // 支持浏览器端全局变量
      }
   2. sw代码必须运行在服务器上
      --> nodejs
      -->
        npm i serve -g
        serve -s build 启动服务器，将build目录下所有资源作为静态资源暴露出去
*/
// 注册serviceWorker
// 处理兼容性问题
if ('serviceWorker' in navigator) {
  window.addEventListener('load', () => {
    navigator.serviceWorker
      .register('/service-worker.js')
      .then(() => {
        console.log('sw注册成功了~');
      })
      .catch(() => {
        console.log('sw注册失败了~');
      });
  });
}
```

### 多进程打包
```js
// loader
{
  test: /\.js$/,
  exclude: /node_modules/,
  use: [
    /*
      开启多进程打包。
      进程启动大概为600ms，进程通信也有开销。
      只有工作消耗时间比较长，才需要多进程打包
    */
    {
      loader: 'thread-loader',
      options: {
        workers: 2 // 进程2个
      }
    },
    {
      loader: 'babel-loader',
      options: {
        presets: [
          [
            '@babel/preset-env',
            {
              useBuiltIns: 'usage',
              corejs: { version: 3 },
              targets: {
                chrome: '60',
                firefox: '50'
              }
            }
          ]
        ],
        // 开启babel缓存
        // 第二次构建时，会读取之前的缓存
        cacheDirectory: true
      }
    }
  ]
},
```

### externals
拒绝某些包被打包进来
```js
const { resolve } = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  entry: './src/js/index.js',
  output: {
    filename: 'js/built.js',
    path: resolve(__dirname, 'build')
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: './src/index.html'
    })
  ],
  mode: 'production',
  externals: {
    // 拒绝jQuery被打包进来
    jquery: 'jQuery'
  }
};
```


### dll
需要重新创建一个webpack.dll.js

```js
/*
  使用dll技术，对某些库（第三方库：jquery、react、vue...）进行单独打包
    当你运行 webpack 时，默认查找 webpack.config.js 配置文件
    需求：需要运行 webpack.dll.js 文件
      --> webpack --config webpack.dll.js
*/

const { resolve } = require('path');
const webpack = require('webpack');

module.exports = {
  entry: {
    // 最终打包生成的[name] --> jquery
    // ['jquery'] --> 要打包的库是jquery
    jquery: ['jquery'],
  },
  output: {
    filename: '[name].js',
    path: resolve(__dirname, 'dll'),
    library: '[name]_[hash]' // 打包的库里面向外暴露出去的内容叫什么名字
  },
  plugins: [
    // 打包生成一个 manifest.json --> 提供和jquery映射
    new webpack.DllPlugin({
      name: '[name]_[hash]', // 映射库的暴露的内容名称
      path: resolve(__dirname, 'dll/manifest.json') // 输出文件路径
    })
  ],
  mode: 'production'
};
```
# proxy
```js
devserver: {
  proxy: {
    // 进行地址印射 前端解决跨域问题
    "/api" : {
      target: "http://localhost:8888",
      // 路径重写 因为axios发送请求的时候 是携带前缀的
      pathRewrite: {
        "^/api": ""
      }
    }
  }
}
```

# resolve 设置模块如何被解析
resolve帮助从import/require语句中解析合适的加载模块
webpack使用enhanced-resolve来解析文件路径



-------------------------------------------------------------------------------------------------------------------



# loader
## 概念

- pre： 前置 loader
- normal： 普通 loader
- inline： 内联 loader
- post： 后置 loader

执行顺序(优先级大小) pre > normal > inline > post
通过enforce字段指定优先级
```js
// 此时loader执行顺序：loader1 - loader2 - loader3
module: {
  rules: [
    {
      enforce: "pre",
      test: /\.js$/,
      loader: "loader1",
    },
    {
      // 没有enforce就是normal
      test: /\.js$/,
      loader: "loader2",
    },
    {
      enforce: "post",
      test: /\.js$/,
      loader: "loader3",
    },
  ],
},
```

相同优先级的loader执行顺序是从后到前

webpack 会先从左到右执行 loader 链中的每个 loader 上的 pitch 方法（如果有），然后再从右到左执行 loader 链中的每个 loader 上的普通 loader 方法。

loader执行流程

在这个过程中如果任何 pitch 有非undefined返回值，则 loader 链被阻断。webpack 会跳过后面所有的的 pitch 和 loader，直接进入上一个 loader 。

```js
// normal loader
function aLoader(content, sourceMap, meta) {
  console.log("开始执行aLoader Normal Loader");
  content += "aLoader]";
  return `module.exports = '${content}'`;
}

// pitch方法
aLoader.pitch = function (remainingRequest, precedingRequest, data) {
  console.log("开始执行aLoader Pitching Loader");
  console.log(remainingRequest, precedingRequest, data)
  return 'stop!'
};

module.exports = aLoader
```


## 自定义一个loader

### 在所有文件前面自动添加作者信息
```js
const schema = require("./schema.json");

module.exports = function (content) {
  // 获取loader的options，同时对options内容进行校验
  // schema是options的校验规则（符合 JSON schema 规则）
  const options = this.getOptions(schema);

  const prefix = `
    /*
    * Author: ${options.author}
    */
  `;

  return `${prefix} \n\n ${content}`;
};
```
schema.json文件
```json
{
  "type": "object",
  "properties": {
    "author": {
      "type": "string"
    }
  },
  "additionalProperties": false
}
```

### 将es6+代码转换为es5
这个webpack loader需要借助babel的能力

安装依赖
`pnpm i @babel/core @babel/preset-env -D`

```js
const schema = require("./schema.json");
const babel = require("@babel/core");

module.exports = function (content) {
  const options = this.getOptions(schema);
  // 使用异步loader
  const callback = this.async();
  // 使用babel对js代码进行转译
  babel.transform(content, options, (err, result) => {
    callback(err, result.code);
  });
};
```
schema.json文件
```json
{
  "type": "object",
  "properties": {
    "presets": {
      "type": "array"
    }
  },
  "additionalProperties": true
}
```




---------------------------------------------------------------------------------------------------------

# plugin

webpack 在运行过程中会广播事件，插件只需要监听它所关心的事件，就能加入到这条生产线中，去改变生产线的运作。


## 自定义一个插件

插件基本上是class, 需要通过new执行

```js
class TestPlugin {
  constructor() {
    console.log("TestPlugin constructor()");
  }
  // 1. webpack读取配置时，new TestPlugin() ，会执行插件 constructor 方法
  // 2. webpack创建 compiler 对象
  // 3. 遍历所有插件，调用插件的 apply 方法
  apply(compiler) {
    console.log("TestPlugin apply()");

    // 从文档可知, compile hook 是 SyncHook, 也就是同步钩子, 只能用tap注册
    compiler.hooks.compile.tap("TestPlugin", (compilationParams) => {
      console.log("compiler.compile()");
    });

    // 从文档可知, make 是 AsyncParallelHook, 也就是异步并行钩子, 特点就是异步任务同时执行
    // 可以使用 tap、tapAsync、tapPromise 注册。
    // 如果使用tap注册的话，进行异步操作是不会等待异步操作执行完成的。
    compiler.hooks.make.tap("TestPlugin", (compilation) => {
      setTimeout(() => {
        console.log("compiler.make() 111");
      }, 2000);
    });

    // 使用tapAsync、tapPromise注册，进行异步操作会等异步操作做完再继续往下执行
    compiler.hooks.make.tapAsync("TestPlugin", (compilation, callback) => {
      setTimeout(() => {
        console.log("compiler.make() 222");
        // 必须调用
        callback();
      }, 1000);
    });

    compiler.hooks.make.tapPromise("TestPlugin", (compilation) => {
      console.log("compiler.make() 333");
      // 必须返回promise
      return new Promise((resolve) => {
        resolve();
      });
    });

    // 从文档可知, emit 是 AsyncSeriesHook, 也就是异步串行钩子，特点就是异步任务顺序执行
    compiler.hooks.emit.tapAsync("TestPlugin", (compilation, callback) => {
      setTimeout(() => {
        console.log("compiler.emit() 111");
        callback();
      }, 3000);
    });

    compiler.hooks.emit.tapAsync("TestPlugin", (compilation, callback) => {
      setTimeout(() => {
        console.log("compiler.emit() 222");
        callback();
      }, 2000);
    });

    compiler.hooks.emit.tapAsync("TestPlugin", (compilation, callback) => {
      setTimeout(() => {
        console.log("compiler.emit() 333");
        callback();
      }, 1000);
    });
  }
}

module.exports = TestPlugin;
```



# 环境治理
```bash
.
└── config
  ├── webpack.common.js
  ├── webpack.development.js
  ├── webpack.testing.js
  └── webpack.production.js

```
使用webpack-merge合并通用配置

const { merge } = require("webpack-merge");
不同环境运行不同命令
npx webpack --config webpack.development.js