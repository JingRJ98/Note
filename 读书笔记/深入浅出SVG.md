## svg结构

根元素 <svg> ：SVG 文件的根元素是 <svg>，用于包裹所有的 SVG 内容，有点类似于 HTML 中的 <html> 元素。它可以包含用于设置 SVG 画布属性的命名空间和其他属性。
XMLNS（XML命名空间）是XML的一项功能，用于在XML文档中创建命名空间以避免元素和属性名称的冲突。

在SVG中，xmlns属性用于定义SVG文档中使用的命名空间。命名空间是一种机制，允许不同来源的XML元素和属性共存于同一文档中，而不会发生冲突。

例如，在SVG中，命名空间可以指定SVG元素及其属性的标准。xmlns="http://www.w3.org/2000/svg"定义了SVG的默认命名空间。这意味着在SVG文档中，任何没有指定命名空间的元素或属性都被认为属于SVG命名空间。

图形元素：SVG 支持多种图形元素，例如 <rect>（矩形）、 <circle>（圆形）、 <line>（直线）、 <path>（路径）等。这些元素用于创建各种形状，并可以通过设置属性来调整它们的大小、位置、颜色等。

文本元素 <text> ：使用 <text> 元素可以在 SVG 中添加文本。可以设置文本的位置、字体、大小等属性。

分组元素 <g> ：<g> 元素用于将多个图形元素组合在一起，形成一个组。这对于对组内的元素进行整体变换或样式设置很有用。

样式元素 <style> ：<style> 元素允许在 SVG 文件中嵌入样式，类似于 HTML 中的样式表。这使得可以使用 CSS 来定义 SVG 元素的外观。

属性：每个图形元素可以有一些属性，用于定义其外观和行为。例如，width 和 height 是矩形元素的属性，cx、cy 和 r 是圆形元素的属性。

其他元素和属性：除了上述元素外，SVG 还支持许多其他属性和元素，如 <line>、<polygon>、<ellipse>等。可以使用这些属性和元素创建更复杂的图形。

## 借助css变量动态修改颜色

```js
const svgElement = document.querySelector('.codepen');

svgElement.addEventListener('mouseenter', etv => {
    // 设置style里面的自定义变量 '--fill'
    svgElement.style.setProperty(`--fill`, `orange`)
});

svgElement.addEventListener('mouseleave', etv => {
    svgElement.style.setProperty(`--fill`, `lime`)
});
```

--fill实际是css中的变量 (--开头的都是css变量)

```css
.codepen {
    --fill: lime;

    path {
        /* 读取--fill变量, --fill变量会由js修改 */
        fill: var(--fill);
        transition: fill .2s ease-in-out;
    }
}
```

## 命名空间

如果svg独立以xml形式存在, 必须指定xmlns属性, 即命名空间, 确保所有svg元素都可以被正确识别
xmlns="http://www.w3.org/2000/svg"

如果svg元素嵌入在html中, 不需要指定命名空间, html解析器会自动为svg添加命名空间

但是如果html元素本身有命名空间 还是需要为svg添加命名空间

xmlns:xlink是在为xlink声明命名空间, 以便在svg元素中添加超链接

```html
<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="100" height="100">
    <a xlink:href="https://www.example.com" target="_blank">
        <circle cx="50" cy="50" r="40" fill="red" />
    </a>
</svg>
```

## 层叠顺序
`代码后面的会覆盖在前面的元素之上 `

在 SVG 中，图层是通过元素的顺序来定义的。
位于后面（出现在源码后面）的元素在图层中（Z 轴层级）会显示在较前面（出现在源码前面）的元素之上。
另外，到目前为止，我们是无法通过 CSS 的 z-index 属性来调整 SVG 元素的堆叠顺序（据说在 SVG 2.0 中将会得到支持）。


# 第六章 svg-坐标
svg中元素的参考坐标原点是 svg画布 的左上角
