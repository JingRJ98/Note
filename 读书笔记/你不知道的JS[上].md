# 第一部分
# 第一章 作用域
LHS查询
赋值操作的左侧,赋值操作的目标是谁

RHS查询
赋值操作的右侧,值究竟是什么

注意不一定是左侧和右侧
```js
console.log(a) // 是一个RHS查询,它要查询a究竟是什么
```

# 第二章 词法作用域
词法作用域意味着作用域是`书写代码时`函数声明的位置决定的
作用域链是在`函数定义`的时候创建的

例1-字节原题
```js
var value = 1;

function foo() {
  console.log(value);
}

function bar() {
  var value = 2;
  foo();
}

bar(); 
// 1

//////////////////////////
// 效果和fun从另一个文件中引入的一样
// fun中用到的a在fun写的时候就确定了
```

例2
```js
var scope = "global scope";
function checkscope(){
    var scope = "local scope";
    function f(){
        return scope;
    }
    return f();
}
checkscope(); // local scope


//////////////////////

var scope = "global scope";
function checkscope(){
    var scope = "local scope";
    function f(){
        return scope;
    }
    return f;
}
checkscope()();
// 两段代码都会打印：local scope。因为JavaScript采用的是词法作用域，函数的作用域基于函数创建的位置。
// 引用《JavaScript权威指南》的回答就是：
// JavaScript 函数的执行用到了作用域链，这个作用域链是在函数定义的时候创建的。嵌套的函数 f() 定义在这个作用域链里，其中的变量 scope 一定是局部变量，不管何时何地执行函数 f()，这种绑定在执行 f() 时依然有效。

```

## 欺骗词法作用域
eval
with

```js
function fun(obj) {
  with(obj) {
    a = 2
  }
}

const o1 = {
  a: 1
}

const o2 = {
  b: 1
}

fun(o1)
console.log(o1.a) // 2

fun(o2)
console.log(o2.a)// undefined
console.log(a)// 1
// a被泄漏到全局作用域了
```

with不推荐使用 因为会造成内存泄漏 严格模式已经禁止with

# 第三章 函数作用域和块作用域
函数作用域包含函数大括号和`函数的形参`, 外部不可获取

# 第四章 提升
函数比变量更优先被提升

# 第五章 闭包
函数及其对外部词法环境的引用捆绑

------


# 第二部分 this

# 第一章 this
+ 严格模式下的函数的默认this无法指向window对象, 而是指向undifined

+ 谁调用函数 谁是函数的this
例1
```js
function foo() {
  console.log(this.a);
}
const obj1 = {
  a: 1,
  foo
}

const obj2 = {
  a: 2,
  obj1
}

obj2.obj1.foo();// 1
```

例2
```js
function foo() {
    console.log(this.a);
}
const obj1 = {
    a: 1,
    foo
}
var a = 999
const fun = obj1.foo

// 此时调用函数fun的是window(非严格模式下)
fun()// 999
```

例3
```js
function doFn(cb) {
  cb()
}

function foo() {
  console.log(this.a);
}

const obj = {
  a: 1,
  foo
}

var a = 2

// 参数传递也是一种赋值, 和例2一样,实际上this指向调用doFn的window(非严格模式下)
doFn(obj.foo)
```

+ call apply修改this绑定
这种绑定只能执行一次

+ new关键字修改this绑定
new操作


+ call/apply优先级比默认this绑定优先级更高

+ new和call/apply无法一起使用, 但是用了new的更优先

+ 如果call/apply/bind传入的是null或者undefined,那么会使用默认this指向(非严格模式下指向window)


