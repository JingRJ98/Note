# 第一部分 类型和语法


# 第一章 类型

+ undefined
```js
var a 
typeof a // undefined
typeof b // undefined

// 但是这里的b直接用会报错 b is not defined
```

# 第二章 值
+ 使用 delete 运算符可以将单元从数组中删除，但是请注意，单元删除后，数组的 length 属性并不会发生变化

+ 如果字符串键值能够被强制类型转换为十进制数字的话，它 就会被当作数字索引来处理。
```js
const a = []
a['2'] = 0
console.log(a); // [ , , 0 ]
console.log(a.length);// 3
```


---------



# 第二部分 异步和性能
**待定**

