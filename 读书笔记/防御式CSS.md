# 防御式CSS
> 作者: 大漠
> 掘金小册
> https://juejin.cn/book/7199571709102391328?enter_from=course_center&utm_source=course_center

# 第四章 最小内容尺寸
- css中设置了flex/grid布局, 子元素设置文字的换行也不生效
因为 Flex 项目和 Grid 项目在收缩的时候，最小尺寸不能小于其最小内容尺寸 
最简单的解决办法就是设置子元素 min-width: 0
另一个解决办法是overflow: auto或者scroll


简单概括
子元素设置flex: 1的时候,最好显式加上min-height: 0;

# 第五章 布局中滚动失效
- Flex 容器上设置 justify-content: end 致使容器水平滚动失效 
在不考虑书写模式（或阅读模式）之下，水平方向的内容是向容器右侧溢出，垂直方向的内容是向容器底部溢出。因此，在设计滚动条的时候就约定了，只有容器下方或右侧内容有多余，才需要滚动 。

如果这个滚动容器刚好是一个 Flex 容器，并且在 Flex 容器上设置了 justify-content: flex-end
就会导致 Flex 容器的内容向左或向上溢出。这样就违背了滚动容器滚动条的设计规则，自然就无法触发容器的滚动条出现。
简单地说，justify-content 取值 flex-end 会让内容反向溢出，致使滚动条失效 。

解决方案: 给第一个子元素设置 margin-left （或 margin-inline-start）的值为 auto 即可。
- flex容器设置align-items: center的时候
如果容器宽度比较小 子元素宽度比较大, 会出现左边有一部分在容器外面,并且滚动条也没办法向左滚动

解决方案: 给子元素设置margin的值为auto

# 第七章 元素之间的间距
- gap
只要元素是一个 Flex 容器或网格容器，它们的子元素（Flex 项目或网格项目）之间的间距都可以使用 gap 属性。不过它有一个局限性，就是所有元素之间的间距需要是相等的。如果元素之间的间距不相等，则无法使用 gap 来控制。

- 使用兄弟选择器可以解决的问题
比如有两个并排的按钮, 只有两个按钮的时候,第二个按钮才会跟左边的按钮有间距,只有一个按钮的时候不要显示和左边边缘的间距
可以使用
```css
.button + .button {
    margin-left: 1rem;
}
```
如果一个按钮紧挨着另一个按钮，以防万一，给第二个按钮加一个 margin-left

- 有时候可以使用borer代替padding
当子元素滚动的时候也期望出现padding这个间距的时候,可以使用透明的border来实现

# 第八章 粘性定位失效
- position: sticky 在下面几种情形会失效：
1. 粘性定位元素（即显式设置 position: sticky 元素）的父元素只要是它的祖先元素）显式设置了 overflow 属性的值为 hidden 、scroll 或 auto；
MDN: 一个粘性定位元素会“固定”在离它最近的一个拥有“滚动机制”的祖先上（当该祖先的 overflow 是 hidden、scroll、auto 或 overlay 时），即便这个祖先不是最近的真实可滚动祖先。这有效地抑制了任何 “sticky” 行为
比如，我们在 body 元素上设置了一个 overflow-x 的值为 hidden ，此时，body 后代设置 position: sticky 的元素将会失效。
解决: 需要在溢出容器上设置一个高度
2. 粘性定位元素没有指定一个阈值；
3. 粘附容器（即粘性定位元素的父元素）的高度（height）或其高度的计算值和粘性定位元素高度一样。
比如父容器设置了flex, 子元素 algin-items 的默认值是 stretch ，它们拉伸 Flex 项目，使子元素等高，并且也和Flex容器高度相等,
此时子元素的粘滞定位就失效了
解决:  容器align-items: flex-start; 或子元素align-self: flex-start; 或 margin-bottom: auto; 

# 第九章 层叠上下文

如果兄弟元素之间设了z-index
其中一个兄弟的子元素也设置了z-index, 可能会出现被覆盖的情况
即使子元素的index再高, 也无法高过组件的兄弟元素,以为不在同一个层叠上下文中
解决办法是删掉祖先元素的z-index

z-index 的大多数问题，都可以通过以下两个指导原则来解决:

检查元素是否创建层叠上下文和按正确的顺序设置了它们的 z-index ；
确保没有父元素限制其子元素的 z-index 级别。祖先元素一旦设置了z-index, 那么辈分就会按祖先的排, 也可能有一些css属性隐式创建了层叠上下文

# 第十章 border-radius
border-radius的属性值可以设置为两个数字用/隔开
表示x轴和y轴上的圆角大小
如果没有/就表示是一个圆形切出来的

border-radius 使用 % 值时，它的相对值是需要分开来计算的，其中 x 轴的 % 值相对于元素的 width 值计算；y 轴的 % 值相对于元素的 height 值计

- 嵌套元素的圆角
当父子元素之间有一个边框或者padding的差距 且两个元素都要设置圆角的时候
不要把两个border-radius的值设为一致,我个人理解这里是切圆的半径大小, 因此两个半径之间应该存在差值
这个差值就是padding或者border-width 的大小
两个半径不同的时候才会形成同心圆

- 如果四个border-radius的属性值设的差距过大
浏览器为了计算出正确的图形, 会根据4个border-radius和宽高来计算一个比例, 然后绘制圆角
简单的说就是  切圆角的那个圆太大了, 浏览器尽可能不让他们重叠, 此时会出现一些意外的bug
所以不要设置几个差距过大的border-radius值 比如`border-radius: 100px 999999px 100px  9999999px`

- resize: both;
支持拖动修改div元素的大小

# 第十一章 阴影
- 对于在图片或视频上添加一个颜色透明层，让图片或视频上的文本更具阅读性，更好的解决方案是借助 CSS 的伪元素，比如 ::before 或 ::after。
- img和video元素无法直接使用内阴影, 需要在外面加一层div 内阴影加在div上

- 子元素的阴影会被父元素上的overflow-hidden截掉
解决办法: 阴影直接加在容器上面

- 在使用阴影的时候，应该尽可能少用模糊半径，即使要用，该值也应该尽可能的小。

- 我们要尽可能在动画中避免改变阴影的值，特别是少用 box-shadow。可以考虑在动画中使用 filter的 drop-shadow()，用它来做动画更有表现力，渲染性能也相对会更好一点。注意，这也只是一个相对值。

- 如果在动效中使用阴影，应该尽可能借助伪元素来完成，这样除了性能更好之外，动效也更细腻。

# 第十三章 防止图片变形
- img 元素中显式设置了 width 和 hegiht 的值，但只在 CSS 中显式设置了其中一个值的话，有可能会对图像造成扭曲、拉伸等现象，
解决办法: 在 CSS 中显式设置 height:auto。

# 第十五章 渐变
- 一种背景设计
首先设置明亮的渐变色
如
```css
body {
    background-image: linear-gradient(to bottom, #f90, #09f);
}
```

然后在上面盖一层半透明的黑色蒙版, 达到一种发亮的效果
```html
<style>
    .modal__overlayer {
      position: fixed;
      top: 0;
      right: 0;
      bottom: 0;
      left: 0;
      /* 加一层半透明的黑色蒙版 达到一种发亮的效果 */
      background-color: rgba(0, 0, 0, 0.8);
      z-index: 2;
      width: 100vw;
      min-height: 100vh;
    }
</style>
<div class="modal__overlayer"></div>
```

- 一种border渐变
设置border有一定的宽度 且颜色为透明 设置backgroud-image为多个值的渐变色
配合background-origin, background-clip
```css
div {
    width: 550px;
    min-height: 697px;
    border-radius: 34px;
    padding: 50px;
    border: 10px solid transparent;
    /* 先指定的图像会在之后指定的图像上面绘制。因此指定的第一个图像“最接近用户” */
    background-image: linear-gradient(to bottom, #fff4f8 1%, #ffd5dc 77%),
    linear-gradient(to bottom, #ff0a53, #ff7860);
    background-repeat: no-repeat;
    /* background-origin 规定了指定背景图片background-image 属性的原点位置的背景相对区域 */
    background-origin: border-box;
    /* background-image第一个渐变的颜色裁剪为padding内部区域, 第二个渐变颜色 为border-box内部的区域 */
    background-clip: padding-box, border-box;
    position: relative;
    z-index: 1;
    padding: 20px;
}
```

transfrom动态效果
```html

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
  <style>
    * {
      margin: 0;
      padding: 0;
      box-sizing: border-box;
    }

    body {
      width: 100vw;
      min-height: 100vh;
      color: #fff;
      display: grid;
      place-content: center;
      padding: 1rem;
    }

    .over {
      position: fixed;
      top: 0;
      right: 0;
      bottom: 0;
      left: 0;
      /* 加一层半透明的黑色蒙版 达到一种发亮的效果 */
      background-color: rgba(0, 0, 0, 0.8);
      z-index: 2;
      width: 100vw;
      min-height: 100vh;
    }

    :root {
      --size: 1;
      --background: #000;
      --color1: #d25778;
      --color2: #ec585c;
      --color3: #e7d155;
      --color4: #56a8c6;
    }

    body {
      color: white;
      background-image: linear-gradient(to bottom, #f30, #09f);
    }

    .ticket-visual_visual {
      z-index: 4;
      width: 650px;
      height: 320px;
      margin: 100px auto;
      position: relative;
      transition: all 300ms cubic-bezier(0.03, 0.98, 0.53, 0.99) 0s;
      border-radius: 10px;
      /* 先指定的图像会在之后指定的图像上面绘制。因此指定的第一个图像“最接近用户” */
      background-image: linear-gradient(to bottom, #000, #000), linear-gradient(to right,
          var(--color1),
          var(--color2),
          var(--color3),
          var(--color4));
      background-repeat: no-repeat;
      /* background-origin 规定了指定背景图片background-image 属性的原点位置的背景相对区域 */
      background-origin: border-box;
      /* background-image第一个渐变的颜色裁剪为padding内部区域, 第二个渐变颜色 为border-box内部的区域 */
      background-clip: content-box, border-box;
      padding: 8px;
    }

    .ticket-visual_profile {
      padding: calc(39px * var(--size)) calc(155px * var(--size)) calc(39px * var(--size)) calc(58px * var(--size));
    }

    .ticket-profile_text {
      margin: 0;
    }

    .ticket-profile_profile {
      display: flex;
      flex-direction: row;
    }

    .ticket-event {
      margin-top: 25px;
      margin-left: -10px;
    }

    .ticket-profile_name {
      font-size: 32px;
      margin: 10px 0 5px 20px;
      font-weight: 700;
    }
  </style>
</head>

<body>
  <div class="ticket-visual_visual" id="ticket">
    <div class="ticket-visual_profile">
      <div class="ticket-profile_profile">
        <div class="ticket-profile_text">
          <p class="ticket-profile_name">jrjjrjjrjjrjjrjjrjjrjjrjjrjjrjjrjrjrjrjrjjrjrjjrj</p>
          <p class="ticket-profile_name">jrjjrjjrjjrjjrjjrjjrjjrjjrjjrjjrjrjrjrjrjjrjrjjrj</p>
          <p class="ticket-profile_name">jrjjrjjrjjrjjrjjrjjrjjrjjrjjrjjrjrjrjrjrjjrjrjjrj</p>
          <p class="ticket-profile_name">jrjjrjjrjjrjjrjjrjjrjjrjjrjjrjjrjrjrjrjrjjrjrjjrj</p>
        </div>
      </div>
    </div>
  </div>
  <div class="over"></div>

  <script>
    const ticketElm = document.getElementById('ticket');
    const { x, y, width, height } = ticketElm.getBoundingClientRect();
    const centerPoint = { x: x + width / 2, y: y + height / 2 };

    window.addEventListener('mousemove', (e) => {
      const degreeX = (e.clientY - centerPoint.y) * 0.015;
      const degreeY = (e.clientX - centerPoint.x) * -0.015;

      ticketElm.style.transform = `perspective(1000px) rotateX(${degreeX}deg) rotateY(${degreeY}deg)`;
    });
  </script>
</body>

</html>
```

# 第十九章 滚动
- 当一个长页面出现一个可以滚动的modal时, 可能会出现一种情况
modal中的内容滚动到底时, 开始滚动modal背后的长页面, 即滚动穿透

为了解决这个问题, 可以在modal出现的时候给body添加一个类名, 比如.modal-open
这样父页面在modal出现的时候就无法再滚动了
```css
.modal--open {
  position: fixed;
  overflow: hidden;
}
```

也可以使用新的属性overscroll-behavior-y
该属性提供了三个可选值，auto 、contain 和 none ，其中 auto 是其初始值，允许滚动穿透。
```css
.modal--content {
  overflow-y: auto;
  overscroll-behavior-y: contain;
}
```

- 如果要移除滚动至顶部或底部的默认滚动特效，需要在 html 或 body 元素上设置 overscroll-behavior 的值为 none 。这也是禁用原生下拉刷新最有效的方案，而且它对于我们定制一个下拉刷新是非常有利的。否则就会出现两个下拉刷新的效果（一个是原生的，一个是定制的）。
参考原章节
https://juejin.cn/book/7199571709102391328/section/7199845609447096358?enter_from=course_center&utm_source=course_center

# 第二十六章 css @规则
- @media查询的是视口
- @container查询容器大小来设置对应的css
需要配合container-type属性
```css
.card__container {
    container-type: inline-size;
}

@container (inline-size > 650px) {}
@container (inline-size > 820px) {}
```

- 伪类选择器 :has
如果 figure 有子元素 img ，那么 figure 就指定一个 padding 值为 0 
```css
figure:has(> img) {
    padding: 0;
}
```

```css
.card:has(img) {
    /* 选择有 img 作为后代元素的 .card 元素 */  
}  

.card:has(> img) {
    /* 选择有 img 作为子元素的 .card 元素 */
}  

.card:has(.card__thumb) {
    /* 选择有类名为 .card__thumb 作为后代元素的 .card 元素 */
}  

.card:has(h2 + p) {
    /* 选择有一个 h2 元素后紧跟一个 p 元素的 .card 元素 */
}  

.card:has(h2 ~ p){
    /* 选择有一个 h2 元素，且后面有 p 元素的 .card 元素 */
} 
```

和其他伪类选择器组合
```css
/* 匹配不包含任何标题元素的 section 元素 */ 
section:not(:has(h1, h2, h3, h4, h5, h6)){} 
 
/* 匹配任何包含非标题元素的 section 元素 */ 
section:has(:not(h1, h2, h3, h4, h5, h6)){} 
```

