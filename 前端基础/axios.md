# HTTP交互过程

客户端向服务器发请求
    请求行  
    请求头  
    请求体

服务端返回响应
    状态行
    响应头
    实体内容

1、前端向后端发送请求（请求报文）

2、后台服务器收到请求后，调度服务器处理应用请求，想服务器端返回HTTP响应（响应报文）

3、浏览器收到响应，解析响应进行渲染页面

# API 分类
REST API  - restful
进行CRUD哪个操作由请求方式来决定
同一个请求路径可以进行多个操作
请求方式会用到GET、POST、PUT、DELETE操作


非REST API - restless
请求方式不决定请求的CRUD操作
一个请求只能对应一个操作
一般只有GET/POST


# 分清401和403
认证和授权
+ 用户是否未提供身份验证凭据？认证是否还有效？这种类型的错误一般是未认证（401 Unauthorized）
+ 用户经过了正常的身份验证，但没有访问资源所需的权限？这种一般是未授权（403 Forbidden）


## 使用json-server 搭建rest API
json-server是用来快速搭建REST API的工具

下载安装  `sudo yarn add -g json-server`

创建一个db.json

监视db.json`json-server --watch db.json`


# REST API(测试get、post、put、delete操作)

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <!-- 引入axios库 -->
    <script src="https://cdn.bootcdn.net/ajax/libs/axios/0.21.1/axios.min.js"></script>
</head>
<body>
  <div>
    <button onclick="testGet()">GET请求</button>
    <button onclick="testPost()">POST请求</button>
    <button onclick="testPut()">PUT请求</button>
    <button onclick="testDelete()">DELETE请求</button>
  </div>

  <script>
    const testGet = () => {
      // GET获取整个posts数据
      // axios.get('http://localhost:3000/posts')

      // parames参数查询  获取posts数组里面id为2的元素（对象）
      // axios.get('http://localhost:3000/posts/2')

      // query参数查询 遍历posts数组，返回符合要求的元素组成的新数组
      axios.get('http://localhost:3000/posts?id=1')
      .then( response => { console.log('GET请求获取到的：',response.data); } )
    }

    const testPost = () => {
      axios.post('http://localhost:3000/posts',{"title":"ttt2","author":"Isaac"})
      .then( response => { console.log('POST请求获取到的：',response.data); } )
    }

    const testPut = () => {
      axios.put('http://localhost:3000/posts/3',{"title":"cc","author":"Isaac"})
      .then( response => { console.log('Put请求获取到的：',response.data); } )
    }

    const testDelete = () => {
      // 删除id为3的数据
      axios.delete('http://localhost:3000/posts/7')
      .then( response => { console.log('Delete请求获取到的：',response.data); } )
    }
  </script>
</body>
</html>
```


# XMLHttpRequest(XHR)

XHR对象可以与服务器进行交互，可以从url处获取数据，而无需让整个页面刷新

允许网页在不影响用户操作的情况下更新页面的**局部**内容


# 区别一般的http请求与ajax请求
1、ajax请求是一种特别的http请求

2、对服务器端来说，没有任何区别，区别在浏览器端

3、浏览器端发送请求，只有XHR或者fetch发出的才是ajax请求，其他的请求都是非ajax请求

4、浏览器收到响应
    a.一般请求：浏览器一般会直接显示响应体数据，也就是我们常说的刷新/跳转页面
    b.ajax请求。浏览器不会对页面进行任何更新操作，只是调用监视的回调函数并传入响应相关数据

# API

XMLHttpRequest() 创建XHR对象的构造函数

status 响应状态码值，比如200  404

statusText 响应状态文本

readyState 标识请求状态的只读属性
    0 初始
    1 open()之后
    2 send()之后
    3 请求中
    4 请求完成

onreadystatechange 监视的回调函数，绑定readyState改变的监听

responseType 指定响应数据类型，如果是json 得到响应之后自动解析响应体数据

response 响应体数据，数据类型取决于responseType的指定

timeout 指定请求超时时间  默认为0 没有任何限制

ontimeout 绑定超时的监听

onerror 绑定请求网络错误的监听

open() 初始化一个请求  参数为(method,url,[async])

sned(data) 发送一个请求

abort() 中断请求

getResponseHeader(name)   获取指定名称的响应头值

getAllResponseHeaders()  获取所有响应头组成的字符串

setRequestHeader(name,value)  设置请求头

# XHR的ajax封装（简单的axios）

指定query参数`?id=2`

data既可以使用对象形式，也可以使用字符串形式


# axios的理解和使用
前端最流行的ajax库

react/vue都推荐使用axios发送ajax请求

文档：`https://github.com/axios/axios`

## 特点

基本的promise异步ajax请求库

浏览器端和node端都可以使用

支持请求/响应拦截器

支持请求取消

请求/响应数据交换

批量发送多个请求


## 常用语法

axios(url [, config])

```js
// Send a POST request
axios({
  method: 'post',
  url: '/user/12345',
  data: {
    firstName: 'Fred',
    lastName: 'Flintstone'
  }
});
```
axios(config): 通用/最本质的发任意类型请求的方式

axios(url[, config]): 可以只指定 url 发 get 请求

axios.request(config): 等同于 axios(config) axios.get(url[, config]): 发 get 请求

axios.delete(url[, config]): 发 delete 请求

axios.post(url[, data, config]): 发 post 请求
 
axios.put(url[, data, config]):  发 put 请求

axios.defaults.xxx: 请求的默认全局配置

axios.interceptors.request.use(): 添加请求拦截器

axios.interceptors.response.use(): 添加响应拦截器

axios.create([config]): 创建一个新的 axios(它没有下面的功能)

axios.Cancel(): 用于创建取消请求的错误对象

axios.CancelToken(): 用于创建取消请求的 token 对象

axios.isCancel(): 是否是一个取消请求的错误

axios.all(promises): 用于批量执行多个异步请求

axios.spread(): 用来指定接收所有成功数据的回调函数的方法


## 基本使用
*注意：不要使用`open with liver server`打开，请求发送一次就会消失！！！*
*使用`open in default browser`打开*
```html
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>axios基本使用</title>
    <link crossorigin="anonymous" href="https://cdn.bootcss.com/twitter-bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.bootcdn.net/ajax/libs/axios/0.21.1/axios.min.js"></script>
</head>
<body>
  <div class="container">
    <h2 class="page-header">基本使用</h2>
    <button class="btn btn-primary"> 发送GET请求 </button>
    <button class="btn btn-warning" > 发送POST请求 </button>
    <button class="btn btn-success"> 发送 PUT 请求 </button>
    <button class="btn btn-danger"> 发送 DELETE 请求 </button>
  </div>
    <script>
      //获取按钮
      const btns = document.querySelectorAll('button');

      //第一个
      btns[0].onclick = function(){
        //发送 AJAX 请求
        axios({
          //请求类型
          method: 'GET',
          //URL
          // url: 'http://localhost:3000/posts/2',// 返回id为2的对象
          // url: 'http://localhost:3000/posts?id=1',// 返回数组包含id为1的对象
          url: 'http://localhost:3000/posts',// 返回数组包含所有对象
        }).then(response => {
          console.log(response);
          console.log(response.data);
        });
      }

      //添加一篇新的文章
      btns[1].onclick = function(){
        //发送 AJAX 请求
        axios({
          //请求类型
          method: 'POST',
          //URL
          url: 'http://localhost:3000/posts',
          //设置请求体
          data: {
            title: "今天天气不错, 还挺风和日丽的",
            author: "张三"
          }
        }).then(response => {
          console.log(response);
          console.log(response.data);
        });
      }

        //更新数据
        btns[2].onclick = function(){
          //发送 AJAX 请求
          axios({
            //请求类型
            method: 'PUT',
            //URL
            url: 'http://localhost:3000/posts/3',
            //设置请求体
            data: {
              title: "今天天气不错, 还挺风和日丽的",
              author: "李四"
            }
          }).then(response => {
            console.log(response);
          });
        }

        //删除数据
        btns[3].onclick = function(){
          //发送 AJAX 请求
          axios({
              //请求类型
              method: 'delete',
              //URL
              url: 'http://localhost:3000/posts/3',
          }).then(response => {
              console.log(response);
          });
        }
    </script>
</body>
</html>
```
# 封装axios实例
## axios.creat(config)

1.根据指定配置创建一个新的axios实例, 每个新的axios instance都有自己的配置

2.新的axios instance没有取消请求和批量发送请求的方法

3.为什么要设计这个语法

例：两个后台一个端口号3000，一个端口号4000
```js
axios.defaults.baseURL = 'http://localhost:3000'

axios.get('/get')

// 此时该如何在不修改全局配置的情况下，向端口4000发送请求？

const instance = axios.create({
  baseURL: 'http://localhost:4000'
})

instance.get('/get')
```

## 拦截器及运行流程

流程: 请求拦截器2 => 请求拦截器 1 => 发ajax请求 => 响应拦截器1 => 响
应拦截器 2 => 请求的回调

注意: 此流程是通过 promise 串连起来的, 请求拦截器传递的是 config, 响应
拦截器传递的是 response
```js
// 添加请求拦截器(回调函数)
axios.interceptors.request.use(
  config => {
    console.log('request interscptor1 onResolved()')
    return config
  },
  error => {
    console.log('request interceptor1 onRejected()')
    return Promise.reject(error)
  }
)

axios.interceptors.request.use(
  config => {
    console.log('request interscptor2 onResolved()')
    return config
  },
  error => {
    console.log('request interceptor2 onRejected()')
    return Promise.reject(error)
  }
)

// 添加响应拦截器
axios.interceptors.response.use(
  response => {
    console.log('response interceptor1 onResolved()');
    return response
  },
  error => {
    console.log('response interceptor1 onRejected()');
    return Promise.reject(error)
  }
)

axios.interceptors.response.use(
  response => {
    console.log('response interceptor2 onResolved()');
    return response
  },
  error => {
    console.log('response interceptor2 onRejected()');
    return Promise.reject(error)
  }
)
axios.get('http://localhost:3000/posts')
  .then(response => {
    console.log(response.data)
  })
  .catch(error => {
    console.log(error.message)
  })

// 注意响应和请求拦截器的顺序，以及同一个请求/响应拦截器 1 2的顺序
// request interscptor2 onResolved()
// request interscptor1 onResolved()
// response interceptor1 onResolved()
// response interceptor2 onResolved()
```

## 取消请求

1. 基本流程
配置 cancelToken 对象
缓存用于取消请求的 cancel 函数
在后面特定时机调用 cancel 函数取消请求
在错误回调中判断如果 error 是 cancel, 做相应处理

2. 实现功能
点击按钮, 取消某个正在请求中的请求
在请求一个接口前, 取消前面一个未完成的请求

```js

let cancel  // 用于保存取消请求的函数

// 添加请求拦截器
axios.interceptors.request.use((config) => {
  // 在准备发请求前, 取消未完成的请求
  if (typeof cancel === 'function') {
    cancel('取消请求')
  }
  // 添加一个cancelToken的配置
  config.cancelToken = new axios.CancelToken((c) => { // c是用于取消当前请求的函数
    // 保存取消函数, 用于之后可能需要取消当前请求
    cancel = c
  })

  return config
})

// 添加响应拦截器
axios.interceptors.response.use(
  response => {
    cancel = null
    return response
  },
  error => {
    if (axios.isCancel(error)) {// 取消请求的错误
      // cancel = null
      console.log('请求取消的错误', error.message) // 做相应处理
      // 中断promise链接
      return new Promise(() => {})
    } else { // 请求出错了
      cancel = null
      // 将错误向下传递
      // throw error
      return Promise.reject(error)
    }
  }
)

function getProducts1() {
  axios({
    url: 'http://localhost:4000/products1',
  }).then(
    response => {
      console.log('请求1成功了', response.data)
    },
    error => {// 只用处理请求失败的情况, 取消请求的错误的不用
      console.log('请求1失败了', error.message)
    }
  )
}

function getProducts2() {

  axios({
    url: 'http://localhost:4000/products2',
  }).then(
    response => {
      console.log('请求2成功了', response.data)
    },
    error => {
      console.log('请求2失败了', error.message)
    }
  )
}

function cancelReq() {
  // 执行取消请求的函数
  if (typeof cancel === 'function') {
    cancel('强制取消请求')
  } else {
    console.log('没有可取消的请求')
  }
}
```


------



# 封装一个axios

不封装axios, 直接调用axios暴露的一些api, 例如get, set请求等
但是一个项目是多人开发,每个人调用axios的请求方式不一会比较混乱
且代码会比较冗余,例如请求共同的域名等

## 封装目的
1. 请求拦截
2. 响应拦截
3. 常见错误信息处理
4. 请求头设置
5. api集中管理
6. 取消重复请求
7. 重复发送请求
8. 请求缓存

## axios封装
```ts
import axios, { AxiosRequestConfig, AxiosResponse, ResponseType } from 'axios';
import { stringify } from 'query-string';
import omit from 'omit';

import { clearToken, getToken, getLoginUrl, getRedirectURL, skError } from '@/utils/utils';

declare const process;
const { BASE_URL } = process.env;

const pendingAjax: any = new Map();
const fastClickMsg = '数据请求已被取消';
const CancelToken = axios.CancelToken;

const removePendingAjax = (config: any, c?: any) => {
  const data = typeof config.data === 'string' ? config.data : JSON.stringify(config.data);
  const url = config.method + config.url + stringify(omit(['manualBaseUrl'], config.params)) + data;
  if (pendingAjax.has(url)) {
    c ? c(fastClickMsg) : pendingAjax.delete(url);
  } else {
    c && pendingAjax.set(url, { cancel: c });
  }
};

const axiosConfig: AxiosRequestConfig = {
  baseURL: BASE_URL,
  transformResponse: [
    function (data: AxiosResponse) {
      return data;
    },
  ],
  // 请求响应超时时间
  timeout: 1800000,
  responseType: 'json',
  headers: { 'Content-Type': 'application/json;charset=UTF-8' },
  paramsSerializer: function (params) {
    return stringify(params);
  },
};

const AxiosInstance = axios.create(axiosConfig);

// 请求拦截
AxiosInstance.interceptors.request.use(
  (config) => {
    config.cancelToken = new CancelToken((c) => {
      removePendingAjax(config, c);
    });
    if (config.params?.manualBaseUrl) {
      config.baseURL = config.params.manualBaseUrl;
      delete config.params.manualBaseUrl;
    } else {
      config.baseURL = BASE_URL;
    }
    // 设置token
    config.headers.Authorization = getToken();

    // 请求拦截的第一个参数函数需要返回请求config
    return config;
  },
  (error) => Promise.reject(error)
);

// 响应拦截
AxiosInstance.interceptors.response.use(
  (response) => {
    removePendingAjax(response.config);
    // 如果响应整体里面有false 视为请求失败
    if (response.data && typeof response.data === 'object' && response.data.result === false) {
      return Promise.reject(response.data.error);
    }
    // 直接取出相应的data返回
    return response.data;
  },
  (error) => {
    const { response } = error;
    // 错误处理
    if (response && response instanceof Object) {
      removePendingAjax(response?.config);
      // 如果权限不存在
      if (response.status === 401) {
        // 清除token并且跳转到登录页,同时携带重定向路径
        clearToken().then((_) => {
          window.location.href = `${getLoginUrl()}?redirectURL=${getRedirectURL()}`;
        });
      } else if (response.status === 413) {
        // 某些特殊的请求错误约定(业务逻辑)
        // return Promise.reject('文件大小超过最大限制');
      }

      return Promise.reject(response.data);
    }
    pendingAjax.clear();
    return Promise.reject(error);
  }
);

interface Fetch {
  get: <T = any>(url: any, qs?: any, responseType?: ResponseType) => Promise<AxiosResponse<T>>;
  delete: <T = any>(url: any, qs?: any, data?: any) => Promise<AxiosResponse<T>>;
  post: <T = any>(url: any, data: any, qs?: any) => Promise<AxiosResponse<T>>;
  patch: <T = any>(url: any, data: any, qs?: any) => Promise<AxiosResponse<T>>;
  put: <T = any>(url: any, data: any, qs?: any) => Promise<AxiosResponse<T>>;
}

// 进一步封装不同的请求方法
const fetch: Fetch = {
  get: (url, qs, responseType = 'json') => AxiosInstance.get(url, { params: qs, responseType }),
  delete: (url, qs = '', data) => AxiosInstance.delete(url, { params: qs, data }),
  post: (url, data, qs) => AxiosInstance.post(url, data, { params: qs }),
  patch: (url, data, qs) => AxiosInstance.patch(url, data, { params: qs }),
  put: (url, data, qs) => AxiosInstance.put(url, data, { params: qs }),
};

export default fetch;

// 请求错误处理
export const errorHandle = (thrown, errorMessage?: string, cb?: (e) => void) => {
  if (axios.isCancel(thrown)) {
    return false;
  } else {
    const error = typeof thrown === 'string' ? thrown : thrown?.message;
    errorMessage && skError(error ? `${errorMessage}: ${thrown}` : errorMessage);
    cb?.(thrown);
    return true;
  }
};


// ======================= 创建接口 =======================
const getAllTasks = (id, qs) => fetch.get( `/xxxxx/yyyyy/${id}`, {...qs, } )

// ======================= 业务组件中使用 =======================
const getAllData = useCallback(async () => {
  try {
    const res = await getAllTasks(id)
  } catch(e) {
    errorHandle(e, '请求所有任务失败')
  }
})

```

TODO: 源码解析
# axios源码解析