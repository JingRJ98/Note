# 开发环境搭建

1. 安装Node环境 (推荐nvm)

2. 使用npm全局安装typescript `npm i -g typescript` (会自动安装tsc命令)

3. 创建一个ts文件

4. 创建tsconfig.json`tsc --init`

5. 使用tsc对ts文件进行编译 `tsc index.ts`

6. 热更新 `tsc -w`
  我发现`tsc -w`这个命令是走tsconfig配置文件的，会输出到指定的文件夹位置
  `tsc index.ts -w`这个命令会编译到当前文件夹下，不会走配置文件


## tsconfig.json配置详解
```json
{
  "compilerOptions": {
    /* Basic Options */
    // "incremental": true,                   /* TS编译器在第一次编译之后会生成一个存储编译信息的文件，第二次编译会在第一次的基础上进行增量编译，可以提高编译的速度 */
    "target": "es6",                          /* 目标语言的版本: 'ES3' (default), 'ES5', 'ES2015', 'ES2016', 'ES2017', 'ES2018', 'ES2019', 'ES2020', or 'ESNEXT'. */
    "module": "commonjs",                     /* 生成代码的模板标准: 'none', 'commonjs', 'amd', 'system', 'umd', 'es2015', 'es2020', or 'ESNext'. */
    // "lib": ["DOM", "ES2015", "ScriptHost", "ES2019.Array"], /* TS需要引用的库，即声明文件，es5 默认引用dom、es5、scripthost,如需要使用es的高级版本特性，通常都需要配置，如es8的数组新特性需要引入"ES2019.Array" */
    // "allowJs": true,                       /* 允许编译器编译 JS，JSX 文件 */
    // "checkJs": true,                       /* 允许在JS文件中报错，通常与allowJS一起使用 */
    // "jsx": "preserve",                     /* 在 .tsx 文件里支持 JSX: 'preserve', 'react-native', or 'react'. */
    // "declaration": true,                   /* 生成声明文件，开启后会自动生成声明文件 '.d.ts'. */
    // "declarationMap": true,                /* 为声明文件生成 sourceMap */
    // "sourceMap": true,                     /* 为目标文件生成 sourceMap */
    // "outFile": "./",                       /* 将多个相互依赖的文件生成一个文件，如果以 AMD 作为标准，即开启时应设置 "module": "AMD" */
    // "outDir": "./",                        /* 指定输出目录 */
    // "rootDir": "./",                       /* 指定输出文件目录(用于输出)，用于控制输出目录结构 */
    // "composite": true,                     /* Enable project compilation */
    // "tsBuildInfoFile": "./",               /* 增量编译文件的存储位置 */
    // "removeComments": true,                /* 删除注释 */
    // "noEmit": true,                        /* 不输出文件,即编译后不会生成任何 js 文件 */
    // "importHelpers": true,                 /* 通过 tslib 引入 helper 函数，文件必须是模块 */
    // "downlevelIteration": true,            /* Provide full support for iterables in 'for-of', spread, and destructuring when targeting 'ES5' or 'ES3'. */
    // "isolatedModules": true,               /* Transpile each file as a separate module (similar to 'ts.transpileModule'). */

    /* Strict Type-Checking Options */
    "strict": true,                           /* 开启所有严格的类型检查 */
    // "noImplicitAny": true,                 /* 不允许隐式的 any 类型 */
    // "strictNullChecks": true,              /* 不允许把 null、undefined 赋值给其他类型的变量 */
    // "strictFunctionTypes": true,           /* 不允许函数参数双向协变 */
    // "strictBindCallApply": true,           /* Enable strict 'bind', 'call', and 'apply' methods on functions. */
    // "strictPropertyInitialization": true,  /* 类的实例属性必须初始化 */
    // "noImplicitThis": true,                /* 不允许 this 有隐式的 any 类型 */
    // "alwaysStrict": true,                  /* 在代码中注入 'use strict' */

    /* Additional Checks */
    // "noUnusedLocals": true,                /* 检查只声明、未使用的局部变量(只提示不报错) */
    // "noUnusedParameters": true,            /* 检查未使用的函数参数(只提示不报错) */
    // "noImplicitReturns": true,             /* 每个分支都会有返回值 */
    // "noFallthroughCasesInSwitch": true,    /* 防止 switch 语句贯穿(即如果没有 break 语句后面不会执行) */

    /* Module Resolution Options */
    // "moduleResolution": "node",            /* 模块解析策略，ts 默认用 node 的解析策略，即相对的方式导入 */
    // "baseUrl": "./",                       /* 解析非相对模块的基地址，默认是当前目录 */
    // "paths": {},                           /* 路径映射，相对于 baseUrl */
    // "rootDirs": [],                        /* 将多个目录放在一个虚拟目录下，用于运行时，即编译后引入文件的位置可能发生变化，这也设置可以虚拟 src 和 out 在同一个目录下，不用再去改变路径也不会报错 */
    // "typeRoots": [],                       /* 声明文件目录，默认时 node_modules/@types */
    // "types": [],                           /* 加载的声明文件包 */
    // "allowSyntheticDefaultImports": true,  /* Allow default imports from modules with no default export. This does not affect code emit, just typechecking. */
    "esModuleInterop": true,                  /* 允许 export = 导出，由import from 导入 */
    // "preserveSymlinks": true,              /* Do not resolve the real path of symlinks. */
    // "allowUmdGlobalAccess": true,          /* 允许在模块中全局变量的方式访问 umd 模块 */

    /* Source Map Options */
    // "sourceRoot": "",                      /* Specify the location where debugger should locate TypeScript files instead of source locations. */
    // "mapRoot": "",                         /* Specify the location where debugger should locate map files instead of generated locations. */
    // "inlineSourceMap": true,               /* Emit a single file with source maps instead of having a separate file. */
    // "inlineSources": true,                 /* Emit the source alongside the sourcemaps within a single file; requires '--inlineSourceMap' or '--sourceMap' to be set. */

    /* Experimental Options */
    "experimentalDecorators": true,           /* 开启装饰器功能 */
    "emitDecoratorMetadata": true,            /* 添加元数据 */

    /* Advanced Options */
    "forceConsistentCasingInFileNames": true  /* Disallow inconsistently-cased references to the same file. */
  }
}
```

- 如果一直提示Cannot find name 'Map'. Do you need to change your target library? Try changing the 'lib' compiler option to 'es2015' or later.
修改tsconfig.json也没用,需要安装`npm i @types/node -D`
## 基本类型

- 类型声明
  指定类型后，当为变量赋值时，TS编译器会自动检查值是否符合类型声明，符合则赋值，否则报错
  简而言之，类型声明给变量设置了类型，使得变量只能存储某种类型的值
```typescript
  let 变量: 类型 = 值;

  function fn(参数: 类型, 参数: 类型): 类型{
    ...
  }

  const fun = (参数: 类型): 类型 => {}
  const fun: (参数: 类型) => 类型 = (参数) => {}
```

- 基本类型：
  |  类型   |       例子        |              描述              |
  | :-----: | :---------------: | :----------------------------: |
  | number  |    1, -33, 2.5    |            任意数字            |
  | string  | 'hi', "hi", `hi`  |           任意字符串           |
  | boolean |    true、false    |       布尔值true或false        |
  | 字面量  |      其本身       |  限制变量的值就是该字面量的值  |
  |   any   |         *         |            任意类型            |
  | unknown |         *         |         类型安全的any          |
  |  void   | 空值（undefined） |     没有值（或undefined）      |
  |  never  |      没有值       |          不能是任何值          |
  | object  |  {name:'孙悟空'}  |          任意的JS对象          |
  |  array  |      [1,2,3]      |           任意JS数组           |
  |  tuple  |       [4,5]       | 元素，TS新增类型，固定长度数组 |
  |  enum   |    enum{A, B}     |       枚举，TS中新增类型       |

- any（不建议使用）

   ```typescript
    let d: any = 4;
    d = 'hello';
    d = true;
    d.sayHello() // 不会报错
    ```

- unknown
  一个类型安全的any
  ```typescript
    let notSure: unknown = 4;
    notSure = 'hello';

    notsure.sayHello() // 会报错
  ```
- void
  表示没有返回值的函数
  ```typescript
  const cb: () => void = () => {}
  ```

- never
  表示永远不会出现的值 (用不到)
  是undefined和null的子类型（定义undefined的值只能被赋值为undefined和never）
  never可以赋值给任何类型
  但是任何类型(除了never)都不可以赋值给never
  ```typescript
    function error(message: string): never {
      throw new Error(message);
    }

    let ne: never;
    let nev: never;
    let an: any;

    ne = 123; // Error
    ne = nev; // OK
    ne = an; // Error
    ne = (() => { throw new Error("异常"); })(); // OK
    ne = (() => { while(true) {} })(); // OK
  ```

- object（没啥用）
   ```typescript
    let obj: object = {};
  ```

- array(两种写法)
  ```typescript
    let list: number[] = [1, 2, 3];
    let list: Array<number> = [1, 2, 3];
  ```

- tuple
  固定长度和类型的数组
  ```typescript
    let x: [string, number];
    x = ["hello", 10];
    x = ['hello', 10, 10]; // Error
    x = [10, 'hello']; // Error
  ```

  元祖类型里面最后一个元素可以是剩余元素
  ```ts
  type R = [number, ...string[]];
  let rt: R = [6, "S", "K", "L"];
  console.log(rt[0]);
  console.log(rt[1]);
  ```

  TypeScript 3.4 还引入了对只读元组的新支持。我们可以为任何元组类型加上 readonly 关键字前缀，以使其成为只读元组。
  `const p: readonly [number, number] = [10, 20];`

- enum 枚举类型
  如果标识符没有赋值，那么标识符的index就是它的值（从0开始）
  如果标识符赋值为数字，那么接下去没有赋值的标识符的值就是 数字递增

  ```typescript
    enum Color {
      Red,
      Green,
      Blue,
    }
    let c: Color = Color.Green;

    enum Color {
      Red = 1,
      Green,
      Blue,
    }
    let c: Color = Color.Green;// 2

    enum Color {
      Red = 1,
      Green = 2,
      Blue = 4,
    }
    let c: Color = Color.Green;
  ```

- 类型断言

  有些情况下，变量的类型对于我们来说是很明确，但是TS编译器却并不清楚，此时，可以通过类型断言来告诉编译器变量的类型，断言有两种形式：

    - 第一种

       ```typescript
        let someValue: unknown = "this is a string";
        let strLength: number = (someValue as string).length;
        ```

    - 第二种

       ```typescript
        let someValue: unknown = "this is a string";
        let strLength: number = (<string>someValue).length;
        ```

- 双重类型断言
  ```ts
  interface Person {
    name: string;
    age: number;
  }

  // 先把字符串断言成any 再断言成Person
  // 不知道有啥用
  const person = 'randy' as any as Person; // ok
  ```
  - 1.双重断言基于两个前提：

    (1) 子类型可以被断言为父类型
    (2) any 这种类型可以被赋值给几乎其他任何类型（never 等除外）
  - 2.双重断言断言可以实现 子类型转换为另外一种子类型（ 子类型->父类型->子类型）

  - 3.尽量不使用双重断言，双重断言会破坏原有类型关系



### any unknown never
1. 任何类型都可以赋值给顶级类型变量(any unknown)
2. unknown不可以赋值给其他类型(除了unknown any)
3. any和never可以赋值给任何类型
4. 任何类型(除了never)都不可以赋值给never

`any      <==>   任何类型`
`unknown  <==    任何类型`
`unknown  !==>   任何类型(除了unknown any)`
`never    ==>    任何类型`
`never    <==    只有never`

*A型血可以接受A型血,也可以给A型血输血，B同理，AB血型可以接受任何血型，O型血可以输血给任何血型的人*

### 接口
```ts
interface Person {
  name: string;
  age?: number;
  [propName: string]: string;
}

let tom: Person = {
  name: 'Tom',
  gender: 'male'
};
```

### HTML元素类型
可以通过console.dir(dom元素),打印dom元素的js对象格式,最后显示的就是类型

-------------

## 3、编译选项

### 自动编译整个项目

如果直接使用tsc指令，则可以自动将当前项目下的所有ts文件编译为js文件。

但是能直接使用tsc命令的前提时，要先在项目根目录下创建一个ts的配置文件 tsconfig.json

tsconfig.json是一个JSON文件，添加配置文件后，只需只需 tsc 命令即可完成对整个项目的编译

### 配置选项
#### include
定义希望被编译文件所在的目录
- 默认值：["\*\*/\*"]
  ```json
    "include":["src/**/*", "tests/**/*"]
  ```
- 上述示例中，所有src目录和tests目录下的文件都会被编译

#### exclude
定义需要排除在外的目录
- 默认值：["node_modules", "bower_components", "jspm_packages"]

- 示例：
  ```json
    "exclude": ["./src/hello/**/*"]
  ```

- 上述示例中，src下hello目录下的文件都不会被编译

#### extends
定义被继承的配置文件
- 示例：
```json
  "extends": "./configs/base"
```
- 上述示例中，当前配置文件中会自动包含config目录下base.json中的所有配置信息

#### files
指定被编译文件的列表，只有需要编译的文件少时才会用到
- 示例：

```json
"files": [
    "core.ts",
    "sys.ts",
    "types.ts",
    "scanner.ts",
    "parser.ts",
    "utilities.ts",
    "binder.ts",
    "checker.ts",
    "tsc.ts"
  ]
```

- 列表中的文件都会被TS编译器所编译


### 项目选项

#### target
设置ts代码编译的目标版本

- 可选值：
  ES3（默认）、ES5、ES6/ES2015、ES7/ES2016、ES2017、ES2018、ES2019、ES2020、ESNext

- 示例：

```json
"compilerOptions": {
  "target": "ES6"
}
```

- 如上设置，我们所编写的ts代码将会被编译为ES6版本的js代码

#### lib
指定代码运行时所包含的库（宿主环境）

- 可选值：
  ES5、ES6/ES2015、ES7/ES2016、ES2017、ES2018、ES2019、ES2020、ESNext、DOM、WebWorker、ScriptHost ......

- 示例：

```json
"compilerOptions": {
    "target": "ES6",
    "lib": ["ES6", "DOM"],
    "outDir": "dist",
    "outFile": "dist/aa.js"
}
```

#### module

设置编译后代码使用的模块化系统

- 可选值：
CommonJS、UMD、AMD、System、ES2020、ESNext、None

- 示例：

```typescript
  "compilerOptions": {
      "module": "CommonJS"
  }
```

#### outDir

编译后文件的所在目录

- 默认情况下，编译后的js文件会和ts文件位于相同的目录，设置outDir后可以改变编译后文件的位置

- 示例：

```json
"compilerOptions": {
    "outDir": "dist"
}
```

- 设置后编译后的js文件将会生成到dist目录

#### outFile

将所有的文件编译为一个js文件

- 默认会将所有的编写在全局作用域中的代码合并为一个js文件，如果module制定了None、System或AMD则会将模块一起合并到文件之中

- 示例：

```json
"compilerOptions": {
    "outFile": "dist/app.js"
}
```

#### rootDir
指定代码的根目录，默认情况下编译后文件的目录结构会以最长的公共目录为根目录，通过rootDir可以手动指定根目录

- 示例：

```json
"compilerOptions": {
    "rootDir": "./src"
}
```

#### allowJs

是否对js文件编译

#### checkJs

是否对js文件进行检查

- 示例：

```json
  "compilerOptions": {
      "allowJs": true,
      "checkJs": true
  }
```

#### removeComments

是否删除注释
- 默认值：false

#### noEmit

不对代码进行编译
- 默认值：false

#### sourceMap

是否生成sourceMap
- 默认值：false



### 严格检查

#### strict
启用所有的严格检查，默认值为true，设置后相当于开启了所有的严格检查
#### alwaysStrict
总是以严格模式对代码进行编译
#### noImplicitAny
禁止隐式的any类型
#### noImplicitThis
禁止类型不明确的this
#### strictBindCallApply
严格检查bind、call和apply的参数列表
#### strictFunctionTypes
严格检查函数的类型

#### strictNullChecks
严格的空值检查

#### strictPropertyInitialization
严格检查属性是否初始化

#### noFallthroughCasesInSwitch
检查switch语句包含正确的break

#### noImplicitReturns
检查函数没有隐式的返回值
#### noUnusedLocals

检查未使用的局部变量


#### noUnusedParameters
检查未使用的参数

### 高级

#### allowUnreachableCode
检查不可达代码
- 可选值：
  true，忽略不可达代码
  false，不可达代码将引起错误
#### noEmitOnError
有错误的情况下不进行编译
- 默认值：false

## 4、webpack

通常情况下，实际开发中我们都需要使用构建工具对代码进行打包，TS同样也可以结合构建工具一起使用，下边以webpack为例介绍一下如何结合构建工具使用TS。

1. 初始化项目
  进入项目根目录，执行命令 ``` npm init -y```
  主要作用：创建package.json文件

2. 下载构建工具

    ```
    npm i -D webpack webpack-cli webpack-dev-server typescript ts-loader clean-webpack-plugin
    ```
    - 共安装了7个包
      - webpack
        - 构建工具webpack
      - webpack-cli
        - webpack的命令行工具
      - webpack-dev-server
        - webpack的开发服务器
      - typescript
        - ts编译器
      - ts-loader
        - ts加载器，用于在webpack中编译ts文件
      - html-webpack-plugin
        - webpack中html插件，用来自动创建html文件
      - clean-webpack-plugin
        - webpack中的清除插件，每次构建都会先清除目录

3. 根目录下创建webpack的配置文件webpack.config.js

    ```javascript
      const path = require("path");
      const HtmlWebpackPlugin = require("html-webpack-plugin");
      const { CleanWebpackPlugin } = require("clean-webpack-plugin");

      module.exports = {
          optimization:{
            minimize: false // 关闭代码压缩，可选
          },

          entry: "./src/index.ts",

          devtool: "inline-source-map",

          devServer: {
            contentBase: './dist'
          },

          output: {
            path: path.resolve(__dirname, "dist"),
            filename: "bundle.js",
            environment: {
              arrowFunction: false // 关闭webpack的箭头函数，可选
            }
          },

          resolve: {
            extensions: [".ts", ".js"]
          },

          module: {
            rules: [
              {
                test: /\.ts$/,
                use: {
                  loader: "ts-loader"
                },
                exclude: /node_modules/
              }
            ]
          },

          plugins: [
            new CleanWebpackPlugin(),
            new HtmlWebpackPlugin({
              title:'TS测试'
            }),
          ]
      }
    ```

4. 根目录下创建tsconfig.json，配置可以根据自己需要

    ```json
      {
          "compilerOptions": {
              "target": "ES2015",
              "module": "ES2015",
              "strict": true
          }
      }
      ```

5. 修改package.json添加如下配置

    ```json
      {
        ...略...
        "scripts": {
          "test": "echo \"Error: no test specified\" && exit 1",
          "build": "webpack",
          "start": "webpack serve --open chrome.exe"
        },
        ...略...
      }
      ```

6. 在src下创建ts文件，并在并命令行执行```npm run build```对代码进行编译，或者执行```npm start```来启动开发服务器



## 5、Babel

- 经过一系列的配置，使得TS和webpack已经结合到了一起，除了webpack，开发中还经常需要结合babel来对代码进行转换以使其可以兼容到更多的浏览器，在上述步骤的基础上，通过以下步骤再将babel引入到项目中。

  1. 安装依赖包：
     -
     ```
     npm i -D @babel/core @babel/preset-env babel-loader core-js
     ```
     - 共安装了4个包，分别是：
       - @babel/core
         - babel的核心工具
       - @babel/preset-env
         - babel的预定义环境
       - @babel-loader
         - babel在webpack中的加载器
       - core-js
         - core-js用来使老版本的浏览器支持新版ES语法

  2. 修改webpack.config.js配置文件
     ```ts
       ...略...
       module: {
        rules: [
          {
            test: /\.ts$/,
            use: [
              {
                loader: "babel-loader",
                options:{
                  presets: [
                    [
                      "@babel/preset-env",
                      {
                        "targets":{
                            "chrome": "58",
                            "ie": "11"
                        },
                        "corejs":"3",
                        "useBuiltIns": "usage"
                      }
                    ]
                  ]
                }
              },
              {
                loader: "ts-loader",
              }
            ],
            exclude: /node_modules/
          }
        ]
       }
       ...略...
       ```

     - 如此一来，使用ts编译后的文件将会再次被babel处理，使得代码可以在大部分浏览器中直接使用，可以在配置选项的targets中指定要兼容的浏览器版本。

----------

# 面向对象
## 类（class）
  ```typescript
  class 类名 {
    属性名: 类型;
    constructor(参数: 类型){
      this.属性名 = 参数;
    }
    方法名(){
      ....
    }
  }
  ```
  ```typescript
  class Person{
    name: string;
    age: number;

    constructor(name: string, age: number){
      this.name = name;
      this.age = age;
    }

    sayHello(){
      console.log(`大家好，我是${this.name}`);
    }
  }
  ```

使用类：
  ```ts
  const p = new Person('孙悟空', 18);
  p.sayHello();
  ```

- 类的泛型
```ts
export class Filter<T> implements ExceptionFilter {
  sayHi(exception: T, host: ArgumentsHost) {}
}
```
## 面向对象的特点
封装 继承 多态

- 封装
  - 对象实质上就是属性和方法的容器，它的主要作用就是存储属性和方法，这就是所谓的封装


## 修饰符
1. 访问修饰符
从上到下逐步严格

public：默认的修饰符，表示该属性或方法是公开的，可以在任何地方被 访问/修改
protected：表示该属性或方法是受保护的，可以在类内部和子类中访问，但外部无法访问
private：该属性或方法是私有的，只能在类内部被访问，外部和子类都无法访问/修改

**public**
支持类中修改
支持子类中修改
支持实例中修改
```typescript
class Person{
  public name: string; // 写或什么都不写都是public
  public age: number;

  constructor(name: string, age: number){
    this.name = name; // 可以在类中修改
    this.age = age;
  }

  sayHello(){
    console.log(`大家好，我是${this.name}`);
  }
}

class Employee extends Person{
  constructor(name: string, age: number){
    super(name, age);
    this.name = name; //子类中可以修改
  }
}

const p = new Person('孙悟空', 18);
p.name = '猪八戒';// 可以通过实例对象修改

console.log(p.name);
console.log(p.sayHello());
```

**private**
只支持类中访问/修改
```typescript
class Person{
    private name: string;
    private age: number;

    constructor(name: string, age: number){
        this.name = name; // 可以修改
        this.age = age;
    }

    sayHello(){
        console.log(`大家好，我是${this.name}`);
    }
}

class Employee extends Person{
    constructor(name: string, age: number){
        super(name, age);
        this.name = name; //子类中不能修改
    }
}

const p = new Person('孙悟空', 18);
p.name = '猪八戒';// 实例不能修改 报错
```

**protected**
```typescript
  class Person{
      protected name: string;
      protected age: number;

      constructor(name: string, age: number){
          this.name = name; // 可以修改
          this.age = age;
      }

      sayHello(){
          console.log(`大家好，我是${this.name}`);
      }
  }

  class Employee extends Person{

      constructor(name: string, age: number){
          super(name, age);
          this.name = name; //子类中可以修改
      }
  }

  const p = new Person('孙悟空', 18);
  p.name = '猪八戒';// 实例不能修改 报错
```


2. 只读修饰符readonly
readonly：表示该属性是只读的，只能在类的构造函数中赋值，之后不能再被修改。但需要注意的是，尽管属性是只读的，仍然可以在构造函数中被修改。

3. 静态修饰符static
static：表示该属性或方法是静态的，属于类本身而不是类的实例。静态成员可以通过类名直接访问，而不需要创建类的实例。
静态属性不能通过this关键字访问，因为this总是指向类的实例。


静态属性属于类, 所有该类的实例**共享**类的静态属性, 可以理解成只有一个值
```ts
class Ticket {
  static count = 10;
  name: string;

  constructor(name: string) {
    this.name = name;
  }

  sale() {
    if (Ticket.count > 0) {
      console.log(this.name + "卖出了一张票, 票号为" + Ticket.count);
      Ticket.count--;
    }
  }
}

const t1 = new Ticket('窗口1')
const t2 = new Ticket('窗口2')
const t3 = new Ticket('窗口3')

t1.sale() // 10
t2.sale() // 9
t3.sale() // 8
```

4. abstract 修饰符
抽象类
当 abstract 修饰符用在一个类声明上时，这个类被定义为抽象类。抽象类不能被直接实例化，只能被继承。它通常作为其他类的基类，用于定义通用的功能和结构，而其具体实现则留给派生类完成。
```ts
abstract class Animal {
    abstract makeSound(): void; // 抽象方法
    move(): void {
        console.log("roaming the earth...");
    }
}
```
抽象方法
当 abstract 修饰符用在类内部的方法声明上时，这个方法被定义为抽象方法。抽象方法没有具体的实现代码，只定义方法的签名。在继承抽象类的派生类中，必须对所有的抽象方法提供具体的实现。
```ts
abstract class Animal {
    abstract makeSound(): void; // 抽象方法
    move(): void {
        console.log("roaming the earth...");
    }
}

class Dog extends Animal {
    makeSound(): void {
        console.log("Bark");
    }
}
```

抽象属性
尽管 abstract 修饰符通常与方法一起使用，但在 TypeScript 中，你也可以将其用于类属性。抽象属性在抽象类中声明，但必须在派生类中定义。
```ts
abstract class Shape {
    abstract readonly numberOfSides: number;
    // 其他抽象或具体成员...
}

class Triangle extends Shape {
    readonly numberOfSides = 3;
    // 具体实现...
}
```
在设计时需要注意的是，abstract 修饰符不能与 private 修饰符一起使用，因为 private 成员不能在派生类中被访问或实现，而 abstract 成员则要求派生类提供实现。此外，abstract 修饰符不能用于静态成员，因为静态成员不需要在派生类中实现



### 综合示例
```ts
class Person {
    // 公共属性
    public name: string;

    // 私有属性
    private age: number;

    // 受保护属性
    protected gender: string;

    // 只读属性
    readonly isHuman: boolean = true;

    // 静态属性
    static species: string = 'Homo sapiens';

    constructor(name: string, age: number, gender: string) {
        this.name = name;
        this.age = age; // 可以在构造函数中给私有属性赋值
        this.gender = gender;
        // this.isHuman = false; // 错误：不能给只读属性赋值（除了在构造函数中）
    }

    // 公共方法
    public sayHello() {
        console.log(`Hello, my name is ${this.name}`);
    }

    // 私有方法
    private getAge() {
        return this.age;
    }

    // 受保护方法（可以在子类中访问）
    protected describeGender() {
        return `I am a ${this.gender}`;
    }

    // 静态方法
    static getSpecies() {
        return Person.species;
    }
}

// 创建Person类的实例
const person = new Person('Alice', 30, 'female');
console.log(person.name); // Alice（可以访问公共属性）
// console.log(person.age); // 错误：不能访问私有属性
// console.log(person.gender); // 错误：不能访问受保护属性（在外部）
console.log(person.isHuman); // true（可以访问只读属性）
console.log(Person.getSpecies()); // Homo sapiens（可以访问静态方法）

// 子类
class Employee extends Person {
    private employeeId: string;

    constructor(name: string, age: number, gender: string, employeeId: string) {
        super(name, age, gender);
        this.employeeId = employeeId;
    }

    // 可以访问受保护方法和属性
    public describe() {
        console.log(this.describeGender()); // I am a female
        console.log(`Employee ID: ${this.employeeId}`);
    }
}

// 创建Employee类的实例
const employee = new Employee('Bob', 25, 'male', 'E12345');
employee.describe();
```
修饰符可以两两组合
在TypeScript中，并不是所有的修饰符都可以任意组合使用。一些修饰符是互斥的，而有些则可以组合。以下是一些常见的修饰符组合及其含义：

```ts
class Example {
    // 三个访问修饰符是互斥的
    // 使用访问修饰符
    public publicProperty: string;
    private privateProperty: string;
    protected protectedProperty: string;

    // 使用readonly和访问修饰符
    public readonly publicReadonlyProperty: string;
    private readonly privateReadonlyProperty: string;
    protected readonly protectedReadonlyProperty: string;

    // 使用static和访问修饰符
    public static publicStaticProperty: string;
    private static privateStaticProperty: string;
    protected static protectedStaticProperty: string;

    // 使用static和readonly组合
    public static readonly publicStaticReadonlyProperty: string;

    // 使用abstract和访问修饰符
    public abstract publicAbstractMethod(): void;
    protected abstract protectedAbstractMethod(): void;
    // 注意：抽象类中不能有 private abstract 成员
}
```

## 属性存取器
我们可以在类中定义一组读取、设置属性的方法，这种对属性读取或设置的属性被称为属性的存取器
读取属性的方法叫做getter方法，设置属性的方法叫做setter方法
```typescript
  class Person{
    private _name: string;

    constructor(name: string){
      this._name = name;
    }

    get name(){
      // 禁止return this.name 这样是循环引用 会递归报错
      return this._name;
    }

    set name(name: string){
      this._name = name;
    }
  }

  const p1 = new Person('孙悟空');
  console.log(p1.name); // 通过getter读取name属性
  p1.name = '猪八戒'; // 通过setter修改name属性
```

例如一个类中定义了一个数组属性, 可以通过get来定义一个属性, 例如判断数组是否为空
计算属性
```ts
class Example {
  private _list: number[] = [];

  get isEmpty() {
    return this._list.length === 0;
  }
}

// 使用
const example = new Example();
console.log(example.isEmpty); // true
```
## this
在类中，this表示当前实例对象

## 继承
通过继承可以将其他类中的属性和方法引入到当前类中，从而实现对已有类的扩展
```typescript
class Animal{
    name: string;
    age: number;

    constructor(name: string, age: number){
        this.name = name;
        this.age = age;
    }
}

class Dog extends Animal{
    bark(){
        console.log(`${this.name}在汪汪叫！`);
    }
}

const dog = new Dog('旺财', 4);
dog.bark();
```

## 类中方法区别
### 作为普通函数使用 this都指向实例
定义在实例上和定义在原型上的区别是
如果用同一个类生成多个实例，那么定义在原型上的方法会被所有实例共享，而定义在实例上的方法不会被共

```ts
class MyClass {
  constructor() {}

  // 箭头函数 直接定义在实例上
  arrowMethod = () => {
    console.log("Arrow:", this);
  };

  // 普通方法 定义在prototype上
  regularMethod() {
    console.log("Regular:", this);
  }
}

const instance = new MyClass();

// 调用普通方法
instance.regularMethod(); // 输出: Regular: MyClass {...}

// 调用箭头函数方法
instance.arrowMethod(); // 输出: Arrow: MyClass {...}


// 实例和原型上的方法的区别
class WithArrow {
  constructor() {}
  method = () => {
    console.log('This is an arrow function method');
  };
}

class WithRegular {
  method() {
    console.log('This is a regular method');
  }
}

const arrowInstance1 = new WithArrow();
const arrowInstance2 = new WithArrow();

const regularInstance1 = new WithRegular();
const regularInstance2 = new WithRegular();

// false
console.log(arrowInstance1.method === arrowInstance2.method);
// true
console.log(regularInstance1.method === regularInstance2.method);
```

### 作为回调
```ts
class Counter {
  constructor() {
    this.count = 0;
  }

  incrementArrow = () => {
    this.count++;
    console.log('Arrow count:', this.count);
  };

  incrementRegular() {
    this.count++;
    console.log('Regular count:', this.count);
  }
}

const counter = new Counter();

// 模拟事件处理，直接调用不会影响 `this` 的指向
setTimeout(counter.incrementArrow, 1000); // 正确输出: Arrow count: 1

// 需要手动绑定 `this`，否则 `this` 将为 `undefined`
setTimeout(counter.incrementRegular.bind(counter), 1000); // 正确输出: Regular count: 2
```

通过继承可以在不修改类的情况下完成对类的扩展
发生继承时，如果子类中的方法会替换掉父类中的同名方法，这就称为方法的重写
只有定义在原型上的普通方法才可以被改写
```typescript
  class Animal{
      name: string;
      age: number;

      constructor(name: string, age: number){
          this.name = name;
          this.age = age;
      }
      // 箭头函数 直接定义在实例上 子类中不可以重写
      disabedFun = () => {}

      // 普通方法 定义在prototype上 子类中可以重写
      run(){
          console.log(`父类中的run方法！`);
      }
  }

  class Dog extends Animal{
      bark(){
          console.log(`${this.name}在汪汪叫！`);
      }
      // 重写父类中的run方法
      run(){
          console.log(`子类中的run方法，会重写父类中的run方法！`);
      }
  }

  const dog = new Dog('旺财', 4);
  dog.bark();
  ```


# extends和implements
在TypeScript（TS）中，implements 和 extends 是两个关键字，它们各自有不同的用途，但都与类的继承有关。以下是它们之间的主要区别：

## extends
extends 关键字用于创建一个类（派生类），该类继承另一个类（基类或父类）的属性和方法。
派生类会继承基类的所有公共（public）和受保护的（protected）成员（属性和方法）。
派生类可以添加新的成员，重写（覆盖）基类中的方法，但不能直接访问基类的私有（private）成员。
extends 用于实现类的继承关系，实现的是“是一个”（is-a）的关系。

```typescript
class Animal {
  name: string;

  constructor(name: string) {
    this.name = name;
  }

  speak() {
    console.log(`${this.name} makes a noise.`);
  }
}

class Dog extends Animal {
  bark() {
    console.log(`${this.name} barks.`);
  }
}

const myDog = new Dog('Spike');
myDog.bark(); // Spike barks.
myDog.speak(); // Spike makes a noise.
```

extends也可以用于interface之间的集成
```ts
interface Person {
  name: string;
  age: number;
}

// 集成的interface可以新增类型 但是相同属性类型必须和父interaface一致
interface Student extends Person {
  id: number;
  name: string;
}

// 必须包含子interface和父interface的所有属性
const a: Student = {
  name: "张三",
  age: 18,
  id: 1001
}


/// 多个interface集成
interface Shape {
  area(): number;
}

interface Colored {
  color: string;
}

interface ColoredShape extends Shape, Colored {}

const circle: ColoredShape = {
  color: "red",
  area: () => Math.PI * 4 * 4,
};
```


## implements
implements 关键字用于确保一个类遵循某个接口（interface）
接口定义了一个对象应该具有的结构，但不包含实现。
一个类可以实现一个或多个接口，并必须提供接口中定义的所有属性和方法的实现。
implements 用于实现接口，实现的是“像”（like-a）的关系。
示例：

```typescript
interface Shape {
    area: number;
    perimeter: number;
    draw(): void;
}

class Circle implements Shape {
    area: number;
    perimeter: number;

    constructor(radius: number) {
        this.area = Math.PI * radius ** 2;
        this.perimeter = 2 * Math.PI * radius;
    }

    draw(): void {
        console.log("Drawing a circle");
    }
}

// 创建一个Circle实例
const circle = new Circle(5);
circle.draw(); // Drawing a circle


///// 例2
interface Flyable {
  fly(): void;
}

interface Swimable {
  swim(): void;
}

class Bird implements Flyable {
  fly() {
    console.log("Flying...");
  }
}

class Fish implements Swimable {
  swim() {
    console.log("Swimming...");
  }
}
```
总结
extends 用于创建子类（派生类），子类继承父类的属性和方法。
implements 用于确保类遵循某个接口定义的契约，并提供接口中所有成员的实现。
一个类可以同时继承一个父类（使用 extends）并实现多个接口（使用 implements）。
继承是“是一个”的关系，而接口实现是“像”的关系。

同时使用
```ts
// 定义一个接口
interface Drivable {
  drive(): void;
}

// 定义另一个接口
interface Flyable {
  fly(): void;
}

// 定义一个基类
class Car {
  startEngine(): void {
    console.log("Engine started");
  }
}

// 定义一个子类，继承自 Car，并实现 Drivable 和 Flyable 接口
// implements 关键字后面可以跟多个接口，用逗号分隔
// 且implements中的属性必须在类中实现, 除非接口中的属性是可选的(用?定义可选属性或方法)

// extends 不重写父类的方法就默认继承父类的方法
class FlyingCar extends Car implements Drivable, Flyable {
  drive(): void {
    console.log("Driving on the road");
  }

  fly(): void {
    console.log("Flying in the sky");
  }
}

// 使用 FlyingCar 类
const myFlyingCar = new FlyingCar();
myFlyingCar.startEngine(); // 从 Car 类继承的方法
myFlyingCar.drive();       // 实现 Drivable 接口的方法
myFlyingCar.fly();         // 实现 Flyable 接口的方法
```


## 装饰器
装饰器就是解决在不修改原来类、方法，属性，参数的时候为其添加额外的功能。
项目中使用装饰器需要修改tsconfig.json
`"experimentalDecorators": true,`
```json
{
  "compilerOptions": {
    "target": "ES2020",
    "useDefineForClassFields": true,
    "lib": ["ES2020", "DOM", "DOM.Iterable"],
    "module": "ESNext",
    "skipLibCheck": true,
    /* 开启装饰器功能 */
    "experimentalDecorators": true,

    /* Bundler mode */
    "moduleResolution": "bundler",
    "allowImportingTsExtensions": true,
    "resolveJsonModule": true,
    "isolatedModules": true,
    "noEmit": true,
    "jsx": "react-jsx",

    /* Linting */
    "strict": true,
    "noUnusedLocals": true,
    "noUnusedParameters": true,
    "noFallthroughCasesInSwitch": true,
    "baseUrl": ".",
    "paths": {
      "@/*": ["src/*"]
    }
  },
  "include": ["src"],
  "references": [{ "path": "./tsconfig.node.json" }]
}
```


### 类装饰器
可以给类定义多个装饰器，并且执行的过程是从下到上。
装饰器函数接收类的构造函数(class)作为参数。

类的构造函数类型是`new (...args: any[]) => any`
装饰器的从后往前执行，并且类装饰器返回值会覆盖当前修饰的类。
```ts
// 装饰器函数
function ClassFunctionExtends<T extends new (...args: any[]) => any>(Class: T) {
  console.log("Class", Class);
  // 扩充了参数class的功能
  class Logger extends Class {
    constructor(...args: any[]) {
      console.log("增加前日志信息");
      super(...args);
      console.log("增加后日志信息");
    }
  }
  // 初始化修饰的类时是执行这个Logger类，间接初始化修饰的类
  return Logger
}

// 目标类
@ClassFunctionExtends
class Test {
  // 先执行原来构造函数
  constructor(private name: string) {
    this.name = name;
  }
  say() {
    console.log(this.name, "say");
  }
}
let test = new Test("jrj");

test.say()
```

## 函数装饰器
### 普通方法装饰器
装饰器参数一：表示该方法所属类的原型。class.prototype
装饰器参数二：表示该方法的方法名。
装饰器参数三：表示对于该方法的描述对象。类型为PropertyDescriptor, 类似于Object.defineProperty中的属性描述对象。


### 静态方法装饰器
参数一：表示该方法所属的类的构造函数。class
参数二：表示该方法的方法名。
参数三: 属性描述对象

```ts
class Person {
  constructor() {}

  @MethodInterceptor
  say(p1: string, p2: string) {
    console.log("say.....p1, p2", p1, p2);
  }
}

function MethodInterceptor(ClassPrototype: any, methodName: any, methodDesc: PropertyDescriptor) {
  // 保存以前的函数体
  let origin = methodDesc.value;
  console.log("origin:", origin);

  // 修改函数体
  methodDesc.value = function (...args: any[]) {
    console.log("this:", this); // Person
    args = args.map((arg) => arg + "增加相同内容")
    console.log("args", args) // args [ 'zh增加相同内容', 'llm增加相同内容' ]

    // 调用以前的函数
    origin.apply(this, args)
  }
}

const p = new Person()
p.say("zh", "llm")
// say.....p1, p2 zh增加相同内容 llm增加相同内容
```
## 属性装饰器
参数一表示该属性对应类的原型对象 class.prototype
参数二表示该属性的属性名。

可以返回一个属性描述对象 返回值将替换属性原本的描述对象
```ts
function propDecorate(target: any, propName: string): any {
  // 参数一表示该属性类的原型对象
  // 参数二表示该属性的名字
  console.log(target == Teacher3.prototype, propName) // true name
  let descriptor: PropertyDescriptor = {
    writable: false
  }
  return descriptor
}

class Teacher3 {
  @propDecorate // 这里的修饰器返回一个属性描述对象，将会改变name属性的描述对象
  name: string
  constructor(name: string) {
    this.name = name
  }
}

const t3 = new Teacher3('zh')
t3.name = 'llm' // 这里将会报错
```

## 参数装饰器
参数一：表示该参数对应方法类的原型对象constructor.prototype
参数二：该参数所属于的函数名
参数三：该参数在参数列表中的索引(函数的第几个参数, 从0开始)
```ts
function paramDecorate(target: any, key: string, paramIndex: number) {
  console.log(target === Teacher3.prototype, key, paramIndex)
}

class Teacher3 {
  name: string
  constructor(name: string) {
    this.name = name
  }

  sayText(@paramDecorate text: string, @paramDecorate other: any) {
    return text
  }
}

const t3 = new Teacher3('zh')

// true sayText 1
// true sayText 0
```

## 实例
对类方法包装报错
```ts
const userInfo: any = undefined

function catchError(msg: string) {
  return function (target: any, key: string, descriptor: PropertyDescriptor) {
    // 获取当前方法
    const fn = descriptor.value
    descriptor.value = function () {
      try {
        fn()
      } catch (e) {
        console.log(msg)
      }
    }
  }
}

  class Test1 {
    @catchError('userInfo.name 不存在')
    getName() {
      return userInfo.name
    }
    @catchError('userInfo.age 不存在')
    getAge() {
      return userInfo.age
    }
  }

  const test1 = new Test1()
  test1.getName() // userInfo.name 不存在
  test1.getAge() // userInfo.age 不存在
```


## 装饰器执行顺序
属性装饰器 -> (方法参数装饰器 -> 方法装饰器) -> 构造器参数装饰器 -> 类装饰器。




------------

# 类型体操

## number获取数组元素
对于array 可以使用arr[number]获取到数组中所有元素

## typeof
获取变量或者属性的类型
```ts
interface P {
  name: string;
  age: number;
}

const a: P = {
  name: 'jrj',
  age: 19
}

type Person = typeof a
```


## extends
泛型约束
`T extends P`   T里面的属性可以多 不能少

```ts
interface P {
  name: string
}
const fun = <T extends P>(param: T) => {}

fun({name:'jrj'})
fun({name:'jrj', age: 19})
fun({age: 19}) // 报错
```
泛型约束也可以加上默认值
```ts
type TSConfig<TStrict extends {strict:boolean}={strict:true}> = {
	strict: TStrict['strict']
};

type test_TSConfig_default = Expect<Equal<TSConfig, { strict: true }>>;

type test_TSConfig_true = Expect<Equal<TSConfig<{ strict: true }>, { strict: true }>>;

type test_TSConfig_false = Expect<Equal<TSConfig<{ strict: false }>, { strict: false }>>;

type test_TSConfig_boolean = Expect<Equal<TSConfig<{ strict: boolean }>, { strict: boolean }>>;
```

## in 遍历
遍历枚举类型
```ts
type Keys = "a" | "b" | "c"

type Obj = {
  [p in Keys]: any;
}
// => { a: any, b: any, c: any }
```

## keyof 获取接口所有key 转换为联合类型

用于获取某种类型的所有键，其返回类型是联合类型。
```ts
interface Person {
  name: string;
  age: number;
}

type K1 = keyof Person; // "name" | "age"

const a: K1 = 'age'
const b: K1 = 'name'
const c: K1 = 'name1' // 报错
```

```ts
// 一个返回函数中某个属性的对应值的函数
const fun = (obj: object, key: string) => {
  return obj[key]
  // 报错元素隐式具有 "any" 类型，因为类型为 "string" 的表达式不能用于索引类型 "{}"。
  // 在类型 "{}" 上找不到具有类型为 "string" 的参数的索引签名
}


// 使用keyof约束
// 使其不报错
const fun = <T extends object, K extends keyof T>(obj: T, key: K) => {
  return obj[key]
}
// 首先定义了 T 类型并使用 extends 关键字约束该类型必须是 object 类型的子类型
// 然后使用 keyof 操作符获取 T 类型的所有键，其返回类型是联合类型
// 最后利用 extends 关键字约束 K 类型必须为 keyof T 联合类型的子类型。
```

## infer 指代元素 获取元祖第一个元素
在条件类型语句中，可以用 infer 声明一个类型变量并且对它进行使用。
简单说就是用它取到函数返回值的类型方便之后使用。
???
```ts
type ReturnType<T> = T extends (
  ...args: any[]
) => infer R ? R : any;
// 取到函数返回的类型 声明为R 留待稍后使用
// 吾不知其名，强字之曰道
```
>*有物混成，先天地生。*
>*寂兮寥兮，独立而不改，周行而不殆，可以为天地母。*
>*吾不知其名，强字之曰道，强为之名曰大。*


```ts
type First<T extends any[]> = T extends [infer First, ...infer Tail] ? First : never
```

## 遍历数组
T需要是一个数组类型
T[number]
```ts
type Arr = ['a', 'b', 'c', ['d']]

type Res<T extends any[]> = T[number]

type r = Res<Arr> //  ["d"] | "a" | "b" | "c"
```
## 获得数组长度
`T['length']`

## 利用递归解开嵌套包
```ts
// ============= Test Cases =============
type X = Promise<string>
type Y = Promise<{ field: number }>
type Z = Promise<Promise<string | number>>
type Z1 = Promise<Promise<Promise<string | boolean>>>

MyAwaited<X> // string
MyAwaited<Y> // { field: number }
MyAwaited<Z> // string | number
MyAwaited<Z1> // string | boolean
// @ts-expect-error
type error = MyAwaited<number>

type MyAwaited<T extends Promise<any>> = T extends Promise<infer R> ? R extends Promise<any> ? MyAwaited<R> : R : T
```

## 常用工具类型
### Partial<T>
将类型的属性变成可选
```ts
interface P {
  name: string;
  age: number;
  gender: string;
}

type Partial<T> = {
  [P in keyof T]?: T[P];
};

// keyof T  获取T类型的所有键(返回一个联合类型)
// in 遍历这个联合类型
// []表示取到的键
```
#### 缺点 : 只能处理第一层
```ts
interface P {
  id: string;
  name: string;
  fruits: {
    apple: number;
    orange: number;
  }
}

type NewP = Partial<P>;

const x: NewUserInfo = {
  name: 'x',
  fruits: {
    orange: 1,
  }
}
// 报错
```

### 自定义工具类型DeepPartial<T>
```ts
interface P {
  id: string;
  name: string;
  fruits: {
    apple: number;
    orange: number;
  }
}

type DeepPartial<P> = {
  // 如果是 object，则递归类型
  [U in keyof P]?: P[U] extends object ? DeepPartial<P[U]> : P[U]
};

type P2 = DeepPartial<P>; // 现在T上所有属性都变成了可选啦
```


### Required<T>
将T类型的属性变成必选
```ts
interface P {
  name?: string;
  age?: number;
}

type P1 = Required<P>

const a: P1 = {
  name: 'jrj'
}
// 报错

type Required<T> = {
  [P in keyof T]-?: T[P];
};
//  -? 是代表移除 ? 这个 modifier 的标识
```

**DeepRequired<T>**
```ts
// 模仿上面的DeepPartial
type MyRequired<T> = {
  [P in keyof T]-?: T[P] extends object ? MyRequired<T[P]> : T[P]
}
```

### Readonly<T>
将T中所有属性变成只读属性 不允许重新赋值
```ts
interface P {
  name: string;
  age: number;
}

type P1 = Readonly<P>
type P2 = Readonly<Partial<P>>

const a: P1 = {
  name: 'jrj'
}
// 报错

const b: P2 = {
  name: 'jrj'
}// 不报错

b.name = 'ew'
// 报错


/**
 * Make all properties in T readonly
 */
type Readonly<T> = {
  readonly [P in keyof T]: T[P];
};

type MyDeepReadonly<T> = {
  readonly [P in keyof T]: T[P] extends object ? MyDeepReadonly<T[P]> : T[P]
}
```

### Pick<T, K>
从T中挑选出一些属性出来
```ts
interface T {
  title: string;
  des: string;
  com: boolean;
}

type T1 = Pick<T, "title" | "com">;

const a: T1 = {
  title: "Clean room",
  com: false,
};

type Pick<T, K extends keyof T> = {
  [P in K]: T[P];
};
```

### Exclude<T, U>
Exclude<T, U> 的作用是将T中属于U的类型移除掉。
```ts
type T0 = Exclude<"a" | "b" | "c", "a">; // "b" | "c"
type T1 = Exclude<"a" | "b" | "c", "a" | "b">; // "c"
type T2 = Exclude<string | number | (() => void), Function>; // string | number

type Exclude<T, U> = T extends U ? never : T;
```

### Extract<T, U>
```ts
// Extract<T, U> 的作用是从 T 中提取出 U。
type Extract<T, U> = T extends U ? T : never;
```

### Omit<T, K>
Omit<T, K> 的作用是使用 T 类型中除了 K 类型的所有属性，来构造一个新的类型。
```ts
interface T {
  title: string;
  des: string;
  com: boolean;
}

type T1 = Omit<T, "des">;

const t: T1 = {
  title: "room",
  com: false,
};

type Omit<T, K extends keyof any> = Pick<T, Exclude<keyof T, K>>;
// 1.获取T类型的所有属性键
// 2.Exclude<keyof T, K> 是将keyof T中属于K的类型移除掉的类型
```

# ts练习题系列
## 1
```ts
type User = {
  id: number;
  kind: string;
};

function makeCustomer<T extends User>(u: T): T {
  // Error（TS 编译器版本：v4.4.2）
  // Type '{ id: number; kind: string; }' is not assignable to type 'T'.
  // '{ id: number; kind: string; }' is assignable to the constraint of type 'T',
  // but 'T' could be instantiated with a different subtype of constraint 'User'.
  return {
    id: u.id,
    kind: 'customer'
  }
}


// 解决
function makeCustomer<T extends User>(u: T): T {
  return {
    ...u,
    id: u.id,
    kind: 'customer'
  }
}
```


## 2

我们希望参数 a 和 b 的类型都是一致的，即 a 和 b 同时为 number 或 string 类型。当它们的类型不一致的值，TS 类型检查器能自动提示对应的错误信息。
```ts
function f(a: string | number, b: string | number) {
  if (typeof a === 'string') {
    return a + ':' + b; // no error but b can be number!
  } else {
    return a + b; // error as b can be number | string
  }
}

f(2, 3); // Ok
f(1, 'a'); // Error
f('a', 2); // Error
f('a', 'b') // Ok



// 解决
// 函数重载
function f(a: string, b: string): string
function f(a: number, b: number): number
function f(a: string | number, b: string | number) {
  if (typeof a === 'string' || typeof b === 'string') {
    return a + ':' + b;
  } else {
    return a + b
  }
}

f(2, 3); // Ok
f(1, 'a'); // Error
f('a', 2); // Error
f('a', 'b') // Ok
```


## 3
TS 内置的工具类型：Partial<T>，它的作用是将某个类型里的属性全部变为可选项
那么如何定义一个 SetOptional 工具类型，支持把给定的 keys 对应的属性变成可选的
对应的使用示例如下所示：
```ts
type Foo = {
	a: number;
	b?: string;
	c: boolean;
}

// 测试用例
type SomeOptional = SetOptional<Foo, 'a' | 'b'>;

// type SomeOptional = {
// 	a?: number; // 该属性已变成可选的
// 	b?: string; // 保持不变
// 	c: boolean;
// }
```


在实现 SetOptional 工具类型之后，如果你感兴趣，可以继续实现 SetRequired 工具类型，利用它可以把指定的 keys 对应的属性变成必填的。对应的使用示例如下所示：
```ts
type Foo = {
	a?: number;
	b: string;
	c?: boolean;
}

// 测试用例
type SomeRequired = SetRequired<Foo, 'b' | 'c'>;
// type SomeRequired = {
// 	a?: number;
// 	b: string; // 保持不变
// 	c: boolean; // 该属性已变成必填
// }
```


**答案**SetOptional
```ts
type Format<T> = {
  [P in keyof T]: T[P]
}// 用来对交叉类型进行格式化处理, 否则会出现我们自定义的工具类型

type SetRequired<T, K extends keyof T> =
  Format<Partial<Pick<T, K>> & Pick<T, Exclude<keyof T, K>>>

  // Format<
  //   Partial<Pick<T, K>>  /* Partial<A>作用是把 A 所有类型变为可选 这里的A是后面从T中挑出所有属于K*/
  //   &
  //   Pick<T, Exclude<keyof T, K>> /* 从T中挑出不属于K的 这部分不需要变成可选*/
  // >
  // Format作用是把这个交叉类型整合到一起

// or
type SetRequired<T, K extends keyof T> = Format<{
  [P in K]-?: T[P]
} & Pick<T, Exclude<keyof T, K>>>
```


**答案**SomeRequired
```ts
// 同上
type SomeRequired =
  Format<Required<Pick<T, K>> & Pick<T, Exclude<keyof T, K>>>
```


## 4
`Pick<T, K extends keyof T>` 的作用是将某个类型中的子属性挑出来，变成包含这个类型部分属性的子类型。
```ts
interface Todo {
  title: string;
  description: string;
  completed: boolean;
}

type TodoPreview = Pick<Todo, "title" | "completed">;

const todo: TodoPreview = {
  title: "Clean room",
  completed: false
};
// 那么如何定义一个 ConditionalPick 工具类型，支持根据指定的 Condition 条件来生成新的类型，对应的使用示例如下：

interface Example {
	a: string;
	b: string | number;
	c: () => void;
	d: {};
}

// 测试用例：
type StringKeysOnly = ConditionalPick<Example, string>;
// => {a: string}

// 答案
type ConditionalPick<T, V> = {
  [K in keyof T as T[K] extends V ? K : never]: T[K];
}
// as 我理解成等于号 = ,后面是三目运算
// 如果K in keyof T  获取T所有键 并遍历
// 如果T[K]即属性的value值 符合V的约束的挑选出来
// 不符合的delete掉 变成never
```


## 5
定义一个工具类型 AppendArgument，为已有的函数类型增加指定类型的参数，新增的参数名是 x，将作为新函数类型的第一个参数。具体的使用示例如下所示：
获取函数的参数类型Parameters<function>
获取函数的返回值类型 ReturnType<function>
```ts
type Fn = (a: number, b: string) => number
type AppendArgument<F, A> = // 你的实现方式

type AppendArgument<F extends (...args: any) => any, A>
  = (x: A, ...args: Parameters<F>) => ReturnType<F>
// 或者
type AppendArgument<F, A> = F extends (...args: infer Args) => infer Return ?
  (x: A, ...args: Args) => Return : never

type FinalFn = AppendArgument<Fn, boolean>
// (x: boolean, a: number, b: string) => number
```


## 6
定义一个 NativeFlat 工具类型，支持把数组类型拍平（扁平化）。具体的使用示例如下所示：

```ts
type NaiveFlat<T extends any[]> = {
  [P in keyof T]: T[P] extends any[] ? T[P][number] : T[P]
}[number]

// 测试用例：
type NaiveResult = NaiveFlat<[['a'], ['b', 'c'], ['d']]>
// NaiveResult的结果： "a" | "b" | "c" | "d"
```
在完成 NaiveFlat 工具类型之后，在继续实现 DeepFlat 工具类型，以支持多维数组类型：
```ts

type DeepFlat<T extends any[]> = {
  [K in keyof T]: T[K] extends any[] ? DeepFlat<T[K]> : T[K]
}[number]

// 测试用例
type Deep = [['a'], ['b', 'c'], [['d']], [[[['e']]]]];
type DeepTestResult = DeepFlat<Deep>
// DeepTestResult: "a" | "b" | "c" | "d" | "e"
```



```ts
type RequiredByKeys<T, K = keyof T> = Omit<Omit<T, keyof T & K> & {
  [P in keyof T as P extends K ? P : never]-?: T[P]
}, never>
```

## 7.两数之和
包括定义加法 减法 获取数组除了第一个元素尾部 数组转联合类型
```ts
// ts => LeetCode第一题: 两数之和
type TwoSum<A extends number[], T extends number> =
  A['length'] extends 0
    ? false
    : Sub<T, A[0]> extends Arr2Union<Tail<A>>
      ? true
      : TwoSum<Tail<A>, T>

// 生成长度为 N 的数组Arr
type toArray<L, Arr extends any[] = []> =
  Arr['length'] extends L
    ? Arr
    : toArray<L, [...Arr, any]>

type Add<T extends number, K extends number> =
  [...toArray<T>, ...toArray<K>]['length']

type Sub<T extends number, K extends number> =
  toArray<T> extends [...toArray<K>, ...infer R]
    ? R['length']
    : never

type Tail<N extends any[]> = N extends [infer F, ...infer Tail] ? Tail : []

type Arr2Union<T extends any[]> = T[number]

type res = TwoSum<[1,2,3], 5>
```

## 实现链式调用,并且保存链式的key,value
```ts
// ============= Test Cases =============
declare const a: Chainable

const result1 = a
  .option('foo', 123)
  .option('bar', { value: 'Hello World' })
  .option('name', 'type-challenges')
  .get()

const result2 = a
  .option('name', 'another name')
  // @ts-expect-error
  .option('name', 'last name')
  .get()

const result3 = a
  .option('name', 'another name')
  .option('name', 123)
  .get()

type cases = [
  Expect<Alike<typeof result1, Expected1>>,
  Expect<Alike<typeof result2, Expected2>>,
  Expect<Alike<typeof result3, Expected3>>,
]

type Expected1 = {
  foo: number
  bar: {
    value: string
  }
  name: string
}

type Expected2 = {
  name: string
}

type Expected3 = {
  name: number
}


type Chainable<T = {}> = {
  // key在T中已经出现过吗?
  // 出现过的话 Value出现过吗? 出现过不加进去,没出现过,可以把key加进去
  // 如果key也没出现过,把key加进去
  option: <K extends string, V>(key: K extends keyof T ? V extends T[K] ? never : K: K, value: V) =>
  Chainable<Omit<T, K> & Record<K, V>>;
  get: () => T
}
```

## 判断接口中的某个属性
```ts
// ============= Test Cases =============
interface Cat {
  type: 'cat'
  breeds: 'Abyssinian' | 'Shorthair' | 'Curl' | 'Bengal'
}

interface Dog {
  type: 'dog'
  breeds: 'Hound' | 'Brittany' | 'Bulldog' | 'Boxer'
  color: 'brown' | 'white' | 'black'
}

type Animal = Cat | Dog;

LookUp<Animal, 'dog'> // Dog
LookUp<Animal, 'cat'> // Cat


type LookUp<U, T> = U extends {type: T} ? U : never;
```

## 首字母转换大小写
自带工具类型
```ts
/**
 * Convert string literal type to uppercase
 */
type Uppercase<S extends string> = intrinsic;

/**
 * Convert string literal type to lowercase
 */
type Lowercase<S extends string> = intrinsic;
```


## 元组转对象
```ts
type TupleToObject<T extends readonly any[]> = {
  [K in T[number]]: K
}

const tuple = ['tesla', 'model 3', 'model X', 'model Y'] as const

type cases = [
  Expect<Equal<TupleToObject<typeof tuple>, { tesla: 'tesla'; 'model 3': 'model 3'; 'model X': 'model X'; 'model Y': 'model Y' }>>,
]
```

## 使不报错
```ts
type User = {
  id: number;
  kind: string;
};

function makeCustomer<T extends User>(u: T): T {
  // Error（TS 编译器版本：v4.4.2）
  // Type '{ id: number; kind: string; }' is not assignable to type 'T'.
  // '{ id: number; kind: string; }' is assignable to the constraint of type 'T',
  // but 'T' could be instantiated with a different subtype of constraint 'User'.
  return {
    id: u.id,
    kind: 'customer'
  }
}
```

Textends User类型,可能包含User中没有的属性, 现在只返回了id和kind
1. 强制函数返回为User
2. return 里面加上...u


## 函数重载
本道题我们希望参数 a 和 b 的类型都是一致的，即 a 和 b 同时为 number 或 string 类型。当它们的类型不一致的值，TS 类型检查器能自动提示对应的错误信息。
```typescript
function f(a: string | number, b: string | number) {
  if (typeof a === 'string') {
    return a + ':' + b; // no error but b can be number!
  } else {
    return a + b; // error as b can be number | string
  }
}

f(2, 3); // Ok
f(1, 'a'); // Error
f('a', 2); // Error
f('a', 'b') // Ok
```

```ts
function f(a: string, b: string): string
function f(a: number, b: number): number
function f(a: string | number, b: string |number) {
  if (typeof a === 'string') {
    return a + ':' + b; // no error but b can be number!
  } else {
    return a + +b; // error as b can be number | string
  }
}



f(2, 3); // Ok
f(1, 'a'); // Error
f('a', 2); // Error
f('a', 'b') // Ok
```

## 指定属性设为可选
```ts

type Foo = {
	a: number;
	b?: string;
	c: boolean;
}

/**
 * 指定属性设为可选 其他属性不变
 */
type SetOptional<T, K extends keyof T> = Partial<Pick<T, K>> & Omit<T, K>

// 引申的一些, Partial, Omit, Pick排列组合
type SetOptionalOmit<T, K extends keyof T> = Pick<T, K> & Partial<Omit<T, K>>;
type SetRequired<T, K extends keyof T> = Omit<T, K> & Required<Pick<T, K>>;
type SetRequiredOmit<T, K extends keyof T> = Pick<T, K> & Required<Omit<T, K>>;
```

## 根据value的类型选属性
```ts
type ConditionalPick<T, V> = {
  // 遍历T的每个属性
  // 如果T[K]符合value类型, 取出K, 否则never
  [K in keyof T as T[K] extends V ? K : never]: T[K]
}
```
根据value的值排除属性
```ts
type OmitByType<T, U> = {
  [K in keyof T as T[K] extends U ? never : K]: T[K]
}
```

## 函数新增参数
```ts
type Fn = (a: number, b: string) => number
/**
 * 函数F中新增一个参数x, x类型为A
 */
type AppendArgument<F extends (...args: any[]) => any, A> = F extends (...args: infer args) => infer R ? ((x: A, ...args: args) => R) : never


// 或者使用官方的
// 获取函数参数
type Parameters<T extends (...args: any) => any> = T extends (...args: infer P) => any ? P : never;

// 获取函数返回值
type ReturnType<T extends (...args: any) => any> = T extends (...args: any) => infer R ? R : any;
```

## 数组扁平化
```ts
type NaiveFlat<T extends any[]> = T[number] extends any[] ? T[number][number] : T[number]

// 测试用例：
type NaiveResult = NaiveFlat<[['a'], ['b', 'c'], ['d']]>
// NaiveResult的结果： "a" | "b" | "c" | "d"

// 注意这里递归不能直接传T[number]进去,而是要infer一个P变量
// ts没有闭包?
type DeepFlat<T extends any[]> = T[number] extends Array<infer P> ? P extends any[] ? DeepFlat<P> : P : T[number]

// 测试用例
type Deep = [['a'], ['b', 'c'], [['d']], [[[['e']]]]];
type DeepTestResult = DeepFlat<Deep>
// DeepTestResult: "a" | "b" | "c" | "d" | "e"
```

## 只允许接受空对象
```ts
type EmptyObject = {
  // 即将所有的key都设为never
  [k in string | number | symbol]: never;
}
```

## 只取出限定范围内的key
```ts
type SomeType =  {
  prop: string
}

// 只取出限定范围内的key值和key对应的value
type OnlyTKeys<T, U extends T, K = keyof T> = {
  [k in keyof U]: k extends K ? U[k]: never;
}
// 更改以下函数的类型定义，让它的参数只允许严格SomeType类型的值
function takeSomeTypeOnly<T extends SomeType>(x: OnlyTKeys<SomeType, T>) { return x }

// 测试用例：
const x = { prop: 'a' };
takeSomeTypeOnly(x) // 可以正常调用

const y = { prop: 'a', addditionalProp: 'x' };
takeSomeTypeOnly(y) // 将出现编译错误
```

## 确保参数不是空数组
```ts
// 参数的第一项一定是T, 至少有一项,保证了不能为空
type NonEmptyArray<T> = [T, ...T[]]

const a: NonEmptyArray<string> = [] // 将出现编译错误
const b: NonEmptyArray<string> = ['Hello TS'] // 非空数据，正常使用
```

## 实现一个join
```ts
type JoinStrArray<Arr extends string[], Separator extends string> =
  Arr extends [infer A, ...infer B]
  ? `${A extends string ? A : ''}${
    // 要判断B是否还有元素
      B extends [string, ...string[]]
        ? `${Separator}${JoinStrArray<B, Separator>}`
        : ''
    }`
  : ''

// 测试用例
type Names = ["Sem", "Lolo", "Kaquko"]
// type Names = ["Sem"]
// type Names = []
type NamesComma = JoinStrArray<Names, ","> // "Sem,Lolo,Kaquko"
type NamesSpace = JoinStrArray<Names, " "> // "Sem Lolo Kaquko"
type NamesStars = JoinStrArray<Names, "⭐️"> // "Sem⭐️Lolo⭐️Kaquko"
```

## 实现trim
```ts
// type TrimLeft<S extends string> = S extends `${' '|'\n'|'\t'}${infer Rest}` ? TrimLeft<Rest> : S
type TrimLeft<V extends string> = V extends ` ${infer R}` ? TrimLeft<R> : V
type TrimRight<V extends string> = V extends `${infer R} ` ? TrimRight<R> : V
type Trim<V extends string> = TrimRight<TrimLeft<V>>

// 测试用例
type res = Trim<' semlinker '>
// 'semlinker'
```

## 实现Equal
```ts
/**
 * @see https://github.com/microsoft/TypeScript/issues/27024
 * @see https://stackoverflow.com/questions/68961864/how-does-the-equals-work-in-typescript/68963796#68963796
*/
type IsEqual<A, B> = (<T>() => T extends A ? 1 : 2 ) extends (<T>() => T extends B ? 1 : 2) ? true : false
```

## 实现Include
```ts
type IsEqual<X, Y> = (<T>() => T extends X ? 1 : 2) extends (<T>() => T extends Y ? 1 : 2) ? true : false

type Includes<T extends Array<any>, E> = T extends [infer A, ...infer R]
  ? IsEqual<A, E> extends true
    ? true
    : Includes<R, E>
  : false

type I0 = Includes<[], 1> // false
type I1 = Includes<[2, 2, 3, 1], 2> // true
type I2 = Includes<[2, 3, 3, 1], 1> // true
```

## 合并两个接口
```ts
type Merge<FirstType, SecondType> = {
  [K in keyof (FirstType & SecondType)]:
    K extends keyof SecondType
    ? SecondType[K]
    : K extends keyof FirstType ? FirstType[K] : never
}
```

## 去除不确定的索引类型
```ts
interface Foo {
  [key: string]: any;
  [key: number]: any;
  bar(): void;
}

type RemoveIndexSignature<T> = {
  // 利用extends反向来扩大范围 用as过滤掉string和number key
  [K in keyof T as string extends K ? never : number extends K ? never : K]: T[K]
}

type FooWithOnlyBar = RemoveIndexSignature<Foo>; //{ bar: () => void; }
```

## 去除readonly
```ts
type Foo = {
  readonly a: number;
  readonly b: string;
  readonly c: boolean;
};

type Mutable<T, Keys extends keyof T = keyof T> = {
  -readonly[K in Keys]: T[K]
} & Pick<T, Exclude<keyof T, Keys>>

const mutableFoo: Mutable<Foo, 'a'> = { a: 1, b: '2', c: true };

mutableFoo.a = 3; // OK
mutableFoo.b = '6'; // Cannot assign to 'b' because it is a read-only property.
```

## 检查是否是联合类型
```ts
type IsUnion<T, U = T> = T extends U? ([T,U] extends [U,T] ?  false: true) : never;

type I0 = IsUnion<string|number> // true
type I1 = IsUnion<string|never> // false
type I2 =IsUnion<string|unknown> // false
```

## 检查是否是never
```ts
type IsNever<T> = [T] extends [never] ? true : false;
type I0 = IsNever<never> // true
type I1 = IsNever<never | string> // false
type I2 = IsNever<null> // false
```

## 实现split
```ts
type Item = 'semlinker,lolo,kakuqo';

type Split<
	S extends string,
	Delimiter extends string,
> = S extends `${infer A}${Delimiter}${infer B}` ? [A, ...Split<B, Delimiter>] : [S]

type ElementType = Split<Item, ','>; // ["semlinker", "lolo", "kakuqo"]
```


## 实现ToPath, 读取.和[]
```ts
type Split<S, divide extends string> = S extends `${infer A}${divide}${infer Rest}` ? [A, ...Split<Rest, divide>] : [S]

type ToPath<S extends string> =
S extends '' ? [] :
S extends `${infer A}[${infer B}]${infer C}` ? [...ToPath<A>, B, ...ToPath<C extends `.${infer D}` ? D : C>] : Split<S, '.'>

type T1 = ToPath<'foo.bar.baz'> //=> ['foo', 'bar', 'baz']
type T2 = ToPath<'foo[0].bar.baz'> //=> ['foo', '0', 'bar', 'baz']
type T3 = ToPath<'foo.bar[0].baz'> //=> ['foo', 'bar', '0', 'baz']
type T4 = ToPath<'foo.bar.baz[0]'> //=> ['foo', 'bar', 'baz', '0']
type T5 = ToPath<'baz[0]'> //=> ['baz', '0']
type T6 = ToPath<'[0].foo[1][3].bar[2]'> //=> ['0','foo','1','3','bar','2']


type Test1 = Split<'foo.bar.zzz.', '.'>
type Test2 = Split<'foo bar zzz', ''>
type Test3 = Split<'foo bar zzz', ' '>
type Test4 = Split<'fobarzo', 'o'>
```

## 实现replaceAll
```ts

Equal<ReplaceAll<'foobar', 'bar', 'foo'>, 'foofoo'>
Equal<ReplaceAll<'foobar', 'bag', 'foo'>, 'foobar'>
Equal<ReplaceAll<'foobarbar', 'bar', 'foo'>, 'foofoofoo'>
Equal<ReplaceAll<'t y p e s', ' ', ''>, 'types'>
Equal<ReplaceAll<'foobarbar', '', 'foo'>, 'foobarbar'>
Equal<ReplaceAll<'barfoo', 'bar', 'foo'>, 'foofoo'>
Equal<ReplaceAll<'foobarfoobar', 'ob', 'b'>, 'fobarfobar'>
Equal<ReplaceAll<'foboorfoboar', 'bo', 'b'>, 'foborfobar'>
Equal<ReplaceAll<'', '', ''>, ''>

type ReplaceAll<S extends string, From extends string, To extends string> =
From extends ''
? S
: S extends `${infer F}${From}${infer L}`
  ? `${ReplaceAll<F, From, To>}${To}${ReplaceAll<L, From, To>}`
  : S
```

## 求对象的差集

ts中&和|操作恰恰相反
A & B得到两个对象的合集
A | B得到两个对象的交集
```ts
type Foo = {
  name: string
  age: string
}
type Bar = {
  name: string
  age: string
  gender: number
}
type Coo = {
  name: string
  gender: number
}

type cases = [
  Expect<Equal<Diff<Foo, Bar>, { gender: number }>>,
  Expect<Equal<Diff<Bar, Foo>, { gender: number }>>,
  Expect<Equal<Diff<Foo, Coo>, { age: string; gender: number }>>,
  Expect<Equal<Diff<Coo, Foo>, { age: string; gender: number }>>,
]

// keyof (A | B)得到交集的key
// Omit从合集中排除交集部分, 剩下的就是差集
type Diff<O, O1> = Omit<O & O1, keyof (O | O1)>
```

## ReturnType的使用
某个自定义hook返回的内容通过props传递给其他组件, props类型定义的时候可以使用
结合typeof
```ts
const searchParams = useSearchQuery();

// 其他组件的props类型
interface OtherProps {
  /** 使用ReturnType<typeof hooks> */
  searchParams: ReturnType<typeof useSearchQuery>;
}
```

## 限制数字的范围, 例如限制秒数只能取0-59
```ts
type Util<
  N extends number,
  Result extends Array<unknown> = [],
> = Result['length'] extends N
  ? Result
  : Util<N, [...Result, Result['length']]>

type Seconds = Util<60>[number] // 0 | 1 | 2 |...59
```