# 什么是node.js
node.js是一个基于v8引擎的js运行时环境
它使得js可以运行在浏览器以外的地方
相对于大部分服务端语言来说,nodejs有很大的不同,它采用了单线程,通过异步的方式来处理并发问题

# 浏览器内核
Gecko 早期NetScape Mozilla FireFox
Trident IE4-IE11
Webkit Safari
Blink Webkeit的一个分支 Chrime Edge

浏览器内核就是浏览器的排版引擎

## 浏览器工作流程

+ 为什么加载html都时候遇到js会暂停
因为js代码有可能会操作dom 导致浪费性能

## 常见js引擎
+ spidermonkey 第一款作者开发
+ chakra 微软开发 用于IE
+ javascriptCode webkit中引擎 apple开发
+ V8 谷歌开发 最强大的 chrome的js引擎

## webkit
+ webcore 负责HTMl解析 布局 渲染等工作
+ javascriptCode 负责解析js代码

## V8
c++编写的开源的高性能js和WebAssembly引擎 实现ECMAScript 和 WebAssenbly

+ parse模块
会将js代码转换为AST(asbtract syntax tree)
注意 如果函数没有被调用 是不会被转换为AST的

+ Ignition模块
会将AST转换为字节码
收集TurboFan优化需要的信息(比如函数参数的类型信息 有了类型才能真实的运算)
如果函数只执行一次 Igintion只会解释执行字节码

+ TurboFan模块
将字节码转换为CPU直接运行的 机器码
如果一个函数多次执行 会被标记为热点函数 会经过TurboFan直接转换为优化后的机器码 提高性能
如果函数的类型发生了变化(比如sum(num1, num2)) 参数传递了字符串
那么优化的机器码并不能直接执行 还需要反向优化为字节码 再重新转换为机器码

[v8](https://v8.dev/)


# 进程和线程
进程: 程序运行的环境(工厂)
线程: 实际进行运算的东西(工人)

`pslist -dmx <id>`查看该进程下的线程

# nvm
+ nvm管理node版本
当项目需要依赖不同node版本的时候,安装nvmNode环境管理
[nvm官网](https://github.com/nvm-sh/nvm)

README查看安装方式

`curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.2/install.sh | bash`
如果没有nvm
```bash
export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm
```


+ 如果下载node_modules没有跑起来,考虑下是不是安装依赖的node版本有问题
此时需要nvm切换node版本

+ 常用命令
```bash
nvm list 打印当前机器上的node版本
nvm ls ：打印出所有的版本
nvm install stable：安装最稳定的版本
nvm install v8.9.2 ： 安装node的8.9.2的版本
nvm uninstall v8.9.2 ： 删除node的8.9.2的版本
nvm install 12  安装node的12大版本里面最新的
nvm current ：当前使用的node版本
nvm use v8.9.2 ：将node改为8.9.2版本
nvm alias default 0.12.7：设置默认 node 版本为 0.12.7
```

+ nvm下载缓慢问题
切换为淘宝镜像源
`echo 'export NVM_NODEJS_ORG_MIRROR=https://npm.taobao.org/mirrors/node' >> ~/.zshrc`

然后要执行
`source ~/.zshrc`
使得`echo`生效


# node程序传递参数
在node 执行制定的js文件后可以传递有一些参数进去
`node index.js jrj ags=18`

node中的全局对象是`process`
process.argv中包含着传递的参数
arguments vector (vector类数组结构)

# 全局对象

## __dirname
每个模块中都有的, 在命令行交互中不可以使用
所在文件的所在目录的绝对路径

## __filename
每个模块中都有的, 在命令行交互中不可以使用

## exports
## module

## process

## console

## 定时器 计时器等

# javascript 模块化
## no_module
通过script标签引入
原生js没有模块化 变量容易被污染

通过立即执行函数来保护各个js文件中的代码,缺点是各个模块没法引入其他js文件中的变量
所以立即执行函数最后需要将外面可能引入的东西return 出去


*模块本身应当可以导出 也可以导入 同时各个模块之间不能相互污染*
# commonjs -- node中最重要的模块化规则

导出 exports module.module
导入 require

+ 使用require和exports导入导出的数据 共享引用地址
即一个中修改变量(修改exports对象) 另一个中也会随机发生改变

+ module.exports
每一个js模块都是Module的实例
简单来说
module.exports = exports = require('......')

本质上 node是通过module.exports在导出
如果修改了module.exports 那么exports对象就失去了意义

## require查找细节
+ 如果require的是核心模块 直接返回 并停止查找
+ 如果查找./  ../  /开头
没有后缀名的查找顺序
找没有后缀名的文件
找.js
找.json
找.node

+ 如果都没有找到
那就将刚刚的东西作为路径
找该路径下的index.js
找该路径下的index.json
找该路径下的index.node

+ 都没找到 不断向上遍历该路径下的node_module


## 模块加载过程
+ 模块第一次被引入的时候 里面的js代码会执行一次

而且是先执行引入文件中的js
后执行本文件的js

+ 如果模块被多次加载 会缓存 最终只会运行一次

+ 如果多个文件之间循环引用了 会怎么加载
Node会采用深度优先搜索来 从根文件作为根节点 向下dfs

## commonjs的缺点
模块的加载是同步的 只有等到对应的引入的模块加载完毕 当前文件代码才会被执行
这是因为服务器加载js文件是非常快的
对于浏览器就是不行 所以浏览器一般不适用commonJs模块化规则

## AMD 异步模块定义
已经很少用了


## CMD 通用模块定义
已经很少用了
异步加载模块 还吸收了commonJs的优点

## ES module
+ 导出的是值的引用 原文件的变量改了 引入的变量也会改
+ 编译时加载的 并且是异步的


## commonJs和ESModule的区别
commonJs是被加载的时候运行，esModule是编译的时候运行
commonJs输出的是值的浅拷贝，ESModule输出值的引用
commentJs具有缓存。在第一次被加载时，会完整运行整个文件并输出一个对象，拷贝（浅拷贝）在内存中。下次加载文件时，直接从内存中取值

# 常见内置模块
## path
用于对路径和模块进行处理 提供了很多好用的方法

例如在不同的系统中MAC linux windows路径使用不同的正反斜杠来作为文件路径
可能在MAC系统开发的项目 需要到windows上部署,所以不能使用字符串来做文件路径分隔符
`path.resolve`方法来拼接文件路径
```js
const path = require('path')
const base = '/User/jrj'
const name = 'a.txt'
const fileUrl = path.resolve(base, name)
```
拼接路径问题
```js
const base = '/User/xxx'
const file = '/jrj/yyy'
// 如果file也是/开头  会被认为是绝对路径 最终拼接出来的只有file
const fileUrl = path.resolve(base, file)
// 如果要两个路径拼接 使用./开头 或者第二个路径前面没有/

// 如果第二个路径是../开头 那么会被第一个路径最后一个目录去掉 直接变成前面的(.pop())
```

### path.resolve和path.join的区别
```js
// join只是简单的拼路径,也可以处理.和..
path.join('/foo', 'bar', 'baz/asdf', 'quux', '..');  // 返回: '/foo/bar/baz/asdf'
path.join('a', 'b', 'c') // return: "a/b/c"
path.join('/a', 'b', 'c') // return: "/a/b/c"
path.join('/a', '/b', 'c') // return: "/a/b/c"
path.join('/a', '/b', '/c') // return: "/a/b/c"


// resolve会进行解析的动作,从右往左直到解析到/开头的路径
// 如果resolve的第一个参数没有/ 就会加上绝对路径 当前目录所在路径
path.resolve('/a', 'b', 'c') // return: "/a/b/c"
path.resolve('/a', '/b', 'c') // return: "/b/c"
path.resolve('/a', '/b', '/c') // return: "/c"

// 如果想要拼接
path.resolve('/a', './b') // /a/b
```

获取文件后缀
```js
const path = require('path')
const sub = path.extname(filePath)
```

## fs
任何一个为服务器端服务的语言通常都有自己的文件系统
服务器需要将各种数据 文件放到不同的地方
用户数据大多数都是放在数据库中的
比如某些配置文件或者用户资源都是以文件的形式存储在操作系统上的

fs就是node的文件系统
node因此可以开发前端自动化脚本

- 文件写入
当需要持久化保存数据的场景,应该想到文件写入
  - 下载文件
  - 安装软件
  - 保存程序日志, 如git
  - 编辑器保存文件
```js
/**
 * 创建一个文件: 座右铭.txt
 * 写入一句话
 */

const fs = require('fs')
fs.writeFile('./座右铭.txt', '三人行则必有我师', err => {
  console.log('error :>> ', error);
})
// 如果使用fs.writeFileSync则是同步写入
// writeFileSync没有第三个异步回调函数参数
// 必须等待写完再执行下面的操作
```

+ 追加写入
```js
const fs = require('fs')
fs.appendFile('./座右铭.txt', '\n择其善者而从之,其不善者而改之', err => {
  if (err) {
    console.log('写入失败')
    return
  }
  console.log('写入成功');
})
```

- 流式写入
程序打开一个文件是需要时间的,流式写入可以减少打开关闭文件的次数
流式写入适用于大文件写入或者频繁写入的常见
wirteFile适合写入频率较低的场景
```js
const fs = require('fs')

// 创建写入流对象
const ws = fs.createWriteStream('./观书有感.txt')

// write
ws.write('半亩方塘一鉴开\r\n')
ws.write('天光云影共徘徊\r\n')
ws.write('问渠那得清如许\r\n')
ws.write('为有源头活水来\r\n')

ws.close()
```

- 文件读取

电脑开机
播放视频
查看图片
编辑器打开文件

```js
const fs = require('fs')

// 异步读取
fs.readFile('./观书有感.txt', (err,data) => {
  if(err){
    console.log('读取失败')
    return
  }
  console.log('读取成功');
  // buffer
  console.log(data);
  console.log(data.toString());
})

// 同步读取
const data = fs.readFileSync('./观书有感.txt')
console.log('同步先执行\r\n', data.toString());
```

- 流式读取
```js
const fs = require('fs')
// 创建读取流对象
const rs = fs.createReadStream('./bytedance.mp4')


// 绑定data事件
rs.on('data', chunk => {
  console.log(chunk)
  console.log(chunk.length) // 65535字节 => 64kb
  // 每次读取64kb
})

rs.on('end', () => {
  console.log('读取完成');
})
```

- 复制文件
第二种方式更好,流式读写占用的系统资源更少

```js
const fs = require('fs')
// 使用process查看内存使用情况
const process = require('process')

// 1
// const data = fs.readFileSync('./bytedance.mp4')
// fs.writeFileSync('./byte2.mp4', data)
// const sum = process.memoryUsage().rss/1024/1024
// console.log('sum :>> ', sum); // 286MB


// 2 流式读取与写入
const rs = fs.createReadStream('./bytedance.mp4')
const ws = fs.createWriteStream('./byte3.mp4')

rs.on('data', chunk => {
  ws.write(chunk)
})

rs.on('end', () => {
  const sum = process.memoryUsage().rss / 1024 / 1024
  console.log('sum :>> ', sum); // 63MB
})
```

- 重命名和移动
`fs.rename(oldPath, newPath, callback)`
`fs.renameSync(oldPath, newPath)`

```js
const fs = require('fs')

// 重命名
fs.rename('./座右铭.txt', './论语.txt', (err) => {
  if (err) {
    console.log('操作失败');
    return
  }
  console.log('操作成功');
})


// 文件移动
fs.rename('./论语.txt', '../Note/论语.txt', (err) => {
  if (err) {
    console.log('操作失败');
    return
  }
  console.log('操作成功');
})

// 文件删除
fs.unlink(path, callback)
fs.unlinkSync(path, callback)
fs.rmSync(path, callback)
```


- 文件夹操作
```js
const fs = require('fs')

// 创建文件夹
fs.mkdir('./新文件夹', err => {
  if(err){
    console.log('创建失败');
    return
  }
  console.log('创建成功');
})

// 递归创建文件
fs.mkdir('./a/b/c', {
  // 配置项: 递归创建文件夹,必须要加
  recursive: true
}, err => {
  if (err) {
    console.log('创建失败');
    return
  }
  console.log('创建成功');
})

const fs = require('fs')

// 读取文件夹
fs.readdir('./', (err, data) => {
  if(err){
    console.log('读取失败');
    return
  }

  console.log('读取成功', data);
  // [
  //   '.DS_Store',     'a',
  //   'byte2.mp4',     'byte3.mp4',
  //   'bytedance.mp4', 'index.html',
  //   'index.js',      'index.md',
  //   '新文件夹',      '观书有感.txt',
  //   '论语.txt'
  // ]
})

// 批量重命名
fs.readdir('./', (err, data) => {
  console.log('data :>> ', data);
  data.forEach((f, i) => {
    if (f[0] !== '.') {
      const newName = `0${i}${f}`
      fs.renameSync(`./${f}`, `./${newName}`)
    }
  })
})


// 删除文件夹
fs.rmdir('./新文件夹', err => {
  if (err) {
    console.log('删除失败');
    return
  }

  console.log('删除成功');
})


// 递归删除
fs.rmdir('./a', {
  recursive: true
}, err => {
  if (err) {
    console.log('删除失败');
    return
  }

  console.log('删除成功');
})

fs.rm(path, { recursive: true }, err => {})
```

- 查看资源信息
```js
const fs = require('fs')

fs.stat('./bytedance.mp4', (err, data) => {
  if (err) return
  console.log('查看成功 :>> ', data);
  console.log(data.isFile());
  console.log(data.isDirectory());
})
```

- fs路径
相对路径 下面两种等效
./index.js
index.js

- 练习1
把某项目下所有xxx.en-US.md文档删除, 注意是递归删除
```js
const fs = require('fs');
const path = require('path');

const handle = (p) => {
  fs.readdir(p, (e, data) => {
    if (e) {
      console.log('读取文件失败', e);
      return
    }
    data.forEach(f => {
      // 递归拼接新路径
      const filedir = path.join(p, f);
      fs.stat(filedir, (err, stat) => {
        if (err) {
          console.log('加载文件失败', err)
          return
        }
        if (stat.isFile()) {
          if (f.includes('.en-US.md')) {
            fs.unlinkSync(filedir)
          }
        } else if (f !== 'node_modules') {
          handle(filedir)
        }
      })
    })
  })
}

handle(path.resolve(__dirname))
```




**node API**
这些API大多提供多种调用方式
+ 同步操作文件: 代码会被阻塞 不会继续执行
+ 异步操作文件 代码不会被阻塞 需要传入回调函数 当获取到结果的时候 回调函数执行
+ 异步Promise操作文件 代码不会被阻塞 通过fs.promise调用方法 会返回一个promise 可以用try catch处理

### 文件描述符

## events模块
```js
const EventEmitter = require('events')
// 1.创建发射器
const emitter = new EventEmitter()

// 2.监听某一个事件 等等
emitter.on()
```

# glob
glob 在正则出现之前就有了，主要用于匹配文件路径，例如大名鼎鼎的 gulp 就使用了 glob 规则来匹配、查找并处理各种后缀的文件

在前端工程化的过程中，不可避免地会用 Node.js 来读取文件，例如想找到 src 目录下所有 js 和 jsx
`yarn add glob`

```js
const glob = require('glob')
const files = glob.sync('src/**/*.js{,x}')
```

glob 语法在命令行就支持，不需要安装任何依赖
例如创建 a1.js 到 a3.js、b1.js 到 b3.js 这 6 个文件
```js
$ touch {a,b}{1..3}.js
$ ls
a1.js a2.js a3.js b1.js b2.js b3.js
```

简单的说 glob实现了文件名的正则,可以在代码和命令行中使用

# 包管理工具详解

有时候需要npm run xxxx
但是`start` `stop` `test` `restart`这四个不需要run 可以直接`npm start`

## 版本规范
版本号前面符号
x.y.z
x表示不兼容的修改
y表示向下兼容的功能新增
z表示向下兼容的bug修复

^x.y.z 表示x不更新  y和z始终安装最新的
~x.y.z 表示x y不更新  z始终安装最新的

(简单的说 ~比^更保险安全)
不加符号就表示锁版本 不更新任何版本

安装一个^开头的包,比如时^1.1.0, 实际这个包的最新版本是1.2.3, 那真正安装的是其实是1.2.3这个版本
除非配置pckage-lock.json

## engines
package.json中的engines用于指定Node和NPM的版本号
在安装过程中会先检查engines版本 不符合就报错

甚至还可以指定所在的操作系统 但是很少使用

## npm install原理

### package-lock.json(类似的lock.json是干嘛的)
多人开发时依赖包安装的问题
看了上面版本号的指定后，我们可以知道，当我们使用了 ^ 或者 ~ 来控制依赖包版本号的时候 ，多人开发，就有可能存在大家安装的依赖包版本不一样的情况，就会存在项目运行的结果不一样。


我们举个例子：
假设我们中安装了 vue, 当我们运行安装 npm install vue -save 的时候，在项目中的package.json 的 vue 版本是  vue: ^3.0.0, 我们电脑安装的vue版本就是 3.0.0 版本，我们把项目代码提交后，过了一段时间，vue 发布了新版本 3.0.1，这时新来一个同事，从新 git clone 克隆项目，执行 npm install安装的时候，在他电脑的vue版本就是 3.0.1了，因为^只是锁了主要版本，这样我们电脑中的vue版本就会不一样，从理论上讲（大家都遵循语义版本控制的话），它们应该仍然是兼容的，但也许 bugfix 会影响我们正在使用的功能，而且当使用vue版本3.0.0和3.0.1运行时，我们的应用程序会产生不同的结果。


package-lock.json 是在 npm(^5.x.x.x)后才有，中途有几次更改
介绍
官方文档是这样解释的：package-lock.json 它会在 npm 更改 node_modules 目录树 或者 package.json 时自动生成的 ，它准确的描述了当前项目npm包的依赖树，并且在随后的安装中会根据 package-lock.json 来安装，保证是相同的一个依赖树，不考虑这个过程中是否有某个依赖有小版本的更新。
它的产生就是来对整个依赖树进行版本固定的（锁死）。
当我们在一个项目中npm install时候，会自动生成一个package-lock.json文件，和package.json在同一级目录下。package-lock.json记录了项目的一些信息和所依赖的模块。这样在每次安装都会出现相同的结果. 不管你在什么机器上面或什么时候安装。
当我们下次再npm install时候，npm 发现如果项目中有 package-lock.json 文件，会根据 package-lock.json 里的内容来处理和安装依赖而不再根据 package.json。

简单描述一下 package-lock.json 生成的逻辑。假设我们现在有三个 package，在项目 lock-test中，安装依赖A，A项目面有B，B项目面有C
```js
// package lock-test
{ "name": "lock-test", "dependencies": { "A": "^1.0.0" }}
// package A
{ "name": "A", "version": "1.0.0", "dependencies": { "B": "^1.0.0" }}
// package B
{ "name": "B", "version": "1.0.0", "dependencies": { "C": "^1.0.0" }}
// package C
{ "name": "C", "version": "1.0.0" }
```
在这种情况下 package-lock.json, 会生成类似下面铺平的结构
```js
// package-lock.json
{
  "name": "lock-test",
  "version": "1.0.0",
  "dependencies": {
    "A": { "version": "1.0.0" },
    "B": { "version": "1.0.0" },
    "C": { "version": "1.0.0" }
  }
}
```
如果后续无论是直接依赖的 A 发版，或者间接依赖的B, C 发版，只要我们不动 package.json, package-lock.json 都不会重新生成。

A 发布了新版本 1.1.0，虽然我们 package.json 写的是 ^1.0.0 但是因为 package-lock.json 的存在，npm i 并不会自动升级，
我们可以手动运行 npm i A@1.1.0 来实现升级。
因为 1.1.0 package-lock.json 里记录的 A@1.0.0 是不一致的，因此会更新 package-lock.json 里的 A 的版本为 1.1.0。
B 发布了新版本 1.0.1, 1.0.2, 1.1.0, 此刻如果我们不做操作是不会自动升级 B 的版本的，但如果此刻 A 发布了 1.1.1，虽然并没有升级 B 的依赖，但是如果我们项目里升级 A@1.1.1，此时 package-lock.json 里会把 B 直接升到 1.1.0 ,因为此刻^1.0.0的最新版本就是 1.1.0。
经过这些操作后 项目 lock-test 的 package.json 变成

```js
// package
lock-test{ "dependencies": { "A": "^1.1.0" }}
```

对应的 package-lock.json 文件
```js
{
  "name": "lock-test",
  "version": "1.0.0",
  "dependencies": {
    "A": { "version": "1.1.0" },
    "B": { "version": "1.1.0" },
    "C": { "version": "1.0.0" }
  }
}
```

这个时候我们将 B 加入我们 lock-test 项目的依赖, B@^1.0.0，package.json如下
{ "dependencies": { "A": "^1.1.0", "B": "^1.0.0" }}

我们执行这个操作后，package-lock.json 并没有被改变，因为现在 package-lock.json 里 B@1.1.0 满足 ^1.0.0 的要求
但是如果我们将 B 的版本固定到 2.x 版本, package-lock.json 就会发生改变
{ "dependencies": { "A": "^1.1.0", "B": "^2.0.0" }}


因为存在了两个冲突的B版本，package-lock.json 文件会变成如下形式
```js
{
  "name": "lock-test",
  "version": "1.0.0",
  "dependencies": {
    "A": {
      "version": "1.1.0",
      "dependencies": {
        "B": { "version": "1.1.0" }
      }
    },
  "B": { "version": "2.0.0" },
  "C": { "version": "1.0.0" }
  }
}
```

package-lock.json 的生成逻辑是为了能够精准的反映出我们 node_modules 的结构，并保证能够这种结构被还原。


# 实现自己的脚手架工具
(一般不由初中级前端开发 完全是开眼界)

## 自定义终端命令
0. 建立一个文件夹
1. index.js
```js
// hashbang 表示当前文件要用本机node来执行
// 这里用的相对路径 找到机器装的node
#!/usr/bin/env node
console.log('Hello World');
```

2. package.json
```js
// 在外层添加bin
// 添加我们想要指定的命令 以及对应执行的文件
{
  "name": "test-rj-cli",
  "version": "1.0.0",
  "description": "",
  "main": "index.js",
  "bin": {
    "jrj": "index.js"
  },
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "keywords": [],
  "author": "",
  "license": "ISC"
}
```

3. `npm link`
把命令添加到环境中(我理解是应该把整个包添加到全局中 即使在其他路径下输入jrj 也能显示helloworld 删除的话要-g)

4. 如果要删除该命令的话
`npm remove -g test-rj-cli`

# Buffer
计算机中所有的内容 文字图片音视频 最终都会使用二进制显示
js很少可以直接处理多媒体内容, Buffer类中存储的是二进制数据
可以把buffer看成一个存储二进制的数组
每个数组元素可以存储8位二进制数

一个类似数组的对象,表示固定长度的字节序列
专门用来处理二进制数据
大小固定,无法调整大小
每个大小是1个字节

## 创建buffer

```js
let buf = Buffer.alloc(10)
// 可能会包含旧内存数据
let buf_2 = Buffer.allocUnsafe(10)
let buf_3 = Buffer.from('hello')
let buf_3 = Buffer.from([1, 2, 3])
```

每个buffer元素是1字节数据
1字节 = 8bit
可以表示为两位16进制数字

## 溢出
单个buffer元素最多只有8位二进制,最多表示255
超过的话会舍弃高位

```js
const msg = 'Hello World'
const buffer = Buffer.from(msg)

const msg = '你好'
const buffer = Buffer.from(msg)
// 默认UTF-8编码 一个汉字占据3个字节
// <Buffer e4 bd a0 e5 a5 bd>

const msg = '你好'
const buffer = Buffer.from(msg, 'utf16le')
// 指定utf16编码 一个汉字就只占2个字节 (此时可能无法显示一些生僻字等等)
// <Buffer 60 4f 7d 59>
```

```js
// 创建8位buffer 每一位占据8个bit 一个字节
const buffer = Buffer.alloc(8)
```

创建buffer的时候 并不会频繁地向操作系统申请内存,会默认先申请一个8*1024字节大小的内存(8kb)

## buffer解码
```js
const msg = '你好'
const buffer = Buffer.from(msg)
buffer.toString()

const buffer = Buffer.from(msg, 'utf16le')
buffer.toString("utf16le")
// 编解码的规则要一致
```

## buffer和文件操作
```js
const fs = require('fs')

// 如果没有中间的encoding配置 默认读取出来的是buffer格式二进制
fs.readFile('./test.txt',{encoding: 'utf-8'}, (err, data) => console.log('data :>> ', data))

fs.readFile('./ironman.jpeg', (err, data) => {
  console.log('image :>> ', data);
  // 重新复制这个图片
  fs.writeFile('./another.png', data, () => {})
})
```

可以借助sharp库来对图片进行处理


# 事件循环和异步IO

浏览器中的EventLoop是根据HTML5定义的规范实现的 不同浏览器可能有不同实现
node中的EventLoop是用libuv实现的

对文件的操作都需要进行系统调用(操作系统的文件系统`)

## 阻塞IO
调用结果返回之前,当前线程处于阻塞态,调用线程只有在得到调用结果之后才会继续执行

## 非阻塞IO
调用执行之后,当前线程不会停止执行,只需要过一段时间来检查一下有没有结果返回即可

开发中的很多耗时操作都可以基于非阻塞调用
比如socket通信
文件IO操作

### 非阻塞IO的问题
为了确定是否读取到了完整的数据,需要频繁的轮询

libuv提供线程池,会通过轮询或者其他方式等待结果
获取到结果后,就可以将对应的回调放到时间循环中(某一个事件队列)
时间循环可以接管后续的回调工作,告诉js应用程序执行对应的回调函数



## 阻塞式也成为同步调用


# node事件循环
事件循环会不断从任务队列中取出对应的事件执行

## 事件循环阶段
一次完整的时间循环分成多个阶段
1. 定时器(timers) 本阶段执行已经被setTimeout() 和setInterval()的调度回调函数
2. 待定回调(pending callback) 对于某些系统操作(如TCP错误类型)执行回调 比如TCP链接时接收到econnrefused
3. idle, prepare 仅系统内部使用
4. 轮询 检索新的IO事件  执行与IO相关的回调
5. 检测 setImmediate()回调函数执行
6. 关闭的回调函数, 如socket.on('close', () => {})

## node的宏任务 微任务
宏任务: setTimeout, setInterval, IO事件, setImmediate, close事件
微任务: Promise.then, process.nextTck, queueMicrotask


## 任务队列
1. main script  同步代码   *浏览器也有*
2. nextTicks  process.nextTick(() => {})
3. other microtask 例如await之后的或者promise.then  *浏览器也有*
4. timers setTimeout   *浏览器也有*
5. setImmediate

# stream 流
连续字节的一种表现形式和抽象概念
流式可读的 也是可以写的

node中很多对象都是基于流实现的

http模块的Request和Response对象

所有的流都是EventEmitter实例

## 基本流类型
wriable
readable
duplex  可读可写
transform

### readable


# HTTP

## 创建服务
```js
const http = require('http')

// 创建服务对象
const server = http.createServer((request, response) => {
  response.end('Hello World')// 设置响应体
})

// 监听端口
app.listen(9999, () => console.log('服务器启动成功'))
```

## 提取HTTP报文
```js
const http = require('http')
const url = require('url')

// 创建服务对象
const server = http.createServer((request, response) => {
  // 获取请求方法
  console.log(request.method);
  // 获取请求url
  console.log(request.url); // 只包含路径和params参数

  let res = url.parse(request.url, true)
  console.log('res :>> ', res);

  // 获取http协议版本
  console.log(request.httpVersion); // 1.1
  // 获取请求头
  console.log(request.headers)
  response.end('Hello World')// 设置响应体
})

app.listen(9999, () => console.log('服务器启动成功'))
```
## 提取路径和查询字符串
```js
const http = require('http')

const server = http.createServer((req, res) => {
  // 实例化url对象
  const url = new URL(req.url, 'http://127.0.0.1')
  console.log(url)
  const { pathname, searchParams } = url

  res.end('url')
})

server.listen(9999, () => {})
```

## 搭建HTTP服务
支持get请求
```js
const http = require('http')

const server = http.createServer((req, res) => {
  // 获取请求的方法
  let { method } = req
  // 获取请求的url
  const { pathname } = new URL(req.url, 'http://127.0.0.1')
  res.setHeader('content-type', 'text/html;charset=utf-8')
  if (method === 'GET' && pathname === '/login') {
    res.end('登录')
  } else if (method === 'GET' && pathname === '/reg') {
    res.end('注册')
  } else {
    res.end('404 Not Found')
  }
})

app.listen(9999, () => console.log('服务器启动成功'))
```

## 设置HTTP响应报文
```js
const http = require('http')

const server = http.createServer((req, res) => {
  // 设置响应状态码
  res.statusCode = 203
  // 响应状态描述
  res.statusMessage = 'iloveyou'
  // 响应头
  res.setHeader('content-type', 'text/html;charset=utf-8')
  // 自定义响应头
  res.setHeader('myHeader', 'hello world')
  // 响应体
  res.write('love')
  res.write('love')
  res.write('love')
  res.write('love')
  res.end('test')
  // 只能有一个end
  // end可以为空
  // res.end()
})

app.listen(9999, () => console.log('服务器启动成功'))
```

- 设置响应为一个dom表格, 有样式显示
即设置响应为一个字符串(html)
```js
const http = require('http')

const server = http.createServer((req, res) => {
  res.end(`
  <!DOCTYPE html>
  <html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
      td {
        padding: 20px 40px;
      }
      table tr:nth-child(odd) {
        background: #aef;
      }
      table tr:nth-child(even) {
        background: #fcd;
      }
      table,td {
        border-collapse: collapse;
      }
    </style>
  </head>
  <body>
    <table border="1">
      <tr><td></td><td></td><td></td></tr>
      <tr><td></td><td></td><td></td></tr>
      <tr><td></td><td></td><td></td></tr>
      <tr><td></td><td></td><td></td></tr>
    </table>
    <script>
    const tds = document.querySelectorAll('td')
    tds.forEach(item => {
      item.onclick = () => {
        item.style.background = '#333'
      }
    })
  </script>
  </body>
  </html>
  `)
})

app.listen(9999, () => console.log('服务器启动成功'))
```

- 如何优化上述案例?
借助fs模块读不同类型的文件
```js
const http = require('http')
const fs = require('fs')

// 针对不同的请求文件类型设置不同请求
const server = http.createServer((req, res) => {
  let { pathname } = new URL(req.url, 'http://127.0.0.1')
  console.log('pathname', pathname)
  if (pathname === '/') {
    let html = fs.readFileSync(__dirname + '/test.html')
    res.end(html)
  } else if (pathname === '/test.css') {
    let css = fs.readFileSync(__dirname + '/test.css')
    res.end(css)
  } else if (pathname === '/test.js') {
    let js = fs.readFileSync(__dirname + '/test.js')
    res.end(js)
  } else {
    res.statusCode = 404
    res.end('<h1>404 not found</h1>')
  }

})

server.listen(9999, () => console.log('服务器启动成功'))
```

如果加载的html文件里面引用了其他地方的js文件,css文件, 图片等
浏览器会发多个请求依次请求这些资源


## 静态资源和动态资源
图片 视频 css js HTML 字体文件一般为静态资源

动态资源指内容经常更新的资源

## 电脑环境变量的作用
以qq为例, 在命令行中的命令首先会在当前路径下找有没有qq的可执行文件
如果没找到,就会到电脑的环境变量的路径中,如果找到了就执行,如果都没有找到就会报错'xx命令不是内部或外部的可执行文件'

即在任意地方的命令行都可以执行想要的文件



----------

# express

## 初体验
```js
// 导入express
const express = require('express')

// 创建应用对象
const app = express()

// 创建路由
app.get('/home', (req, res) => {
  res.end('hello express')
})

// post接口需要结合html的form来请求,直接在url中敲路径是GET请求
app.post('/login', (req, res) => {
  res.edn('post')
})

app.all('/test', (req, res) => {
  res.end('all')
})

// 可以用于兜底
app.all('*', (req, res) => {
  res.end('404 Not Found')
})

app.listen('9999', () => console.log('服务已经启动'))
```

## express路由
路由确定了应用程序如何响应客户端对特定端点的请求
一个路由由请求方法,路径和回调函数组成

app.<method>(路径, 回调函数)
all方法表示针对所有的方法,只要路由能够匹配,都会执行回调


## 获取请求报文参数
```js
const express = require('express')

const app = express()

app.get('/request', (req, res) => {
  console.log('req.method', req.method)
  console.log('req.url', req.url)
  console.log('req.path', req.path)
  console.log('req.query', req.query)
  console.log('req.httpVersion', req.httpVersion)
  console.log('req.headers', req.headers)
  console.log('req.ip', req.ip)
  // 获取
  console.log(req.get('host'))

  res.end('Hello Express')
})

app.listen('9999', () => console.log('服务已经启动'))
```

## 提取路由参数
```js
// 冒号加变量
app.get('/:id.html', (req, res) => {
  res.setHeader('content-type', 'text/html;charset=utf-8')
  // 此处id是变量
  console.log(req.params.id)
  res.end('Hello Express')
})
```

可以通过require导入json的内容
```js
const data = require('./data.json')
```
+ 小练习: 根据不同的id响应不同歌手的信息
```js
const express = require('express')
const app = express()

const dataSource = [
  {
    singer: '周杰伦',
    song: '七里香',
    id: '1'
  },
  {
    singer: '孙燕姿',
    song: '阴天',
    id: '2'
  },
  {
    singer: '林俊杰',
    song: '江南',
    id: '3'
  },
]

app.get('/singer/:id.html', (req, res) => {
  // 不设响应头中文乱码
  res.setHeader('content-type', 'text/html;charset=utf-8')
  const id = req.params.id

  const data = dataSource.find(({ id: dataId }) => dataId === id)
  if(!data){
    res.end('404 Not Found')
  }
  res.end(`
  <!DOCTYPE html>
  <html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
      h1 {
        color: #008c8c;
      }
    </style>
  </head>
  <body>
    <h1>${data.singer}</h1>
    <h2>${data.song}</h1>
  </body>
  </html>
  `)
})

app.listen('9999', () => console.log('服务已经启动'))
```

## 获取请求体设置
通过第三方包 body-parser
```js
const express = require('express')
const bodyParser = require('body-parser')
const path = require('path')

const app = express()

// 解析json请求体的中间件函数
const jsonParser = bodyParser.json()
// 请求querystring格式的中间件
const urlencodeMiddleware = bodyParser.urlencoded({extended: false})

app.get('/login', (req, res) => {
  // 响应文件内容
  res.sendFile(path.resolve(__dirname, 'test.html'))
})

app.post('/login', urlencodeMiddleware, (req, res) => {
  // 中间件函数执行完毕后会在req身上添加一个body属性
  console.log(req.body); // { username: 'fwafdwa', password: 'fdsafds' }
  // 获取用户名密码
  res.send('响应登录操作')
})

app.listen('9999', () => console.log('服务已经启动'))
```


## 响应设置(原生和express封装的api)
```js
const express = require('express')

const app = express()

app.get('/response', (req, res) => {
  // native node
  // res.statusCode = 404
  // res.statusMessage = 'love'
  // res.setHeader('xxx', 'yyy')
  // res.write('Hello Express')

  // res.end('response')

  // 支持链式调用
  // res.status(200)
  // res.set('aaa', 'bbb')
  // res.send('Hello Expressssssss')

  res.status(500).set('wefqfew', 'dd').send('Hello express链式调用')
})

app.listen(9999, () => console.log('服务器启动成功'))
```

```js
const express = require('express')
const app = express()

app.get('/a', (req, res) => {
  // 跳转响应重定向
  res.redirect('http://www.bilibili.com')
  // 下载响应
  res.download(_dirname + '/packahe.json')
  // JSON响应
  res.json({
    "xxx": "aaa"
  })
  // 文件响应
  res.sendFile(__dirname + '/xxx.html')
})

app.listen(9999, () => {})
```
当接口发送给localhost:9999/a的时候,响应状态码为302,并且相应头携带location属性

## 中间件设置
中间件本质是一个回调函数
中间件函数可以像路由的回调函数一样访问请求对象(request), 响应对象(response)
中间件可以获取请求报文的内容, 还能对响应做出一些处理

- 类型
1. 全局中间件
2. 路由中间件


### 全局中间件
每个请求到达服务端之后都会执行全局中间件函数

记录每个请求的url和ip地址, 避免在每个路由中都要编写同样的逻辑
```js
const fs = require('fs')
const express = require('express')
const path = require('path')

// 定义全局中间件函数
function recordLogMiddleware(req, res, next) {
  const { url, ip } = req
  // fs模块创建文件并写入url和ip
  fs.appendFileSync(path.resolve(__dirname, '../access.log'),
  // 加上时间变成日志
  `${new Date().toLocaleDateString()};\rurl:${url};\r\nip:${ip}\r`
  )

  // 调用next, 继续执行回调后续的操作,例如响应等等, 不然会卡在这
  next()
}

const app = express()

// 使用中间件函数
app.use(recordLogMiddleware);

app.get('/home', (req, res) => {
  res.send('主页')
})

app.get('/admin', (req, res) => {
  res.send('后台首页')
})

app.all('*', (req, res) => {
  res.send(`<h1>404 Not Found</h1>`)
})

app.listen('9999', () => console.log('服务已经启动'))
```


### 路由中间件
满足一定的路由规则之后才会执行路由中间件
```js
/**
 * 针对/admin或者/setting这样的请求,要求url必须携带code=521这样的参数,未携带则提示'暗号错误'
 */

const express = require('express')
const app = express()

// 路由中间件函数
function checkCodeMiddleware(req, res, next) {
  if (req.query.code === '521') {
    next()
  } else {
    res.send('暗号错误')
  }
}

// 路由中间件放在需要约束的请求中
app.get('/admin', checkCodeMiddleware, (req, res) => {
  res.send('后台首页')
})

app.get('/setting', checkCodeMiddleware, (req, res) => {
  res.send('后台设置')
})

app.get('/other', (req, res) => {
  res.send('其他请求')
})

app.listen('9999', () => console.log('服务已经启动'))
```

### 静态资源中间件
```js
const express = require('express')
const path = require('path')

const app = express()

// 静态资源中间件函数
app.use(express.static(path.resolve(__dirname, '../public')))
// localhost:9999/index.html
// 此时会在public目录中查找index.html内容并渲染
// 访问图片也一样
// localhost:9999/images.apple.svg


app.listen('9999', () => console.log('服务已经启动'))
```

index.html文件为默认打开的资源
`localhost:9999`不写参数也是打开public/index.html
所以网站首页一般写在public下面的index.html
如果public下有index.html
同时路由也有index.html
**谁写前面,优先执行谁**

+ 优先静态资源
```js
const express = require('express')
const path = require('path')
const app = express()
app.use(express.static(path.resolve(__dirname, '../public')))
app.get('/index.html', (req, res) => {
  res.send('我是动态路由的相应index.html')
})
app.listen(9999, () => console.log('服务器启动'))
```

+ 优先路由里面的内容
```js
const express = require('express')
const path = require('path')
const app = express()
app.get('/index.html', (req, res) => {
  res.send('我是动态路由的相应index.html')
})
app.use(express.static(path.resolve(__dirname, '../public')))
app.listen(9999, () => console.log('服务器启动'))
```
如果静态资源和路由规则同时存在,谁先匹配就响应谁

路由响应动态资源,静态资源中间件响应静态资源


# 防盗链
从网站上复制的图片地址不可以使用
```html
<!-- 无法显示图片 -->
<img src="https://respic.3d66.com/coverimg/cache/2d40/a2a743cd1ee31080527af68e80f3f701.jpg!medium-size-2-p?v=5196536">
```
- 原因: 图片所在的服务开启了防盗链功能
其他域名网站无法使用respic.3d66.com的静态资源

防止外部网站盗用网站资源

- 原理
请求头`refer`属性会携带请求网站的协议,域名和端口
服务器会通过refer属性来判断请求来源,如果不是满足条件的网站请求,就返回404

- 实现: 只允许127.0.0.1域名访问,不允许localhost域名访问
```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Document</title>
</head>
<body>
  <img src="http://127.0.0.1:9999/images/apple.svg" alt="">
</body>
</html>
```
```js
const express = require('express')
const path = require('path')
const app = express()
app.use((req, res, next) => {
  // 请求可能没有referer属性
  const referer = req.get('referer')
  if (referer) {
    // 检查请求头的refer
    const { hostname } = new URL(referer)
    if (hostname === '127.0.0.1') {
      next()
    }else {
      res.status(404).send(`404 NotFound`)
      return
    }
  } else {
    next()
  }
})
app.use(express.static(path.resolve(__dirname, '../public')))
app.listen(9999, () => console.log('服务器启动'))
```

# 路由模块化
将不同功能的路由写入不同的路由文件
- App

```js
const express = require('express')
const homeRouter = require('../routes/homeRouter')
const adminRouter = require('../routes/adminRouter')

const app = express()

app.use(homeRouter)
app.use(adminRouter)

app.all('*', (req, res) => {
  res.send(`<h1>404 Not Found</h1>`)
})

app.listen(9999, () => {
  console.log('服务器启动');
})
```

- homeRouter
```js
const express = require('express')
const router = express.Router()

// 创建路由规则
router.get('/home', (req, res) => {
  res.send('前台首页')
})

router.get('/search', (req, res) => {
  res.send('内容搜索')
})

module.exports = router
```

- adminRouter
```js
const express = require('express')
const router = express.Router()

router.get('/admin', (req, res) => {
  res.send('后台首页')
})

router.get('/setting', (req, res) => {
  res.send('设置')
})

module.exports = router
```

# 模版引擎
模版引擎是分离用户界面和业务数据的一种技术

```js
const ejs = require('ejs')
const fs = require('fs')

let you = '你'

const s = fs.readFileSync('src/ejs/01.txt').toString() // 我爱 <%= you %>
// 使用ejs渲染
let res = ejs.render(s, {you})
console.log('res', res)
```

## ejs列表渲染
<%=%> 带等号的时候意味着变量占用
- txt文件
```txt
<ul>
  <% xiyou.forEach(item => { %>
    <li><%= item %></li>
  <% })%>
</ul>
```
- js文件
```js
const ejs = require('ejs')
const fs = require('fs')
const path = require('path')

const xiyou = ['a', 'b', 'c']

const res = ejs.render(
  fs.readFileSync(path.resolve(__dirname, '02.txt')).toString(),
  {xiyou}
)

console.log('res', res)
```

## ejs条件渲染
实际上就是在<%  %> 中间写js代码
```js
/**
 * 通过变量判断
 * true 输出欢迎回归
 * false 输出登录
 */

const ejs = require('ejs')

let isLogin = false

const res = ejs.render(`
  <% if(flag) { %>
    <span>欢迎回归</span>
  <% } else {%>
    <p>登录</p>
  <% } %>
`, {flag: isLogin}) // 注意这里变量的对应关系,属性名是ejs中使用的, 属性值是代码中的变量

console.log('res', res)
```

## express中使用ejs

```js
const express = require('express')
const path = require('path')

const app = express()

app.set('view engine', 'ejs')
// 设置模版文件的存放位置
app.set('views', path.resolve(__dirname, 'views'))

app.get('/home', (req, res) => {
  let title = 'Hello World'
  //render接受两个参数 1. 模版文件名 2.数据
  // home.ejs
  res.render('home', {title})
})

app.listen(9999, () => console.log('服务器启动'))
```

- home.ejs
```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Document</title>
  <style>
    h2 {
      color: #008c8c;
    }
  </style>
</head>
<body>
  <h2>
    <%= title %>
  </h2>
</body>
</html>
```

# express-generator
快速创建一个应用基础的骨架, 类似脚手架 快速搭建出代码的模版骨架
`express -e <文件名>`
`cd 文件名`
`pnpm i`
`pnpm start` 实际上执行的是bin里面的www文件


## router
```js
var express = require('express');
// 用于处理文件上传
const formidable = require('formidable')
var router = express.Router();
const path = require('path')

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

// 显示网页的表单
router.get('/portrait', (req, res) => {
  // 加载 portrait.ejs静态资源文件
  res.render('portrait')
})

router.post('/portrait', (req, res) => {
  const form = formidable({
    multiples: true,
    // 设置上传文件的保存路径
    uploadDir: path.resolve(__dirname, '../public/images'),
    // 保持文件后缀
    keepExtensions: true,
  });

  form.parse(req, (err, fields, files) => {
    if (err) {
      next(err);
      return;
    }
    // 服务器保存该图片的访问url
    let url = `/images/${files.portrait.newFilename}`
    // res.json({ fields, files });
    res.send(url)
  });
})

module.exports = router;

```
## 文件上传报文
- 模版文件
```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>文件上传</title>
  <style>
    h1 {
      color: #008c8c;
    }
  </style>
</head>
<body>
  <h1>文件上传</h1>
  <!-- 文件上传必须使用这个属性设置:  enctype="multipart/form-data" -->
  <!-- 否则上传文件只有key-value的名称 -->
  <!-- action属性表示触发的路由 -->
  <form action="/portrait" method="post" enctype="multipart/form-data">
    用户名: <input type="text" name="username"><br>
    头像: <input type="file" name="portrait"><br>
    <hr>
    <button>提交</button>
  </form>
</body>
</html>
```

# 会话控制
## cookie
### 添加cookie
通过res.cookie(key, value, [option])设置cookie
即响应给客户端的cookie,请求头为set-cookie 由客户端保存, 下次请求的时候,客户端带上服务器收到cookie会知道请求来自哪个客户端

如果不设置option,浏览器关闭销毁
如果设置了option, 例如maxAge, 那么maxAge到达之前都不会销毁cookie, 关闭浏览器也不会
maxAge到了之后, 浏览器请求不再携带cookie

### 删除cookie
res.clearCookie(key)
例如用户退出登录的时候删除cookie

### 读取cookie
const cookieParser = require('cookie-parser)
app.use(cookieParser())

然后可以通过req.cookies来获得

## session

