## ES5_Object对象方法扩展

### Object.create(object, descriptors)

作用：以指定对象为 原型 创建新的对象，为新的对象指定新的属性，并对属性进行描述

value：指定值

writeable：当前属性值是否可修改，默认为false

configurable：当前属性是否可删除，默认为false

enumerable：当前属性能否用for in枚举，默认为false

```js
var obj1 = {
    name: "蔡徐坤",
    age: "20"
}
var obj2 = Object.create(obj1, {
    height: {
        value:180,
        writable:true,
        configurable:true,
        enumerable:true
    }
})
console.log(obj2)
for (var key in obj2) {
    console.log(key)
}
```

### Object.defineProperties(object, descriptors)

Object.defineProperties() 方法直接在一个对象上定义新的属性或修改现有属性，并返回该对象。

get：用来获取当前属性值的回调函数，获取扩展属性值时自动调用

set：修改当前属性值时触发的回调函数，并且实参为修改后的值

存取器属性：setter用来存值，getter用来取值

```js
    var obj = {
        firstname: "ABC",
        lastname: "DEF"
    }
    Object.defineProperties(obj, {
        fallname: {
            get:function () {
                console.log("get")
                return this.fallname = this.firstname + this.lastname
            },
            set:function (data) {
                this.firstname = data
                console.log("set" + data)
            }
        }
    })
    console.log(obj.fallname)
    obj.fallname = "123"
    console.log(obj.fallname)

```


### 对象本身的两个方法get set
get propertyName() {}
set propertyName() {}

```js
var flag = 100
var obj = {
    name:"abc",
    age:18,
    get weight() {
        return flag
    },
    set weight(data) {
        flag = data
        return flag
    }
}
console.log(obj.weight)
obj.weight = 130
console.log(obj.weight)
```

## 安全整数
javascript能够表示的整数范围在（-2^53,2^53）
Number.isSafeInteger()判断一个数是否落在安全书范围内

# promise
> 前言：
> 本部分写于2021.09
> 校对、更新于2022.02


--------


# 第一章 绪论

## 区别实例对象与函数对象
实例对象：new关键字产生的对象，称为实例对象，简称为对象

函数对象：函数是一种对象，将函数作为对象使用，称为函数对象

括号左边是函数
.左边是对象
函数也是一种对象
```js
  function Fn() {   // Fn函数 
  }
  const fn = new Fn() // Fn是构造函数  fn是实例对象(简称为对象)
  console.log(Fn.prototype) // Fn是函数对象
  Fn.call({}) // Fn是函数对象
  $('#test') // jQuery函数
  $.get('/test') // jQuery函数对象
```


## 两种回调函数
自定义的函数，自己没有调用，最终执行的就是回调函数
### 同步回调
立即执行，完全执行完了才结束，不会放到回调队列中

```js
  // 1. 同步回调函数
  // 按顺序从上到下执行
  // const arr = [1, 3, 5]
  arr.forEach(item => { // 遍历回调, 同步回调函数, 不会放入列队, 一上来就要执行完
    console.log(item)
  })
  console.log('forEach()之后')
```


### 异步回调
不会立即执行 会放到回调队列中等待执行

```js
  // 2. 异步回调函数
  setTimeout(() => { // 异步回调函数, 会放入队列中等待执行
    console.log('timout callback()')
  }, 0)
  console.log('setTimeout()之后')
```


## 错误Error
### 错误类型
Error 所有错误的父类型
ReferenceError 引用的变量不存在
TypeError 数据类型不正确的错误
RangeError 数据值不在允许的范围之内
SyntaxError 语法错误


错误不处理下面的代码不执行
```js
  // ReferenceError: 引用的变量不存在
        // console.log(a) // ReferenceError: a is not defined

  // TypeError: 数据类型不正确的错误
        // let b
        // // console.log(b.xxx) // TypeError: Cannot read property 'xxx' of undefined
        // b = {}
        // b.xxx() // TypeError: b.xxx is not a function

  // RangeError: 数据值不在其所允许的范围内
        // function fn() {
        //   fn()
        // }
        // fn() // RangeError: Maximum call stack size exceeded
        // 超过能调用的次数限制

  // SyntaxError: 语法错误
        // const c = """" // SyntaxError: Unexpected string
```
### 错误处理
捕获错误
try ... catch
```js
try{
  let a;
  console.log(a.xxx);
}
catch(error){
  console.log(error);
}
// 这里的error是对象
```
抛出错误
throw error
```js
  // 抛出错误: throw error
  function something() {
    if (Date.now()%2===1) {
      console.log('当前时间为奇数, 可以执行任务')
    } else { // 如果时间是偶数抛出异常, 由调用来处理
      throw new Error('当前时间为偶数无法执行任务')
    }
  }
```
### 错误信息
message 错误信息
stack属性 函数调用栈记录信息



-----------------



# 第二章 promise的理解和使用
## promise是什么
是js中进行异步编程的新的解决方案（旧的是纯回调函数）

1、从语法上看promise是一个构造函数

2、从功能上说promise对象是用来封装一个异步操作并获取结果

### 状态改变
pending变为resolved

pending变为rejected

只有这两种，且一个promise对象只能改变一次
无论成功 还是失败，都会有一个结果数据

成功的结果数据一般称为value
失败的结果数据一般称为reason


### 基本流程
1、new Promise()
传一个函数作为参数，函数中启动异步任务
pending状态

2、执行异步操作
成功执行resolve() promise对象变为resolved状态
失败执行reject() promise对象变为rejected状态

3、.then
回调onResolve()

.then/catch回调onRejected()

4、返回一个新的promise对象


```js
  // 1、创建新的promise
  const p = new Promise((resolve, reject) => {//执行器函数
    // 2、执行异步任务
    setTimeout(() => {
      const time = Date.now()// 如果当前时间是偶数代表成功，否则失败
      // 3.1成功 调用resolve(value)
      if (time % 2 === 0) {
        resolve('成功的，value=' + time)
      }
      else {
        // 3.2失败 调用reject(reason)
        reject('失败的，reason='+ time)
      }
    }, 1000)
  })

  p.then(
    value => {
      // 接收得到的成功的value数据  onResolved
      console.log('成功的回调',value);
    },
    reason => {
      // 接收得到的失败的reason数据  onRejected
      console.log('失败的回调',reason);
    }
  )
```

## 为什么使用promise
纯回调形式的异步操作必须先指定回调函数再执行异步任务

promise执行器函数是`同步回调`，先执行异步任务，再指定回调函数

1、相对于纯回调函数指定回调函数的形式更加的灵活


灵活：
    启动异步任务 => 返回promise对象 => 给promise对象绑定回调函数（甚至可以在异步任务得到结果之后再指定回调函数）

纯回调: 候车，人（指定回调）必须在车（异步结果）到来之前等待

promise: 拿快递，人（指定回调）可以在快递（异步结果）到之后或者之前去等待

2、支持链式调用，解决回调地狱
什么是回调地狱？回调函数嵌套调用，涉及到多个异步操作，前后耦合性大，异常处理麻烦

promise可以从上到下通过`.then()`实现链式调用，清晰明了，异常可以穿透到最后的`.catch`

回调地狱的最终解决方案async await


## 如何使用promise
### API
```js
  1. Promise构造函数: Promise (excutor) {}
      excutor函数: 同步执行  (resolve, reject) => {}
      resolve函数: 内部定义成功时我们调用的函数 value => {}
      reject函数: 内部定义失败时我们调用的函数 reason => {}
      说明: excutor会在Promise内部立即同步回调,异步操作在执行器中执行

  2. Promise.prototype.then方法: (onResolved, onRejected) => {}
      onResolved函数: 成功的回调函数  (value) => {}
      onRejected函数: 失败的回调函数 (reason) => {}
      说明: 指定用于得到成功value的成功回调和用于得到失败reason的失败回调
            返回一个新的promise对象

  3. Promise.prototype.catch方法: (onRejected) => {}
      onRejected函数: 失败的回调函数 (reason) => {}
      说明: then()的语法糖, 相当于: then(_, onRejected)

  4. Promise.resolve方法: (value) => {}
      value: 成功的数据或promise对象
      说明: 返回一个成功/失败的promise对象

  5. Promise.reject方法: (reason) => {}
      reason: 失败的原因
      说明: 返回一个失败的promise对象

  6. Promise.all方法: (promises) => {}
      promises: 包含n个promise的数组
      说明: 返回一个新的promise, 只有所有的promise都成功才成功, 只要有一个失败了就直接失败


      const p1 = Promise.resolve(1)
      const p2 = Promise.resolve(2)
      const p3 = Promise.resolve(3)

      const pAll = Promise.all([p1,p2,p3])

      pAll.then(
        value => {console.log('成功',value);},
        reason => {console.log('失败',reason);}
      )
      // 成功  [1, 2, 3]


      const p1 = Promise.resolve(1)
      const p2 = Promise.reject(2)
      const p3 = Promise.reject(3)

      const pAll = Promise.all([p1,p2,p3])

      pAll.then(
        value => {console.log('成功',value);},
        value => {console.log('失败',value);}
      )
      // 失败   2


  7. Promise.race方法: (promises) => {}
      promises: 包含n个promise的数组
      说明: 返回一个新的promise, 第一个完成的promise的结果状态就是最终的结果状态

```

```js
语法糖
  4. Promise.resolve()
    const p1 = new Promise((resolve,reject) => {
      resolve(1)
    })
    // 等价于
    const p1 = Promise.resolve(1)
```
<!-- 没有痛苦就没有快乐 -->



### .then 和.catch
在.then()中主要就是看回调函数的个数和顺序，
一个就默认是成功回调，在前面的就是成功的回调
且先看then()

.catch()中只接受失败的回调
和直接执行.then(_, () => {})一样
也会返回一个新的promise实例

```js
  const p = Promise.reject(666)
  p
    .then(
      null,
      reason => {console.log('失败1',reason);}
    )
    .catch(
      reason => {console.log('失败2',reason);}
    )

// 失败1  666

--------------

  p
    .then(
      // 只有一个函数是作为onResolved()成功回调
      reason => {console.log('失败1',reason);}
    )
    .catch(
      reason => {console.log('失败2',reason);}
    )

// 失败2  3

--------------
  p
    .then(
      value => {console.log('成功1',value);},
      reason => {console.log('失败1',reason);}
    )
    .catch(
      reason => {console.log('失败2',reason);}
    )

// 失败1  3

```


----------------------



### 关键问题
1.	如何改变promise的状态?
  (1)执行器函数里面resolve(value): 如果当前是pendding就会变resolved
  (2)执行器函数里面reject(reason): 如果当前是pendding就会变rejected
  (3)抛出异常: 如果当前是pendding就会变为rejected
    ```js
      const p = new Promise((resolve, reject)=> {
        setTimeout(() => {
          // resolve('成功')
          // reject('失败的数据')
          throw 404
          // 抛出错误也会变为rejected状态
          // reason就是抛出的错误
        }, 2000)
      })
      p.then(
        reason => {
          console.log(reason);// 404
        }
      )
    ```

2. 一个promise指定多个成功/失败的回调函数都会调用吗？
   当pending状态改变为对应状态时都会调用
    ```js
    const p = new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve('成功')
        // reject('失败的数据')
        // throw 404
        // 抛出错误也会变为rejected状态
        // reason就是抛出的错误
      }, 2000)
    })
    p.then(
      value => {
        console.log(value);
      },
      reason => {
        console.log(reason);
      }
    )
    p.then(
      value => {
        console.log(value);
      },
      reason => {
        console.log(reason);
      }
    )

    // 成功
    // 成功
    ```


3. 改变promise状态和指定回调函数谁先谁后?
    *等快递：可以先去等快递（状态改变），也可以等快递到了再去拿（回调处理）*
    (1)都有可能, 正常情况下是先指定回调再改变状态, 但也可以先改状态再指定回调
    (2)如何先改状态再指定回调?
      *如何在快递到了再去拿?*
      ①在执行器中直接调用resolve()/reject() (同步执行)
      *等快递到了*
      ②延迟更长时间才调用then() （加定时器函数）
      *延长更多时间再去拿*
    (3)什么时候才能得到数据?
      *什么时候才能拿到快递？*
      ①如果先指定的回调, 那当状态发生改变时, 回调函数就会调用, 得到数据
      *如果人先到了，那么当快递到了，人就可以拿*
      ②如果先改变的状态, 那当指定回调时, 回调函数就会调用, 得到数据
      *如果快递先到了，那么人什么时候到，什么时候拿到*


4. promise的执行器函数里面的函数（resolve()/reject()）是同步执行的，
   .then本身是同步执行的，但是里面的成功/失败的处理回调函数都是`异步`执行的
   ```js
    const p = new Promise((resolve, reject) => {
      console.log(11);
      // 状态只能改一次，改完就不可以再改变
      resolve(1)
      console.log(22);
      reject(2)
    })
    p.then(
        // then本身是同步执行的
        // then里面的回调函数是异步的
        value => {
          console.log(value);
        },
        reason => {
          console.log(reason);
        }
    )
    console.log('体系外的同步');
    // 11 22 '体系外的同步' 1
   ```


5.	`.then()`会返回一个新的promise对象
`promise.then()`返回的新promise的结果状态由什么决定?
  (1)简单表达: 由then()指定的回调函数执行的结果决定
  (2)详细表达:
      ①如果抛出异常, 新promise状态变为`rejected`, `reason`为抛出的异常
      ②如果返回的是非promise的任意值, 新promise状态变为resolved, value为返回的值(例如返回数字)
      ③如果返回的是另一个新promise, 此promise的结果就会成为新promise的结果
      4 如果then没有传函数或者写了一个字符串,那么会忽略这一个then

  (3) 实例 Promise.then返回值 会返回一个新的promise实例
  1. 如果then没有传函数,则返回then前面的promise的结果
  ```js
  const p1 = Promise.resolve(1)
  const p2 = Promise.reject(1)

  // 注意then里面没有函数
  const p3 = p1.then() // 成功 1
  const p4 = p2.then() // 报错 没有catch

  setTimeout(console.log,0, p3)
  setTimeout(console.log,0, p4)
  ```
  2. 如果then里面有空函数 返回成功的undefined, 有返回值且不是promise,then都返回Promise.resolve包裹的这个不是promise的值

  ```js
  const p1 = Promise.resolve(1)

  const p2 = p1.then(() => {
    return Promise.reject('bar')
  })

  setTimeout(console.log, 0, p2) // 成功的bar
  ```

  3. 返回的是promise实例,则整个then返回的promise状态由前面这个promise状态决定


```js
  new Promise((resolve, reject) => {
    // 状态只能改变一次，且改变之后无法再改
    resolve(1)
    // 所以下面这一行无效
    reject(2)
  }).then(
    value => {// 执行成功回调函数 无返回值 默认返回undefined
      console.log('onResolved1()', value)
    },
    reason => {
      console.log('onRejected1()', reason)
    }
  ).then(
    value => {// 此回调函数根据上一个.then()返回的promise对象状态决定 执行成功 但是返回了一个undefined
      console.log('onResolved2()', value)
    },
    reason => {
      console.log('onRejected2()', reason)
    }
  )

  // 1  undefined
```

```js
  new Promise((resolve, reject) => {
    // resolve(1)
    reject(2)
  }).then(
    value => {
      console.log('onResolved1()', value)
    },
    reason => {
      // 执行失败回调
      console.log('onRejected1()', reason)
      // 返回了一个非promise的值
      return 66
    }
  ).then(
    // 因为上面的then中的回调返回的是非promise的值
    // 因此此处promise状态是 resolved,value就是上面回调返回的 66
    value => {
      console.log('onResolved2()', value)
    },
    reason => {
      console.log('onRejected2()', reason)
    }
  )
  // onRejected 2 onResolved 66
```


6. promise如何串连多个操作任务?
  (1)promise的`then()返回一个新的promise`
  (2)通过then的链式调用串连多个同步/异步任务
  ```js
    new Promise((resolve, reject) => {
      setTimeout(() => {
        console.log("执行任务1(异步)")
        resolve(1)
      }, 1000);
    }).then(
      value => {
        console.log('任务1的结果: ', value)
        console.log('执行任务2(同步)')
        return 2
      }
    ).then(
      value => {
        console.log('任务2的结果:', value)

        return new Promise((resolve, reject) => {
          // 启动任务3(异步)
          setTimeout(() => {
            console.log('执行任务3(异步))')
            resolve(3)
          }, 1000);
        })
      }
    ).then(
      value => {
        console.log('任务3的结果: ', value)
      }
    )
  ```



7. promise异常传/穿透?
   (1)当使用promise的then链式调用时, 可以在最后指定处理失败的回调`.catch()`
   (2)前面任何操作出了异常, 都会传到最后失败的回调中处理

```js
  new Promise((resolve, reject) => {
    // resolve(1)
    reject(1)
  }).then(
    value => {
      console.log('onResolved1()', value)
      return 2
    }
  ).then(
    value => {
      console.log('onResolved2()', value)
      return 3
    }
  ).then(
    value => {
      console.log('onResolved3()', value)
    }
  ).catch(
    reason => {
      console.log('onRejected',reason);
    }
  )
  // onRejected 1
```
当前面的执行出现错误，不是从第一个promise一下子到最后的catch()中
会发生异常穿透
等价于：
```js
    new Promise((resolve, reject) => {
      // resolve(1)
      reject(1)
    }).then(
      value => {
        console.log('onResolved1()', value)
        return 2
      },
      reason => {
        throw reason
      }
    ).then(
      value => {
        console.log('onResolved2()', value)
        return 3
      },
      reason => {
        throw reason
      }
    ).then(
      value => {
        console.log('onResolved3()', value)
      },
      reason => {
        throw reason
      }
    ).catch(
      reason => {
        console.log('onRejected', reason);
      }
    )
```
注意，除了`throw reason` 抛出异常
也等价于 `reason => Promise.reject(reason)`
都是传递失败状态

箭头函数单条语句不加return，则默认返回单条语句
所以`throw reason`需要在`{}`,而`reason => Promise.reject(reason)`不需要
就是返回一个promise作为这一级.then()的返回对象

8. 中断promise链
   (1)当使用promise的then链式调用时, 如果中断, 便不再调用后面的回调函数
   (2)办法: 在回调函数中返回一个`pendding`状态的promise对象
   `return new Promise(() => {})`
   因为没有成功或失败的状态，不会继续传递或者失败的状态
   从而实现终中断promise链条

<!-- 妙啊 -->

## .finally
该方法不管成功失败都会执行

```js
const p1 = Promise.resolve(1)

const p2 = p1.finally()
const p2 = p1.finally(() => {})
const p2 = p1.finally(() => Promise.resolve())
const p2 = p1.finally(() => Promise.resolve(2))
const p2 = p1.finally(() => '')
const p2 = p1.finally(() => Error('error'))

// 所有上面的返回值都是Pormise<resolved>  1

// 如果返回的是待定的期约,或者显示返回了一个拒绝期约,就和返回值保持一致
const p3 = p1.finally(() => new Promise(() => {})) // pending的promise实例
const p3 = p1.finally(() => Promise.reject()) // 失败状态的undefined
```

----------------


# 第三章 手写（自定义promise）
## 函数纯享版
```js
/**
 * 自定义myPromise函数模块
 *
 * ES5  IIFE
 *
 * Isaac_Jeeen 2021_09_27
 */

(function (Window) {
  const PENDING = 'pending'
  const RESOLVED = 'resolved'
  const REJECTED = 'rejected'

  /**
   * 执行器函数
   * @param {Function} executor
   */
  function myPromise(executor) {
    // 将myP实例对象保存起来
    const self = this
    // 状态 初始值为pending
    self.status = PENDING
    // 用于存储结果数据的属性
    self.data = undefined
    // 保存.then中的回调 （如果有的话）
    // 数组中每个元素的结构{onResolved(){} , onRejected(){}}
    self.callback = []

    function resolve(value) {
      // 如果状态不是pending，就不能改
      if (self.status !== PENDING) return
      // 改状态
      self.status = RESOLVED
      // 保存value
      self.data = value
      // 如果有回调，异步执行 onResolved函数
      if (self.callback.length > 0) {
        self.callback.forEach(callbackObj => {
          setTimeout(() => {// 【关联1】
            callbackObj.onResolved(value)
          })
        });
      }
    }

    function reject(reason) {
      // 如果状态不是pending，就不能改
      if (self.status !== PENDING) return
      // 改状态
      self.status = REJECTED
      // 保存value
      self.data = reason
      // 如果有回调，异步执行 onRejected函数
      if (self.callback.length > 0) {
        self.callback.forEach(callbackObj => {
          setTimeout(() => {// 【关联1】
            callbackObj.onRejected(reason)
          })
        });
      }
    }

    try {
      executor(resolve, reject)
    } catch (error) {
      // 如果执行器抛出异常，myPromise变为失败状态
      reject(error)
    }
  }

  /**
   * myPromise 实例对象 的then()
   * @param {Function} onResolved 处理成功的回调
   * @param {Function} onRejected 处理失败的回调
   * 返回一个新的myPromise 实例对象
   */
  myPromise.prototype.then = function (onResolved, onRejected) {
    // 保存myPromise实例的this
    const self = this
    // 指定回调函数的默认值(必须是函数)
    // 不指定成功的回调就直接传递值
    onResolved = typeof onResolved === 'function' ? onResolved : value => value
    // 不指定失败的回调就抛出异常， 实现异常穿透
    onRejected = typeof onRejected === 'function' ? onRejected : reason => { throw reason }

    // 返回一个新的myPromise对象 这个就是【B】
    return new myPromise((resolve, reject) => {
      /**
       * 将重复的代码抽离、复用
       * @param {function} callback
      */
      function handle(callback) {
        // 1.如果抛出异常，返回失败状态的myPromise reason是error
        // 2.如果回调函数return不是myPromise对象，那么返回成功状态的myPromise，value就是回调函数返回值
        // 3.如果回调return就是myPromise对象，那么返回的myPromise根据return的myPromise结果
        try {
          // 接收上一个myPromise的回调返回的结果
          const res = callback(self.data)
          if (res instanceof myPromise) {
            // 3.如果回调return就是myPromise对象，那么返回的myPromise根据return的myPromise结果

            // 这里的res就是上一个myPromise对象的 成功回调 返回的新myPromise【A】对象
            // 直接调用then来获得返回的【A】的状态，以便return出一个状态一样的myPromise 【B】

            // --------------------
            res.then(
              // 注意！！
              // 这里的resolve和reject都是调用 return 出来的这个【B】的
              // 因为要让【B】成功！！！
              // 让一个myPromise对象成功的方法就是调用它的resolve()
              // 前一个value是【A】（res）的成功的value  传过来的
              // 后一个value是传给【B】的！！因为要让【B】成功，所以调用【B】的resolve(),并传入【A】的value
              // reject同理，【B】按照【A】的状态来
              value => resolve(value),
              reason => reject(reason)
            )
            // -----------------------

            // 上面的横线内等价于

            // -----------------------
            // res.then(resolve, reject)
            // ------------------------

            // 解释
            // 原来的value => resolve(value),
            // 就是一个函数，把【A】的value传给【B】的resolve()
            // 等价于直接用【B】的resolve当回调，且不要执行
            // 因为回调函数三原则（自定义的函数，自己没有调用，最终执行）

            /**
             * （1）
             *      const fun = () => {}
             *      div.onclick = event => fun(event)
             * 等价于
             * （2）
             *      div.onclick = fun
             * 千万不要写成
             * div.onclick = fun()
             * 这样的话就等于把fun执行的结果给点击了
             *
             * 回调应该是一个函数！！！而不是执行结果
             * 回调函数不用我们执行而是绑定的事物执行，回调函数三原则（自定义的函数，自己没有调用，最终执行）
             * 对于（1），回调函数是箭头函数，然后等它被执行完，结果还是一个函数执行结果[fun(event)]
             *
             * 对于（2），回调函数就是fun（没执行），等它被执行的时候形参就是绑定的点击事件的event
             */
          } else {
            // 2.如果回调函数return不是myPromise对象，那么返回成功的myPromise【B】，value就是返回值
            // 如果上一个的回调函数return不是myPromise对象
            // 那么返回成功的myPromise【B】，value就是返回值
            // 直接调用返回的myPromise【B】对象的resolve
            // 把上一个myPromise【A】对象的成功回调返回的结果传进去
            resolve(res)
          }
        } catch (error) {
          // 1.如果抛出异常，返回失败的myPromise reason是error
          // 因为要返回一个失败的myPromise，所以调用返回的myPromise【B】的reject()
          // 因为要失败的信息，所以error传入当前myPromise【B】的reject(error)
          reject(error)
        }
      }

      // 如果是先指定的回调，后修改状态
      // 那么status就是pengding，可以将成功失败回调函数收集起来
      // 等待resolve | reject修改完状态后再调用
      if (self.status === PENDING) {
        self.callback.push({
          // 这里只是保存函数，异步在执行器函数里面（先指定回调，后有结果）见【关联1】
          // 因为后有结果，所以状态是pending，保存回调在上面执行
          // 这个OnResolved | onRejected函数只是自定义的一个函数，名字这么叫方便上面执行器执行
          onResolved() {
            // 虽然只是保存函数，但是需要根据回调函数的结果改变新myPromise【B】状态
            handle(onResolved)
          },
          onRejected() {
            handle(onRejected)
          }
        })
      }
      // 如果是修改完状态再指定回调函数
      // 已经有状态后且状态已经是成功的（resolved）
      // 就可以直接异步执行成功回调
      // onResolved(that.data) 此处是因为已经有状态了， that.data已经被更新
      else if (self.status === RESOLVED) {
        // .then中的两个回调函数本身需要异步执行
        setTimeout(() => {
          handle(onResolved)
        })
      } else {
        // 同理
        // self.status === rejected
        setTimeout(() => {
          handle(onRejected)
        })
      }
    })
  }

  /**
   * myPromise 实例对象 的catch()
   * @param {Function} onRejected 只有失败的回调
   * 返回一个新的myPromise 实例对象
   */
  myPromise.prototype.catch = function (onRejected) {
    // return then返回的新promise,只有失败回调，异常穿透
    return this.then(undefined, onRejected)
  }

  /**
   * myPromise 构造函数 的resolve()
   * @param {*} value 指定成功状态的值
   * 返回指定结果的成功的myPromise
   */
  myPromise.resolve = function (value) {
    // 返回一个新的myPromise对象，可以是成功的，也可以是失败的
    // 成功的情况
      // 1.1收到是是非myPromise值，成功value就是这个value
      // 1.2收到的是成功的myPromise，成功value就是接受的myPromise的成功value
    // 失败的情况
      // 2.1收到一个失败的myPromise，失败reason就是接受的myPromise的失败reason

    return new myPromise((resolve, reject) => {// 【B】
      if (value instanceof myPromise) {
        // 接受的参数是myPromise【A】
        // 主要就是根据传进来的【A】的状态，自适应更改return出去的新myPromise【B】状态
        // 见.then()的实现

        // 这里的resolve | reject都是 【B】的
        // 成功就延续成功的value,失败就延续失败的reason
        // 成功就resolve(),失败就reject(),参数都是上一个myPromise【A】自动传进去的
        value.then(resolve, reject)
        // 等价于
        // value.then(
        //   v => resolve(v),
        //   r => reject(r)
        // )
      } else {
        // 接受参数非myPromise
        // 直接让这个返回的myPromise对象成功
        resolve(value)
      }
    })
  }

  /**
   * myPromise 构造函数 的reject()
   * @param {*} reason 指定失败状态的值
   * 返回指定结果的失败的myPromise
   */
  myPromise.reject = function (reason) {
    // 只接受 非 myPromise （一般值）
    // 返回一个新的只能是失败的myPromise对象
    // 将失败的原因传递下去
    return new myPromise((resolve, reject) => {
      // 因为要返回一个失败的myPromise对象
      // 让一个myPromise对象失败的途径就是调用它的reject()
      // 同时将失败的原因reason传递进去
      reject(reason)
    })
  }

  /**
   * myPromise 构造函数 的all()
   * @param {myPromise[]} arr
   * 返回一个myPromise 实例对象 （只有当所有参数全部成功时才成功）
   */
  myPromise.all = function (arr) {
    // 返回一个myPromise对象,所有的都成功才会返回成功的myPromise对象，value为成功值的数组
    // 有一个失败就返回失败的myPromise，reason是失败的那个reason
    // 凡是跟形参myPromise【A】状态有关的，步骤如下
    // 1、return new myPromise
    // 2、执行器函数里面执行形参相关的.then，因为.then就是能获得成功/失败回调的结果并根据结果返回一个新myPromise【B】
    // 3、为了让return的新的myPromise状态与之相关，要调用return的这个myPromise【B】的resolve/reject

    let count = 0 // 用来计数成功的myPromise数量
    const len = arr.length// 形参中myPromise的数量
    // 为什么要这么定义数组？为了保证数组中传入值的顺序正确！push()无法正确记录异步 | 同步的顺序
    const res = new Array(len) // 用来保存所有形参中成功的myPromise的数据
    return new myPromise((resolve, reject) => {
      // 遍历获取形参中所有myPromise的结果
      arr.forEach((p, index) => {
        // 包裹一层myPromise的作用是防止参数数组中元素不是myPromise
        // 这样不管传入的是不是myPromise，是什么状态的myPromise
        // myPromise.resolve()方法都会继承下来
        // 成功的情况
          // 1.1收到是是非myPromise值，成功value就是这个value
          // 1.2收到的是成功的myPromise，成功value就是接受的myPromise的成功value
        // 失败的情况
          // 2.1收到一个失败的myPromise，失败reason就是接受的myPromise的失败reason
        myPromise.resolve(p).then(
          v => {
            // 只要进入此处就说明有一个myPromise成功了
            count++
            // 为什么要借助index来保存成功的值？为了保证数组中传入值的顺序正确！push()无法正确记录异步 | 同步的顺序
            res[index] = v
            if (count === len) {
              // 说明形参中所有myPromise全部是成功态
              resolve(res)
            }
          },
          reject
        )
      })
    })
  }

  /**
   * myPromise 构造函数 的race()
   * @param {myPromise[]} arr
   * 返回一个myPromise实例对象，结果由第一个 完成的 myPromise决定
   */
  myPromise.race = function (arr) {
    return new myPromise((resolve, reject) => {
      arr.forEach(p => {
        // 包裹一层myPromise的作用是防止参数数组中元素不是myPromise
        // 这样不管传入的是不是myPromise，是什么状态的myPromise
        // myPromise.resolve()方法都会继承下来
        // 成功的情况
          // 1.1收到是是非myPromise值，成功value就是这个value
          // 1.2收到的是成功的myPromise，成功value就是接受的myPromise的成功value
        // 失败的情况
          // 2.1收到一个失败的myPromise，失败reason就是接受的myPromise的失败reason
        myPromise.resolve(p).then(
          value => {
            // 第一个 异步完成的 且成功的直接返回成功的myPromise
            resolve(value)
          },
          reason => {
            // 第一个 异步完成的 且失败的直接返回失败的myPromise
            reject(reason)
          }
        )
      })
    })
  }

  // 向外暴露
  window.myPromise = myPromise
})(window)
```


## class版本
```js
/**
 * 自定义myPromise函数模块
 *
 * class
 *
 * Isaac_Jeeen 2021_09_27
 */

(function (Window) {
  const PENDING = 'pending'
  const RESOLVED = 'resolved'
  const REJECTED = 'rejected'

  class myPromise {
    constructor(executor) {
      const self = this
      self.status = PENDING
      self.data = undefined
      self.callback = []

      resolve(value) {
        if (self.status !== PENDING) return
        self.status = RESOLVED
        self.data = value
        if (self.callback.length > 0) {
          self.callback.forEach(callbackObj => {
            setTimeout(() => {
              callbackObj.onResolved(value)
            })
          });
        }
      }

      reject(reason) {
        if (self.status !== PENDING) return
        self.status = REJECTED
        self.data = reason
        if (self.callback.length > 0) {
          self.callback.forEach(callbackObj => {
            setTimeout(() => {
              callbackObj.onRejected(reason)
            })
          });
        }
      }

      try {
        executor(resolve, reject)
      } catch (error) {
        reject(error)
      }
    }

    static resolve(value) {
      return new myPromise((resolve, reject) => {
        if (value instanceof myPromise) {
          value.then(resolve, reject)
        } else {
          resolve(value)
        }
      })
    }

    static reject(reason) {
      return new myPromise((resolve, reject) => {
        reject(reason)
      })
    }

    then(onResolved, onRejected) {
      const self = this
      onResolved = typeof onResolved === 'function' ? onResolved : value => value
      onRejected = typeof onRejected === 'function' ? onRejected : reason => { throw reason }
      return new myPromise((resolve, reject) => {
        const handle = (callback) => {
          try {
            const res = callback(self.data)
            if (res instanceof myPromise) {
              res.then(resolve,reject)
            } else {
              resolve(res)
            }
          } catch (error) {
            reject(error)
          }
        }
        if (self.status === PENDING) {
          self.callback.push({
            onResolved() {
              handle(onResolved)
            },
            onRejected() {
              handle(onRejected)
            }
          })
        } else if (self.status === RESOLVED) {
          setTimeout(() => {
            handle(onResolved)
          })
        } else {
          setTimeout(() => {
            handle(onRejected)
          })
        }
      })

    }

    catch(onRejected) {
      return this.then(undefined, onRejected)
    }

    static all(arr) {
      const values = new Array(arr.length)
      let count = 0
      return new myPromise((resolve, reject) => {
        arr.forEach((p, index) => {
          myPromise.resolve(p).then(
            value => {
              values[index] = value
              count++
              if (count === arr.length) {
                resolve(values)
              }
            },
            reason => {
              reject(reason)
            }
          )
        })
      })
    }

    static race = function (arr) {
      return new myPromise((resolve, reject) => arr.forEach(p => myPromise.resolve(p).then(resolve,reject)))
    }

  }
  window.myPromise = myPromise
})(window)
```



## 测试
```html
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <title>测试自定义的promise</title>
  <script src="./Promise_class.js"></script>
</head>

<body>
  <script>
    const p = new myPromise((resolve,reject) => {
      console.log(Date.now());
      setTimeout(() => {
        // resolve(1)
        reject(1)
      },1000)
    })

    p.then(
      value => {
        console.log(Date.now());
        console.log('成功1',value);
      },
      reason => {
        console.log(Date.now());
        console.log('失败1',reason);
      }
    )


    p.then(
      value => {
        console.log(Date.now());
        console.log('成功2',value);
      },
      reason => {
        console.log(Date.now());
        console.log('失败2',reason);
      }
    )
  </script>


  <!-- <script>
    const p = new myPromise((resolve, reject) => {
      setTimeout(() => {
        // resolve(1)
        reject(1)
        console.log('reject已经改变状态');
      }, 1000)
    }).then(
      value => {
        console.log('成功1', value);

      },
      reason => {
        console.log('失败1', reason);
        // throw 4
        return new myPromise((resolve, reject) => reject(666))
      }
    ).then(
      value => {
        console.log('成功2', value);

      },
      reason => {
        console.log('失败2', reason);

      }
    ).catch(
      reason => {
        console.log('失败3', reason);
      }
    )
  </script> -->

  <!-- <script>
    const p1 = myPromise.resolve(1)
    const p2 = myPromise.resolve(new myPromise((resolve, reject) => reject(2)))
    const p3 = myPromise.resolve(new myPromise((resolve, reject) => resolve(2)))
    p1.then(

      value => {
        console.log('成功', value);
      }
    )

    p2.then(

      value => {
        console.log('成功', value);
      },
      reason => {
        console.log('失败',reason);
      }
    )

    p3.then(

      value => {
        console.log('成功', value);
      },
      reason => {
        console.log('失败',reason);
      }
    )
  </script> -->


  <!-- <script>
    const p1 = myPromise.resolve(1)
    const p2 = myPromise.resolve(2)
    const p3 = myPromise.resolve(3)
    // const p4 = myPromise.resolve(4)
    const p4 = new myPromise((resolve,reject) => {
      setTimeout(() => {
        resolve(4)
      },2000)
    })

    const res = myPromise.all([p4,p2,p3,p1])
    res.then(
      v => {
        console.log('成功',v);
      },
      r => {
        console.log('失败',r);
      }
    )
    // console.log(res);
  </script> -->

  <!-- <script>
    const p1 = Promise.reject(1)
    const p2 = Promise.resolve(p1)
    const p3 = Promise.resolve(p2)

    p3.then(
      v => {
        console.log('成功',v);
      },
      r => {
        console.log('失败',r);
      }
    )


  </script> -->

   <!-- <script>
    new myPromise((resolve, reject) => {
      // resolve(1)
      reject(2)
      setTimeout(() => {
        reject(4)
      }, 1000)
    }).then(
      v => { console.log('成功', v) },
      r => {
        console.log('失败', r);
        return new myPromise((resolve, reject) => {
          reject(5)
        })
      }
    ).then(
      v => { console.log('成功', v); },
      r => {
        console.log('失败', r);
        throw -1
      }
    ).then(
      v => {},
    ).catch(
      r => {
        console.log('失败', r);
      }
    ).then(
      v => {
        console.log('成功',v);
        // 中断promise
        return new myPromise(() => {})
      },
      v => {
        console.log('失败',v);
      },
    ).then(
      v => {
        console.log('成功',v);
      },
      v => {
        console.log('失败',v);
      },
    )
  </script> -->

  <!-- <script>
    // const p1 = myPromise.resolve(1)
    // const p2 = myPromise.resolve(myPromise.resolve(2))
    const p3 = myPromise.reject(3)
    const p4 = myPromise.resolve(myPromise.reject(4))

    // const p1 = Promise.resolve(1)
    // const p2 = Promise.resolve(Promise.resolve(2))
    // const p3 = Promise.reject(3)
    // const p4 = Promise.resolve(Promise.reject(4))

    // p1.then(
    //   v => {
    //     console.log('p1成功',v);
    //   },
    //   r => {
    //     console.log('p1失败',r);
    //   }
    // )
    // p2.then(
    //   v => {
    //     console.log('p2成功',v);
    //   },
    //   r => {
    //     console.log('p2失败',r);
    //   }
    // )
    p3.then(
      v => {
        console.log('p3成功',v);
      },
      r => {
        console.log('p3失败',r);
      }
    )
    p4.then(
      v => {
        console.log('p4成功',v);
      },
      r => {
        console.log('p4失败',r);
      }
    )
  </script> -->

  <script>
    // const p1 = myPromise.resolve(1)
    // const p2 = myPromise.resolve(myPromise.resolve(2))
    // const p3 = myPromise.reject(3)
    // // const p4 = myPromise.resolve(myPromise.reject(4))
    // const p5 = new myPromise((y,n) => {
    //   setTimeout(() => {
    //     y(5)
    //   },2500)
    // })


    const p1 = new myPromise((y, n) => {
      setTimeout(() => {
        y(1)
      }, 3000)
    })
    const p2 = new myPromise((y, n) => {
      setTimeout(() => {
        y(2)
      }, 2800)
    })
    const p3 = new myPromise((y, n) => {
      setTimeout(() => {
        y(3)
      }, 100)
    })

    const p4 = new myPromise((y, n) => {
      y(4)
    })

    // const pAll = myPromise.all([p5, p1, p2])
    // pAll.then(
    //   v => {
    //     console.log('成功', v);
    //   },
    //   r => {
    //     console.log('失败', r);
    //   }
    // )

    const pRace = myPromise.race([p1, p2, p3, p4,9])
    pRace.then(
      v => {
        console.log('成功', v);
      },
      r => {
        console.log('失败', r);
      }
    )
  </script>

</body>

</html>
```
------------------


# async与await

1. async + 函数
  函数的返回值为promise对象
  promise状态由函数执行的结果决定

2. await 表达式
  可以把await理解成promise.then()的回调，用来获取promise的状态
  await右侧的表达式一般为promise对象, 但也可以是其它的值
  如果表达式是promise对象, await返回的是promise处理的结果
  如果表达式是其它值, 直接将此值作为await的返回值

3. 注意:
  await必须写在async函数中, 但async函数中可以没有await
  如果await后面的promise失败了, 就会抛出异常, 需要通过try...catch来捕获处理

async放在函数的前面，函数执行返回一个promise对象
```js
  async function fun () {
    // return 21
    throw 7878
  }

  const res = fun()
  console.log(res);// Promise实例对象

  res.then(
    value => {
      console.log('成功',value);
    },
    reason => {
      console.log('失败',reason);
    }
  )
```


await右侧是promise对象
得到的 结果是promise的状态
```js
  function fun2() {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve(666)
      }, 2000)
    })
  }

  async function fun3() {
    const res = await fun2()
    console.log(res);
  }
  fun3()
```
await右边的不是promise对象时
得到的结果就是这个值本身



# 异步中的宏队列和微队列

`队列：先进先出`

微队列和宏队列都是用来保存微任务和宏任务的
微任务：promise回调，mutation回调是

宏任务：dom回调，ajax回调，定时器回调

同一种队列任务按照先后顺序执行
不同的队列微队列优先
每次准备执行宏任务之前，都必须保证所有的微任务都被执行

同步 =>  微队列  =>  宏队列

```js
  setTimeout(() => { // 会立即放入宏列队
    console.log('timeout callback1()')
  }, 0)
  setTimeout(() => { // 会立即放入宏列队
    console.log('timeout callback2()')
  }, 0)
  Promise.resolve(1).then(
    value => { // 会立即放入微列队
      console.log('Promise onResolved1()', value)
    }
  )

  Promise.resolve(2).then(
    value => { // 会立即放入微列队
      console.log('Promise onResolved2()', value)
    }
  )


  // Promise onResolved1() 1
  // Promise onResolved2() 2
  // timeout callback1()
  // timeout callback2()
```


每次取出宏任务时，都要将微任务一个接一个取出来执行（微先宏后）
```js
  setTimeout(() => { // 会立即放入宏列队
    console.log('1')
    Promise.resolve(3).then(
      value => { // 会立即放入微列队
        console.log('2', value)
      }
    )
  }, 0)
  setTimeout(() => { // 会立即放入宏列队
    console.log('3')
  }, 0)
  Promise.resolve(1).then(
    value => { // 会立即放入微列队
      console.log('4', value)
    }
  )
  Promise.resolve(2).then(
    value => { // 会立即放入微列队
      console.log('5', value)
    }
  )

  // 4 5 1 2 3
```
-----------------

# promise习题 | 事件循环练习题
同 微 宏

1、从上到下
2、promise的执行器同步执行
3、promise的.then()异步执行，且回调是微任务
4、定时器回调是宏任务

## 1、
```js
  setTimeout(()=>{
    console.log(1)// 宏
  },0)
  Promise.resolve().then(()=>{
    console.log(2)// 微
  })
  Promise.resolve().then(()=>{
    console.log(4)// 微
  })
  console.log(3)// 同

  //  3 2 4 1
```

## 2、
```js
  setTimeout(() => {
    console.log(1)// 宏
  }, 0)
  new Promise((resolve) => {
    console.log(2)// 同
    resolve()
  }).then(() => {
    console.log(3)// 异步 微
  }).then(() => {
    console.log(4)// 异步 微
  })
  console.log(5)// 同

  // 2 5 3 4 1
```



## 3、
```js
  const first = () => (new Promise((resolve, reject) => {
    // 执行器同步
    console.log(3)// 同
    let p = new Promise((resolve, reject) => {
      // 执行器同步
      console.log(7)// 同
      setTimeout(() => {
        console.log(5)// 宏
        resolve(6)// 无效
      }, 0)
      resolve(1)// 同步，且状态只改一次
    })
    resolve(2)
    p.then((arg) => {
      console.log(arg)// 微
    })
  }))

  // first()执行返回Promise对象
  first().then((arg) => {
    console.log(arg)// 微
  })
  console.log(4)// 同步

  // 3 7 4 1 2 5
```
### 4、

```js
  setTimeout(() => {
    console.log("0")// 宏
  }, 0)

  new Promise((resolve, reject) => {
    console.log("1")// 同
    resolve()
  }).then(() => {
    // 异步 微
    console.log("2")// 同
    new Promise((resolve, reject) => {
      console.log("3")// 同
      resolve()
    }).then(() => {
      // 这个4执行完了才代表有了状态，才能向微队列中放入 待执行的 5
      console.log("4")
    }).then(() => {
      console.log("5")
    })
    // 所以这里立即有了成功的状态 成功值是undefined
  }).then(() => {
    // 根据
    console.log("6")
  })

  new Promise((resolve, reject) => {
    // 同
    console.log("7")
    resolve()
  }).then(() => {
    // 异步 微
    console.log("8")
  })

  // 微队列：[2 8 4 6 5]

  // 2和8平辈
  // 4和6平辈
  // 5辈分最小

  // 1 7
  // 2 3 8
  // 4 6 5 0
```

# 事件循环练习

+ await后面跟着一个promise, await返回的相当于promise里面最终的值

注意下面两段代码的不同之处

```js
const test = async () => {
  let a = 0

  const res = await new Promise((resolve, reject) => {
    setTimeout(() => {
      a = 1
      resolve(a)
    }, 2000)
  })

  console.log(res)
}

test() // 隔两秒之后打印1

// ==================================================
const test = async () => {
  let a = 0

  const v = await new Promise((resolve, reject) => {
    setTimeout(() => {
      a = 1
      resolve(a)
    }, 2000)
  }).then(v => {
    console.log('then', v);
    return 2
  })
  console.log('await', v)
}

test() // 隔两秒后输出1 2
// 注意这里的await接受的是then最终返回的值, then里面返回2了
// 如果then没有返回值, await返回的其实是undefined
```


Promise 的 .then 或者 .catch 可以被调用多次，但这里 Promise 构造函数只执行一次。
或者说 promise 内部状态一经改变，并且有了一个值，那么后续每次调用 .then 或者 .catch 都会直接拿到该值。
```js
const promise = new Promise((resolve, reject) => {
  setTimeout(() => {
    console.log('timer')
    resolve('success')
  }, 1000)
})
const start = Date.now();
promise.then(res => {
  console.log(res, Date.now() - start)
})
promise.then(res => {
  console.log(res, Date.now() - start)
})

// timer
// success 1006
// success 1007
```


+ then返回的值会被包裹在Promise.resolve中
```js
Promise.resolve().then(() => {
  return new Error('error!!!')
}).then(res => {
  console.log("then: ", res)
}).catch(err => {
  console.log("catch: ", err)
})

// 因为包裹在resolve中  所以被then捕获 而不是catch
```
+ .then 或 .catch 返回的值不能是 promise 本身，否则会造成死循环。
```js
const promise = Promise.resolve().then(() => {
  return promise;
})
promise.catch(console.err)
```

+ .then 或者 .catch 的参数期望是函数，传入非函数则会发生值透传

第一个then和第二个then中传入的都不是函数，一个是数字类型，一个是对象类型，因此发生了透传，将resolve(1) 的值直接传到最后一个then里。
```js
Promise.resolve(1)
  .then(2)
  .then(Promise.resolve(3))
  .then(console.log)

// 1
```

+ finally
.finally()方法不管Promise对象最后的状态如何都会执行
.finally()方法的回调函数不接受任何的参数，也就是说你在.finally()函数中是没法知道Promise最终的状态是resolved还是rejected的
它最终返回的默认会是一个上一次的Promise对象值，不过如果抛出的是一个异常则返回异常的Promise对象。

```js
Promise.resolve('1')
  .then(res => {
    console.log(res)
  })
  .finally(() => {
    console.log('finally')
  })
Promise.resolve('2')
  .finally(() => {
    console.log('finally2')
  	return '我是finally2返回的值'
  })
  .then(res => {
    console.log('finally2后面的then函数', res)
  })

  // 1 finally2 finally finally2后面的then函数 2
```

且就算finally2返回了新的值，它后面的then()函数接收到的结果却还是上一次Pormise的状态，
```js
Promise.resolve('1')
  .finally(() => {
    console.log('finally')
    return Promise.resolve(666)
  }).then(console.log)

  // finally  1
```

+ 如果promise返回reject
那么不会进入then, 而是直接进入catch
```js
function promise1 () {
  let p = new Promise((resolve) => {
    console.log('promise1');
    resolve('1')
  })
  return p;
}
function promise2 () {
  return new Promise((resolve, reject) => {
    reject('error')
  })
}
promise1()
  .then(res => console.log(res)) // 第1个微任务
  .catch(err => console.log(err))// 不会进入
  .finally(() => console.log('finally1')) // 第3个微任务

promise2()
  .then(res => console.log(res)) // 不会进入
  .catch(err => console.log(err)) // 第2个微任务
  .finally(() => console.log('finally2')) // 第4个微任务

// promise1
// 1
// error
// finally1
// finally2
```

+ promise.all
```js
function runAsync(x, t) {
  const p = new Promise(r => setTimeout(() => r(x, console.log(x)), t * 1000))
  return p
}
Promise.all([runAsync(1, 1), runAsync(2, 0.5), runAsync(3, 2)])
  .then(res => console.log(res))
// 0.5秒时刻打印2
// 1秒时刻打印1
// 2秒时刻打印 3 紧接着打印[1, 2, 3]
```

```js
// Promise.all按照返回结果的时间顺序打印
// 一组中有一个reject都不会进入then
// 一组中.catch是会捕获最先的那个异常(只有一个), 同理then里面第二个回调函数也只会接收到最先返回的错误
function runAsync(x) {
  const p = new Promise(r => setTimeout(() => r(x, console.log(x)), 1000))
  return p
}
function runReject(x) {
  const p = new Promise((res, rej) => setTimeout(() => rej(`Error: ${x}`, console.log(x)), 1000 * x))
  return p
}
Promise.all([runAsync(1), runReject(4), runAsync(3), runReject(2)])
  .then(res => console.log(res))
  .catch(err => console.log(err))

// // 1s后输出
// 1
// 3
// // 2s后输出
// 2
// Error: 2
// // 4s后输出
// 4
```

## 06/19
+ Promise.race all
Promise.all()的作用是接收一组异步任务，然后并行执行异步任务，并且在所有异步操作执行完后才执行回调。
.race()的作用也是接收一组异步任务，然后并行执行异步任务，只保留取第一个执行完成的异步操作的结果，其他的方法仍在执行，不过执行结果会被抛弃。
Promise.all().then()结果中数组的顺序和Promise.all()接收到的数组顺序一致。
all和race传入的数组中如果有会抛出异常的异步任务，那么只有最先抛出的错误会被捕获，并且是被then的第二个参数或者后面的catch捕获；但并不会影响数组中其它的异步任务的执行。

+ async await
```js
async function async1() {
  console.log("async1 start");
  await async2();
  console.log("async1 end");
}
async function async2() {
  console.log("async2");
}
async1();
console.log('start')

// async1 start
// async2
// strart
// async1 end
```

首先一进来是创建了两个函数的，我们先不看函数的创建位置，而是看它的调用位置
发现async1函数被调用了，然后去看看调用的内容
执行函数中的同步代码async1 start，之后碰到了await，它会阻塞async1后面代码的执行，因此会先去执行async2中的同步代码async2，然后跳出async1
跳出async1函数后，执行同步代码start
在一轮宏任务全部执行完之后，再来执行刚刚await后面的内容async1 end。

在这里，你可以理解为「紧跟着await后面的语句相当于放到了new Promise中，下一行及之后的语句相当于放在Promise.then中」。

转换为promise表示的**伪代码**
```js
async function async1() {
  console.log("async1 start");
  // 原来代码
  // await async2();
  // console.log("async1 end");

  // 转换后代码
  new Promise(resolve => {
    console.log("async2")
    resolve()
  }).then(res => console.log("async1 end"))
}
async function async2() {
  console.log("async2");
}
async1();
console.log("start")
```

+ async/await
async函数在抛出返回值时，会根据返回值类型开启不同数目的微任务

return结果值：非thenable、非promise（不等待）
return结果值：thenable（等待 1个then的时间）
return结果值：promise（等待 2个then的时间）

例1
```js
async function testA () {
  return 1;
}
​
testA().then(() => console.log(1));
Promise.resolve()
   .then(() => console.log(2))
   .then(() => console.log(3));
​
// (不等待)最终结果👉: 1 2 3
```

例2
```js
async function testB() {
  return {
    then(cb) {
      cb();
    }
  };
}

testB().then(() => console.log(1));
Promise.resolve()
  .then(() => console.log(2))
  .then(() => console.log(3));

 // (等待一个then)最终结果👉: 2 1 3
```

例3
```js
async function testC() {
  return new Promise((resolve, reject) => {
    resolve()
  })
}

testC().then(() => console.log(1));
Promise.resolve()
  .then(() => console.log(2))
  .then(() => console.log(3));

// (等待两个then)最终结果👉: 2 3 1




async function testC() {
  return new Promise((resolve, reject) => {
    resolve()
  })
}

testC().then(() => console.log(1));
Promise.resolve()
  .then(() => console.log(2))
  .then(() => console.log(3))
  .then(() => console.log(4))

  // (等待两个then)最终结果👉: 2 3 1 4
```

+ await的右边
非.then
和promise.resolve后面第一个then平辈
```js
async function test() {
  console.log(1);
  await 1;
  console.log(2);
}

test();
console.log(3);
 // 最终结果👉: 1 3 2

////////////////////////////////////////////////////////

function func() {
  console.log(2);
}

async function test() {
  console.log(1);
  // await右边函数会执行里面的同步代码
  await func();
  console.log(3);
}

test();
console.log(4);

 // 最终结果👉: 1 2 4 3

////////////////////////////////////////////////////////

async function test() {
  console.log(1);
  // await右边是非then 非promise 不需要等待then时间,(即和下面普通的最老的then平辈)
  await 123
  console.log(2);
}

test();
console.log(3);

Promise.resolve()
  .then(() => console.log(4))
  .then(() => console.log(5))
  .then(() => console.log(6))
  .then(() => console.log(7));

 // 最终结果👉: 1 3 2 4 5 6 7


////////////////////////////////////////////////////////
Promise.resolve()
  .then(() => console.log(4))
  .then(() => console.log(5))
  .then(() => console.log(6))
  .then(() => console.log(7));

async function test() {
  console.log(1);
  // await右边是非then 非promise 不需要等待then时间,(即和下面普通的最老的then平辈)
  await 123
  console.log(2);
}

test();
console.log(3);



 // 最终结果👉: 1 3 4 2 4 5 6 7
```

.then
await 后面接 thenable 类型，需要等待一个 then 的时间之后执行
和promise.resolve后面第2个then平辈
```js
async function test() {
  console.log(1);
  // await后面接一个可以then的值
  // 那么 await下面的代码和下面的promise后面第二个then平辈
  await {
    then(cb) {
      cb();
    },
  };
  console.log(2);
}

test();
console.log(3);

Promise.resolve()
  .then(() => console.log(4))
  .then(() => console.log(5))
  .then(() => console.log(6))
  .then(() => console.log(7));

 // 最终结果👉: 1 3 4 2 5 6 7

```

promise
注意: 这个和直接async返回promise类型不一样
await右边接promise 不需要等待两个then
**和promise.resolve后面第一个then平辈**
```js
async function test () {
  console.log(1);
  await new Promise((resolve, reject) => {
    resolve()
   })
  console.log(2);
}
​
test();
console.log(3);
​
Promise.resolve()
   .then(() => console.log(4))
   .then(() => console.log(5))
   .then(() => console.log(6))
   .then(() => console.log(7));
​
// 最终结果👉: 1 3 2 4 5 6 7
```

+ 练习1
```js
async function func() {
  console.log(1);
  await 1;
  console.log(2);
  await 2;
  console.log(3);
  await 3;
  console.log(4);
}

async function test() {
  console.log(5);
  await func();
  console.log(6);
}

test();
console.log(7);

Promise.resolve()
  .then(() => console.log(8))
  .then(() => console.log(9))
  .then(() => console.log(10))
  .then(() => console.log(11));

// 5 1 7 2 8 3 9 4 10 6 11
```

+ 练习2
```js
async function async2() {
  new Promise((resolve, reject) => {
    resolve()
  })
}

async function async3() {
  return new Promise((resolve, reject) => {
    resolve()
  })
}

async function async1() {
  // 方式一：最终结果：B A C D await右边promise 和最老的then平辈
  // await new Promise((resolve, reject) => {
  //     resolve()
  // })

  // 方式二：最终结果：B A C D  await右边undefined 和最老的then平辈
  // await async2()

  // 方式三：最终结果：B C D A (等两个then)
  await async3()

  console.log('A')
}

async1()

new Promise((resolve) => {
  console.log('B')
  resolve()
}).then(() => {
  console.log('C')
}).then(() => {
  console.log('D')
})
```

+ 练习3
await会一直等promise执行完再进入await下面的代码
```js
function func() {
  console.log(2);

  // 方式一：1 2 4  5 3 6 7
  // Promise.resolve()
  //     .then(() => console.log(5))
  //     .then(() => console.log(6))
  //     .then(() => console.log(7))

  // 方式二：1 2 4  5 6 7 3
  // await会一直等promise执行完再进入await下面的代码
  return Promise.resolve()
    .then(() => console.log(5))
    .then(() => console.log(6))
    .then(() => console.log(7))
}

async function test() {
  console.log(1);
  await func();
  console.log(3);
}

test();
console.log(4);
```

+ 练习4
```js
function func() {
  console.log(2);

  return Promise.resolve()
    .then(() => console.log(5))
    .then(() => console.log(6))
    .then(() => console.log(7))
}

async function test() {
  console.log(1);
  await func()
  console.log(3);
}

test();
console.log(4);

new Promise((resolve) => {
  console.log('B')
  resolve()
}).then(() => {
  console.log('C')
}).then(() => {
  console.log('D')
})

 // 最终结果👉: 1 2 4    B 5 C 6 D 7 3
```


+ 练习5
```js
async function test() {
  console.log(1);
  await Promise.resolve()
    .then(() => console.log(5))
    .then(() => console.log(6))
    .then(() => console.log(7))
  console.log(3);
}

test();
console.log(4);

new Promise((resolve) => {
  console.log('B')
  resolve()
}).then(() => {
  console.log('C')
}).then(() => {
  console.log('D')
})

 // 最终结果👉: 1 4  B   5 C 6 D 7 3
```

+ 练习6
综上，await一定要等到右侧的表达式有确切的值才会放行，否则将一直等待（阻塞当前async函数内的后续代码）
```js
function func() {
  return new Promise((resolve) => {
    console.log('B')
    // resolve() 故意一直保持pending
  })
}

async function test() {
  console.log(1);
  await func()
  console.log(3);
}

test();
console.log(4);
// 最终结果👉: 1 B 4 (永远不会打印3)


// ---------------------或者写为👇-------------------
async function test() {
  console.log(1);
  await new Promise((resolve) => {
    console.log('B')
    // resolve() 故意一直保持pending
  })
  console.log(3);
}

test();
console.log(4);
 // 最终结果👉: 1 B 4 (永远不会打印3)
```

+ 练习7
```js
async function func() {
  console.log(2);
  // async返回的then要等待一个then时间
  return {
    then(cb) {
      cb()
    }
  }
}

async function test() {
  console.log(1);
  await func();
  console.log(3);
}

test();
console.log(4);

new Promise((resolve) => {
  console.log('B')
  resolve()
}).then(() => {
  console.log('C')
}).then(() => {
  console.log('D')
})

 // 最终结果👉: 1 2 4 B     C 3 D
```


+ 结论：
async函数在抛出返回值时，会根据返回值类型开启不同数目的微任务

return结果值：非thenable、非promise（不等待）
return结果值：thenable（等待 1个then的时间）
return结果值：promise（等待 2个then的时间）



await右值类型区别
接非 thenable 类型，会立即向微任务队列添加一个微任务then，但不需等待

接 thenable 类型，需要等待一个 then 的时间之后执行

接Promise类型(有确定的返回值)，会立即向微任务队列添加一个微任务then，但不需等待

TC 39 对await 后面是 promise 的情况如何处理进行了一次修改，移除了额外的两个微任务，在早期版本，依然会等待两个 then 的时间







## 原始数据类型Symbol

ES5中对象的属性名都是字符串，容易造成重名，污染环境，ES6中添加了一种原始数据类型symbol

### 特点

1.symbol属性对应的值是唯一的，解决命名冲突问题

2.symbol值不能与其他数据进行计算，包括同字符串拼接

3.for in, for of遍历时不会遍历symbol的属性

### 使用

1.调用Symbol函数得到symbol的值

```js
let symbol = Symbol();// 不是构造函数 不需要new
console.log(symbol);
let obj = {
    name: "jack",
    age: 18
}
obj[symbol] = "hello";// 属性选择器
console.log(obj);
```

2.传参标识（唯一）

```js
let symbol2 = Symbol('aaa2');// 可以给不同的symbol传入唯一标识的参数
let symbol3 = Symbol('aaa3');
console.log(symbol2 == symbol3);// false
```

3.内置Symbol值
除了定义自己使用的Symbol值以外，ES6还提供了11个内置的Symbol值，指向语言内部的使用方法

## iterator接口机制

概念：iterator是一种接口机制，为各种不同的数据结构提供统一的访问机制

作用：
1.为各种数据结构，提供一个统一的，简便的访问接口

2.使得数据结构的成员能够按某种次序排列

3.ES6创造了一种新的遍历命令for of循环，iterator接口主要供for of消费

工作原理：
    创建一个指针对象（遍历器对象），指向数据结构的起始位置
     第一次调用next方法，指针自动指向数据结构的下一个成员
     接下来不断调用next方法，指针会一直往后移动，直到指向最后一个成员
     每调用next方法返回的是一个包含value和done的对象（value：当前成员的值，done：布尔值）
     value表示当前成员的值，done对应的布尔值表示当前的数据的结构是否遍历结束
     当遍历结束的时候返回的value值是undefined，done值为true

```js
function myIterator(arr){
  let index = 0;// 记录指针的位置
  return {
    // 函数返回值是遍历器对象
    next(){// 简写的对象方法
      // 判断句
      // index小于数据长度时继续向后遍历
      // 一旦到数据末尾，返回str，修改done
      return index<arr.length?{
          value: arr[index++],// arr[index++]下标先使用再加加
          done: false
      }:{
          value: '遍历完成！',
          done: true
      }
    }
  }
}
// 准备一组数据测试
let arr = [1,2,3,4,5];
let iteratorObj = myIterator(arr);
console.log(iteratorObj.next());
console.log(iteratorObj.next());
console.log(iteratorObj.next());
console.log(iteratorObj.next());
console.log(iteratorObj.next());
console.log(iteratorObj.next());// 此时返回undefined
console.log(iteratorObj.next());// 此时返回undefined
```


原生具备iterator接口的数据（可用for of遍历）
     数组，字符串，arguments，set容器，map容器
     iterator没有部署在obj上

```js
let arr = [1, 2, 3, 4]
for (const item of arr) {
  console.log(item)
}

let str = "abcdef";
for(let i of str){
  console.log(i);
}
```

## Generator函数

概念：
可暂停函数，惰性求值函数
1.ES6提供的解决异步编程的方案之一
2.Generator函数是一个状态机，内部封装了不同的数据
3.用来生成遍历器对象
4.可暂停函数（惰性求值），yield可暂停，next方法可启动，每次返回的是yield后的表达式结果

特点：
1.function与函数名之间有一个*号
2.内部用yield表达式来定义不同的状态

```js
function* myGenerator(){
    console.log('start');
    let result = yield "hello!";// 暂停

    console.log(result);// 如果next()不传参，输出undefined 否则输出参数
    yield "1";// 每个yield通过.next()方法执行完都会暂停一次

    console.log(12333);
    yield "2";// 暂停

    return 'over'
}
// myGenerator();// 函数内部并没有执行

let MG = myGenerator();
// MG.next();// 指针对象调用next方法
console.log(MG.next());// {value: "hello!", done: false}
console.log(MG.next('12333333'));// {value: "1", done: false}
console.log(MG.next());// {value: "2", done: false}
console.log(MG.next());// {value: "return的值", done: true}

```

对象Symbol.iterator 与Generator相结合

```js
let obj = {name: "abc", age: 18}
obj[Symbol.iterator] = function* () {
    yield 1
    yield 2
    yield 3
    yield 4
    return 5
}
for (let i of obj) {
    console.log(i)
}
```

一个斐波那契数列生成器
```ts
function* fibGenerator(){
  let a = 0
  let b = 1
  while (true) {
    yield a
    let sum = a + b
    a = b
    b = sum
  }
};

/**
 * const gen = fibGenerator();
 * gen.next().value; // 0
 * gen.next().value; // 1
 */

const gen = fibGenerator();
console.log('gen :>> ', gen);
console.log(gen.next()); // 0
console.log(gen.next()); // 1
console.log(gen.next()); // 1
console.log(gen.next()); // 2
console.log(gen.next()); // 3
console.log(gen.next()); // 5
console.log(gen.next()); // 8
```

## async函数

概念：真正意义上解决异步回调问题，同步流程表达异步操作

本质：Generator的语法糖（语法糖：在原有的语法基础上更加完善）

语法：
    async function foo () {
    await 异步操作
    await 异步操作
    }

特点：
1.不需要像Generator去调用next方法
2.返回的总是Promise对象，可以用then方法进行下一波操作
3.async取代Generator函数的*号，await取代Generator的yield
4.语义上更明确，使用简单
```js
// async函数

async function fun(){
  return new Promise (resolve => {// 箭头函数一个参数可以省略括号
    setTimeout(() => {
      resolve();
    },2000)
  })
}

async function fun1(){
  console.log('start',new Date().toTimeString());
  await fun();
  console.log('over',new Date().toTimeString());
}

fun1();

function fun2(){
  return "xxxxx";
}
async function fun3(){
  // let result = await Promise.reject(123);
  // console.log(result);
  // 输出报错

  let result1  = await Promise.resolve(123);
  console.log(result1);
  // 输出123
}
fun3();
```

# 模块化
## import
### 常见用法
```js
import App from './App'
import { foo, bar as baz } from './module'
import React, { useState } from 'react'
import './index.css'
```

### 不常见用法
```js
import { default as foo } from './module'
import * as foo from './module'
import theDefault, * as foo from './module'
```

### 不常见用法解释
```js
// 将所有的部分导出导入到一个整个对象中
import * as foo from './module'
// 使用: module.a module.b
```

## export

### 常见用法
```js
export default function(){}
export default class {}
export default function App(){}
export default class Person {}
export default Comp

export const util = () => {}
export const obj = {}
export class Person {}
export { foo, bar as boo }

export { foo, bar as boo } from './module'
```

### 不常见用法
```js
export { foo as default }

export * from './module'
export * as module from './module'
export { default } from './module'
export { default as foo } from './module'
export { foo as default } from './module'
export { foo as default, bar, baz } from './module'
```

### 不常见用法解释
```js
function foo() {}
export { foo as default }

// 等价于
export default function foo(){}
// 也等价于
function foo() {}
export default foo
```

```js
// 将辅助文件中所有的内容原封不动的导入再导出
// 辅助文件中的默认还是默认, 部分导出还是部分导出
// 一般用于主文件聚拢聚合
export * from './module'
```

```js
// 把默认导出 先导入 再变成部分导出
export { default as foo } from './module'
//////////////////////////使用示例///////////////////////
// 辅助文件
export default calss XXX {}
// 主文件(通常是index.ts 用于聚拢辅助文件到一个index)
export { default as XXX } from './子组件路径'

// 外部使用
import { XXX } from './主文件的文件夹名称'
////////////////////////////////////////////////////////
```
