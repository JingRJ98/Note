# CSS3
## 简介
# 导学
CSS全称Cascading Style Sheets
样式表由规则组成，规则由选择器 ＋ 声明块组成（CSS属性+CSS属性值组成的键值对）
网页分为三层结构 结构 HTML  表现 CSS  交互 JavaScript
CSS层叠样式表: 网页是一个分层结构, CSS为每层设置样式，最后看到的是最上层效果
```html
<body>
  <!--
    1、内联样式表用style属性设置
      color颜色 font-size表示字号，px像素
    2、内部样式表 在<head>里设置style 标签用{},{}里面设置属性，对每一个标签设置
    3、外部样式表 在外部文件建立.css文件
      标签用{},{}里面设置属性
        -用link调用 实现多页面复用
        -head中用link
  -->
  <p style="color: rgb(247, 8, 8); font-size: 65px;">床前明月光 疑是地上霜</p>
</body>
```

## 盒模型相关属性默认值
`left`/`top`/`right`/`bottom`/`width`/`height` 默认值auto

`margin` /`padding `默认值0

`border-width`默认值medium
## 盒模型百分比参考
元素的`width` `height`百分比参考于包含块的`width` `height`
元素的`margin` `padding`百分数参考于包含块的`width`

# CSS选择器
浏览器读取选择器的顺序为从从右往左` div ul li #id{} `
## 选择器优先级
当使用不同的选择器，选中同一个元素时并且设置相同的样式时 这时样式之间产生了冲突，最终到底采用哪个选择器定义的样式，由选择器的优先级决定，优先级高的优先显示
优先级的规则：
内联样式 优先级 1000
ID选择器 优先级 0100
类，伪类，属性 优先级 0010
元素选择器 优先级 0001
通配选择器 优先级 0000
继承的样式 没有优先级

当选择器中包含多种选择器时，需要将多种选择器的优先级不进位相加然后再比较 (譬如说 类名加的越多 权重越高)
但是注意，选择器优先级的计算不会超过它的最大数量级，如果选择器的优先级一样，则使用新声明的样式

并集选择器的优先级是单独计算 例如：div , p , #p1 , .hello {}
可以在样式的最后，添加一个 !important ,则此时该样式会获得一个最高优先级（超过内联样式）尽量避免使用
## 常用选择器
### 通配选择器
选择所有元素
`*{}`
### 元素选择器
选择指定元素
```css
p {
  color: red;
}
```
### id选择器
通过元素的id属性值选中唯一的一个元素
```css
#name {
  color: red;
}
```
### 类选择器
通过元素的clss属性值选中一组元素 `.class {}`
### 并集选择器
`,` 分隔
通过并集选择器可以同时选中多个选择器对应的元素 语法：选择器1 , 选择器2 , 选择器3 {}
例：为id为p1，或者class为p2的元素，或者h1设置背景颜色为黄色
```css
#p1, .p2, h1 {
  background-color: yellow;
}
```
### 交集选择器/复合选择器
通过交集选择器选择同时满足多个选择器的元素
```css
/* 两个选择器之间不能有空格，必须连续书写。 */
p.container {
  color: red;
}
```

sass中交集选择器
```scss
.box {
  &.class {
    // 这里表示class="box calss"的元素 即交集 而非父子关系
  }
}
```

### 子元素选择器
通过子元素选择器可以选择指定父元素的指定子元素 语法：父元素 > 子元素 {}

### 后代元素选择器
通过后代选择器可以选择指定元素的指定后代元素 语法：祖先元素 后代元素 {}

### 兄弟元素选择器
通过兄弟元素选择器可以选择一个元素后紧挨着的指定的兄弟元素
语法：
元素1 + 元素2 {} 必须相邻
元素1 ~ 后面所有元素 {} 不一定要相邻

为span后`一个`p标签设置背景颜色为黄色
```css
span + p {
  background-color: yellow;
}
```
为span后`所有`p标签设置背景颜色为黄色
```css
span ~ p {
  background-color: yellow;
}
```
### 属性选择器
```html
<style>
  /* 属性选择器
  [属性名] 选中所有带该属性名的元素
  标签[属性名] 选中标签同时带属性名的元素
  [属性名=属性值] 只选中属性名同时等于属性值的元素
  [属性名^=属性值] 选中以属性值开头的元素
  [属性名$=属性值] 选中以属性值结尾的元素
  [属性名*=属性值] 选中含有属性值的元素
  */
  /* [title]{color: red;} */
  /* p[title]{color: red;} */
  /* [title='abc']{color: red;} */
  p[title^=abc]{color: red;}
  /* [title$=abc]{color: red;} */
  /*[title*=abc]{color: red;} */
</style>
<body>
  <h1 title="abc">我是标题</h1>
  <p title="abc">少小离家老大回</p>
  <p title="abcdef">乡音无改鬓毛衰</p>
  <p title="wewdasjhabckosda">儿童相见不相识</p>
  <p title="helloabc">笑问客从何处来</p>
  <p>落霞与孤鹜齐飞</p>
  <p>秋水共长天一色</p>
</body>
```

#### 根据部分属性值选择
[className|='btn']
表示选择含有className属性 且该属性值以btn开头 或者btn-开头 注意破折号



#### 不区分属性值大小的标识符
`a[href$='.pdf' i]`
表示匹配所有含有href属性的a标签 且href值以.pdf结尾 不区分大小写的元素的样式


#### 伪类选择器

表示一种状态(hover, active, 第几个等等)

选择器	例子	例子描述
:active	a:active	匹配被点击的链接
:checked	input:checked	匹配处于选中状态的 <input> 元素
:disabled	input:disabled	匹配每个被禁用的 <input> 元素
:empty	p:empty	匹配任何没有子元素的 <p> 元素
:enabled	input:enabled	匹配每个已启用的 <input> 元素
:first-child	p:first-child	匹配父元素中的第一个子元素 <p>，<p> 必须是父元素中的第一个子元素
:first-of-type	p:first-of-type	匹配父元素中的第一个 <p> 元素
:focus	input:focus	匹配获得焦点的 <input> 元素
:hover	a:hover	匹配鼠标悬停其上的元素
:in-range	input:in-range	匹配具有指定取值范围的 <input> 元素
:invalid	input:invalid	匹配所有具有无效值的 <input> 元素
:lang(language)	p:lang(it)	匹配每个 lang 属性值以 "it" 开头的 <p> 元素
:last-child	p:last-child	匹配父元素中的最后一个子元素 <p>， <p> 必须是父元素中的最后一个子元素
:last-of-type	p:last-of-type	匹配父元素中的最后一个 <p> 元素
:link	a:link	匹配所有未被访问的链接
:not(selector)	:not(p)	匹配每个非 <p> 元素的元素
:nth-child(n)	p:nth-child(2)	匹配父元素中的第二个子元素 <p>
:nth-last-child(n)	p:nth-last-child(2)	匹配父元素的倒数第二个子元素 <p>
:nth-last-of-type(n)	p:nth-last-of-type(2)	匹配父元素的倒数第二个子元素 <p>


```html
<style>
    /*
    伪类选择器（不存在的class 特殊的class）
    :first-child 第一个
    :last-child 最后一个
    :nth-child() 括号里面可以写任意数
        n 表示全选
        2n 偶数位
        even 偶数位
        2n+1 奇数位
        odd 奇数位
    当标签不同时，可以用first-of-type指定全类型查找
    :not() 除了第几个
      */

      /* ul>li:first-child{
          color: red;
      } */

      /* ul>li:last-child{color: red;} */

    /* ul>li:first-of-type{
          color: red;
      }  */

    ul>li:not(:nth-child(odd))
    {
      color: red;
    }
</style>
<body>
    <ul>
      <li>0</li>
      <li>1</li>
      <li>2</li>
      <li>3</li>
      <li>4</li>
      <li>5</li>
      <li>6</li>
    </ul>
</body>
```

```html
<style>
    /* 链接分为访问过的和没访问过的
    a:link 没访问过
    a:visited 访问过 且只能改变颜色
    a:hover 鼠标移动变化
    a:active 鼠标点击变化
    */

a:link{color: springgreen;}
a:visited{color: violet;}
a:hover{color: rgba(46,139,87,0.5);}
a:active{color: yellow;}
</style>
<body>
    <a href="https://www.baidu.com">baidu</a>
    <br><br>
    <a href="1233433">没访问过</a>
</body>
```
#### 伪元素
表示元素(不存在)
::before ::after
```html
  <style>
    /*
    ::first-letter 第一个字母
    ::first-line 第一行
    ::selection 选中
    ::before
    ::after
        before和after通常要结合content使用
      */
      p::first-letter{font-size: 35px;}
      p::first-line{color: yellow;}
      p::selection{background-color: turquoise;}
      div::before{
        content:'〔';
        color: teal;
      }
      div::after{
        content: '〕';
        color: teal;
      }
  </style>
```
### 继承
继承是指祖先元素应用的设置可以复刻到后代元素上

一.不可继承性的属性
1.display：规定元素应该生成的框的类型
2.文本属性：vertical-align：垂直文本对齐
text-decoration：规定添加到文本的装饰
text-shadow：文本阴影效果
white-space：空白符的处理
unicode-bidi：设置文本的方向

3.盒子模型的属性：width，height，margin，margin-top，margin-right，margin-bottom，margin-left，border，border-style，border-top（right，bottom，left）-style

border-top（right，bottom，left）-width，border-top（right，bottom，left）-color，border-top，border-right，border-bottom，border-left，

padding，padding-top，padding-right，padding-bottom，padding-left

4.背景属性：background，background-color，background-image，background-repeat，background-position，background-attachment

5.定位属性：float，clear，position，top，right，bottom，left，min-width，min-height，max-width，max-height，overflow，clip，z-index

6.生成内容属性：content，content-seset，content-increment

7.轮廓样式属性：outline，outline-style，outline-width，outline-color

8.页面样式属性：size，page-break-before，page-break-after

9.声音样式属性：pause，pause-before，pause-after，cue，cue-before，cue-after，play-during

二.有继承性的属性

1.字体系列属性：font：组合字体
font-family：规定元素的字体系列
font-weight：设置字体的粗细
font-size：设置字体的尺寸
font-style：设置字体的风格
font-variant：设置小型大写字母的字体显示文本，这意味着所有的小写字母均会被转换为大写，但是所有使用小型大写字体的字母与其余文本相比，其字体尺寸更小
font-stretch：对当前的font-family进行伸缩变形，所有主流浏览器都不支持
font-size-adjust：为某个元素规定一个aspect值，这样就可以保持首选字体的x-height

2.文本系列属性：text-indent：文本缩进
text-align：文本水平对齐
line-height：行高
word-spacing：增加或减少文单词间的空白（即字间隔）
letter-spacing：增加或减少字符间的空白（字符间距）
text-transform：控制文本大小写
direction：规定文本的书写方向
color：文本颜色

3.元素可见性：visibility

4.表格布局属性：caption-side，border-collapse，border-spacing，empty-cells，table-layout

5.列表布局属性：list-style，list-style-image，list-style-type，list-style-position

6.生产内容属性：quotes

7.光标属性：cursor

8.页面样式属性：page，page-break-inside，Windows，orphans

9.声音样式属性：speak、speak-punctuation、speak-numeral、speak-header、speech-rate、volume、voice-family、pitch、pitch-range、stress、richness、、azimuth、elevation

三.所有元素可继承属性

1.元素可见性：visibility

2.光标属性：cursor

四.内联元素可以继承的属性

1.字体系列属性

2.除text-indent，text-align之外的文本系列属性

五.块级元素可以继承的属性

1.text-indent，text-align

## 文档流
网页是多层结构，通过CSS可以为每一层设置样式，网页只能看到最上面一层样式，最下面一层称作文档流，创建的元素默认在文档流上排列，元素有两种状态，在文档流上和不在文档流上
在文档流上的元素特点
块元素
独占一行，默认宽度是父元素的全部，默认高度被子元素撑开
行内元素，不独占一行，只占自身大小，一行放不下，自动换行，宽度和高度只占自身大小

## 盒子模型
CSS将页面中所有元素设置为矩形盒子，布局即放到不同位置盒子组成
内容区content, 内边距padding, 边框border, 外边距margin

标准盒模型中width指的是内容区域content的宽度；height指的是内容区域content的高度
IE盒模型中的width指的是内容、边框、内边距总的宽度；height指的是内容、边框、内边距总的高度
box-sizing的默认属性是content-box
box-sizing: content-box 是W3C盒子模型
box-sizing: border-box 是IE盒子模型
box-sizing: inherit 规定应从父元素继承 box-sizing 属性的值。

边框 设置边框至少设置三个样式
边框宽度 border-width
边框颜色 border-color
边框样式 border-style
边框大小影响盒子大小

## css元素自身宽度
如果元素的内容仅是字符串，那么它的最小内容尺寸会等于内容中最长字符串的宽度；
如果元素的内容是图文混合，那么它的最小内容尺寸会等于图片的原始尺寸。

浏览器计算的最小内容尺寸 超出设置的宽度的时候,就会出现水平滚动条(设置了overflow的情况下)

### 边框
```html
    <style>
        /*
        边框宽度有默认值 一般为三个像素
        当border-width设置
            四个值时，按照上，右，下，左顺序
            三个值时，上，左右，下的顺序
            两个值时，上下，左右的顺序

            除了border-width 还可以专门制定某一方向的边框
            border-xxx-width
                xxx为top right bottom left

        border-color默认为黑色，同样可以制定top right bottom left
        border-style
            solid 实线
            dotted 点虚线
            dashed 线虚线
            double 双线
            同样有top right bottom left
         */

         /* border 的简写
         border:x x x;  x的顺序无要求
          */
         .box1{
            width: 300px;
            height: 300px;
            background-color: lime;
            /*
            border-width: 10px 20px 30px;
            border-color: maroon rgb(200,40,180) #fba #ff0000;
            border-top-style: solid;
            border-right-style: dotted;
            border-bottom-style: dashed;
            border-left-style: double; */
            border: 10px blue solid;
        }
    </style>
```
### 内边距
内边距padding 内容区和边框之间的距离
内边距大小影响盒子大小
内容区背景颜色会延伸到内边距上
padding-top，bottom,left,right

### 外边距
外边距不影响盒子可见框大小
一共有四个方向
top, left, bottom, right

设置margin的左、上值时，盒子自己移动

设置margin的右、下值时会挤走别人（如果有别的元素的 话

margin 的值为正数时远离, 负数时靠近

### 水平布局
元素水平方向的布局收到以下几个属性的共同控制
    margin-left
    border-left
    padding-left
    width
    padding-right
    border-right
    margin-right

子元素在父元素中的水平布局必须满足的等式关系
margin-left + border-left + padding-left + width + padding-right + border-right + margin-right
=父元素的内容区宽度
如果等式相加不成立，，称为过度约束，浏览器会自动调整等式
    调整情况
    1、七个属性中没有auto，自动调整margin-right使等式满足
    2、七个属性值中有三个可以设置为auto margin-left width margin-right
    优先调整auto使吗，满足等式
    width默认auto，100%
    margin-left和margin-right的auto都是0
        若果两个外边距为auto，width为固定值，则子元素居中
        常用于使子元素居中

### overflow
```html
<style>
  .box1{
    width: 200px;
    height: 200px;
    background-color: #bfa;
    /* 父元素被子元素的大小撑开 */
    /* overflow: hidden; */
    /* overflow: scroll; */
    overflow: auto;
  }
  .inner{
    width: 100px;
    height: 400px;
    background-color: red;
    /* 子元素超过父元素 称为溢出 */
  }
  /* 使用overflow来设置溢出的数据
      visible 显示溢出内容
      hidden 裁减掉溢出内容
      scroll 生成两个滚动条，使内容在内容区显示
      auto 根据内容大小自动生成滚动条适应

      overflow-x 单独处理横轴
      overflow-y 单独处理纵轴
  */
</style>
<body>
  <div class="box1">
    <div class="inner"></div>
  </div>
</body>
```

### 外边距的重叠问题
```html
    <style>
        .box1,.box2{
            width: 200px;
            height: 200px;
        }
        .box1{
            background-color: #bfa;
            margin-bottom: -100px;
        }
        .box2{
            background-color: red;
            margin-top: -200px;
        }
        /* 相邻的垂直方向的外边距会发生重叠
            兄弟元素的重叠
                两个外边距都是正值 取最大值
                一正一负 取和
                两个负值 取负的两者绝对值最大

            兄弟元素之间的重叠有利于开发
        */

        .box3{
            width: 200px;
            height: 200px;
            background-color: #bfa;
            /* padding-top: 100px; */
            border-top: #bfa 1px solid;
        }
        .box4{
            width: 100px;
            height: 100px;
            background-color: #bbbbaa;
            margin-top: 99px;
        }
        /* 相邻的垂直方向的外边距会发生重叠
        父子元素之间的重叠不利于开发
        子元素的外边距会传递给父元素
            两种处理方式
                1、不使用外边距
                将padding设置为子元素的高度
                再将父元素的高度减去子元素的高度
                2、使父子元素的外边距不相邻
                给父元素添加上边框
                然后子元素移动下边距
                最后子元素移动的下边距应减去父元素上边框的长度
                3. BFC
         */
    </style>
<body>
    <div class="box1"></div>
    <div class="box2">2</div>
    <div class="box3">
        <div class="box4"></div>
    </div>
</body>
```
### 行内元素的盒模型
行内元素垂直方向不影响页面布局!

```html
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <style>
      a{
        background-color:#bfa;
        width: 60px;
        height: 60px;
        display: block;
        /* visibility: hidden; */
      }
      .box1{
        width: 100px;
        height: 100px;
        background-color: orange;
      }
    </style>
</head>
<body>
    <!--
        行内元素不支持设置宽度和高度，行内元素的大小由内容决定
        超链接也是行内元素

        行内元素可以设置padding，margin，border，但是垂直方向不影响页面布局

        display 设置元素的显示类型
            inline 设为行内元素
            block 设为块元素
            inline-block 行内块（既可以设置宽高，又不独占一行）
            table 表格
            none 元素隐藏不显示，也不占据位置

        visibility 设置元素的显示状态
            visible 默认，正常显示
            hidden 隐藏，但占据位置
     -->
    <a href="https://www.baidu.com">百度</a>
    <div class="box1"/>
    </body>
```
### 轮廓和圆角
```html
<style>
        .box1,.box2{
            width: 200px;
            height: 200px;
            background-color: #baf;
        }
        .box1{
            outline: 10px #2fa solid;
            border-radius: 50%;
            box-shadow: 50px 50px 20px rgb(126, 122, 122);
        }
        .box2{
            margin-top: 150px;
            border-radius: 130px;
            /* border-radius超过50%时，浏览器会按比例自动缩小到刚好是一个圆的圆角 */
        }
    </style>
<body>
    <!--
        outline 设置元素轮廓线 和border语法一样
            不同点：轮廓不影响布局
        box-shadow 阴影 阴影不影响布局
            语法 box-shadow：10px 10x 10px black;
                第一 二个值 设置偏移量
                    第一个值水平方向，正数向右，负数向左
                    第二个值竖直方向，正数向下，负数向上
                第三个值模糊半径

        border-radius
        默认值0 不可继承
        不能用负值
            border-top-left-radius
            border-top-right-radius
            border-bottom-right-radius
            border-bottom-left-radius
                两个属性值，/隔开，表示椭圆角
                    第一个值X轴半径
                    第二个值Y轴半径
            border-radius:四个值时，左上，右上，右下，左下
                          三个值时，左上，右上左下，右下
                          两个值时，左上右下，右上左下
                          （对角线）
     -->
     <div class="box1"></div>
     <div class="box2"></div>
</body>
```

## 浮动
浮动元素不会盖住文字，可以利用该特点设置文字环绕图片效果
  从文档流中脱离的元素，其性质特点发生变化
      1、块元素不再独占一行
      2、宽度和高度被内容撑开
      3、行内元素变成块元素
      脱离文档流后不用在区分行内元素和块元素
通过设置浮动可以使元素向父元素左右移动
float属性
  none 不浮动
  left 向左浮动
  right 向右

元素设置浮动后，水平布局不强调等式成立

设置浮动的元素完全从文档流中脱离，不占据位置

特点
  1、浮动元素完全脱离文档流
  2、元素向父元素的左边或者右边移动
  3、默认不会从父元素中移出
  4、浮动元素不会超过其他浮动元素
  5、如果上边有没浮动的元素，浮动元素无法超过未浮动的
  6、浮动元素不会超过浮动的兄弟元素，最多一样高


### 高度塌陷问题
```html
<style>
  .box1{
    /* 第一种 float: left; */
    /*  第二种 display: inline-block; */
    /* 第三种 不影响布局 最常用 */
    overflow: hidden;
    border: 10px red solid;
  }
  .inner{
    float: left;
    width: 100px;
    height: 100px;
    background-color: #bff;
  }
  .box2{
    width: 100px;
    height: 100px;
    background-color: #fbf;
  }
  /*
    浮动布局中，父元素的高度会被子元素撑开
    当子元素脱离文档流后，无法撑起父元素的高度，导致父元素出现高度塌陷
    父元素高度丢失后
    剩下的元素会自动上移，导致布局混乱
  */
</style>
<body>
    <div class="box1">
        <div class="inner"></div>
    </div>
    <div class="box2"></div>
</body>
```
### BFC
```html
<style>
  .box1{
      width: 200px;
      height: 200px;
      background-color: #bff;
      /* float: left; */
      overflow: hidden;
  }
  .box2{
      width: 200px;
      height: 200px;
      background-color: #fbb;
      /* overflow:hidden 开启BFC
      不会被浮动元素盖住 */
      overflow: hidden;
  }
  .box3{
      width: 100px;
      height: 100px;
      background-color: green;
      /*
      box1开启了overflow：hidden;开启BFC，父元素和子元素的
      外边距不重叠
      本来子元素下移会带动父元素一起下移，但是此时子元素单独移动
        */
      margin-top: 100px;
  }
</style>
<body>
    <div class="box1">
        <div class="box3"></div>
    </div>
    <div class="box2"></div>
</body>
```
### clear
```html
<style>
  .box1{
      width: 200px;
      height: 200px;
      background-color: #bfb;
      float: left;
  }
  .box3{
      width: 200px;
      height: 200px;
      background-color: #abf;
      clear: both;
  }
  .box2{
      width: 400px;
      height: 400px;
      background-color: #fbb;
      float: right;
  }
</style>
<body>
    <!--
        如果不希望某个元素因为其他浮动元素的影响而改变
        可以通过clear清除浮动元素对其他元素的影响
        clear
            left 清除左侧浮动元素的影响
            right 清除右侧浮动元素的影响
            both 清除两侧中影响最大的浮动元素的影响
        原理
            浏览器自动为元素设置margin-top
     -->
     <div class="box1">
     </div>
     <div class="box2"></div>
     <div class="box3"></div>
</body>
```
### 高度塌陷的最终解决方案
```html
    <style>
        .box1{
            border: 10px red solid;
        }
        .box2{
            width: 200px;
            height: 200px;
            background-color: #bfa;
            /* 子元素靠左浮动，撑不起父元素的大小了 */
            float: left;
        }
        /*
            子元素发生浮动，但是当::after处clear（）消除浮动元素的影响
            父元素必须将::after包含在内，于是父元素整体被撑开
                不过在这之前，因为::after是一个行内元素
                所以::after会在子元素的左边
                要先将::after块元素化

                ::before因为在.box1里面.box2的上方，所以包在内时依然受到浮动影响
         */
        .clearfix::after,
        .clearfix::before {
          content: '';
          display: table;
          clear: both;
        }
    </style>
<body>
    <div class="box1 clearfix">
        <div class="box2"></div>
        <!-- .box1::after就是现在所在的位置 -->
    </div>
</body>
```
```css
/* 下面这段代码可以同时解决高度塌陷和外边距重叠带动一起移动的问题 */
.clearfix::before,
.clearfix::after{
    content: '';
    /* 在子元素的上下设置一个空的table */
    display: table;
    clear: both;
}
```
## 定位

定位（position）
通过定位可以将元素改变到任意位置
    static 默认 不开启定位
    relative 相对定位
    absolute 绝对定位
    fixed 固定定位
    sticky 粘滞定位
### 相对定位
position: relative
相对于元素原来的位置定位 所以叫相对定位
在使用相对定位时，无论是否进行移动，元素仍然占据原来的空间。因此，移动元素会导致它覆盖其它框。
1、不设置偏移量，不会发生变化
    偏移量（offset）
        top
        bottom
        left
        right
    只针对开启了相对定位的元素
    偏移量为负时反方向移动
2、偏移是相对元素在文档流中的初始位置
3、开启相对定位会提升元素的层级，但是不改变元素的性质，也不会脱离文档流

### 绝对定位

position: absolute
相对定位的元素并未脱离文档流，而绝对定位的元素则脱离了文档流
绝对定位元素相对于最近的非 static 祖先元素定位。
    1、开启绝对定位，不设置偏移量，元素【位置】不发生变化
    2、开启绝对定位，元素从文档流中脱离
    3、开启绝对定位后，行内元素变成块元素，宽高被内容撑开
    4、开启绝对定位会提升层级（脱离文档流）
      开启相对定位不脱离文档流但是也会提升元素的层级
    5、正常情况相对当前最近的块元素
        开启绝对定位后的参照于最近的开启定位的包含块
        若所有最近的包含块都没有开启定位，则参照于HTML
        html称为初始包含块

### 固定定位

固定定位也是绝对定位的一种
大部分特点和绝对定位一样
但是固定定位是参照于浏览器的可视口
不会随着网页滚动条移动而移动
### 粘滞定位
```html
<!DOCTYPE html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <title>w3导航条</title>
    <link rel="stylesheet" href="../03layout/reset.css">
    <style>
        /*
            粘滞定位
                position:sticky;
                和相对定位类似，大部分特点相同
            可以在页面到达某个位置时，将元素固定
         */
         body{
             height: 8000px;
         }
        .box1{
            width: 1208px;
            height: 48px;
            background-color:rgb(232,231,227);
            margin: 600px auto;

            /* 注意！！！！！！！！ */
            position: sticky;
            top: 20px;
            /* 注意！！！！！！！！ */
        }

        .box1 .inner{
            float: left;
            height: 48px;
            /* 元素在父元素中成垂直居中 */
            line-height: 48px;

        }
        a{
            /* 重要：需要将超链接（行内元素）转换成块元素 */
            display: block;
            font-size: 18px;
            text-decoration: none;
            color: rgb(156,156,154);
            padding-left: 39px;
            padding-right: 39px;
        }
        .box1 .inner:last-child a{
            padding-left: 43px;
            padding-right: 45px;
        }
        a:hover{
            background-color: rgb(63,63,63);
            color:  #E8E7E3;
        }

        /*
            1.0
            问题：
                1、inner左右padding没有完全充满box1
                    解决
                    .box1 .inner:last-child a
                        box1里面最后一个inner的div里面的a设置左右内间距
                    原网页将每一个inner设置宽度
                2、box宽高不准
                    解决  截图工具量，打开网页观察
                3、超链接字鼠标移入效果
                    把超链接转换成块元素解决
                    可设置宽高和字颜色
         */
    </style>
</head>
<body>
    <div class="box1">
        <div class="inner">
            <a href="#">HTML/CSS</a>
        </div>
        <div class="inner">
            <a href="#">Browser Side</a>
        </div>
        <div class="inner">
            <a href="#">Sever Side</a>
        </div>
        <div class="inner">
            <a href="#">Programming</a>
        </div>
        <div class="inner">
            <a href="#">XML</a>
        </div>
        <div class="inner">
            <a href="#">Web Building</a>
        </div>
        <div class="inner">
            <a href="#">Reference</a>
        </div>
    </div>
</body>
</html>
```
### 绝对定位元素的位置
```html
    <style>
        .box1{
            width: 600px;
            height: 600px;
            background-color: #bfa;
            position: relative;
        }
        .box2{
            width: 100px;
            height: 100px;
            background-color: #abf;
            border-radius: 50%;
            box-shadow: 10px 10px 20px #ddd ;

            position: absolute;
            left: 20px;
            right: 0px;
            margin: 0px auto;
            /* right 被调整了 */

            top: 20px;
            bottom: 20px;
            margin-top: auto;
            margin-bottom: auto;
            /* 此时子元素位于包含块的中心位置 */

        }
        /*
            水平布局
                margin-left+border-left+padding-left+width+padding-right+border-right+margin-righht=包含块的宽度

                当过度约束时
                    若无auto属性值，浏览器自动调整right以使上式配平
                    当有auto时，先调整auto

                    left和right默认就是auto
                    当left和right为固定值，margin左右为auto时，元素居中
         */
         /*
            垂直布局
                margin-top+border-top+padding-top+height+padding-bottom+border-bottom+margin-bottom=包含块的height
          */
    </style>
<body>
    <div class="box1">
        <div class="box2"></div>
    </div>
</body>
```
### 元素的层级
元素层级
对于开启了定位的元素，z-index可以指定元素的层级高度

z-index需要一个整数值作为参数
值越大元素层级越高
若果层级都没指定，或者指定的参数值一样
则优先靠下的

祖先元素指定的层级再高，也不会高过子元素 “水涨船高”

元素的z-index默认是0
## 文字样式

```css
p {
    color: red;

    /*font-size设置字体大小，默认文字大小为16px
    font-size: 40px;

    /*font-family设置文字字体
     在网页中将字体分成5大类：
	   serif（衬线字体）
     sans-serif（非衬线字体）
     monospace （等宽字体）
     cursive （草书字体）
     fantasy （虚幻字体）
     可以将字体设置为这些大的分类,当设置为大的分类以后，
     浏览器会自动选择指定的字体并应用样式
     一般会将字体的大分类，指定为font-family中的最后一个字体
    */
    font-family: serif;

    /*font-style设置文字斜体
        normal 默认值，文字正常显示
        italic 文字会以斜体显示
        oblique 文字会以倾斜的效果显示
        italic和oblique效果往往一样
        一般都用italic
    */
    font-style: italic;

    /*font-weight设置文字加粗效果
      normal 默认值，文字正常显示
      bold 文字加粗显示
      该样式也可以指定100-900之间的9个值，但是由于用户的计算机往往没有这么多级别的字体
    */
    font-weight: bold;

    /*font-variant设置小型大写字母
     可选值：
         normal 默认值，文字正常显示
         small-caps 文本以小型大写字母显示
    */
    font-variant: small-caps;

    /*font样式简写文字样式
     文字的大小必须是倒数第二个样式
     文字的字体必须是倒数第一个样式
    */
    font: small-caps bold italic 60px "微软雅黑";
}
```
### 图标字体
```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <link rel="stylesheet" href="./fontawesome/font-awesome-4.7.0/css/font-awesome.css">
</head>
<body>
    <!--
        图标字体（icon）
        网页中常用的图标，可以用图片代替，但是体积大且不灵活
        所以将常用图标设置为字体格式
        通过font-face引入

        fontawesome
            1、通过类名引入
                class="fas/fab icon名"
     -->
     <i class="fas "></i>
</body>
</html>
```

## 文本样式

```css
P {
    /*设置行间距的方式
     CSS中没有提供直接行间距的方式
     只能通过设置行高来间接地设置行间距，行高越大行间距越大
     line-height设置行高
     行间距 = 行高 - 字体大小
     可选值：
         1.指定一个大小 如40px
         2.指定一个百分数，则会相对于文字大小去计算行高
         3.指定一个数值，则行高会设置文字大小的相应的倍数

     font样式也可以指定行高在字体大小后加反斜杠 /可选值
    */
    line-height: 2;
    font: 30px/2 sans-serif;

    /*text-transform设置文本大小写
         none 默认值 不做处理
         capitalize 单词首字母大写 通过空格识别单词
         uppercase 所有字母都大写
         lowercase 所有字母都小写
    */
    text-transform: lowercase;

    /*text-decoration设置文本的修饰
         none 默认值 不加任何修饰
         underline 为文本加下划线
         overline 为文本加上划线
         line-through 为文本加删除线

    超链接会默认添加下划线，需要将文本装饰样式设置为none
    */
    text-decoration: line-through;

    /*letter-spacing设置字符间距*/
    letter-spacing: 10px;

    /*word-spacing设置单词间距*/
    word-spacing: 20px;

    /*text-align设置文本对齐方式
      left 默认值，文本靠左对齐
      right 文本靠右对齐
      center 文本居中对齐
      justify  两端对齐 原理：通过调整文本之间的空格大小，来达到两端对齐
    */
    text-align: justify;

    /*text-indent设置文本首行缩进
     正值 向右缩进
     负值 向左缩进
     一般em作单位
    */
    text-indent: 2em;

    /*white-space设置文本是否换行
         normal 连续的空白符会被合并，换行符会被当作空白符来处理。填充line盒子时，必要的话会换行。
         nowrap 和 normal 一样，连续的空白符会被合并。但文本内的换行无效。
         pre 连续的空白符会被保留。在遇到换行符或者<br>元素时才会换行。
         pre-wrap 连续的空白符会被保留。在遇到换行符或者<br>元素，或者需要为了填充line盒子时才会换行。
         pre-line 连续的空白符会被合并。在遇到换行符或者<br>元素，或者需要为了填充line盒子时会换行。
         break-spaces 与 pre-wrap的行为相同，除了：
             任何保留的空白序列总是占用空间，包括在行尾。
             每个保留的空格字符后都存在换行机会，包括空格字符之间。
             这样保留的空间占用空间而不会挂起，从而影响盒子的固有尺寸（最小内容大小和最大内容大小）。
    */
    white-space:nowarp;

    /*text-overflow属性确定如何向用户发出未显示的溢出内容信号。它可以被剪切，
    显示一个省略号或显示一个自定义字符串。
        clip
            这个关键字的意思是"在内容区域的极限处截断文本"，因此在字符的中间可能会发生截断。
            为了能在两个字符过渡处截断，你必须使用一个空字符串值 ('')。此为默认值。
        ellipsis
            这个关键字的意思是“用一个省略号 ('…', U+2026 HORIZONTAL ELLIPSIS)来表示被截断的文本”。
            这个省略号被添加在内容区域中，因此会减少显示的文本。如果空间太小到连省略号都容纳不下，
            那么这个省略号也会被截断。
        <string>
            <string>用来表示被截断的文本。字符串内容将被添加在内容区域中，所以会减少显示出的文本。
            如果空间太小到连省略号都容纳不下，那么这个字符串也会被截断。*/
}
```
### 文本溢出显示省略
```css
.p{
  /* 文字不换行 */
  white-space: nowrap;
  /* 裁剪溢出的部分 */
  overflow: hidden;
  /* 文字溢出部分显示省略号 */
  text-overflow: ellipsis;
}
```
实际案例 文字换行+溢出显示省略
```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Document</title>
  <style>
    * {
      margin: 0;
      padding: 0;
      box-sizing: border-box;
    }

    body {
      width: 100vw;
      min-height: 100vh;
      font-family: "Exo", "Bungee Shade", cursive, Arial, sans-serif;
      background-color: #567;
      color: #fff;
      display: grid;
      place-content: center;
      gap: 1rem;
    }

    .container {
      overflow-x: hidden;
      resize: horizontal;
      min-inline-size: 320px;
      max-inline-size: 768px;
    }

    .cards {
      background-color: #fff;
      box-shadow: 0 0 .2em .25em rgb(0 0 0 / .125);
      border-radius: 4px;
      color: #333;
      display: flex;
      flex-direction: column;
    }

    .card__content {
      display: flex;
      flex-direction: column;
      gap: 1rem;
      padding: 1rem;
    }

    .card__content::-webkit-scrollbar-track {
      -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
      background-color: #F5F5F5;
      border-radius: 10px;
    }

    .card__content::-webkit-scrollbar {
      height: 4px;
      width: 4px;
      background-color: #F5F5F5;
      border-radius: 10px;
    }

    .card__content::-webkit-scrollbar-thumb {
      background-color: #f36;
      border: 1px solid rgb(0 0 0 / .12);
      border-radius: 10px;
    }

    .card {
      display: flex;
      border-radius: 4px;
      border: 1px solid rgb(0 0 0 / .01);
      box-shadow: 0 0 .25em .125em rgb(0 0 0 / .125);
      padding: 1rem;
      gap: 1rem;
    }

    .card img {
      display: block;
      object-fit: cover;
      object-position: center;
      border-radius: 4px;
      max-width: 80px;
      aspect-ratio: 1;
      border: 2px solid rgb(0 0 0 / .5);
      flex-shrink: 0;
    }

    .card h4 {
      font-size: clamp(.875rem, 3vw + 1rem, 1.25rem);
      white-space: nowrap;
      overflow: hidden;
      text-overflow: ellipsis;
    }

    .card p {
      margin-top: 10px;
      overflow: hidden;
      text-overflow: ellipsis;
      display: -webkit-box;
      -webkit-line-clamp: 2;
      -webkit-box-orient: vertical;
      line-height: 1.65;
      font-size: .75rem;
      color: #666;
    }

    .card .content {
      flex: 1 1 0%;
      min-width: 0;
    }

    button {
      --blue: #187ff5;
      --white: #ffffff;
      display: inline-flex;
      padding: .5em 1em;
      align-items: center;
      justify-content: center;
      border-radius: 6px;
      margin-block: auto;
      margin-inline-start: auto;
      border: 1px solid transparent;
      color: var(--white);
      background: var(--blue);
      flex-shrink: 0;
    }

    .card {
      display: flex;
      flex-wrap: nowrap;
    }
  </style>
</head>

<body>
  <div class="container">
    <div class="cards">
      <div class="card__content">
        <div class="card">
          <img src="https://picsum.photos/400/300?random=1" alt="">
          <div class="content">
            <h4>防御式 CSS</h4>
            <p>如何编写防御式 CSS，使你的代码变得更健壮！</p>
          </div>
          <button>阅读全文</button>
        </div>
        <div class="card">
          <img src="https://picsum.photos/400/300?random=2" alt="">
          <div class="content">
            <h4>现代 Web 布局</h4>
            <p>现代 Web 布局中深入介绍了如何使用 CSS 相关技术来构建更具有创意的 Web 布局!</p>
          </div>
          <button>阅读全文</button>
        </div>
        <div class="card">
          <img src="https://picsum.photos/400/300?random=3" alt="">
          <div class="content">
            <h4>CSS Flexbox Layout</h4>
            <p>CSS Flexbox 是 CSS 中非常重要的一个功能模块，它主要用来帮助 Web 开者快速构建 Web 布局。目前为止它已成为主流布局技术之一。</p>
          </div>
          <button>阅读全文</button>
        </div>
        <div class="card">
          <img src="https://picsum.photos/400/300?random=4" alt="">
          <div class="content">
            <h4>CSS Grid Layout</h4>
            <p>CSS Grid 和 CSS Flexbox 是当下最受欢迎的两种布局技术。CSS Grid 是唯一一种二维布局技术，使用它可以让 Web 开发者以最少的代码量，最快的速度构建出最复杂的 Web 布局。</p>
          </div>
          <button>阅读全文</button>
        </div>
        <div class="card">
          <img src="https://picsum.photos/400/300?random=5" alt="">
          <div class="content">
            <h4>Flexbox 和 Grid 中的换行</h4>
            <p>Flexbox 和 Grid 布局有着不同的换行技术与特点，那么在构建 Web 布局时，我们要如何使用它们的换行技术，才能让 Web 布局更灵活。</p>
          </div>
          <button>阅读全文</button>
        </div>
      </div>
    </div>
  </div>
</body>
</html>
```
## 背景background

background-image 用来设置背景图片
background-image: url("./大黄猫/dahuangmao-04.png");
同时设置背景颜色和图片则背景颜色成为图片的背景
若背景颜色大，则背景图重复

background-repeat 设置背景图片的重复方式
    repeat 默认 沿着x和y方向重复
    repeat-x 沿着x方向重复
    repeat-y 沿着y方向重复
    no-repeat 不重复

background-repeat: no-repeat;

background-position 可以用来设置图片的位置
  1、通过top left right bottom center确定图片的位置
      需要指定两个值，如果只有一个值，则第二个值默认为center
  2、通过像素指定

background-position: center center;
background-position: -64px 400px;
像素指定前一个值是x方向 后一个是y方向
#### background-size

background-size设置背景图片的大小
默认值auto 不可继承

属性值：
    百分比：指定背景图片相对背景区的百分比。背景区由background-origin设置。默认为盒模型的内容区和内边距
    auto：以背景图片的比例缩放背景图片

注意：单值时，这个值设置的是图片的宽度，图片的高度为auto
两个值时，第一个值设置图片的宽度，第二个值设置图片的高度

#### background-clip

background-clip  设置元素的背景（背景图片或颜色）是否延伸到边框下面。
如果没有设置背景图片（background-image）或背景颜色（background-color），那么这个属性只有在边框（ border）被设置为非固实（soild）、透明或半透明时才能看到视觉效果（与 border-style 或 border-image 有关），否则，本属性产生的样式变化会被边框覆盖。

属性值：
border-box
背景延伸至边框外沿（但是在边框下层）。

padding-box
背景延伸至内边距（padding）外沿。不会绘制到边框处。

content-box
背景被裁剪至内容区（content box）外沿。

text
背景被裁剪成文字的前景色。

    border-box （默认）背景出现在边框下
    padding-box 背景只出现在内容区和内边距
    content-box 只出现在内容区

#### background-origin设置背景图片偏移量的计算原点
background-origin 规定了指定背景图片background-image 属性的原点位置的背景相对区域.
初始值padding-box 不可继承
首先背景默认是从padding-box左上开始绘制，以boder-box右下为边界进行剪裁
可选值：
border-box
背景图片的摆放以border区域为参考
padding-box
背景图片的摆放以padding区域为参考
content-box
背景图片的摆放以content区域为参考
注意：当使用 background-attachment 为fixed时，该属性将被忽略不起作用。
    padding-box （默认）position从内边距的左上角开始计算
    content-box 从内容区的左上角开始计算
    border-box 从边框的左上角开始计算
#### background

此属性是一个 简写属性，可以在一次声明中定义一个或多个属性：background-clip、background-color、background-image、background-origin、background-position、background-repeat、background-size，和 background-attachment。

对于所有简写属性，任何没有被指定的值都会被设定为它们的 初始值。

```html
    <style>
        .box1{
            width: 500px;
            height: 500px;
            background-color: #abf;
            background-image: url("./小米首屏.png");
            background-repeat: no-repeat;
            /*
                background-size 设置图片大小
                第一个指定的值表示宽
                第二个指定的值表示高
                如果只指定一个值(x方向)，则第二个值默认为auto（比例不变）
                cover 图片比例不变，将元素铺满
                contain 图片比例不变，图片完整显示
             */
             /* background-size: cover; */
             /* background-size: contain; */
             background-size: 500px;

            /* 设置子元素的滚动条 */
             overflow: scroll;
        }
        .box2{
            width: 300px;
            height: 2000px;
            background-color: #fbb;
            background-image: url("./大黄猫/dahuangmao-04.png");
            background-repeat: repeat-x;
            /*
                background-attachment 设置背景图片是否随着背景颜色移动
                    可选值
                    scroll （默认）随着背景颜色的移动而移动
                    fixed 不随着背景颜色的移动而移动
             */
             background-attachment: fixed;
        }
    </style>
<body>
    <div class="box1">
        <div class="box2">
            今天天气真不错
            今天天气真不错
            今天天气真不错
            今天天气真不错
            今天天气真不错
            今天天气真不错
            今天天气真不错
            今天天气真不错
            今天天气真不错
            今天天气真不错
            今天天气真不错
            今天天气真不错
            今天天气真不错
            今天天气真不错
            今天天气真不错
            今天天气真不错
            今天天气真不错
            今天天气真不错
            今天天气真不错
            今天天气真不错
            今天天气真不错
            今天天气真不错
            今天天气真不错
            今天天气真不错
            今天天气真不错
            今天天气真不错
            今天天气真不错
        </div>
    </div>
</body>
```
## 渐变
```html
    <style>
        .box1{
            width: 200px;
            height: 200px;
            /*
                渐变可以设置一些复杂颜色
                渐变是图片而不是颜色
                background-image
                  linear-gradient
                    可以指定方向（默认向下）
                    在linear-gradient前面可以指定方向、
                    xxxdeg 角度
                    xxxturn 圈数
                      0turn=0deg
                      360deg=1turn=0turn
                      90deg=0.25turn
                      270deg=0.75turn
                      180deg=0.5turn

                  repeating-linear-gradient 平铺
             */
             background-image: linear-gradient(180deg,#bfa,#abf);
        }
        .box2{
            width: 200px;
            height: 200px;
            background-image: linear-gradient(0.5turn,#bfa,#abf);
        }
        .box3{
            width: 400px;
            height: 400px;
            /* 从50px到100px是红色到黄色的渐变，然后重复平铺 */
            background-image: repeating-linear-gradient(red 50px,yellow 100px);
        }
    </style>
<body>
    <div class="box1"></div>
    <div class="box2"></div>
    <div class="box3"></div>
</body>
```
### 径向渐变
```css
.box1{
  width: 400px;
  height: 400px;
  /* radial-gradient 从中心向四周方向的渐变叫做径向渐变 */
  background-image: radial-gradient(100px,#bfa,#abf);
}
```

------------------------------


# 三列布局
要求
1、两边固定，当中自适应
2、当中内容要完整显示
3、当中优先加载

定位写三列布局的缺点，定位必须要有包含块开启相对定位，提升层级，不利于写网页布局
浮动写三列布局，无法实现当中列优先加载
## 圣杯布局
```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>圣杯布局</title>
    <style>
        #header,
        #footer{
            height: 50px;
            line-height: 50px;
            background-color: #abf;
            border: 1px red solid;
            text-align: center
        }
        #body .left,
        #body .right{
            width: 200px;
            background-color: yellow;
            /* 开启浮动后父元素需要解决高度塌陷 */
            float: left;
        }
        /* 解决高度塌陷 */
        .clearfix::before,
        .clearfix::after{
            content: '';
            display: table;
            clear: both;
        }
        #body .middle{
            background-color: red;
            /* 让left和right上来需要开启浮动 */
            float: left;
            /* 浮动的元素宽高只占内容大小 所以指定宽度100%包含块宽度 */
            width: 100%;
        }
        #body .left{
            /* -100%和middle的包含块一样，所以向左边移动一个middle的宽度 */
            margin-left: -100%;
            position: relative;
            left: -200px;
        }
        #body .right{
            /* -200px指right的向左移动自己的宽度 居于middle的右边 */
            margin-left: -200px;
            position: relative;
            right: -200px;
        }
        /*
            让middle显示出来
            然后给left和right开启相对定位
         */
        #body{
            padding: 0 200px;
            /* 添加一个min-width否则最小窗口布局会乱 */
            min-width: 800px;
        }

    </style>
</head>
<body>
    <!--
        要求
        1、两边固定 当中自适应
        2、当中列要完整显示
        3、当中列要优先加载
     -->
    <div id="header">header</div>
    <div id="body" class="clearfix">
        <!-- 中间优先加载 写在最上面 -->
        <div class="middle">middle</div>
        <div class="left">left</div>
        <div class="right">right</div>
    </div>
    <div id="footer">footer</div>
</body>
</html>
```
## 圣杯伪等高布局
```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>圣杯布局</title>
    <style>
        #header,
        #footer{
            height: 50px;
            line-height: 50px;
            background-color: #abf;
            border: 1px red solid;
            text-align: center
        }
        #body .left,
        #body .right{
            width: 200px;
            background-color: yellow;
            /* 开启浮动后父元素需要解决高度塌陷 */
            float: left;
        }
        /* 解决高度塌陷 */
        .clearfix::before,
        .clearfix::after{
            content: '';
            display: table;
            clear: both;
        }
        #body .middle{
            background-color: red;
            /* 让left和right上来需要开启浮动 */
            float: left;
            /* 浮动的元素宽高只占内容大小 所以指定宽度100%包含块宽度 */
            width: 100%;
        }
        #body .left{
            /* -100%和middle的包含块一样，所以向左边移动一个middle的宽度 */
            margin-left: -100%;
            position: relative;
            left: -200px;
        }
        #body .right{
            /* -200px指right的向左移动自己的宽度 居于middle的右边 */
            margin-left: -200px;
            position: relative;
            right: -200px;
        }
        /*
            让middle显示出来
            然后给left和right开启相对定位
         */
        #body{
            padding: 0 200px;
            /* 添加一个min-width否则最小窗口布局会乱 */
            min-width: 800px;
        }
        /*
            伪等高布局
                将left right 和middle的padding向下延拓
                body的margin回到原来的位置
                裁剪多余空间
         */
        #body .left,
        #body .middle,
        #body .right{
            padding-bottom: 100000px;
            margin-bottom: -100000px;
        }
        #body{
            overflow: hidden;
        }
    </style>
</head>
<body>
    <!--
        要求
        1、两边固定 当中自适应
        2、当中列要完整显示
        3、当中列要优先加载
     -->
    <div id="header">header</div>
    <div id="body" class="clearfix">
        <!-- 中间优先加载 写在最上面 -->
        <div class="middle">middle
            <p>我是字</p>
            <p>我是字</p>
            <p>我是字</p>
            <p>我是字</p>
            <p>我是字</p>
            <p>我是字</p>
            <p>我是字</p>
            <p>我是字</p>
            <p>我是字</p>
            <p>我是字</p>
            <p>我是字</p>
            <p>我是字</p>
            <p>我是字</p>
            <p>我是字</p>
        </div>
        <div class="left">left</div>
        <div class="right">right</div>
    </div>
    <div id="footer">footer</div>
</body>
</html>
```
## 双飞翼伪等高布局
双飞翼布局和圣杯布局的不同点

圣杯布局将body设置padding再将left和right通过相对行为设置到middle的两边

双飞翼布局在middle中再设置一个div 将内容写在div中，对div设置padding


双飞翼布局和圣杯布局的共同点

两种布局都将中间列写在前面优先加载，都将三列浮动然后通过负外边距完成布局
```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>双飞翼伪等高布局</title>
    <link rel="stylesheet" href="./reset.css">
    <style>
        body{
            /* 设置最小宽度确保middle有内容显示 */
            /* 最小宽度设置为2*left+right */
            min-width: 600px;
        }
        .header,
        .footer{
            height: 50px;
            background-color: silver;
        }
        /* 设置body */
        /* 设置伪等高 */
        .body{
            overflow: hidden;
        }
        .body .middle,
        .body .left,
        .body .right{
            padding-bottom: 10000px;
            margin-bottom: -10000px;
            float: left;
        }
        .body .left,
        .body .right{
            background-color: #bfa;
            width: 200px;
        }
        .body .middle{
            width: 100%;
            background-color: #abf;
        }
        .body .left{
            margin-left: -100%;
        }
        .body .right{
            margin-left: -200px;
        }
        /*
            双飞翼布局和圣杯布局的不同点
                圣杯布局将body设置padding再将left和right通过相对行为设置到middle的两边
                双飞翼布局在middle中再设置一个div 将内容写在div中，对div设置padding
        */
        .body .middle .box1{
            padding: 0 200px;
        }
    </style>
</head>
<body>
    <div class="header"></div>
    <div class="body">
        <div class="middle">
            <div class="box1">
                <p>我是字</p>
                <p>我是字</p>
                <p>我是字</p>
                <p>我是字</p>
                <p>我是字</p>
                <p>我是字</p>
            </div>
        </div>
        <div class="left">
            <p>left</p>
        </div>
        <div class="right">
            right
        </div>
    </div>
    <div class="footer"></div>
</body>
</html>
```
# 解决IE6的fixed失效问题（使用绝对定位模拟固定定位）
当html和body其中有一个设置了overflow滚动条设置给文档
当html和body两个都设置了overflow的滚动条作用给文档，body的滚动条作用给自己
div的父元素是body，body的父元素是html，html的包含块是视窗大小的初始包含块
```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <link rel="stylesheet" href="./reset.css">
    <style>
        html,body{
            /* 子元素撑开的高度 */
            height: 100%;
            /* 禁止系统滚动条 */
            overflow: hidden;
        }
        body{
            /*
                因为html和body都设置了overflow
                所以此时body的滚动条作用给自己（body）
                此时滚动条滚动的是body而非初始包含块
                因为body里面的开启绝对定位的元素相对于初始包含块定位的，
                初始包含块没有移动
                所以用绝对定位模拟了固定定位
                固定定位是相对于视口定位的，初始包含块一般就是一个视口大小的矩形
            */
            overflow: auto;
            border: 2px red solid;
        }
        .test{
            width: 200px;
            height: 200px;
            background-color: #abf;
            /* 写在第一个div下面看不见，开启绝对定位 */
            position: absolute;
            left: 100px;
            top: 100px;
        }
    </style>
</head>
<body>

    <div class="box1" style="height: 2000px;"></div>
    <!-- 此时系统出现滚动条 -->
    <div class="test"></div>
</body>
</html>
```
# stickyfooter 粘连布局
```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <title>Document</title>
    <style>
        /*
            要求
            1、main的内容足够长时，footer跟着main
            2、main的长度达不到页面高度时，footer保持在页面底部
        */
        *{
            margin: 0;
            padding: 0;
        }
        html,
        body{
            height: 100%;
        }
        .container{
            /*
                实际上.container容器不能定死高度，否则footer永远位于页面底部
                .container的最小高度为100%（继承自html下的body）
                当.main的内容足够长时，.container的高度被其中的.main撑开
            */
            min-height: 100%;
        }
        /* 设置footer样式 */
        .footer{
            height: 60px;
            line-height: 60px;
            text-align: center;
            background-color: silver;
            /* 负margin将footer移动上来 */
            margin-top: -60px;
        }
        /*
            此时还有个问题，当main中内容足够多时，底部会和footer重叠
            应该给.main设置padding-bottom
            为什么不给.container加padding
            因为加后会发生.container加高60px，footer挤到下面
        */
        .main{
            padding-bottom: 60px;
        }
    </style>
</head>
<body>
    <div class="container">
        <div class="main">
            main<br>
            main<br>
            main<br>
            main<br>
            main<br>
        </div>
    </div>
    <div class="footer">
        footer
    </div>
</body>
</html>
```
## 两列布局
```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        *{
            margin: 0;
            padding: 0;
        }
        body{
            min-width: 600px;
        }
        .left{
            width: 200px;
            height: 200px;
            background-color: #bfa;
            float: left;
        }
        .right{
            height: 200px;
            background-color: #abf;
            overflow: hidden;
        }
    </style>
</head>
<body>
    <div class="left">left</div>
    <div class="right">right</div>
</body>
</html>
```
# 兄弟元素margin重叠问题
```html
    <style>
        /*
            发生，margin重叠的三个条件
                1、属于同一个BFC
                2、两个相邻的box
                3、块级元素
            margin重叠的三个条件只要有一个不满足就不会发生重叠
        */
        .box1,
        .box2,
        .box4,
        .box5,
        .box6,
        .box7{
            width: 200px;
            height: 200px;
            background-color: #abf;
            margin: 100px;
        }
        /*
            不满足条件1
            box2放到另一个开启BFC的容器中
        */
        .box3{
            overflow: hidden;
        }
        /*
            不满足条件2
            在box4和box5中间加一个有高度的div
            不建议，因为height发生变化了
        */
        .box{
            height: 1px;
        }
        /*
            不满足条件3
            将其中一个box(box6)设置为inline-block
            不满足块元素
        */
        .box6{
            display: inline-block;
        }
    </style>
<body>
    <div class="box1">1</div>
    <div class="box3">
        <div class="box2">2</div>
    </div>

    <div class="box4">4</div>
    <div class="box"></div>
    <div class="box5">5</div>

    <div class="box6">6</div>
    <div class="box7">7</div>
</body>
```
# 父子元素的margin传递问题
```html
    <style>
        .box1 {
            width: 300px;
            height: 300px;
            background-color: #abf;
        }

        .box2 {
            width: 100px;
            height: 100px;
            background-color: #bfa;
            margin: 50px;
        }

        /*
          发生margin重叠的三个条件
              1、属于同一个BFC
              2、两个相邻的box
              3、块级元素
          margin重叠的三个条件只要有一个不满足就不会发生重叠
        */
        /*
            解决方法
                1、套一个div
                2、添加border隔开
                3、设置行内块
        */
        /* 1 */
        .box {
            /* 开启BFC第四条 */
            overflow: hidden;
        }
    </style>
<body>
    <div class="box1">
        <div class="box">
            <div class="box2"></div>
        </div>
    </div>
</body>
```
# 子元素浮动父元素塌陷问题

清除浮动
  1、直接加高度（扩展性不好）
  2、开启BFC（IE6/7不支持）
      1.根元素
      2.float属性不为none
      3.position为absolute或fixed
      4.overflow不为visible
      5.display为inline-block,table-cell,table-caption,flex,inline-flex
      浮动盒子和定位盒子宽高都由内容决定
  3、br标签清浮动（IE6不支持）
  4、空标签清浮动（违反结构行为样式相分离的原则）
  5、伪元素（推荐 但是要加haslayout）

### 方式1
```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>清除浮动方式一</title>
    <style>
        .box1{
            border: 2px red solid;
            /* 直接加高度 */
            height: 200px;
        }
        .box2{
            width: 200px;
            height: 200px;
            background-color: silver;
            float: left;
        }
    </style>
</head>
<body>
    <div class="box1">
        <div class="box2"></div>
    </div>
</body>
</html>
```
### 方式2
```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>清除浮动方式二</title>
    <style>
        .box1{
            border: 2px red solid;
            /* 开启BFC */
            /* overflow: hidden; */
            /* position: absolute; */
            /* float: left; */
            display: inline-block;
        }
        .box2{
            width: 200px;
            height: 200px;
            background-color: silver;
            float: left;
        }
    </style>
</head>
<body>
    <div class="box1">
        <div class="box2"></div>
    </div>
</body>
</html>
```
### 方式3
```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>清除浮动方式三</title>
    <style>
        .box1{
            border: 2px red solid;
        }
        .box2{
            width: 200px;
            height: 200px;
            background-color: silver;
            float: left;
        }
    </style>
</head>
<body>
    <div class="box1">
        <div class="box2"></div>
        <!-- br标签 -->
        <br clear="both">
    </div>
</body>
</html>
```
### 方式4
```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>清除浮动方式三</title>
    <style>
        .box1{
            border: 2px red solid;
        }
        .box2{
            width: 200px;
            height: 200px;
            background-color: silver;
            float: left;
        }
    </style>
</head>
<body>
    <div class="box1">
        <div class="box2"></div>
        <!-- 空标签 -->
        <div style="clear: both;"></div>
    </div>
</body>
</html>
```
### 方式5
```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>清除浮动方式三</title>
    <style>
        .box1{
            border: 2px red solid;
        }
        .box2{
            width: 200px;
            height: 200px;
            background-color: silver;
            float: left;
        }
        /* 伪元素 */
        .box1::after{
            content: '';
            display: table;
            clear: both;
        }
    </style>
</head>
<body>
    <div class="box1">
        <div class="box2"></div>
    </div>
</body>
</html>
```
# 元素垂直水平居中
// TODO: 垂直水平居中

# 图片垂直居中
CSS 的属性 vertical-align 用来指定行内元素（inline）或表格单元格（table-cell）元素的垂直对齐方式。
注意 vertical-align 只对行内元素、表格单元格元素生效：不能用它垂直对齐块级元素。
```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>图片垂直水平居中</title>
    <style>
        * {
            margin: 0;
            padding: 0;
        }
        html , body {
            height: 100%;
            text-align: center;
        }
        body:after {
            content: "";
            display: inline-block;
            height: 100%;
            vertical-align: middle;
        }
        img {
            width: 200px;
            vertical-align: middle;
        }
    </style>
</head>
<body>
    <img src="../img/01.jpg" alt="">
</body>
</html>
```


------------------------------
## 动画
### 过渡
```html
    <style>
        .box1{
            width: 600px;
            height: 600px;
            background-color: silver;
        }
        .box2{
            width: 80px;
            height: 80px;
            background-color: #aabbff;
            margin-left: 0px;
            transition-property: all;
            transition-duration: 2s;
            transition-timing-function: cubic-bezier(0.77,1.46,1,-0.84);
        }
        .box3{
            width: 80px;
            height: 80px;
            background-color: #bfa;
            margin-top: 200px;
            transition-property: all;
            transition-duration: 2s;
        }
        .box4{
            width: 80px;
            height: 80px;
            margin-top: 160px;
            background-color: teal;
            transition-duration: 2s;
            transition-timing-function: ease-in;
            transition-delay: 1s;
        }
        /*
            过渡（transition）指定属性发生变化的方式
                transition—property 指定要过渡的属性，多个属性用逗号隔开
                transition-duration 指定效果持续的时间，单位s/ms
                transition-timing-function 指定过渡的时序函数
                    ease（默认）慢速开始，先加速，再减速
                    linear 线性运动
                    ease-in 加速运动
                    ease-out 减速运动
                    ease-in-out 先加速，再减速
                    cubic-bezier() 贝塞尔曲线
                    https://cubic-bezier.com

                    steps() 变化的步骤
                        可以指定第二个值end（默认）、start 表示在时间的结束发生运动和时间的开始发生运动

                transition-delay 表示延迟一段时间后开始发生运动
                transition 可以简写以上所有属性 但是前一个时间表示持续时间 后一个时间表示延迟时间
        */
        .box1:hover div{
            margin-left: 520px;
            background-color: yellow;
        }
    </style>
<body>
    <div class="box1">
        <div class="box2"></div>
        <div class="box3"></div>
        <div class="box4"></div>
    </div>
</body>
```
### 动画

设置关键帧 @keyframes 指定名字
    from{} 表示起点
    to{} 表示终点
    可以用百分数指定中间的位置
animation-name 指定刚刚设置的关键帧
animation-duration 动画的执行时间
animation-timing-function 动画的形式
animation-iteration-count 执行次数
    可选值
    整数
    infinite 无限次
animation-direction 动画的方向
    可选值
    normal（默认）from-> to
    reverse 反向 to ->from
    alternate 两边来回
    alternate-reverse 反向的两边来回
animation-play-state 动画状态
    running
    paused 暂停
animation-fill-mode 动画的填充
    none(默认) 执行完毕元素回到初始位置
    forwards 执行完毕元素停在to终点状态
    backwards 动画开始前元素就已经处于from起点状态

### 平移
```html
    <style>
        /*
            变形 不改变布局
            transform
                可选值
                    平移
                    translatex() 向x方向移动
                    translatey() 向y方向移动
                    translatez() 向z方向移动
        */
        .box1{
            width: 200px;
            height: 200px;
            background-color: #abf;
            transform: translatex(100px) translateY(50%);
            /*
            平移也可以使用百分数指定移动距离 百分数相对于自身
            如果指定多个移动效果 需要写在同一个transform后面 空格隔开
            */
        }
        /*
            元素居中的设置
                开启绝对定位时
                top、bottom、left、right同时设为0
                然后设置margin为auto
                但是此方法只适用于固定了大小的元素
                否则元素会占满可视窗口
        */
        .box2{
            background-color: orangered;
            position: absolute;
            /* 相对于包含块移动50% */
            left: 50%;
            top: 50%;
            /* 元素根据自己的实际大小偏移居中 */
            transform: translateX(-50%) translateY(-50%);
        }
    </style>
<body>
    <div class="box1"></div>
    <div class="box2">aaaaaaaaaa</div>
</body>
```
### 旋转

旋转
transform
rotateX()
rotateY()
rotateZ()
旋转单位deg角度 turn 圈数

### 缩放
transform: scaleX();
transform: scaleY();
transform: scaleZ();
括号内为缩放的倍数
缩放原理是对轴进行拉长
所以xoy平面对z轴拉长无效
scale（）表示x和y两个方向拉伸

# Flex 布局
CSS3 弹性盒子（Flexible Box或Flexbox），是一种用于在页面上布置元素的布局模式，使得当页面布局必须适应不同的屏幕尺寸和不同的显示设备时，元素可预测地运行行/列。
对于许多应用程序，弹性盒子模型提供了对块模型的改进，因为它不适用浮动，flex容器的边缘也不会与其内容的边缘折叠。

## display
要想设置flex布局需要指定一个容器作为父容器 父容器中的子元素按照flex相关规则进行布局
任何一个容器都可以设置为flex布局
一个元素可以既是弹性元素又是弹性容器
弹性盒子可以嵌套
```css
.container {
  /* 可以有两种取值 */
  display: flex | inline-flex;
}
```
当设置 flex 布局之后，子元素的 float、clear、vertical-align 的属性将会失效。

### flex和inline-flex的区别
设置flex布局的父容器 如果没有指定宽度
display: flex 默认占据父元素的宽度 (独占一行)
display: inline-flex 默认占所有子元素的宽度和 (不独占一行)

行内元素和块元素一旦设置display 均可以设置宽高了

inline-flex容器为inline特性，因此可以和图片文字一行显示
flex容器保持块状特性，宽度默认100%，不和内联元素一行显示

## flex父元素的属性
### flex-direction
决定主轴的方向(即项目的排列方向)
与主轴垂直的方向称为侧轴
flex-direction
    可选值
    row(默认)横排
    row-reverse 横排反向
    column 纵向
    column-reverse 纵向反向
```css
.container {
  display: flex;
  flex-direction: row | row-reverse | column | column-reverse;
}
```
### flex-wrap
flex-wrap: 决定容器内项目是否可换行
默认情况下，项目都排在主轴线上，使用 flex-wrap 可实现项目的换行。
  可选值
  nowrap (默认)不换行
  wrap 换行
  wrap-reverse 沿着副轴的反向换行 即: 向上换行，第一行在下方, 折叠的第二行元素在第一行上面
```css
.container {
  display: flex;
  flex-wrap: nowrap | wrap | wrap-reverse;
}
```
wrap会给每行元素自动等比例分配剩下的剩余空间 而不是上下两行紧贴在一起

### gap
gap是row-gap 和 column-gap的合并
gap如果只有一个属性值表示行间隔和列间隔相同
flex-warp的换行之后可以使用row-gap设置行间隔

### flex-flow
flex-direction 和 flex-wrap 的简写形式
默认值为: row nowrap
```css
.container {
  /* 即可以只输入一个值 flex-wrap则取默认值 no-wrap */
  flex-flow: <flex-direction> || <flex-wrap>;
}
```

### justify-content
定义了项目在主轴的对齐方式
justify-content 如何分配主轴上的空白空间
  可选值
  flex-start(默认值) 沿着主轴 靠主轴起点排列
  flex-end 沿着主轴 靠终点排列
  center 居中排列
  space-around 空白分配在元素两侧
  space-between 空白分配在元素中间
  space-evenly 空白分配到元素单侧 兼容性不好 很少用

```css
.container {
  display: flex;
  justify-content: flex-start | flex-end | center | space-between | space-around;
}
```

### align-items
定义了项目在副轴上的对齐方式
align-items 元素长度设置为同一长度
  可选值
  stretch (默认值) 元素会拉伸 将元素的高设为相同的值 即如果子项目未设置高度或者设为 auto，将占满整个容器的高度
  flex-start 元素不会拉伸 沿着副轴的起点对齐
  flex-end 元素不会拉伸 沿着副轴的终点对齐
  center 居中对齐
  baseline: 项目的第一行文字的基线对齐
```css
.container {
  display: flex;
  align-items: flex-start | flex-end | center | baseline | stretch;
}
```

### align-content
align-content可以看成和justify-content是相似且对立的属性，justify-content指明水平方向flex子项的对齐和分布方式，而align-content则是指明每一行flex元素的对齐和分布方式

如果所有flex子项只有一行(注意 这是默认主轴是水平的 如果主轴是竖直方向 则这里表示为一列)，则align-content属性是没有任何效果的

stretch
默认值。每一行flex子元素都等比例拉伸。例如，如果共两行flex子元素，则每一行拉伸高度是50%。
flex-start
逻辑CSS属性值，与文档流方向相关。默认表现为顶部堆砌。
flex-end
逻辑CSS属性值，与文档流方向相关。默认表现为底部堆放。
center
表现为整体垂直居中对齐。
space-between
表现为上下两行两端对齐。剩下每一行元素等分剩余空间。
space-around
每一行元素上下都享有独立不重叠的空白空间。
space-evenly
每一行元素都完全上下等分。

## flex子元素的属性
### order
定义该子元素在容器中的排列顺序，数值越小，排列越靠前
默认值为 0  可以为负数
```css
.item {
  order: <number>;
}
```
### flex-basis
定义了在分配多余空间之前，子元素占据的主轴空间，浏览器根据这个属性，计算主轴是否有多余空间
默认值：auto，即项目本来的大小, 这时候 item 的宽高取决于 width 或 height 的值
当主轴为水平方向的时候，设置了 flex-basis，项目的宽度设置值会失效
flex-basis 需要跟 flex-grow 和 flex-shrink 配合使用才能发挥效果

当 flex-basis 值为 0 % 时，是把该项目视为零尺寸的(有子元素就收缩到子元素的宽度 高度不变)，故即使声明该子元素的宽，也并没有什么用。
元素最后的在主轴上的表现会根据, flex-grow和flex-shrink决定, (grow默认0, shrink默认1, 所以会表现成缩小到子元素的宽度)

当 flex-basis 值为 auto 时，则跟根据尺寸的设定值(假如为 100px)，则这 100px 不会纳入剩余空间。

```css
.item {
  flex-basis: <length> | auto;
}
```
### flex-grow
定义该子元素的放大比例
默认值为 0，即如果存在剩余空间，也不放大
负值无效(等同于0)
```css
.item {
  flex-grow: <number>;
}
```
当所有的子元素都以 flex-basis 的值进行排列后，仍有剩余空间，那么这时候 flex-grow 就会发挥作用了
如果所有子元素的 flex-grow 属性都为 1，则它们将等分剩余空间(如果有的话)

如果一个子元素的 flex-grow 属性为 2，其他项目都为 1，则前者占据的 剩余空间 将比其他项多一倍

当然如果当所有项目以 flex-basis 的值排列完后发现空间不够了，且 flex-wrap：nowrap 时，此时 flex-grow 则不起作用了

如果flex-wrap为wrap 那么该子元素的flex-grow表现是基于该元素所在的行的剩余空间计算的
假设有一个元素换行到第二行 第二行只有这一个元素 该元素的grow将只考虑自己

如果所有子元素的flex-grow都小于1 注意当grow加起来小于1,那还剩空白空间
比如三个元素 每个flex-grow设为0.25  那三个元素均分了剩余空白的0.75部分


### flex-shrink
定义了项目的缩小比例
默认值: 1，即如果空间不足，该项目将缩小，负值对该属性无效。
```css
.item {
  flex-shrink: <number>;
}
```
如果所有项目的 flex-shrink 属性都为 1，当空间不足时，都将等比例缩小
如果一个项目的 flex-shrink 属性为 0，其他项目都为 1，则空间不足时，前者不缩小
flex-shrink值越大 缩小的越多(相对比例)

### flex
flex-grow, flex-shrink 和 flex-basis的简写
三个值 增长系数 缩减系数 基础长度
顺序不可乱
默认值initial 相当于0 1 auto
auto 相当于 1 1 auto
none 相当于0 0 auto

0 相当于 0 1 0%
1 相当于 1 1 0%
一个非负数字x，相当于 x 1 0%

当 flex 取值为一个长度或百分比，则视为 flex-basis 值，flex-grow 取 1，flex-shrink 取 1
```css
.item-1 {flex: 0%;}
.item-1 {
  flex-grow: 1;
  flex-shrink: 1;
  flex-basis: 0%;
}

.item-2 {flex: 24px;}
.item-2 {
  flex-grow: 1;
  flex-shrink: 1;
  flex-basis: 24px;
}
```

当 flex 取值为两个非负数字，则分别视为 flex-grow 和 flex-shrink 的值，flex-basis 取 0%
```css
.item {flex: 2 3;}
.item {
  flex-grow: 2;
  flex-shrink: 3;
  flex-basis: 0%;
}
```
当 flex 取值为一个非负数字和一个长度或百分比，则分别视为 flex-grow 和 flex-basis 的值，flex-shrink 取 1
```css
.item {flex: 11 32px;}
.item {
  flex-grow: 11;
  flex-shrink: 1;
  flex-basis: 32px;
}
```

### align-self
单独指定该子元素的排列方式


----------


# Grid布局
父容器设置display: grid; 开启grid
一旦这样做，这个元素的所有直系子元素将成为网格项目

```html
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <title>Grid</title>
  <style>
    .wrapper {
      margin: 60px;
      /* 声明一个容器 */
      display: grid;
      /*  声明列的宽度  */
      /* 也可以使用百分比设置列宽度 */
      grid-template-columns: repeat(4, 200px);
      /* grid-template-columns: 25% 25% 25% 25% */
      /*  声明行间距和列间距  */
      grid-gap: 20px;
      /*  声明行的高度  */
      grid-template-rows: 100px 200px;
    }

    .one { background: #19CAAD; }
    .two { background: #8CC7B5; }
    .three { background: #D1BA74; }
    .four { background: #BEE7E9; }
    .five { background: #E6CEAC; }
    .six { background: #ECAD9E; }
    .item {
      text-align: center;
      font-size: 200%;
      color: #fff;
    }
  </style>
</head>

<body>
  <div class="wrapper">
    <div class="one item">One</div>
    <div class="two item">Two</div>
    <div class="three item">Three</div>
    <div class="four item">Four</div>
    <div class="five item">Five</div>
    <div class="six item">Six</div>
  </div>`
</body>

</html>
```
## grid-template-columns 定义行元素
父元素属性
用于设置每一列所占的宽度
单位有 % px fr
### fr单位
fr 单位代表网格容器中可用空间的一等份。
grid-template-columns: 200px 1fr 2fr 表示第一个列宽设置为 200px，后面剩余的宽度分为两部分，宽度分别为剩余宽度的 1/3 和 2/3。
```css
.grid {
  display: grid;
  grid-template-columns: repeat(4, 1fr);
  grid-column-gap: 10px;
}
```

每列之间有10px间隙,如果使用fr单位,表示把当前vw减去了间隙后再除以4

自由的平分剩余空间

## grid-template-rows
父元素属性
同grid-template-columns, 不过是区分纵向布局

## column-gap
设置列之间的间隙 px %

## row-gap
设置列之间的间隙 px %

## gap
简写
gap: <row-gap> / <column-gap>;
如果只有一个值相当于同时设置行简写和列间隙

## minmax函数
用来设置一个长度范围

## grid-column-start: 3
子元素向后排到多少格开始,同时后面的元素向后

## grid-column-end: 5;
子元素向后排到多少格结束的左边边缘,同时后面的元素向后
和grid-column-start搭配使用可以使一个格子占多个格子

例如: child占据34两个格子
```css
.child {
  grid-column-start: 3;
  grid-column-end: 5;
}
```

## 简写grid-column: <start> / <end>;
##   grid-column-start: span 2
表示直接占据两个格子

##  grid-row-start: 2
表示子元素直接从第二行开始

## 简写 grid-row: <start> / <end>;


##   grid-template-areas
给划分出来的格子区域进行命名
```css
.wrapper {
  grid-template-areas: "header header header"
    "nav content content"
    "nav content content"
    "footer footer footer";
}

#header {
  grid-area: header;
}

#nav {
  grid-area: nav;
}

#content {
  grid-area: content;
}

#footer {
  grid-area: footer;
}
```

##   grid-area
直接设置子元素的区域
`grid-area: column-start/row-start/row-end/column-end`

## auto-fill
自动填充
让一行或者一列中尽可能填充多个元素
```css
/* 列宽固定为300px 但是随着视口宽度的变化自动塞下能塞下的元素 */
grid-template-columns: repeat(auto-fill, 300px);
grid-template-columns: repeat(auto-fill, minmax(300px, 1fr));
```

## grid-auto-flow
控制自动布局算法运作，精确指定在网格中被自动布局的元素怎样排列。
默认的放置顺序是"先行后列"，即先填满第一行，再开始放入第二行，


grid-auto-flow: row dense，表示尽可能填满表格
即如果某一行没有填满,会从后面挑一个能填满的来填满这一行

## grid-template
缩写grid-template-areas grid-template-rows grid-template-columns
```css
.wrapper {
  grid-template:
    "redLand redLand ." 100px
    "blueLand greenLand greenLand" 200px
    "blueLand greenLand greenLand" 1fr / 1fr 1fr 1fr
}
```
注意area后面跟着的是rows的布局
最后加上斜杠后grid-template-columns


## justify-items
设置单元格内 内容的水平位置
## align-items
属性设置单元格内 内容的垂直位置
子元素在对应的格子里靠左或者靠右 靠上或者靠下
start, end, center

## 简写place-items: <align-items> <justify-items>;

## justify-content, align-content
整个内容区域在容器里面的位置


--------------



### CSS样式来源
CSS样式的来源大致为三种： 1.开发人员 2.用户（Internet选项-辅助功能-用户样式表） 3.用户代理

权重（由高到底）： 用户的重要声明 开发人员的重要声明 开发人员的正常声明 用户的正常声明 用户代理的声明

# 自定义字体&字体图标
@font-face 允许网页开发者为其网页指定字体 字体能从远程服务器或者用户本地安装的字体加载
如果提供了local()函数，从用户本地查找指定的字体名称，并且找到了一个匹配项, 本地字体就会被使用. 否则, 字体就会使用url()函数下载的资源
注意：不能在一个CSS选择器中定义@font-face
font-family 所指定的字体名字将会被用于font或font-family属性

# 文本新增样式
### opacity
透明度 不是一个可继承属性 0~1
### text-shadow
text-shadow为文字添加阴影。可以为文字与 text-decorations 添加多个阴影，阴影值之间用逗号隔开。
每个阴影值由元素在X和Y方向的偏移量、模糊半径和颜色值组成。
默认值：none 不可继承

color 可选。可以在偏移量之前或之后指定。如果没有指定颜色，则使用UA（用户代理）自行选择的颜色。 Note: 如果想确保跨浏览器的一致性，请明确地指定一种颜色

offset-x offset-y 必选。这些长度值指定阴影相对文字的偏移量。offset-x 指定水平偏移量，若是负值则阴影位于文字左边。 offset-y 指定垂直偏移量，若是负值则阴影位于文字上面。如果两者均为0，则阴影位于文字正后方（如果设置了blur-radius 则会产生模糊效果)。

blur-radius 可选。这是 length 值。如果没有指定，则默认为0。值越大，模糊半径越大，阴影也就越大越淡

语法：
长度的三个值分别代表x偏移  y偏移  模糊半径
长度值不接受百分比
```
/* offset-x | offset-y | blur-radius | color */
text-shadow: 1px 1px 2px black;

/* color | offset-x | offset-y | blur-radius */
text-shadow: #fc0 1px 0 10px;

/* 可以指定多层阴影，用逗号隔开 */
text-shadow: black 10px 10px 5px, red 20px 20px 5px;
```
### 头像-背景模糊
```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>模糊背景</title>
    <style>
        *{
            margin: 0;
            padding: 0;
        }
        .box{
            width: 100%;
            height: 100px;
            background-color: rgba(0,0,0,0.5);
            position: relative;
        }
        .box .bac{
            position: absolute;
            left: 0;
            right: 0;
            bottom: 0;
            top: 0;
            background-color: red;

            /* 降低层级 否则背景图在头像上面*/
            z-index: -1;
            /* 设置最下面的div背景为图片 */
            background: url(./img/1.JPG) no-repeat;
            /* 背景图按照背景的尺寸100%平铺 不重复 */
            background-size: 100% 100%;

            /* 设置模糊 */
            filter: blur(15px);

        }
        img{
            width: 64px;
            height: 64px;
            margin: 24px 24px;
        }
    </style>
</head>
<body>
    <div class="box">
        <img src="./img/1.JPG" alt="">
        <div class="bac"></div>
    </div>
</body>
</html>
```

### 文字排版
direction 属性用来设置文本、表列水平溢出的方向。
rtl 表示从右到左 (类似希伯来语或阿拉伯语)， ltr 表示从左到右 (类似英语等大部分语言).
从右到左一定要配合unicode-bidi:bidi-override;来使用
```css
.box{
    width: 200px;
    height: 200px;
    border: 1px red solid;
    margin: 80px auto;
    direction: rtl;
    unicode-bidi:bidi-override;
}
```

# 盒模型新增样式
## 盒模型阴影
box-shadow
box-shadow 属性用于在元素的框架上添加阴影效果 ，该属性可设置的值包括，X偏移，Y偏移，阴影模糊半径，阴影扩散半径，和阴影颜色并以多个逗号分隔。
默认初始值none 适用于几乎任何元素 不可继承

### 取值：
inset
如果没有指定inset，默认阴影在边框外，即阴影向外扩散。
使用 inset 关键字会使得阴影落在盒子内部 。此时阴影会在边框之内 (即使是透明边框）、背景之上、内容之下。

offset-x offset-y
这是头两个 length 值，用来设置阴影偏移量。x,y 是按照数学二维坐标系来计算的，只不过y垂直方向向下。 offset-x 设置水平偏移量，正值阴影则位于元素右边，负值阴影则位于元素左边。
offset-y 设置垂直偏移量，正值阴影则位于元素下方，负值阴影则位于元素上方。可用单位请查看 length。
如果两者都是0，那么阴影位于元素后面。这时如果设置了blur-radius 或spread-radius 则有模糊效果。需要考虑 inset

blur-radius
这是第三个 length 值。值越大，模糊面积越大，阴影就越大越淡。 不能为负值。
默认为0，此时阴影边缘锐利。本规范不包括如何计算模糊半径的精确算法

spread-radius
这是第四个 length 值。取正值时，阴影扩大；取负值时，阴影收缩。
默认为0，此时阴影与元素同样大。
需要考虑 inset

color
如果没有指定，则由浏览器决定——通常是color的值，不过目前Safari取透明。

语法：
```
/* x偏移量 | y偏移量 | 阴影颜色 */
box-shadow: 60px -16px teal;

/* x偏移量 | y偏移量 | 阴影模糊半径 | 阴影颜色 */
box-shadow: 10px 5px 5px black;

/* x偏移量 | y偏移量 | 阴影模糊半径 | 阴影扩散半径 | 阴影颜色 */
box-shadow: 2px 2px 2px 1px rgba(0, 0, 0, 0.2);

/* 插页(阴影向内) | x偏移量 | y偏移量 | 阴影颜色 */
box-shadow: inset 5em 1em gold;

/* 任意数量的阴影，以逗号分隔 */
box-shadow: 3px 3px red, -1em 0 0.4em olive;

/* 全局关键字 */
box-shadow: inherit;
box-shadow: initial;
box-shadow: unset;
```

## filter
drop-shadow()函数 参数与box-shadow类似
三个长度单位分别为 x位移  y位移 模糊半径和阴影颜色

多层阴影使用在 filter 属性使用多个 drop-shadow() 函数，并且以空格符隔

## resize
resize CSS属性允许你控制一个元素的可调整大小性 注意：一定要配合overflow:auto来使用
默认值none 不可继承

取值
none：元素不能被用户缩放
both：允许用户在水平和垂直方向上调整元素的大小
horizontal：允许用户在水平方向上调整元素的大小
vertical：允许用户在垂直方向上调整元素的大小

## box-sizing
CSS 中的 box-sizing 属性定义了 user agent 应该如何计算一个元素的总宽度和总高度。

在 CSS 盒子模型的默认定义里，对一个元素所设置的 width 与 height 只会应用到这个元素的内容区即content-box。
如果这个元素有任何的 border 或 padding ，绘制到屏幕上时的盒子宽度和高度会加上设置的边框和内边距值。
这意味着当你调整一个元素的宽度和高度时需要时刻注意到这个元素的边框和内边距。当我们实现响应式布局时，这个特点尤其烦人。

box-sizing 属性可以被用来调整这些表现:
content-box 是默认值。如果你设置一个元素的宽为100px，那么这个元素的内容区会有100px 宽，并且任何边框和内边距的宽度都会被增加到最后绘制出来的元素宽度中。
border-box 告诉浏览器：你想要设置的边框和内边距的值是包含在width内的。也就是说，如果你将一个元素的width设为100px，那么这100px会包含它的border和padding，内容区的实际宽度是width减去(border + padding)的值。大多数情况下，这使得我们更容易地设定一个元素的宽高。

尺寸计算公式：
width = border + padding + 内容的宽度
height = border + padding + 内容的高度
注意：border-box不包含margin


## 转动的风车(没有添加动画版)
```html
    <style>
        .box{
            width: 400px;
            height: 400px;
            /* border: solid 1px red; */

            /* 居中 */
            position: absolute;
            left: 0;
            right: 0;
            top: 0;
            bottom: 0;
            margin: auto;
            /* 设置动画时间 */
            transition: 20s;
        }
        .box > div{
            margin: 10px;
            width: 180px;
            height: 180px;
            background-color: #bfa;
            /* 浮动提升层级半级 */
            float: left;
            box-sizing: border-box;
        }
        #b1,
        #b4{
            border-radius: 0px 50%;
        }
        #b2,
        #b3{
            border-radius: 50% 0px;
        }
        .box:hover{
            transform: rotate(3600deg);
        }
    </style>
<body>
    <div class="box">
        <div id="b1">1</div>
        <div id="b2">2</div>
        <div id="b3">3</div>
        <div id="b4">4</div>
    </div>
</body>
```

## BFC
BFC（block formatting context）块级格式化上下文 是一个独立的渲染区域，只有block-level box参与，它规定了内部的block-level box如何布局，并且与这个外部区域毫不相干

BFC布局规则：
1、内部的box在垂直方向，一个接一个放置
2、BFC区域不会与float box重叠
3、内部的box垂直距离由margin决定，属于同一个BFC的两个相邻box的margin会发生重叠
4、计算BFC高度时，浮动元素也参与计算（清除haslayout）
5、BFC就是页面上一个隔离的独立容器，容器里面的子元素不会影响外部元素

BFC什么时候出现
根元素
float属性值不是none
position 属性值是absolute或者fixed
overflow 不是visible
display 为inline-block table-cell flex inline-flex

## 边框图片
border-image CSS属性允许在元素的边框上绘制图像，这使得绘制复杂的外观组件更加简单，使用 border-image 时，其将会替换掉border-style属性所设置的边框样式

### border-image-source
定义使用一张图片来替代边框样式；如果值为none，则仍然使用border-style定义的样式 默认值：none 不可继承
` border-image-source: url("./img/1.jpg"); `
### border-image-slice
通过border-image-source 引用边框图片后，border-image-slice属性会将图片分割为9个区域：四个角，四个边（edges）以及中心区域。四条切片线，从它们各自的侧面设置给定距离，控制区域的大小。

语法
```css
/* 所有的边 */
border-image-slice: 30%;

/* 垂直方向 | 水平方向 */
border-image-slice: 10% 30%;

/* 顶部 | 水平方向 | 底部 */
border-image-slice: 30 30% 45;

/* 上 右 下 左 */
border-image-slice: 7 12 14 5;

/* 使用fill（fill可以放在任意位置） */
border-image-slice: 10% fill 7 12;

```
### border-image-repeat
定义图片如何填充边框。或为单个值，设置所有的边框；或为两个值，分别设置水平与垂直的边框。 默认值为拉升 不可继承
可选值
```
type
stretch, repeat, round, space 选一。属于单个值的情况。

horizontal
stretch, repeat, round, space 选一。属于两个值的情况。

vertical
stretch, repeat, round, space 选一。属于两个值的情况。

stretch
拉伸图片以填充边框。

repeat
平铺图片以填充边框。

round
平铺图像。当不能整数次平铺时，根据情况放大或缩小图像。

space
平铺图像 。当不能整数次平铺时，会用空白间隙填充在图像周围（不会放大或缩小图像）

inherit
继承父级元素的计算值。
```
### border-image-width
定义图像边框宽度。假如border-image-width大于已指定的border-width，那么它将向内部(padding/content)扩展.

### border-image-outset
定义边框图像可超出边框盒的大小。可使边框外扩，只有正值

## 渐变

### 线性渐变

渐变是图片
为了创建一个线性渐变，需要一个起始点和一个方向（指定一个角度），还需要定义终止色（两种颜色或更多）

默认从上到下发生渐变
`background-image: linear-gradient(red,blue);`

#### 改变渐变的方向

background-image: linear-gradient(to 结束方向，颜色1，颜色2)
`background-image: linear-gradient(to top，red，blue)`

使用角度
垂直向上为0，垂直向下为180，水平向右90，水平向左-90
background-image: linear-gradient(角度deg，颜色1，颜色2)

#### 控制颜色节点的分步

在颜色后添加长度或百分比，定义颜色节点
如：background-image: linear-gradient(45deg，red 10px，blue 20px)

#### 重复渐变
repeating-linear-gradient(60deg, red 10px, green 30px)
#### 实例:发廊灯

```html
<style>
    * {
        margin: 0;
        padding: 0;
    }
    /* 去除滚动条 */
    html, body {
        height: 100%;
        overflow: hidden;
    }

    #wrap {
        /* 居中 */
        position: absolute;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        margin: auto;
        width: 40px;
        height: 300px;
        border: 1px solid;
        overflow: hidden;
    }

    #wrap > .inner {
        height: 300px;
        background-image: repeating-linear-gradient(135deg, black 0, black 10px, white 10px, white 20px);
    }
</style>
<body>
    <div id="wrap">
        <div class="inner">
        </div>
    </div>
</body>
<script>
    var inner = document.querySelector("#wrap > .inner");
    var flag = 0;
    //
    setInterval (function () {
        flag += 1;
        // 渐变图高度不断增加
        inner.style.height = (300 + flag + "px");
        // 渐变图不断向上
        inner.style.marginTop = -flag + "px";
    }, 10)
</script>
```
#### 实例:光斑动画

```html
<style>
    * {
        margin: 0;
        padding: 0;
    }

    html, body {
        height: 100%;
        overflow: hidden;
        text-align: center;
        background-color: black;
    }

    h1 {
        display: inline-block;
        color: rgba(245, 242, 58, 0.767);
        font: bold 100px "方正粗雅宋";
        margin-top: 20px;
        background-image: linear-gradient(135deg, rgba(255, 255, 255, 0) 80px, rgba(255, 255, 255, 1) 100px, rgba(255, 255, 255, 0) 150px);
        -webkit-background-clip: text;
        background-repeat: no-repeat;
        background-position: -400px;
    }
</style>
<body>
    <h1>jrj</h1>
</body>
<script>
    var h1 = document.querySelector("h1");
    var flag = 0;
    setInterval(function (){
        flag++;
        if (flag == 800) {
            flag = 0;
        }
        h1.style.backgroundPositionX = -400 + flag + "px";
    }, 5);
</script>
```
### 径向渐变

CSS radial-gradient() 函数创建了一个图片，其由一个从原点辐射开的在两个或者多个颜色之前的渐变组成。这个方法得到的是一个CSS`<gradient>`数据类型的对象，其是 `<image> `的一种。
与其他渐变相同，径向渐变是一个不固定尺寸的图片，譬如，它没有默认尺寸、比例。具体尺寸由它所定义的元素尺寸决定。
如需要填充容器的循环渐变，请使用CSS的repeating-radial-gradient 方法。
因为` <gradient> `属于 `<image>` 类型，所以它可以用于任何适用于 `<image>` 的地方。正是因为这样，radial-gradient() 不能用于 background-color 和其他属性比如 `<color>` 数据类型。

例：`background-image: radial-gradient(red 10%, blue 30%, yellow 70%);`

#### 改变渐变形状

默认为ellipse椭圆
`background-image: radial-gradient(circle, red, yelllow)`

#### 改变渐变形状的大小

默认为farthest-corner最近角到中心划分
属性值：
closest-side 最近边
farthset-side 最远边
closest-corner 最近角
farthset-corner 最远角

`background-image: radial-gradient(farthset-side circle, red, yelllow)`

#### 改变圆心

`background-image: radial-gradient(farthset-side circle at 20px 20px, red, yelllow)`

# 过渡（transition）

## transition-property

指定应用过渡属性的样式
默认值为all,表示所有可被动画的属性都表现出过渡动画
可以指定多个property

## transition-duration

属性以秒或毫秒为单位指定过渡动画所需的时间（一定要加单位！）
默认值为0s，表示不出现过渡动画

可以指定多个时长，每个时长都会被应用到由transition-property指定的对应属性上
如果指定的时长个数小于属性个数，那么时长列表会依次重复。如果时长列表更长，那么该列表会被裁减，两种情况下，列表属性都保持不变

## transition-timing-function

可以规定多个timing-function，通过transition-property属性，可以根据主列表（transition-property的列表）给每个CSS属性相应的timing-function
如果timing-function的个数比主列表数量要少，缺少的值被设置为初始值（ease），如果timing-function比主列表更多，timing-function函数列表会被截断至合适的大小。
这两种情况下声明的CSS属性都是有效的

属性值

1.ease：（加速再减速）ease函数相当于贝塞尔曲线（0.25，0.1，0.25，1.0）
2.linear：（匀速）linear函数相当于贝塞尔曲线（0.0，0.0，1.0，1.0）
3.ease-in：（加速）ease-in函数相当于贝塞尔曲线（0.42，0.0，1.0，1.0）
4.ease-out：（减速）（0.0，0.0，0.58，1.0）
5.ease-in-out：（加速然后减速）（0.42，0.0，0.58，1.0）
6.cubic-bezier:（贝塞尔曲线https://cubic-bezier.com/
7.step-start：等同于steps(1,start)
   step-end：等同于steps(1,end)
   第一个参数：必须为正整数，指定函数变化的步数
   第二个参数：指定每一步的值发生变化的时间点，默认为end

## transition-delay

该属性规定了在过渡效果开始作用之前需要等待的时间
默认值为0s
可以指定多个延迟时间，每个延迟时间将会分别作用于你所指定的相符合的css属性。如果指定的时长个数小于属性个数，那么时长列表将会重复，如果时长列表更长，那么该列表会被裁减。两种情况下，属性列表都保持不变

## 检测过渡是否完成

当过渡完成时触发一个事件，在符合标准的浏览器下，这个事件是transitionend，在webkit下是webkitTransitionEnd
(每一个拥有过渡的属性在其完成过渡时都会触发一次transitionend事件)

在transition完成前设置display:none 时间同样不会触发

## transition过渡中只关注初始状态和结束状态，没有方法可以获取到过渡中每一帧的状态

## transition过渡在元素首次渲染还没结束的情况下是不会触发的

## transition过渡在绝大部分样式切换时，如果transform变换函数的位置，个数不相同也不会触发

## transition

CSS效率极高，其样式变化往往都是一瞬完成。
transition提供了一种在更改CSS属性时控制动画速度的方法。其可以让属性变化变成一个持续一段时间的过程，而不是立即生效的。
transition CSS 属性是 transition-property，transition-duration，transition-timing-function 和 transition-delay 的一个简写属性。
默认值
transition-delay:0s
transition-duration:0s
transition-property:all

注意：transition属性中，各个值的书写顺序很重要，第一个可以解析为时间的值会赋值给transition-duration，第二个可以解析为时间的值会赋值给transition-delay

推荐书写顺序
过渡时间 过渡样式 过渡形式 延迟时间，过渡时间 过渡样式 过渡形式 延迟时间...

# 2D变形（transform）

transform属性允许修改CSS视觉格式模型的坐标空间
transform属性只对block级元素有效
内部变换都是矩阵变换

注意：transform只对块级元素有效

## 旋转（rotate）

`transform: rotate(360deg);`

## 平移（translate）

X方向平移：`transform: translateX(200px);`
Y方向平移：`transform: translateY(200px);`
简写二维平移：`transform: translate(200px, 200px);`
可单值可双值可正（向右 向下）可负（向左 向上），第一个值为X方向，第二个值为Y方向

## 倾斜（skew）

X方向倾斜：`transform: skewX(45deg);`
Y方向倾斜：`transform: skewY(45deg);`

二维倾斜：`transform: skew(45deg, 45deg);`
同平移translate

## 缩放（scale）

X方向缩放：`transform: scaleX(0.5);`
Y方向缩放：`transform: scaleY(2);`

简写二维缩放：`transform: scale(0.5, 2);`
正值表示缩放程度
scale参数为单值时X，Y方向缩放相同
不推荐使用负值实现缩放效果

### 鼠标移入，按钮变大
```html
<style>
  ul{
    display: flex;
  }
  li{
    margin: 10px;
    width: 30px;
    height: 30px;
    line-height: 30px;
    text-align: center;
    background-color: #bfa;
    list-style: none;
    transition: all 0.4s;
    border-radius: 60%;
    /* 使鼠标移入变成小手 */
    cursor: pointer;
  }
  li:hover{
    transform: scale(1.1);
  }
</style>
<body>
    <ul>
        <li>1</li>
        <li>2</li>
        <li>3</li>
        <li>4</li>
        <li>5</li>
    </ul>
</body>
```

## 基点的变换（只对位移无影响）

`transform-orign: top left;`
`transform-orign: 50px 10px;` 表示参照左上距离坐标定义基点
`transform-orign: 50% 10%;`

## 2D变形组合
可以将上述组合起来简写，但是注意简写的时候涉及到位移需要放到前面
因为涉及到坐标的变动
`transform: translate(10px,10px) scale(1.1) rotate(45deg)`
2D变形内部都是矩阵变换，矩阵变换不可逆，没有交换律
2D变形组合底层时从右往左进行矩阵变换（但使用时按字面意思从左往右理解）

## 实例:扇形导航
```html
    <style>
        * {
            margin: 0;
            padding: 0;
        }
        html, body {
            height: 100%;
            overflow: hidden;
        }
        img {
            width: 50px;
        }
        #wrap {
            position: fixed;
            right: 10px;
            bottom: 10px;
            width: 50px;
            height: 50px;

        }
        #inner {
            height: 100%;
        }
        #inner > img {
            position: absolute;
            left: 0;
            top: 0;
            width: 50px;
            border-radius: 25px;
            background-color: rgba(255, 255, 255, 1);
        }
        #home {
            position: absolute;
            top: 0;
            left: 0;
            width: 50px;
            height: 50px;
            border-radius: 25px;
            background-color: rgba(255, 255, 255, 1);
            transition: 1.5s;
        }

    </style>
<body>
    <div id="wrap">
        <div id="inner">
            <img src="../img/geren.png" alt="">
            <img src="../img/shezhi.png" alt="">
            <img src="../img/xiaoxi.png" alt="">
            <img src="../img/zhuye.png" alt="">
        </div>
        <div id="home">
            <img src="../img/caidan.png" alt="">
        </div>
    </div>
</body>
<script>
    window.onload = function () {
        var homeEle = document.querySelector("#home");
        var imgs =document.querySelectorAll("#inner img");
        var flag = true;
        var c = 140;
        function fn() {
            this.style.transition = "0.2s";
            this.style.transform = "rotate(-360deg) scale(1)";
            this.style.opacity = "1";
            this.removeEventListener("transitionend", fn);
        }
        function getPoint(c, deg){
            var x = Math.round(c * Math.sin(deg * Math.PI /180));
            var y = Math.round(c * Math.cos(deg * Math.PI /180));
            return{left:x, top:y};
        }
        homeEle.onclick = function () {
            if (flag) {
                this.style.transform = "rotate(-720deg)";
                for (var i = 0; i < imgs.length; i++) {
                    imgs[i].style.transition = 0.5 + (i * 0.4) + "s";
                    imgs[i].style.transform = "rotate(-360deg) scale(1)";
                    imgs[i].style.left = -getPoint(c,90 * i / (imgs.length - 1)).left + "px";
                    imgs[i].style.top = -getPoint(c,90 * i / (imgs.length - 1)).top + "px";
                }
            } else {
                this.style.transform = "rotate(720deg)";
                for (var i = 0; i < imgs.length; i++) {
                    imgs[i].style.transition = 0.5 + ((imgs.length - i - 1) * 0.4) + "s";
                    imgs[i].style.transform = "rotate(0deg) scale(1)";
                    imgs[i].style.left = 0 + "px";
                    imgs[i].style.top = 0 + "px";
                }
            }
            flag = !flag
        }
        for (var i = 0; i < imgs.length; i++) {
            imgs[i].onclick = function () {
                this.style.transition = "0.2s";
                this.style.transform = "scale(2)";
                this.style.opacity = "0.1";
                this.addEventListener("transitionend", fn);
            };
        }
    }
</script>
```

# 3D变形

## 透视（perspective）

在CSS3中，perspective用于激活一个3D空间，使具有3D变换的后代元素产生透视效果，属性值就是透视大小（默认none无透视）

perspective和perspective-origin属性能给舞台（scene，变换元素所处的空间）添加纵深感，结果就是元素距离观看者越近就表现得越大，越远就表现得越小（通过变换可以改变元素在Z轴上的位置）。

perspective属性指定观看者的眼睛（假设的）与屏幕 （drawing plane）之间的距离。如果将perspective属性的值设为d，则元素的缩放比例就等于d/(d − z)，z是元素在Z轴上的位置，更准确的说是变换前元素所在的与Z轴垂直的平面在Z轴上的坐标位置。

简单的理解：透视就是人眼距离屏幕的距离
透视的作用是使3D场景产生近大远小的效果
透视是一个不可继承属性，但它可以作用于所有的后代元素

形式：
transform:perspective(depth);
depth的默认值是none，可以设置为一个长度值，这个长度是沿着Z轴距离坐标原点的距离。
若使用perspective()函数，那么它必须放置在transform属性的首位，如果放在其他函数之后，则会被忽略。
这种写法的透视是作用于本身的，一般不使用

perspective:depth;
和perspective()函数一样，depth的默认值是none，可以设置为一个长度值，这个长度是沿着Z轴距离坐标原点的距离。
区别在于perspective属性被用于元素的后代元素，而不是元素本身
应用透视的元素被称为“舞台元素”，舞台元素的所有后代都会受影响（如果后代元素中也添加了perspective属性，效果会叠加不是覆盖）

### 灭点

指的是立体图形沿各条边的延伸线所产生的相交点即透视点的相交点
透视越大，灭点越远，元素变形越小

### 透视基点

透视基点为视角的位置
默认情况下，观看者的眼睛正对着的位置在drawing（surface）的中心。然而可以通过perspective-origin属性改变这个位置（for example, if a web page contains multiple drawings that should share a common perspective property，这句英语是理解perspective与perspective()区别的关键）。
![](_v_images/20200214001004707_2234.png)
透视基点由两个属性控制
perspective:depth;
perspective-origin(xlength, ylength);
perspective-origin默认值为50%，50%

## transform-style

transform-style营造有层级的3D舞台
这是一个不可继承属性，它作用于子元素

值：
flat
设置元素的子元素位于该元素的平面中。
preserve-3d
指示元素的子元素应位于 3D 空间中。

## 3D缩放

transform:scaleZ(number);
transform:scale3d(scaleX, scaleY, scaleZ);

如果只设置scaleZ(number),会发现元素并没有被放大或压缩，scaleZ(number)需要和translateZ(length)配合使用，number称以length得到的值，是元素沿Z轴移动的距离，从而使得感觉被放大或压缩

## 3D旋转

transform: rotateX(angle);
transform: rotateY(angle);
transform: rotateZ(angle);

分别以X，Y，Z正轴作顺时针旋转
```
transform: rotateX(360deg);
transform: rotateY(360deg);
transform: rotateZ(360deg);
```

transform: rotate3d(x, y, z, angle);

x,y,z分别接受一个数值（number），用来计算矢量方向（direction vector），矢量方向是三维空间中的一条线：从坐标系原点到（x, y, z），元素以这条矢量线为轴作顺时针旋转angle

## 3D平移

transform: translateZ(length)是3D Transformation特有的。
translateZ 它不能是百分比的值，这样移动是没有意义的
transform3d(translateX, translateY, translateZ);
单独使用没有作用，变换组合才有作用

## backface-visibility

backface-visibility属性用来设置是否显示元素的背面，默认是显示的。
值：
hidden和visible，默认为visible

## 实例:3D立方体

```html
    <style>
        * {
            margin: 0;
            padding: 0;
        }

        #wrap {
            position: absolute;
            top: 0;
            bottom: 0;
            right: 0;
            left: 0;
            margin: auto;
            width: 400px;
            height: 400px;
            border: 1px solid;
            background-image: url("../img/01.jpg");
            background-size: 100% 100%;
            perspective: 200px;
        }

        #wrap > .box {
            position: absolute;
            top: 0;
            bottom: 0;
            right: 0;
            left: 0;
            margin: auto;
            width: 100px;
            height: 100px;
            transform-style: preserve-3d;
            transform-origin: center center -50px;
            transition: 2s;

        }
        #wrap > .box > div{
            position: absolute;
            width: 100px;
            height: 100px;
            background-color: rgba(123, 123, 123, .3);
            text-align: center;
            font: 50px/100px "微软雅黑";
            border: 1px solid;
            backface-visibility: hidden;
        }
        /* 上 */
        #wrap > .box > div:nth-child(5) {
            transform-origin: bottom;
            transform: rotateX(90deg);
            top: -100px;
        }
        /* 下 */
        #wrap > .box > div:nth-child(6) {
            transform-origin: top;
            transform: rotateX(-90deg);
            top: 100px;
        }
        /* 左 */
        #wrap > .box > div:nth-child(3) {
            transform-origin: right;
            transform: rotateY(-90deg);
             left: -100px;
        }
        /* 右*/
        #wrap > .box > div:nth-child(4) {
            transform-origin: left;
            transform: rotateY(90deg);
            left: 100px;
        }
        /*前*/
        #wrap > .box > div:nth-child(1) {

        }
        /*后*/
        #wrap > .box > div:nth-child(2) {
            transform: translateZ(-100px) rotateX(180deg);
        }
        #wrap:hover > .box {

            transform: rotate3d(1, 1, 1, 360deg);
        }

    </style>
<body>
    <div id="wrap">
        <div class="box">
            <div>前</div>
            <div>后</div>
            <div>左</div>
            <div>右</div>
            <div>上</div>
            <div>下</div>
        </div>
    </div>
</body>
```

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style>
        * {
            margin: 0;
            padding: 0;
        }

        #wrap {
            position: absolute;
            top: 0;
            bottom: 0;
            right: 0;
            left: 0;
            margin: auto;
            width: 400px;
            height: 400px;
            border: 1px solid;
            background-image: url("../img/01.jpg");
            background-size: 100% 100%;
            perspective: 200px;
        }

        #wrap > .box {
            position: absolute;
            top: 0;
            bottom: 0;
            right: 0;
            left: 0;
            margin: auto;
            width: 100px;
            height: 100px;
            transform-style: preserve-3d;
            transform-origin: center center -50px;
            transition: 2s;

        }
        #wrap > .box > div{
            position: absolute;
            width: 100px;
            height: 100px;
            background-color: rgb(217, 229, 46);
            text-align: center;
            font: 50px/100px "微软雅黑";
            border: 1px solid;
            transform-origin: center center -50px;
        }
        /* 上 */
        #wrap > .box > div:nth-child(5) {

            transform: rotateX(90deg);
        }
        /* 下 */
        #wrap > .box > div:nth-child(6) {

            transform: rotateX(-90deg);
        }
        /* 左 */
        #wrap > .box > div:nth-child(3) {

            transform: rotateY(-90deg);
        }
        /* 右*/
        #wrap > .box > div:nth-child(4) {

            transform: rotateY(90deg);
        }
        /*前*/
        #wrap > .box > div:nth-child(1) {

        }
        /*后*/
        #wrap > .box > div:nth-child(2) {
            transform: rotateX(180deg);
        }
        #wrap:hover > .box {
            transform: rotateX(180deg);
        }

    </style>
</head>
<body>
    <div id="wrap">
        <div class="box">
            <div>前</div>
            <div>后</div>
            <div>左</div>
            <div>右</div>
            <div>上</div>
            <div>下</div>
        </div>
    </div>
</body>
</html>
```

# Animation（动画）

## animation-name

animation-name属性指定应用的一系列动画，每个名称代表一个由@keyframes定义的动画序列

值：
none 特殊关键字，表示无关键帧
@keyframename 标识动画的字符串

## animation-duration

animation-duration属性指定一个动画周期的时长
默认值为0s，表示无动画，无单位无效

## animation-timing-function

作用于关键帧周期，而非整个动画周期

动画的默认效果：先加速再加速

ease:先加速再减速，平滑过渡
linear:匀速，线性过渡
ease-in:加速
ease-out:减速
ease-in-out:先加速再减速

cublic-bezier(1, 1, 2, 3)

steps(n,[start | end])
传入一到两个参数，第一个参数表示将动画n等分，然后动画就会平均地运行
第二个参数start表示从动画的开头运行，相反end表示从动画的结尾运行
默认值end

## animation-delay

定义动画开始等待的时间，以秒或毫秒（属于动画外的属性）

## animation-iteration-count
参数：整数数值
定义动画执行的次数，重复的是关键帧@keyframe，动画延迟不会被循环

值：
infinite：无线循环执行动画
number：动画执行的次数，不可为负值

## animation-direction

定义动画执行的方向

值：
normal：正常
reverse：反转，反转的是关键帧和animation-timing-function
alternate：normal+reverse循环
alternate-reverse：reverse+normal循环

## animation-fill-model

用来控制元素在动画外的状态

值：
none 默认值 动画外的状态保持在动画之前的位置
backwards from之前的状态与关键帧from相同
forwards to之后的状态与关键帧to相同
both = backwards+forwards

## animation-play-state

管理动画的运行和停止

值：
running 默认值
paused 停止

## 关键帧（@keyframes）

语法:
```css
@keyframes animationName {
    keyframes-selector {
        css-style;
   }
}
```

animationName 必写项 定义动画的名称

keyframes-selector 必写项 设定动画关键帧的时间点
from:0%to:100%

css-style css声明

```css
@keyframes move {
    0% {
        transform: translateX(100px);
        }
    50% {
        transform: translateX(90px);
        }
    70% {
        transform: translateX(-10px);
        }
    100% {
        transform: translateX(-100px);
        }
}
```

## animation简写属性

animation为动画简写属性

在每个动画定义中，顺序很重要：可以被解析为time的第一个值被分配给animation-duration，第二个分配给animation-delay

`animation: 4s 2s both running 3 alternate steps(1, end);`

# 响应式布局方案

## CSS3媒体查询

CSS3的媒体查询是响应式布局的核心

### 媒体类型

all——所有媒体（默认值）
screen——彩色屏幕
print——打印预览
projection——手持设备
tv——电视
braille——盲文触觉设备
embossed——盲文打印机
speech——“听觉”类似的媒体 设备
tty——不适用像素的设备

### 媒体属性

#### width

width：浏览器窗口的尺寸（min max）


#### device-width

device-width：设备独立像素 PC端看分辨率 移动端具体看机器的参数（min max）

#### device-pixel-ratio

device-pixel-ratio（注意：必须加-webkit-前缀）
像素比，PC端为1 移动端具体看机器参数（min max）

#### orientation

orientation：portrait竖屏 landscape横屏
注意：不用带引号

### 操作符，关键字（only|and|,代表or|not）

#### and

and：and代表与的意思，一般用and链接媒体类型和媒体属性

```css
/* 屏幕宽度 <= 400px的时候才生效 */
@media screen and (max-width: 400px) {
    .inner {
    width: 50px;
    height: 100%;
    background-color: aqua;
    }
}


/* 屏幕宽度>= 420px的时候生效 */
@media screen and (min-width: 420px) {
    .inner {
    width: 50px;
    height: 100%;
    background-color: red;
    }
}
```

#### only

only：和浏览器的兼容性有关
老版本的浏览器只支持媒体类型，不支持媒体属性的查询
```
在老版本浏览器眼里
@media screen and (min-width:800px) and (orientation:landscape) {
    #wrap {
        border:1px solid;
    }
}
解析为
@media screen {
    #wrap {
        border:1px solid;
    }
}
加了关键字only之后解析为
@media only screen and (min-width:800px) and (orientation:landscape) {
    #wrap {
        border:1px solid;
    }
}
```

#### ,
, 代表或的意思
```css
@media screen and (min-width:800px), (orientation:landscape) {
    #wrap {
        border:1px solid;
    }
}
/* 代表screen and (min-width:800px) 或 (orientation:landscape) */
```
#### not
代表非


------------------------------

# less
# 注释
以` // `开头的注释是不会被编译到CSS文件中去的

以` /* */ `包裹的注释会被编译到CSS文件中去

# 变量

用@来申明一个变量：
@变量名: 变量值;
@red: red;
@m: margin;
@selector: #wrap;
1.作为普通属性值来使用：直接使用@red
2.作为选择器和属性名需要加大括号 @{  }` @{selector} { @{m}: 10px;}  `
3.作为URL：@{url}
4.变量的延迟加载 一个{}为一个作用域，{}全部加载完才确定变量的值，也就是说后设的变量值会覆盖先设的变量值

注意变量在Less中属于块级作用域

# Less中的嵌套规则
1.基本的嵌套规则（父子级关系）
2.&的使用（代表外层父元素）

# Less的混合(mixin)
混合就是将同一系列的属性从一个规则集引入到另一个规则集的方式
## 1.普通混合
```
.juzhong {
    //不加()的话.juzhong{}会被编译到CSS中
    position:absolute;
    top:0;
    bottom:0;
    left:0;
    right:0;
    margin:auto;
    width:100px;
    height:100px;
    border:1px solid;
}

#div1 {
    .juzhong;
}
#div2 {
    .juzhong;
}
```

## 2.不带输出的混合

```
.juzhong() {
    //加()不会被编译到CSS中去
    position:absolute;
    top:0;
    bottom:0;
    left:0;
    right:0;
    margin:auto;
    width:100px;
    height:100px;
    border:1px solid;
}

#div1 {
    .juzhong();
}
#div2 {
    .juzhong();
}


```

## 3.带参数的混合

```
.juzhong(@w, @h) {
    position:absolute;
    top:0;
    bottom:0;
    left:0;
    right:0;
    margin:auto;
    width:@w;
    height:@h;
    border:1px solid;
}

#div1 {
    //实参和形参不对应会报错
    .juzhong(100px, 100px);
}

#div2 {
    .juzhong(200px, 200px);
}
```

## 4.带参数且有默认值的混合

```
.juzhong(@w:10px, @h:10px) {
    position:absolute;
    top:0;
    bottom:0;
    left:0;
    right:0;
    margin:auto;
    width:@w;
    height:@h;
    border:1px solid;
}

#div1 {
    //实数可传可不传，但实参顺序要和形参一样
    .juzhong(100px);
}

#div2 {
    .juzhong(200px, 200px);
}
```

## 5.命名参数

```
.juzhong(@w:10px, @h:10px) {
    position:absolute;
    top:0;
    bottom:0;
    left:0;
    right:0;
    margin:auto;
    width:@w;
    height:@h;
    border:1px solid;
}

#div1 {
    //实数可传可不传，可指定传参数，不考虑参数顺序
    .juzhong(@h:100px);
}

#div2 {
    .juzhong(@h:200px, @w:100px);
}
```

## 6.匹配模式
```
@import "./sanjiaoxing.less"

#wrap {
    .sanjiaoxing(匹配符,参数，参数)
}
```

```less
.sanjiaoxing(@_) {
    相同样式
}

.sanjiaoxing(匹配符, @w, @h) {
    不同样式
}
.sanjiaoxing(匹配符, @w, @h) {
    不同样式
}
.sanjiaoxing(匹配符, @w, @h) {
    不同样式
}
```
## 7.arguments变量

```
.border(@a, @b, @c) {
    border: @arguments;
}

#wrap {
    .border(1px, red, solid)
}
```
# Less的运算
Less里面计算，计算双方只需要一方带单位就行
# Less的继承
继承性能上比混合高
灵活性没有混合好（不可设参）
```css
.p1{
    height: 200px;
    width: 200px;
}
.p2:extend(.p1){
    background-color: #bfa;
}


/* 等同于 */
.p1,
.p2 {
  height: 200px;
  width: 200px;
}
.p2 {
  background-color: #bfa;
}
```
```less
.p3{
    .p1();// 直接引用，相当于把p1的样式直接复制过来
    background-color: #bfa;
}

// 等同于
.p3 {
  height: 200px;
  width: 200px;
  background-color: #bfa;
}
```
第二种不建议使用，复制两遍效率不如第一种高
# Less避免编译
~"不会被编译的内容"

`padding: ~"calc(100px + 100)";`
# 导入其他less
` @import "01.less"; `在本less文件中引入其他less文件

---------


# SASS
使用sass的文件有两种后缀名, 总的来说区别不大
scss使用大括号
sass使用缩进

## 变量
SASS通过$符号去声明一个变量
```scss
// 字体大小定义
$font_size_10: 10px;
$font_size_12: 12px;
$font_size_14: 14px;
$font_size_16: 16px;
$font_size_18: 18px;
$font_size_20: 20px;

// 不同类型文字大小
$font_size_x_small: $font_size_12;
$font_size_small: $font_size_14;
$font_size_medium: $font_size_16;
$font_size_large: $font_size_18;
$font_size_x_large: $font_size_20;
$left_panel_width: 330px;
```

变量文件通常以_开头, 例如_color.scss, _fontVariables.scss

以_开头的文件不会被编译成单独的css文件, 否则会编译成单独的css文件
### 使用变量
引入带_的文件时, 去掉下划线
```scss
@import color.scss

body {
  color: $primary-color;
}
```

## 混合 混入
混合指令的出现使你可以定义在样式表中重复使用的样式，这可以使你免去编写过多重复的样式，而且在混合指令 @mixin 中你也可以做一些逻辑处理
```scss
@mixin border-radius($radius) {
          border-radius: $radius;
      -ms-border-radius: $radius;
     -moz-border-radius: $radius;
  -webkit-border-radius: $radius;
}

.box {
  @include border-radius(10px);
}

// 上面的代码建立了一个名为border-radius的Mixin，并传递了一个变量$radius作为参数
// 然后在后续代码中通过@include border-radius(10px)使用该Mixin
// 最终编译的结果如下：

.box {
  border-radius: 10px;
  -ms-border-radius: 10px;
  -moz-border-radius: 10px;
  -webkit-border-radius: 10px;
}
```

带默认值的mixin

```scss
@mixin theme($theme: DarkGray) {
  background: $theme;
  box-shadow: 0 0 1px rgba($theme, .25);
  color: #fff;
}

.info {
  // 直接使用默认参数
  @include theme;
}
.alert {
  @include theme($theme: DarkRed);
}
.success {
  @include theme($theme: DarkGreen);
}
```

## 函数
```scss
// font family
@function font-family($type, $size: Regular) {
  @if $type== 'PingFangSC' {
    @return PingFangSC-$size, PingFang SC, sans-serif;
  } @else {
    @return -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, 'Helvetica Neue', Arial, 'Noto Sans', sans-serif,
      'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol', 'Noto Color Emoji';
  }
}
```

### 函数和 混合的区别
函数主要用于一个属性后面返回的值
混合用于多个属性,即一大段重复性高的代码,只有颜色值不同等






--------------
# design
用于收集一些好的设计, 包括配色 动效等等

# color
- linear-gradient(90deg,rgba(255,0,105,.99) 0,#ff7a00 99%);
- linear-gradient(315deg,#42d392 25%,#647eff);
  background: #15f9cd linear-gradient(45deg, #5d1bbb, #31b49e00);
- background-image: linear-gradient(168deg, #427CFF, #7E2FFF 98%);