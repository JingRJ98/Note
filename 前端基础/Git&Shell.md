# Git
## 初始化
检查已有配置
`git config --list`

配用户名与邮箱
`git config --global user.name "JingRJ98"`
`git config --global user.email 239611376@qq.com`

## git status

查看文件的状态
工作区下所有文件有两种状态： 已跟踪 与 未跟踪
已跟踪文件有三种状态： 已暂存 已修改 已提交
当对一个已暂存的文件进行修改，会出现两种状态，一种已暂存，另一种已修改（未暂存）
需要重新运行`git add xxx`将文件暂存起来

## git diff (--cached/--staged)

git diff
当前哪些文件修改后还未暂存

git diff --cached/--staged
当前哪些文件已暂存还未提交

## git log
查看提交日志

`git log --oneline` => glog
查看历史提交记录(以比较简单的形式显示)

查看当前分支的提交信息, 主要是为了commit id
`glog -1`

## git rm xxx
工作区删除文件且将修改存入暂存区


## git checkout 分支名
切换分支（当前commit对象）
分离HEAD
`git checkout <commitId>` 把HEAD分离出来
`gco <commitId>` 把HEAD分离出来

使用commitId是绝对分离
也可以使用相对引用
### 相对引用 ^
`git checkout develop^`
把HEAD移动到develop上面一次提交

`git checkout develop^^`
把HEAD移动到develop的祖父提交

### 相对引用~
`git checkout develop~4`

##  git branch -v
可以查看所有本地分支的最后一次提交

## 删除分支
强制删除 `git branch -D 分支名` => `gb -D 分支名`
普通删除 `git branch -d 分支名` => `gb -d 分支名`
删除远端 `git push origin -d 分支名` => `gp origin -d 分支名`


# Git存储
## git stash
git stash会将未完成的修改保存到一个栈上

携带commit信息保存
```bash
git stash -m '将本地环境切换为123环境'
```

## git stash pop
栈顶应用且出栈

## git stash apply
栈顶应用但不出栈

## git stash list
查看存储

## git stash drop
删除stash的内容
```bash
# git stash drop stash@{0}  这是删除第一个队列
```
如果不小心删除了 git fsck 找回hash git stash apply hash

## git stash clear
删除所有储存的节点



# 撤回修改
## 工作区修改撤回
`git restore filename`

## 暂存区修改撤回
`git restore --staged filename`

## 修改提交commit
修改当前的commit提交信息
`git commit --amend`

# 重置
## Reset三部曲
### git reset --soft <commitId>

只修改HEAD和分支指向
HEAD以及分支移到前一个提交对象
HEAD可改为指定hash

### git reset [--mixed] HEAD~

修改HEAD和分支指向以及暂存区

### git reset --hard <commitId>
修改HEAD和分支指向，暂存区及工作区

### git reset --hard <id>与 git checkout <id>区别

checkout：改变HEAD，暂存区，工作区，但是它对于工作区是安全的
--hard：改变HEAD，分支，暂存区，工作区，强制覆盖工作目录，可能会造成工作区文件丢失

## checkout

### git checkout hash

改变HEAD，暂存区，工作区

### git checkout --filename

只改变工作区

# 数据恢复

## git reflog

找到需要恢复的提交对象的hash，在那个对象上开新的分支


# tag

Git可以给历史中某一个提交打上标签，以示重要/比较有代表性

## 列出标签
`git tag`

## 搜索标签
`git tag -l 'v1.8.5*'`
```bash
v1.8.5
v1.8.5-rc0
v1.8.5-rc1
v1.8.5-rc2
```

## 创建标签

### 轻量标签
不使用-a,-m等附注的时候创建的是轻量标签
`git tag <tagName> [<指定tag创建位置的commitId>]`
不使用commitId的标签默认创建在当前commit上
轻量标签像是一个不会改变的分支，它只是一个特定提交的引用

### 附注标签
`git tag -a <tagName> [<指定tag创建位置的commitId>] -m "someMessage"`

## 删除标签
git tag -d tagname

## 检出标签
git checkout tagname

## 推送tag
- 推送单一tag
git push origin <tagname>

- 推送全部tag
git push --tags


# Git + ESlint + husky

package.json
```
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1",
    "eslint": "eslint ./",
    "eslint:create": "eslint --init"
  },
  "husky": {
    "hooks": {
      "pre-commit": "npm run eslint",
      "pre-push": "npm test"
    }
  }
```

或者
npx eslint --init
npx eslint 目录

# 参与开源项目
1. fork

2. g clone 自己项目url

3. git remote add upstream 原项目url

4. gco -b develop-new-branch

5. git fetch upstream
   git merge upstream/main
   更新最新的原项目代码 避免冲突

6. ga . gcmsg "feat: some update" git push --set-upstream origin develop-new-branch

7. 原项目仓库 pull request
  base repository指原项目
  head repository指我的项目
  compare & PR


---------

# oh-my-zsh简化命令大全
```bash
alias ga='git add'
alias gaa='git add --all'
alias gapa='git add --patch'
alias gau='git add --update'
alias gav='git add --verbose'
alias gap='git apply'
alias gapt='git apply --3way'

alias gb='git branch'
alias gba='git branch -a'
alias gbd='git branch -d'
alias gbda='git branch --no-color --merged | cmd grep -vE "^([+*]|\s*($(git_main_branch)|$(git_develop_branch))\s*$)" | cmd xargs git branch -d 2>/dev/null'
alias gbD='git branch -D'
alias gbl='git blame -b -w'
alias gbnm='git branch --no-merged'
alias gbr='git branch --remote'
alias gbs='git bisect'
alias gbsb='git bisect bad'
alias gbsg='git bisect good'
alias gbsr='git bisect reset'
alias gbss='git bisect start'

alias gc='git clone'
alias gc!='git commit -v --amend'
alias gcn!='git commit -v --no-edit --amend'
alias gca='git commit -v -a'
alias gca!='git commit -v -a --amend'
alias gcan!='git commit -v -a --no-edit --amend'
alias gcans!='git commit -v -a -s --no-edit --amend'
alias gcam='git commit -a -m'
alias gcsm='git commit -s -m'
alias gcas='git commit -a -s'
alias gcasm='git commit -a -s -m'
alias gcb='git checkout -b'
alias gcf='git config --list'
alias gcl='git clone --recurse-submodules'
alias gclean='git clean -id'
alias gcm='git checkout $(git_main_branch)'
alias gcd='git checkout $(git_develop_branch)'
alias gcmsg='git commit -m'
alias gco='git checkout'
alias gcor='git checkout --recurse-submodules'
alias gcount='git shortlog -sn'
alias gcp='git cherry-pick'
alias gcpa='git cherry-pick --abort'
alias gcpc='git cherry-pick --continue'
alias gcs='git commit -S'
alias gcss='git commit -S -s'
alias gcssm='git commit -S -s -m'
# 自定义的clone命令
# 只下载一个commit的库, 提高下载速度
# 使用 gc1 4.2.3 https://github.com/movie-web/movie-web.git
alias gc1='git clone --depth=1 --branch'


alias gd='git diff'
alias gdca='git diff --cached'
alias gdcw='git diff --cached --word-diff'
alias gdct='git describe --tags $(git rev-list --tags --max-count=1)'
alias gds='git diff --staged'
alias gdt='git diff-tree --no-commit-id --name-only -r'
alias gdup='git diff @{upstream}'
alias gdw='git diff --word-diff'

alias gf='git fetch'
alias gfo='git fetch origin'
alias gfg='git ls-files | grep'
alias gg='git gui citool'
alias gga='git gui citool --amend'
alias ggpur='ggu'
alias ggpull='git pull origin "$(git_current_branch)"'
alias ggpush='git push origin "$(git_current_branch)"'
alias ggsup='git branch --set-upstream-to=origin/$(git_current_branch)'
alias gpsup='git push --set-upstream origin $(git_current_branch)'

alias ghh='git help'

alias gignore='git update-index --assume-unchanged'
alias gignored='git ls-files -v | grep "^[[:lower:]]"'
alias git-svn-dcommit-push='git svn dcommit && git push github $(git_main_branch):svntrunk'

alias gk='\gitk --all --branches &!'
alias gke='\gitk --all $(git log -g --pretty=%h) &!'

alias gl='git pull'
alias glg='git log --stat'
alias glgp='git log --stat -p'
alias glgg='git log --graph'
alias glgga='git log --graph --decorate --all'
alias glgm='git log --graph --max-count=10'
alias glo='git log --oneline --decorate'
alias glol="git log --graph --pretty='%Cred%h%Creset -%C(auto)%d%Creset %s %Cgreen(%ar) %C(bold blue)<%an>%Creset'"
alias glols="git log --graph --pretty='%Cred%h%Creset -%C(auto)%d%Creset %s %Cgreen(%ar) %C(bold blue)<%an>%Creset' --stat"
alias glod="git log --graph --pretty='%Cred%h%Creset -%C(auto)%d%Creset %s %Cgreen(%ad) %C(bold blue)<%an>%Creset'"
alias glods="git log --graph --pretty='%Cred%h%Creset -%C(auto)%d%Creset %s %Cgreen(%ad) %C(bold blue)<%an>%Creset' --date=short"
alias glola="git log --graph --pretty='%Cred%h%Creset -%C(auto)%d%Creset %s %Cgreen(%ar) %C(bold blue)<%an>%Creset' --all"
alias glog='git log --oneline --decorate --graph'
alias gloga='git log --oneline --decorate --graph --all'
alias glp="_git_log_prettily"

alias gm='git merge'
alias gmom='git merge origin/$(git_main_branch)'
alias gmtl='git mergetool --no-prompt'
alias gmtlvim='git mergetool --no-prompt --tool=vimdiff'
alias gmum='git merge upstream/$(git_main_branch)'
alias gma='git merge --abort'

alias gp='git push'
alias gpd='git push --dry-run'
alias gpf='git push --force-with-lease'
alias gpf!='git push --force'
alias gpoat='git push origin --all && git push origin --tags'
alias gpr='git pull --rebase'
alias gpristine='git reset --hard && git clean -dffx'
alias gpu='git push upstream'
alias gpv='git push -v'

alias gr='git remote'
alias gra='git remote add'
alias grb='git rebase'
alias grba='git rebase --abort'
alias grbc='git rebase --continue'
alias grbd='git rebase $(git_develop_branch)'
alias grbi='git rebase -i'
alias grbm='git rebase $(git_main_branch)'
alias grbom='git rebase origin/$(git_main_branch)'
alias grbo='git rebase --onto'
alias grbs='git rebase --skip'
alias grev='git revert'
alias grh='git reset'
alias grhh='git reset --hard'
alias groh='git reset origin/$(git_current_branch) --hard'
alias grm='git rm'
alias grmc='git rm --cached'
alias grmv='git remote rename'
alias grrm='git remote remove'
alias grs='git restore'
alias grset='git remote set-url'
alias grss='git restore --source'
alias grst='git restore --staged'
alias grt='cd "$(git rev-parse --show-toplevel || echo .)"'
alias gru='git reset --'
alias grup='git remote update'
alias grv='git remote -v'


alias gsb='git status -sb'
alias gsd='git svn dcommit'
alias gsh='git show'
alias gsi='git submodule init'
alias gsps='git show --pretty=short --show-signature'
alias gsr='git svn rebase'
alias gss='git status -s'
alias gst='git status'

# alias gsa='git stash'
alias gsta='git stash'
alias gstaa='git stash apply'  # gstaa stash@{n} 应用指定的节点但不从list中删除
alias gstc='git stash clear'
alias gstd='git stash drop' # 删除节点 同样可以指定节点删除
alias gstl='git stash list'
alias gstp='git stash pop' # gstp stash@{n} 应用指定的节点 且list中删除
alias gsts='git stash show --text'
alias gstu='gsta --include-untracked'
alias gstall='git stash --all'
alias gsu='git submodule update'
alias gsw='git switch'
alias gswc='git switch -c'
alias gswm='git switch $(git_main_branch)'
alias gswd='git switch $(git_develop_branch)'

alias gts='git tag -s'
alias gtv='git tag | sort -V'
alias gtl='gtl(){ git tag --sort=-v:refname -n -l "${1}*" }; noglob gtl'

alias gunignore='git update-index --no-assume-unchanged'
alias gunwip='git log -n 1 | grep -q -c "\-\-wip\-\-" && git reset HEAD~1'
alias gup='git pull --rebase' # 已经不推荐使用这个命令 换为gpr
alias gupv='git pull --rebase -v'
alias gupa='git pull --rebase --autostash'
alias gupav='git pull --rebase --autostash -v'
alias glum='git pull upstream $(git_main_branch)'

alias gwch='git whatchanged -p --abbrev-commit --pretty=medium'
alias gwip='git add -A; git rm $(git ls-files --deleted) 2> /dev/null; git commit --no-verify --no-gpg-sign -m "--wip-- [skip ci]"'

alias gam='git am'
alias gamc='git am --continue'
alias gams='git am --skip'
alias gama='git am --abort'
alias gamscp='git am --show-current-patch'
```
# rebase流程
```bash
1.develop分支上拉代码gup(git pull --rebase)
2.有新代码拉下来之后,gco切换回自己的分支
3.在自己分支,将develop合并到自己的分支上grb develop
4.有冲突解决冲突,没冲突合并成功之后切换到gco develop
5.在develop上我会再gup一下,防止有人再次提交了
6.develop没有新代码拉下来,就把自己的分支合并到develop上, grb dev-jrj
7.develop分支上gp推送到远程
```

## 利用rebase进行commit合并
1. git rebase -i <commitId>
表示commitId之后的所有commit进行合并
2. 运行完 1. 会进入vi
3. 将需要合并的commit前面的关键命令改成squash 或者s
4. esc :wq 保存退出

## 利用rebase进行commit信息修改
1. git rebase -i <commitId> || git rebase -i HEAD~n
表示commitId之后的所有commit进行修改
2. 运行完 1. 会进入vi
3. 将需要合并的commit前面的关键命令改成edit 或者e
4. esc :wq 保存退出

# 问题
+ 本地验证失败
常见于切换远程仓库后push(例如从github到gitlab)
`git config --system --unset credential.helper`
卸载凭证助手之后需要重新输入账号密码


+ 如何去除每次都需要输入账号密码?
`git config  credential.helper store`

卸载助手`git config --system --unset credential.helper`

+ 编辑/保存配置文件
`vi ~/.npmrc`vim模式进入配置文件
`source ~/.npmrc`
source命令通常用于保留、更改当前shell中的环境变量。 简而言之，source一个脚本，将会在当前shell中运行execute命令。 source命令它需要一个文件，如果提供了参数，那么将用作传递脚本的位置参数。


+ 合并分支的时候报错
`gl`
`fatal: refusing to merge unrelated histories`

在对应的命令后加上`--allow-unrelated-histories`

例如上面的gl报错的时候使用`gl --allow-unrelated-histories`


+ 出现太多悬空的对象
```bash
dangling blob a2ffdbd2e9488a296ecdaa67c63a5d46ff160408
dangling blob abff393ef266ececc1d002e323a765f4af3614aa
dangling blob acff2522b7528356392c3473144e0faab8ffed54
dangling blob b1ff957a4f3f8655577060245977f1d445b576fd
dangling blob b7ffd7a855ef2842b77c2f9b0fa2b2721a8f3506
dangling blob bbff4929f36d5b0ba9042dc66c5b95f816e74322
dangling blob c0ff3d81ecec20e35445366a75892cfef688a80e
dangling blob c1ffe996fdf38e2751b7becd39632f6d88c2eb5e
dangling blob c0ffb1b9e8bc427ffd3189b026399e1d028f5491
dangling blob c5ff954671d7e104864f1850e6fa7bb786879fe7
dangling blob f5ffddf08473fa2267b8a3a62a698127715a71d5
.......
```
原因: gstall = git stash --all
应该是把nodule_modules中的文件都stash了,然后clear的时候也没有清除干净
使用`git prune`清除这些文件


+ 在指定的commit创建分支
`gcb <branch-name> <commit-id>`


+ git修改分支名字
`git branch -m <oldName> <newName>`
如果修改当前所在的分支名字 直接传入新名字即可 `git branch -m <newName>`

+ 修改commit信息
只修改当前的commit message
`git commit --amend`

修改多个commit message
`grbi HEAD~n` 将前面的pick改为e

+ 删除远端地址
当clone了一个官方库源代码 本地做了一些修改 并不想同步到远端 可以直接删除远端地址
`git remote -v`查看远端地址
`git remote remove origin`删除所有的git远端地址  变成一个彻底的本地项目


+ 放弃本地所有的修改
对于还没有git add的修改文件
`git checkout .`可以放弃所有修改的文件


+ 开发完一个功能，提交了代码，但是这个时候我发现有点小问题，比如单词拼写错了。比如运行linter 告诉我少加了一个空格，我不想为了这个小错误添加一个新的 commit
直接将新的修改提交到上一个commit中
```bash
# 添加这个对应的小的修改
git add .
# 提交并且使用两个特殊的参数
# --amend 修改最新的一次 commit，将现在 staged change 直接添加到上一次 commit 去，不生成新的 commit
# --no-edit amend 的提交不修改提交信息
git commit --amend --no-edit
```


+ 本地项目上传gitlab
1. 创建本地项目
2. git init
3. 到gitlab上创建项目, 复制地址
4. 本地项目 `git remote add origin url`
5. 到gitlab/项目/setting/respository/protect branches 删除保护分支
6. 创建一个别的分支作为默认分支 gp -f


## 设置远程 | 删除远程
`git remote add origin <远程的url>`
删除已存在的远程
`git remote rm origin`
`grrm origin`

## 修改commit的作者和邮箱
OLD_EMAIL 旧邮箱（也就是需要替换掉的 Git 历史中的邮箱）
CORRECT_NAME 新名称
CORRECT_EMAIL 新邮箱
```bash
git filter-branch --env-filter '
OLD_EMAIL="jingrenjie@SKBX878.local"
CORRECT_NAME="isaac"
CORRECT_EMAIL="2291229530@qq.com"
if [ "$GIT_COMMITTER_EMAIL" = "$OLD_EMAIL" ]
then
    export GIT_COMMITTER_NAME="$CORRECT_NAME"
    export GIT_COMMITTER_EMAIL="$CORRECT_EMAIL"
fi
if [ "$GIT_AUTHOR_EMAIL" = "$OLD_EMAIL" ]
then
    export GIT_AUTHOR_NAME="$CORRECT_NAME"
    export GIT_AUTHOR_EMAIL="$CORRECT_EMAIL"
fi
' --tag-name-filter cat -- --branches --tags
```



## 查看远程有无某个分支
1. 拉取远程数据
git pull --rebase (gpr)

2. 列出所有远程分支
git branch -r (gb -r)

3. 接触管道运算符和grep筛选需要的数据
gb -r | grep target-branch-name


## 开发中合并到maste的远程分支会自动被删除, 如何同步到本地
注意 git fetch和git pull --bebase之类的分支并不会同步被代码管理0平台删除的分支
如果需要更新
使用`git remote prune origin` 更新远程分支


## git只保存暂存区的内容, 而非全部内容
`git stash push --staged`
`git stash push --staged -m "commit message"`

------------------------------------------------


# Shell
- man 查看当前命令的说明文档
`man ls`

- help 查看当前命令的简要帮助
只能用于查看内嵌命令的帮助

- --help 查看外部命令的帮助
`ls --help`

- type 查看当前命令是否是内嵌命令
`type cd`
cd是内嵌命令
ls不是

- history 显示之前输入过得所有命令

- reset 彻底清空当前命令行的命令 (ctrl + l只清空当前屏幕)

- pwd 打印当前目录的绝对路径

- ls 列出当前目录的全部内容
注意.开头的文件(隐藏文件)使用ls不会显示

`ls -a` 显示所有文件 包含以.开头的隐藏文件

- `ll` 显示所有文件 包含.开头的隐藏文件 还包含修改时间文件大小等详细信息

- mkdir xxx 在当前目录下新建文件名
`mkdir -p /j/i/k` 创建嵌套的文件 如果父文件夹不存在也创建父文件夹

简写为md

- rmdir xxx 在当前目录下删除文件名
如果文件夹里面有文件 那会删除失败
`rmdir -p /j/i/k` 删除嵌套的文件 如果删完文件后父文件夹没有文件了  就把父文件夹也删除

- touch 创建文件

- cp 复制文件
`cp 源文件 目标路径`
`cp -r 源文件 目标路径` 递归的复制文件夹下所有文件

- rm -rf 懂得都懂
rm -r 递归的删除当前目录下的所有文件(有提示)
rm -f 删除当前目录下的所有文件(无提示)
rm -rf 递归删除所有文件,且没有提示
rm -rf <path> 删除当前目录下某个文件
rm -rf /* 递归删除/下所有文件,且没有提示 **(必须谨慎使用这条命令)**

*如果只删除文件, 而非文件夹, 不要加-rf, rm <filepath>*

- mv 移动文件 修改文件
`mv 源文件 目标地址`
mv 源文件 新文件名字

- cat 查看文件
cat <文件名>
cat -n <文件名> 显示文件内容的时候带上行号

- more 查看文件

- less 查看文件
比more显示文件性能更好 因为是只加载需要显示的内容 按需加载, 相当于虚拟滚动

空格/pagedown 向下翻页
pageup 向上翻页

/ n 向下查找字符串
`/徐凤年` 按 `n`向下查找当前文件中的关键字徐凤年, N向上查找

- echo
echo 打印信息
echo 'test content' > text.txt  创建一个txt文件，并在该文件中写入'test content'

- `>`输出到文件 (这是一个覆盖的操作)

- `>>`输出追加到文件末尾 (追加不会覆盖)

- head显示当前文件头部的内容
默认显示10行
`head <文件>`
`head -n 5 <file>` 显示头5行内容 这里的5可以是任意数字

- tail显示当前文件的尾内容
默认显示10行
`tail <file>`
`tail -n 100 <file>`指定显示多少行内容
`tail -f <file>`实时追踪该文件的尾部内容

例: 查看服务器的后端日志的时候可以使用`tail -n 100 -f <file>`来实时追踪该日志文件的更新

- date显示时间
data +%Y 显示年
data +%m 显示月
data +%d 显示天
...
*设置系统时间*
data -s '2017-06-19 20:52:41'
*联网时间还原*
ntpdata

- cal查看日历
cal -3 可以查看3个月的月份信息 只能看-1 或者-3 其他选项查文档

cal 2024 可以查看2024年一年的12个月信息

- find查找文件
find <查找路径>
`find / -name "*.txt"` 在/下按照名称来查找

- locate 查找文件 基于数据库

- grep过滤查找 经常配合 | 管道符号

`grep -n <要找到内容> <查找的文件>` -n显示所在行号

grep -v 表示逆操作, 即筛选不包含-v后面的元素的文件

- | 管道符号 将前面命令输出的内容输入给后面

`ls | grep .txt` 先查找当前目录下的所有文件 然后将后缀是txt的文件过滤查找出来

## 服务器用户管理, 管理要基于root权限
- useradd 添加用户
useradd -g <group> <name> 将用户添加到组

- su切换用户

- whoami 显示当前用户的身份


- gzip 压缩文件
只能压缩一个后缀为.gz的文件
不会保存原文件
如果同时压缩多个文件 会产生多个压缩包文件
只能压缩文件 不能压缩文件夹

- gunzip 解压文件


- zip 压缩文件
可以压缩文件也可以压缩文件夹(目录)
压缩会保留源文件


zip -r 目录  递归压缩文件
`zip -r myFile.zip /home/extra/file`

- unzip 解压文件
`unzip -d <压缩包>`
-d指定解压的路径

- tar命令
打包 压缩 等

- du 查看硬盘占用情况
du -h 以人类容易阅读的大小单位显示
du -a 不仅查看子目录大小 也查看子文件大小
du -s 查看总和大小
du -sh 查看总和大小(以容易理解的大小单位显示 若如M, G)

- df 查看磁盘使用情况
df -h 同上

- du -h <filepath>
以对人类友好的方式查看文件大小

- du -s -h <filepath>
如果是文件路径
只显示当前文件夹的总和大小 不需要显示每个子文件的大小


- ps -ef
显示当前所有进程
ps：这是 "process status" 的缩写，用于报告当前系统的进程状态。
-e：选项表示选择所有进程。
-f：选项表示使用全格式输出。这通常会显示更多的列，例如 UID（用户 ID）、PID（进程 ID）、PPID（父进程 ID）、C（CPU 使用率）、STIME（开始时间）、TTY（控制终端）、TIME（CPU 时间）和 CMD（命令行）。

- ps -ef | grep nginx
显示所有nginx有关的进程

- ps -ef | grep nginx | grep -v grep
由于 grep 命令本身也会作为一个进程运行，所以 grep nginx 这行通常也会出现在输出中。如果你只想看到与 "nginx" 相关的其他进程，可以使用 grep -v grep 来排除包含 "grep" 的行


- 执行多条命令
使用&&, &, 分号都可以执行多条命令
&& 前一个命令执行成功后才会执行后一个命令
& 多条命令同时执行 没有先后顺序
分号 多条命令同时执行, 有先后顺序, 但是不管前面的有没有成功,都会执行后面的

## $1
表示命令接收的第一个参数
可以在.zshrc中借助$1封装一些命令
```bash
# 利用$1定义简化命令
# 使用 mcd tmp, 此时$1就指后面的tmp文件夹名称
# 实际作用相当于mkdir tmp && cd tmp
mcd() {
  mkdir $1 && cd $1
}

c() {
  cd $1 && ls
}
```

## !$
表示命令的最后一个参数
mkdir foobarbaz
紧接着cd !$ 就相当于cd foobarbaz

比如先vim /etc/nginx/nginx.conf 发现没有编辑权限
重新输入命令只需要 sudo vim !$

------------------

# tree
用于生成项目的树结构
## 安装
```bash
brew install tree

# win安装
yum install tree
```

## tree的使用
tree [OPTIONS] [directory]

-a 显示所有文件和目录。
-A 使用ASNI绘图字符显示树状图而非以ASCII字符组合。
-C 在文件和目录清单加上色彩，便于区分各种类型。
-d 显示目录名称而非内容。
-D 列出文件或目录的更改时间。
-f 在每个文件或目录之前，显示完整的相对路径名称。
-F 根据ls -F，为目录添加一个'/'，为套接字文件添加一个'='，为可执行文件添加一个' *'，为FIFO添加一个' |'
-g 列出文件或目录的所属群组名称，没有对应的名称时，则显示群组识别码。
-i 不以阶梯状列出文件或目录名称。
-I 不显示符合范本样式的文件或目录名称。
-l 跟随目录的符号链接，就像它们是目录一样。避免了导致递归循环的链接
-n 不在文件和目录清单加上色彩。
-N 按原样打印不可打印的字符。
-p 列出权限标示。
-P 只显示符合范本样式的文件或目录名称。
-q 将文件名中的不可打印字符作为问号打印。
-s 列出文件或目录大小。
-t 用文件和目录的更改时间排序。
-u 列出文件或目录的拥有者名称，没有对应的名称时，则显示用户识别码。
-x 将范围局限在现行的文件系统中，若指定目录下的某些子目录，其存放于另一个文件系统上，则将该子目录予以排除在寻找范围外。


## 直接执行tree命令
tree
显示了所有的文件和目录，除了没有文件名的文件（bebel配置文件）：


## tree a
显示所有的文件和目录，包括不含文件名的babel文件


## tree -d
显示目录名称而非内容


## tree -f
在每个文件或目录之前，显示完整的相对路径名称


## tree -I pattern
其中pattern为通配符，不显示符合的文件或目录名称。
比如我们不显示css文件
不显示node_modules文件的内容
```bash
tree -I "node_modules"
```


## tree -P(大写) pattern
仅列出与通配符模式匹配的文件。
比如仅显示css文件
tree -P *.css
```bash
└── test
    ├── css
    │   ├── jquery-ui.css
    │   └── main.css
    ├── img
    │   └── head
    └── js
```
## tree -P 配合 -a 使用
这样我们就可以打印出来所有的问题，包括隐藏的和没有文件名的文件
`tree -P *.* -a`


# 快捷键
ctrl + c 新开一个命令行
ctrl + u 删除光标以前的字符
ctrl + k 删除光标以后的字符
ctrl + l 清屏

# vim
:$可以快速跳转到文件的末尾