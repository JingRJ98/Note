# React
原生js直接操作DOM，效率低
浏览器进行大量的重绘重排
原生js没有组件化的编码方案 代码复用率低
是一个将数据渲染为HTML视图的JS库

# React的特点

1.声明式编码
2.组件化编码
3.支持客户端，服务端渲染
4.高效
    原因：
    1.不直接操作DOM，虚拟DOM
    2.DOM diff算法 最小化页面重绘
5.单向数据流


# 准备
https://reactjs.org/
https://react.docschina.org/


# React JSX
```html
<body>
	<!-- 准备好一个“容器” -->
	<div id="test"></div>

	<!-- 引入react核心库 -->
	<script type="text/javascript" src="../js/react.development.js"></script>
	<!-- 引入react-dom，用于支持react操作DOM -->
	<script type="text/javascript" src="../js/react-dom.development.js"></script>
	<!-- 引入babel，用于将jsx转为js -->
	<script type="text/javascript" src="../js/babel.min.js"></script>

	<script type="text/babel" > /* 此处一定要写babel 不写babel 默认写的jsx */
		//1.创建虚拟DOM
		const VDOM = <h1>Hello,React</h1> /* 此处一定不要写引号，因为不是字符串 */
		//2.渲染虚拟DOM到页面
		// render渲染的意思
		//
		ReactDOM.render(VDOM,document.getElementById('test'))
	</script>
</body>
```

## 虚拟DOM
两种创建方式
1、使用传统的js创建虚拟DOM
	此时如果需要创建多层嵌套的标签内容，无法实现

```html
<body>
    <!--
        使用js方式创建虚拟DOM
     -->
    <div id="test"></div>

    <script src="../js/react.development.js"></script>
    <script src="../js/react-dom.development.js"></script>

    <script>
        // 1、创建虚拟DOM
        // const VDOM = React.creatElement(标签名，标签属性，标签内容)
        const VDOM = React.createElement('h1',{id:'title'},React.createElement('span',{id:'121'},'Hello React'))

        // 2、渲染虚拟DOM到页面
        ReactDOM.render(VDOM,document.getElementById('test'))
    </script>
</body>
```
2、使用jsx创建嵌套的标签，加括号使用多级结构
	直观 方便 不错zan
	使用jsx写的代码一定要引入babel翻译成js
	jsx的目的就是更为方便的创建虚拟DOM
	否则必须使用React.creatElement创建DOM元素，复杂繁琐
```js
	//1.创建虚拟DOM
	const VDOM = (  /* 此处一定不要写引号，因为不是字符串 */
		<h1 id="title">
			<span>Hello,React</span>
		</h1>
	)
	//2.渲染虚拟DOM到页面
	ReactDOM.render(VDOM,document.getElementById('test'))
```
React提供了一些API来创建一种“特别”的一般js对象
`const VDOM = React.createElement('h1', {id:'myTitle'}, 'hello world')`


关于虚拟DOM：
	1.本质是Object类型的对象（一般对象）
	2.虚拟DOM比较“轻”，真实DOM比较“重”，因为虚拟DOM是React内部在用，无需真实DOM上那么多的属性。
	3.虚拟DOM最终会被React转化为真实DOM，呈现在页面上。

```html
	<script type="text/babel" > /* 此处一定要写babel */
		//1.创建虚拟DOM
		const VDOM = (  /* 此处一定不要写引号，因为不是字符串 */
			<h1 id="title">
				<span>Hello,React</span>
			</h1>
		)
		//2.渲染虚拟DOM到页面
		ReactDOM.render(VDOM,document.getElementById('test'))

		const TDOM = document.getElementById('demo')
		console.log('虚拟DOM',VDOM);// Object
		console.log('真实DOM',TDOM);
		// debugger;
		// console.log(typeof VDOM);
		// console.log(VDOM instanceof Object);判断是不是对象
	</script>
```

# JSX
```html

	<script type="text/babel" >
		const myId = 'aTgUiGu'
		const myData = 'HeLlo,rEaCt'

		//1.创建虚拟DOM
		const VDOM = (
			<div>
			// 指定类名要用className，防止和class混淆
				<h2 className="title" id={myId.toLowerCase()}>
					<span style={{color:'white',fontSize:'29px'}}>{myData.toLowerCase()}</span>
				</h2>
				<h2 className="title" id={myId.toUpperCase()}>
					<span style={{color:'white',fontSize:'29px'}}>{myData.toLowerCase()}</span>
				</h2>
				<input type="text"/>
				<good>123333</good>
			</div>
		)
		//2.渲染虚拟DOM到页面
		ReactDOM.render(VDOM,document.getElementById('test'))

		/*
			jsx语法规则：
				1.定义虚拟DOM时，不要写引号。
				2.标签中混入JS表达式时要用{}。
				3.样式的类名指定不要用class，要用className。
				4.内联样式，要用style={{key:value}}的形式去写。
					第一个引号表示js表达，里面的{}表示对象，写键值对
				5.只有一个根标签
				6.标签必须闭合（或者加<xxxxxxx/>自闭合）
				7.自定义的标签首字母
					(1).若小写字母开头，则将该标签转为html中同名元素，若html中无该标签对应的同名元素，则报错。
					(2).若大写字母开头，react就去渲染对应的组件，若组件没有定义，则报错。

		 */
	</script>
```
1.全称 JavaScript XML
2..React定义的一种类似于XML的JS扩展语法：XML + JS
3.作用：用来创建React虚拟DOM对象
`const VDOM = <h1>hello world</h1>`
    注意：不是字符串也并不是HTML/XML标签
    它最终产生的就是一个JS对象
4.标签名任意
5.标签属性任意
6.基本语法规则
    遇到 < 开头的代码，以标签语法解析：HTML同名标签转换为HTML同名元素，其他标签需要特别解析
    遇到 { 开头代码，以JS语法解析：标签中的JS代码必须用{}包含
7.babel.js作用
    浏览器不能直接解析JSX代码，需要babel转译为纯JS代码
    只要用了JSX，都要加上type='text/babel'，声明需要babel来处理

## 表达式和代码
jsx的{}里面只能写最终return 的值 而不能写作js语句

一个表达式可以产生一个值，可以放在需要的地方
```
	(1). a
	(2). a+b
	(3). demo(1)
	(4). arr.map() 数组方法
	(5). function test () {}
```
语句（代码）
```
	(1).if(){}
	(2).for(){}
	(3).switch(){case:xxxx}
```
### jsx练习
```html
<script type="text/babel" >
	// 模拟数据
	// obj不允许作为react的节点
	var data = ['j','r','j'];
	// 创建虚拟DOM
		const VDOM = (
			<div>
				<h1>前端js框架</h1>
				<ul>
   					// 动态读取数据，js表达式要写在{}中
					// 同时使用变量需要放在{}中
					{
						// map方法遍历对象中元素 返回每一个元素传入函数运行结果构成的数组
						data.map((item,index) =>{
							return <li key = {index}>{item}</li>
						})
					}
				</ul>
			</div>
		)

	// 渲染虚拟DOM
			ReactDOM.render(VDOM,document.getElementById('test'))
	</script>
```

# React面向组件编程
## 什么是模块
一般一个js文件就是一个模块
作用：复用js 简化js

## 什么是组件

用来实现特定局部功能效果的代码集合（HTML/CSS/JS/video……）

## 模块化和组件化
应用的js文件都是以模块来编写的应用就是模块化的应用

当应用时以多组件来编写的，就是组件化的应用


## 组件的定义与使用
### 函数式组件
	执行了ReactDOM.render(<MyComponent/>,应用的位置)之后，发生了什么？
		1.React解析组件标签（标签首字母大写），找到了MyComponent组件。
		2.发现组件是使用函数定义的，随后调用该函数，将返回的虚拟DOM转为真实DOM，随后呈现在页面中。

	babel开启严格模式后 禁止自定义函数指向window
```html
	<script type="text/babel">
		//1.创建函数式组件
		// 因为标签的首字母小写时会寻找同名的标签 大写时则渲染相应的组件
		// 所以必须大写
		function MyComponent(){
			console.log(this); //此处的this是undefined，因为babel编译后开启了严格模式
			// 严格模式禁止自定义的函数this指向window
			return <h2>我是用函数定义的组件(适用于【简单组件】的定义)</h2>
		}
		//2.渲染组件到页面
		ReactDOM.render(<MyComponent/>,document.getElementById('test'))
	</script>
```
### 类式组件
如果使用类式组件，必须继承自react里面内置的一个类`React.Component`
```html
	<script type="text/babel">
		//1.创建类式组件
		// React.Component是React内置的类
		class MyComponent extends React.Component {
			// 类式组件这个类里面的构造器可以不写

			// render()方法必须写
			render(){
				//render是放在哪里的？—— MyComponent的原型对象（prototype）上，供实例使用。
					// 这里的实例没有用new 关键字创建，是由react创建的实例对象
				//render中的this是谁？—— MyComponent的实例对象 <=> MyComponent组件（实例）对象。
				console.log('render中的this:',this);
				return <h2>我是用类定义的组件(适用于【复杂组件】的定义)</h2>
			}
		}
		//2.渲染组件到页面
						// 组件标签需要闭合
		ReactDOM.render(<MyComponent/>,document.getElementById('test'))
		/*
			执行了ReactDOM.render(<MyComponent/>.......之后，发生了什么？
				1.React解析组件标签，找到了MyComponent组件。
				2.发现组件是使用类定义的，随后new出来该类的实例，并通过该实例调用到原型上的render方法。
				3.将render返回的虚拟DOM转为真实DOM，随后呈现在页面中。
		*/
	</script>
```
### 注意

1.组件名必须首字母大写
2.虚拟DOM元素只能有一个根元素
3.虚拟DOM元素必须有结束标签


----------------------------------

# 类的复习
	总结：
		1.类中的构造器不是必须要写的，要对实例进行一些初始化的操作（传参），如添加指定属性时才写。
		2.如果B类继承自A类，B和A中构造器一样，B中不需要写构造器
			如果B类中需要接收新的属性，那必须写构造器，并且B类构造器中的super是必须要调用的。
			super()调用父类中已经定义的构造器属性
		3.类中所定义的方法，都放在了类的原型对象（prototype）上，供实例去使用。
```html
	<script type="text/javascript" >
		//创建一个Person类
		class Person {
			//构造器方法,用来接收初始化的数值（实例对象传入的参数）
			constructor(name,age){
				//构造器中的this是谁？—— 类的实例对象
				this.name = name
				this.age = age
			}
			// 类中的一般方法
			speak(){
				//speak方法放在了哪里？——类的原型对象上，供实例使用
				//通过Person实例调用speak时，speak中的this就是实例对象·
				console.log(`我叫${this.name}，我年龄是${this.age}`);
			}
		}

		// 创建一个Student类，继承于Person类
		// 继承 extends
		class Student extends Person {
			constructor(name,age,grade){
				// 必须先使用super()调用父类中已经定义的构造器属性
				super(name,age)
				this.grade = grade
				this.school = '尚硅谷'
			}
			// 父类中继承过来的方法也可以被重写
			speak(){
				console.log(`我叫${this.name}，我年龄是${this.age},我读的是${this.grade}年级`);
				this.study()
			}
			study(){
				//study方法放在类的原型对象上，供实例使用
				//通过Student的实例对象调用study时，study中的this就是该实例对象
				console.log('我很努力的学习');
			}
		}

		class Car {
			constructor(name,price){
				this.name = name
				this.price = price
				// this.wheel = 4
			}
			//类中可以直接写赋值语句,如下代码的含义是：给Car的实例对象添加一个属性，名为a，值为1
			a = 1
			wheel = 4
			static demo = 100
		}
		const c1 = new Car('奔驰c63',199)
		console.log(c1);
		console.log(Car.demo);
	</script>
```



---------------------------------------


# 原生js事件绑定的复习
三种绑定方式

第一种
DOM2级
Element.addEventListener(('click', () => {}, false))

参数：

1.事件的字符串，不要加on

2.回调函数，当事件触发时该函数会被调用

3.是否在捕获阶段触发事件，需要一个布尔值，默认都传false

优点：可以为一个元素的一个事件绑定多个回调函数，多个回调函数按绑定顺序执行

缺点：兼容性不好，不支持IE8及以下

解绑：removeEventListener()，三个参数必须全部一致才能解绑


第二种
DOM0级

Element.onclick = () => {}

优点：兼容性好

缺点：只能为一个元素的一个事件绑定一个监听函数，如果绑定多个，则后绑会覆盖先绑

解绑：赋值null

三种绑定事件，添加回调的方式
```html
	<button id="btn1">按钮1</button>
	<button id="btn2">按钮2</button>
	<button onclick="demo()">按钮3</button>
	<script type="text/javascript" >
		const btn1 = document.getElementById('btn1')
		btn1.addEventListener('click',()=>{
			alert('按钮1被点击了')
		})
		const btn2 = document.getElementById('btn2')
		btn2.onclick = ()=>{
			alert('按钮2被点击了')
		}
		function demo(){
			alert('按钮3被点击了')
		}
	</script>
```

---------------------------------------



# 组件实例对象的三大属性(类式组件)

## 1、state（状态）

state是组件对象最重要的属性，值是对象（可以包含多个数据）

组件被称为状态机，通过更新组件的state来更新对应的页面显示（重新渲染组件）

```html
	<script type="text/babel">
		//1.创建组件
		class Weather extends React.Component{
			//构造器调用几次？ ———— 1次
			constructor(props){
				console.log('constructor');
				// 继承里面必须调用super()方法
				super(props)
				//初始化状态
				this.state = {isHot:false,wind:'微风'}
				//解决changeWeather中this指向问题（原来是undefined 因为类中开启了严格模式）

				// ↓这个this是实例对象
				// 给 实例自身 添加的changeWeather方法，原来是放在原型对象里面，通过原型链找到的
				this.changeWeather = this.changeWeather.bind(this)
				//                      ↑这个this是实例对象，changeWether是原来类里面定义的方法
				// 						通过原型链查找到，通过bind修改this，返回新函数
				// 												↑这个this也是实例对象
			}

			//render调用几次？ ———— 1+n次 1是初始化的那次 n是状态更新的次数
			render(){
				console.log('render');
				//读取状态
				const {isHot,wind} = this.state
				// react绑定点击事件时使用的是onClick，C大写！！
				// onClick = {}使用原生js表达，并且不能加()，加了()表示将函数返回值返回
				return <h1 onClick={this.changeWeather}>今天天气很{isHot ? '炎热' : '凉爽'}，{wind}</h1>
			}

			//changeWeather调用几次？ ———— 点几次调几次
			changeWeather(){
				//changeWeather放在哪里？ ———— Weather的原型对象上，供实例使用
				//由于changeWeather是作为onClick的回调，所以不是通过实例调用的，是直接调用
				//类中的方法默认开启了局部的严格模式（方法这个函数的内部），所以changeWeather中的this为undefined，而不是指向外面的类的实例对象

				console.log('changeWeather');
				//获取原来的isHot值
				const isHot = this.state.isHot
				//严重注意：状态必须通过setState进行更新,且更新是一种合并，不是替换。
				this.setState({isHot:!isHot,wind: '狂风！'})
				console.log(this);

				//严重注意：状态(state)不可直接更改，下面这行就是直接更改！！！
				//this.state.isHot = !isHot //这是错误的写法
				// 单纯数据更改react不认可

				// 需要借助内部API更改
			}
		}
		//2.渲染组件到页面
		ReactDOM.render(<Weather/>,document.getElementById('test'))

	</script>
```

---------

### 补充：类中方法this的指向

```html
<script>
        class Person{
            constructor(name,age){
                this.name = name
                this.age = age
            }

            speak(){
                console.log(this);
            }
        }

        const p1 = new Person('Tom',18)
        // 通过实例调用speak方法
        // speak放在类的原型对象（prototype）上，供实例调用
        // 通过Person的实例调用speak方法时，this指的就是该实例对象
        p1.speak() // 输出Person的实例对象{}

        const x = p1.speak
        x()
        // 注意此时相当于直接调用，this指向window
        // 但是类中的方法默认开启严格模式
        // 所以输出undefined
    </script>
```

--------

### state的简化写法
```js
	//1.创建组件
	class Weather extends React.Component{
		//初始化状态 可以直接写在构造器外面
		state = {isHot:false,wind:'微风'}
		render(){
			const {isHot,wind} = this.state
			return <h1 onClick={this.changeWeather}>今天天气很{isHot ? '炎热' : '凉爽'}，{wind}</h1>
		}
		//自定义方法————要用赋值语句的形式+箭头函数
		// 箭头函数没有自己的this，如果在箭头函数内部使用了this，会向上一层寻找
		// 上一层是构造类，所以箭头函数的this是类的实例对象
		changeWeather = ()=>{
			const isHot = this.state.isHot
			this.setState({isHot:!isHot})
		}
	}
	//2.渲染组件到页面
	ReactDOM.render(<Weather/>,document.getElementById('test'))

```
类中可以直接写赋值语句，不需要事先定义
类中自定义方法使用箭头函数，箭头函数没有自己的this，箭头函数里面的this使用的是外部的this，外部类中的this指的是实例对象
react渲染组件的时候自动new了实例对象


## 2、props（标签属性）

每个组件对象都会有props(properties)属性
组件标签的所有属性都保存在props中

通过标签属性从组件外向组件内传递变化的数据
注意：所有 React 组件都必须像纯函数一样保护它们的 props 不被更改。
基本使用

```js
	// 创建组件
	class Person extends React.Component{

		render(){
			// 解构赋值
			const {name,age,sex } = this.props
			console.log(this);
			return(
				<ul>
					<li>姓名：{name}</li>
					<li>性别：{age}</li>
					<li>年纪：{sex}</li>

				</ul>
			)
		}
	}
	// 渲染组件
	ReactDOM.render(<Person name = 'tom' age = '18' sex = '男'/>,document.getElementById('test1'))


	// 批量传输数据时
	const p = {name:'老刘',age:18,sex:'女'}
	// ReactDOM.render(<Person name={p.name} age={p.age} sex={p.sex}/>,document.getElementById('test3'))
	ReactDOM.render(<Person {...p}/>,document.getElementById('test3'))
```



------------------------------

#### 三点运算符复习
对数组
```js
	let arr1 = [1,3,5,7,9]
	let arr2 = [2,4,6,8,10]
	console.log(...arr1); //展开一个数组
	let arr3 = [...arr1,...arr2]//连接数组
```
对函数
```js
	// ...用于函数传参
	// 求和函数
	function sum(...nums){
		return nums.reduce((preValue,currentValue) => {
			return preValue + currentValue;
		})
	}
	console.log('AAAA'+sum(...arr1));// 25
```

三点运算符不能应用到对象
但是可以使用{}/[]中的三点运算符实现第一层‘深拷贝’
```js

	let person = {
		name: 'tom',
		age: 18
	}
	// 浅拷贝
	let person2 = person;
	// 深拷贝
	let person3 = {...person}
	person.name = 'jrj'
	console.log(person2);
	console.log(person3);


	let arr666 = [1,2,3,[1,2,444],{name: '孙悟空'}]
	// 浅拷贝
	let arr777 = arr666;
	// 深拷贝
	// let arr888 = {...arr666}
	let arr888 = [...arr666]
	arr666[3] = 111
	console.log('浅拷贝'+arr777[3]);
	console.log(arr888[3]);

```
第二层失效
```js
	let arr666 = [1,2,3,[1,2,[666]],{name: '孙悟空'}]
	// 浅拷贝
	let arr777 = arr666;
	// 深拷贝
	// let arr888 = {...arr666}
	let arr888 = [...arr666]
	arr666[3][2] = 111
	console.log('浅拷贝'+arr777[3][2]);
	// 111
	console.log(arr888[3][2]);
	// 111
```

还可以使用三点运算符对对象进行合并

----------------------------------


### 对props进行限制
```js
	//对标签属性进行类型、必要性的限制
	Person.propTypes = {
		name:PropTypes.string.isRequired, //限制name必传，且为字符串
		sex:PropTypes.string,//限制sex为字符串
		age:PropTypes.number,//限制age为数值
		speak:PropTypes.func,//限制speak为函数 必须是func，因为function是关键字
	}
```
设置数据的默认值
```js
	//指定默认标签属性值
	Person.defaultProps = {
		sex:'男',//sex默认值为男
		age:18 //age默认值为18
	}
```

### props简写
```html
	<script type="text/babel">
		//创建组件
		class Person extends React.Component{

			constructor(props){
				//构造器是否接收props，是否传递给super，取决于：是否希望在构造器中通过this访问props
				// console.log(props);
				super(props)
				console.log('constructor',this.props);
			}

			//对标签属性进行类型、必要性的限制
			static propTypes = {
				name:PropTypes.string.isRequired, //限制name必传，且为字符串
				sex:PropTypes.string,//限制sex为字符串
				age:PropTypes.number,//限制age为数值
				// 限制speak为函数
                speak:propTypes.func
			}

			//指定默认标签属性值
			static defaultProps = {
				sex:'男',//sex默认值为男
				age:18 //age默认值为18
			}

			render(){
				// console.log(this);
				const {name,age,sex} = this.props
				//props是只读的
				//this.props.name = 'jack' //此行代码会报错，因为props是只读的 不能修改
				return (
					<ul>
						<li>姓名：{name}</li>
						<li>性别：{sex}</li>
						<li>年龄：{age+1}</li>
					</ul>
				)
			}
		}

		//渲染组件到页面
		ReactDOM.render(<Person name="jerry"/>,document.getElementById('test1'))
	</script>
```
`类`的构造器中是否接收props,是否将props传递给super(),取决于是否希望在构造器中通过实例对象this访问props
接收了不传给super，可能出现未定义的bug
因此实际开发中`基本不写构造器`

### state与props的区别

state：组件自身`内部`可变化的数据
props：从组件`外部`向组件内部传递数据，组件内部只读不修改

### refs（元素标识）

组件内的标签都可以定义ref属性来标识自己


#### 字符串形式的refs的简单使用（不推荐使用）
字符串类型的ref标识存在效率问题
```html
   <!-- 表示写的不是js 而是jsx 一定要写babel -->
    <script type="text/babel">
        // 1、创建组件
        class Demo extends React.Component {
            showData = ()=> {
				// 解构赋值
                const {input1} = this.refs
                alert(input1.value)
            }

            showData2 = ()=> {
				// 解构赋值
                const {input2} = this.refs
                alert(input2.value)
            }
            render() {
                return (
                    <div>
					// 添加字符串作为ref标识
                        <input ref = 'input1' type="text" placeholder = '点击按钮提示数据'/> &nbsp;
                        <button ref = 'btn' onClick = {this.showData}>点我提示数据</button>&nbsp;
                        <input ref = 'input2' onBlur = {this.showData2} type="text" placeholder = '失去焦点提示数据'/>

                    </div>
                )
            }
        }
        // 2、渲染类式组件
        ReactDOM.render(<Demo/>,document.getElementById('test'))
```


#### 回调格式的ref

`<input type='text' ref=(input => this.msginput = input)`
回调函数在组件初始化渲染完成或卸载时自动调用
作用：通过ref获取组件内容特定标签对象，进行读取其相关数据

```html
    <script type="text/babel">
        // 1、创建组件
        class Demo extends React.Component {
            showData = ()=> {
                // 直接从实例对象上获取属性
                const {input1} = this
                alert(input1.value)
            }

            showData2 = ()=> {
                const {input2} = this
                alert(input2.value)
            }
            render() {
                return (
                    <div>
                        将当前节点放到实例对象上起名为input1
                        <input ref = {currentNode => this.input1 = currentNode} type="text" placeholder = '点击按钮提示数据'/> &nbsp;
                        <button onClick = {this.showData}>点我提示数据</button>&nbsp;
                        <input ref = {c => this.input2 = c} onBlur = {this.showData2} type="text" placeholder = '失去焦点提示数据'/>

                    </div>
                )
            }
        }
        // 2、渲染类式组件
        ReactDOM.render(<Demo/>,document.getElementById('test'))
    </script>
```
```html
    <script type="text/babel">
        // 定义组件
        class MyComponent extends React.Component {
            constructor(props) {
                super(props)
                // 自定义函数this强制绑定为组件对象
                this.handleClick = this.handleClick.bind(this)
            }
            // 自定义方法中的this默认为null
            handleClick() {
                alert(this.msgInput.value)
            }
            handleBlur(e) {
                alert(e.target.value)
            }
            render() {
                return (
                    <div>
                        <input type="text" ref={input => this.msgInput = input}/>
                        <button onClick={this.handleClick}>提示已输入内容</button>
                        <input type="text" placeholder='失去焦点提示内容' onBlur={this.handleBlur}/>
                    </div>
                )
            }
        }
        ReactDOM.render(<MyComponent/>, document.getElementById('test'))
```

# react 中的事件处理
react使用的是自定义的事件，而不是使用的原生DOM事件  ---为了更好的兼容性
react中的事件时通过事件委托的方式处理的，委托给组件最外层的元素，  ---- 为了高效


# 组件的组合
非受控组件
```html
	<script type="text/babel">
		//创建组件
		class Login extends React.Component{
			handleSubmit = (event)=>{
				event.preventDefault() //阻止表单提交
				// 这里的ref绑定的是DOM节点
				const {username,password} = this
				// 所以这里是.value
				alert(`你输入的用户名是：${username.value},你输入的密码是：${password.value}`)
			}
			render(){
				return(
					<form onSubmit={this.handleSubmit}>
						{/* 把当前节点设置到组件实例对象上，并起个名字叫username*/}
						用户名：<input ref={c => this.username = c} type="text" name="username"/>
						密码：<input ref={c => this.password = c} type="password" name="password"/>
						<button>登录</button>
					</form>
				)
			}
		}
		//渲染组件
		ReactDOM.render(<Login/>,document.getElementById('test'))
	</script>
```

## 功能界面的组件化编码流程（无比重要）

1.拆分组件：拆分界面，抽取组件

2.实现静态组件：使用组件实现静态页面效果

3.实现动态组件
    1.动态显示初始化数据
    2.交互功能（从绑定事件监听开始）

问题1：数据保存在哪个组件内？
    看数据是某个组件需要（给它）
    还是某些组件需要（给共同的父组件）

问题2：需要在子组件中改变父组件的状态
    子组件不能直接改变父组件的状态
    状态在哪个组件，更新状态的行为就应该在哪个组件
    解决：父组件定义函数，传递给子组件，子组件调用

```jsx
<body>
  <div id="example"></div>
  <script type="text/javascript" src="../js/react.development.js"></s>
  <script type="text/javascript" src="../js/react-dom.development.js"></script>
  <script type="text/javascript" src="../js/prop-types.js"></script>
  <script type="text/javascript" src="../js/babel.min.js"></script>
  <script type="text/babel">
    /*
    1)拆分组件: 拆分界面,抽取组件
    2)实现静态组件: 使用组件实现静态页面效果
    3)实现动态组件
        ① 动态显示初始化数据
        ② 交互功能(从绑定事件监听开始)
     */
    // 应用组件
    class App extends React.Component {
      constructor (props) {
        super(props)
        // 初始化状态
        this.state = {
          todos: ['吃饭', '睡觉', '打豆豆']
        }
        this.add = this.add.bind(this)
      }
      add (todo) {
        const {todos} = this.state
        todos.unshift(todo)
        //更新状态
        this.setState({todos})
      }
      render () {
        const {todos} = this.state
        return (
          <div>
            <TodoAdd add={this.add} count={todos.length} />
            <TodoList todos={todos} />
          </div>
        )
      }
    }

    // 添加todo组件
    class TodoAdd extends React.Component {
      constructor (props) {
        super(props)
        this.addTodo = this.addTodo.bind(this)
      }
      addTodo () {
        // 读取输入数据
        const text = this.input.value.trim()
        // 查检
        if(!text) {
          return
        }
        // 保存到todos
        this.props.add(text)
        // 清除输入
        this.input.value = ''
      }
      render () {
        return (
          <div>
            <h2>Simple TODO List</h2>
            <input type="text" ref={input => this.input=input}/>
            <button onClick={this.addTodo}>Add #{this.props.count}</button>
          </div>
        )
      }
    }
    TodoAdd.propTypes = {
      add: PropTypes.func.isRequired,
      count: PropTypes.number.isRequired
    }

    // todo列表组件
    class TodoList extends React.Component {
      render () {
        const {todos} = this.props
        return (
          <ul>
            {
              todos.map((todo, index) => <li key={index}>{todo}</li>)
            }
          </ul>
        )
      }
    }
    TodoList.propTypes = {
      todos: PropTypes.array.isRequired
    }

    // 渲染应用组件标签
    ReactDOM.render(<App />, document.getElementById('example'))

  </script>
</body>
```

## 收集表单数据

问题：在React应用中，如何收集表单输入数据

包含表单的组件分类：
1.受控组件：表单输入数据能自动收集成状态
	受控组件的优势是减少ref的使用，将节点的数据存入state中，自动收集数据
	获取数据也是从state中拿到
2.非受控组件：需要时才手动读取表单输入框中的数据

### 非受控组件

```js
 // 1、创建组件
        class Login extends React.Component {
            handleSubmit = (event) => {
                // 阻止表单提交
                event.preventDefault()
                const {username,password} = this
                // alert(username.value)
                alert(`你输入的用户名是${username.value}，你输入的密码是${password.value}`)
            }
            render() {
                return (
                    <form onSubmit = {this.handleSubmit}>
                        用户名：<input ref = {c => this.username = c} type="text" name = 'username'/><br/><br/>
                        密码：<input ref = {c => this.password = c} type="password" name= 'password'/><br/><br/>
                        <button>登录</button>
                    </form>
                )
            }
        }

        // 2、渲染
        ReactDOM.render(<Login/>,document.getElementById('test'))
```

### 受控组件

```js
 // 1、创建组件
        class Login extends React.Component {

            // 初始化状态
            state = {
                username:'',
                password:''
            }
            saveUsername = (event) => {
                // 随着输入发生改变
                console.log(event.target.value);
                this.setState({username:event.target.value})

            }
            handleSubmit = (event) => {
                // 阻止表单提交
                event.preventDefault()
                const {username,password} = this.state
                // alert(username.value)
                alert(`你输入的用户名是${username}，你输入的密码是${password}`)
            }
            render() {
                return (
                    <form onSubmit = {this.handleSubmit}>
                        用户名：<input onChange = {this.saveUsername} type="text" name = 'username'/><br/><br/>
                        密码：<input type="password" name= 'password'/><br/><br/>
                        <button>登录</button>
                    </form>
                )
            }
        }

        // 2、渲染
        ReactDOM.render(<Login/>,document.getElementById('test'))
```
# 高阶函数
如果一个函数符合两个条件中的任何一个，该函数就是高阶函数

	1、A函数接收的参数是一个函数，那么A就是高阶函数
	2、若A函数调用的返回值仍然是一个函数，那么A就是高阶函数
	常见的高阶函数
	promise，setTimeout ,arr.map()等，
	数组的很多方法都是高阶函数（返回一个函数）


```html
	<script type="text/babel">
		//创建组件
		class Login extends React.Component{
			//初始化状态
			state = {
				username:'', //用户名
				password:'' //密码
			}

			//保存表单数据到状态中
			saveFormData = (dataType)=>{
				// dataType就是saveFormData调用的时候传入的实参
				return (event)=>{
					// 因为onChange真正调用的是这个箭头函数，实参就是event
					this.setState({[dataType]:event.target.value})
				}
			}

			//表单提交的回调
			handleSubmit = (event)=>{
				event.preventDefault() //阻止表单提交
				const {username,password} = this.state
				alert(`你输入的用户名是：${username},你输入的密码是：${password}`)
			}
			render(){
				return(
					<form onSubmit={this.handleSubmit}>
					{// onChange的回调是this.saveFormData('username')执行结果！}
					{// this.saveFormData('username')返回一个函数}
						用户名：<input onChange={this.saveFormData('username')} type="text" name="username"/>
						密码：<input onChange={this.saveFormData('password')} type="password" name="password"/>
						<button>登录</button>
					</form>
				)
			}
		}
		//渲染组件
		ReactDOM.render(<Login/>,document.getElementById('test'))
	</script>
```


## 另一种写法

```html
	<script type="text/babel">
		//创建组件
		class Login extends React.Component{
			//初始化状态
			state = {
				username:'', //用户名
				password:'' //密码
			}

			//保存表单数据到状态中
			saveFormData = (dataType,event)=>{
				this.setState({[dataType]:event.target.value})
			}

			render(){
				return(
					<form onSubmit={this.handleSubmit}>
						{// 前提：onChange的回调函数的参数就是event！！！}
						用户名：<input onChange={event => this.saveFormData('username',event) } type="text" name="username"/>
					</form>
				)
			}
		}
		//渲染组件
		ReactDOM.render(<Login/>,document.getElementById('test'))
	</script>
```



# 组件生命周期

## 渲染与卸载页面中的组件
渲染组件
`ReactDOM.render(<Life/>,document.getElementById('test'))`
接收两个参数，第一个要被渲染的组件名称，需要加在`</>`中，第二个参数是渲染的节点位置


卸载组件
`ReactDOM.unmountComponentAtNode(document.getElementById('test'))`
接收一个参数，就是卸载哪一个节点上的组件

## 理解

1.组件对象从创建到死亡它会经历特定的生命周期阶段
2.React组件对象包含一系列的钩子函数（生命周期回调函数），在生命周期特定时刻回调
3.我们在定义组件时，可以重写特定的生命周期回调函数，做特定的工作


## 生命周期详述

1.组件的三个生命周期状态：
    Mount：挂载，插入真实DOM
    Update：被重新渲染
    Unmount：解除挂载，被移出真实DOM

2.React为每个状态都提供了钩子函数
    挂载之前 componentWillMount()
    挂在完毕 componentDidMount()
    更新前 componentWillUpdate()
    更新完 componentDidUpdate()
    将要销毁 componentWillUnmount()

3.生命周期流程

第一阶段：第一次初始化渲染显示:ReactDOM.render()
    constructor():创建对象初始化state
    componentWillMount():将要挂载回调
    render():虚拟DOM挂载回调
    componentDidMount:已经挂载

第二阶段：.每次更新state:this.setState()
    componentWillUpdate():将要更新回调
    render():更新（重新渲染）
    componentDidUpdate():已经更新回调

第三阶段：ReactDOM.unmountComponentAtNode(containerDom)
    componentWillUnmount():组件解除挂载前回调


事例：
```js
       // 1、创建组件
        class Life extends React.Component {
            // 方法
            fun = () => {

                // 卸载组件
                ReactDOM.unmountComponentAtNode(document.getElementById('test'))
            }

            // 数据
            state = {
                opacity: 0.1
            }

            // 只调用1次，在初试渲染完页面之后
            componentDidMount() {
                // 把定时器挂在组件自身的属性上
                this.time = setInterval(() => {
                    let { opacity } = this.state
                    opacity -= 0.1
                    if (opacity <= 0) {
                        opacity = 1
                    }
                    this.setState({ opacity })
                }, 200)
            }

            // 组件将要卸载的时候调用
            componentWillUnmount() {
                console.log('@@@');
                // 清除定时器
                clearInterval(this.time)
            }
            // render调用的时机
            // 1、初始化渲染
            // 2、状态更新之后
            // 所以render会调用1+n次
            render() {
                console.log('ren');
                return (
                    <div>
                        <h2 style={{ opacity: this.state.opacity }}>React学不会</h2>
                        <button onClick={this.fun}>好难啊！！！</button>

                    </div>
                )
            }
        }
        // 2、渲染
        ReactDOM.render(<Life />, document.getElementById('test'))
```
## 重要的钩子

1.render():初始化渲染以及更新渲染调用
2.componentDidMount():开启监听，发送Ajax请求
3.componentWillUnmount:做一些收尾工作，如：清除定时器
4.componentWillReceiveProps()

```html
<div id="example"></div>

<script type="text/javascript" src="../js/react.development.js"></script>
<script type="text/javascript" src="../js/react-dom.development.js"></script>
<script type="text/javascript" src="../js/babel.min.js"></script>
<script type="text/babel">
  /*
  需求: 自定义组件
    1. 让指定的文本做显示/隐藏的动画
    2. 切换时间为2S
    3. 点击按钮从界面中移除组件界面
   */
  class Fade extends React.Component {

    constructor (props) {
      super(props)
      console.log('constructor(): 创建组件对象')
      this.state = {
        opacity: 1
      }
      this.removeComponent = this.removeComponent.bind(this)
    }

    componentWillMount () {
      console.log('componentWillMount(): 初始化将要挂载')
    }

    componentDidMount () {// 在此方法中启动定时器/绑定监听/发送ajax请求
      console.log('componentDidMount(): 初始化已经挂载')
      // 保存到当前组件对象中
      this.intervalId = setInterval(function () {
        console.log('--------')
        // 得到当前opacity
        let {opacity} = this.state
        // 更新opacity
        opacity -= 0.1
        if(opacity<=0) {
          opacity = 1
        }
        // 更新状态
        this.setState({opacity})
      }.bind(this), 200)
    }

    componentWillUpdate () {
      console.log('componentWillUpdate(): 将要更新')
    }
    componentDidUpdate () {
      console.log('componentDidUpdate(): 已经更新')
    }

    componentWillUnmount () {// 清除定时器/解除监听
      console.log('componentWillUnmount(): 将要被移除')
      clearInterval(this.intervalId)
    }

    removeComponent () {
      ReactDOM.unmountComponentAtNode(document.getElementById('example'))
    }

    render() {
      console.log('render() 渲染组件')
      return (
        <div>
          <h2 style={{opacity:this.state.opacity}}>{this.props.content}</h2>
          <button onClick={this.removeComponent}>不活了</button>
        </div>
      )
    }
  }
  ReactDOM.render(<Fade content="react学不会, 怎么办?"/>, document.getElementById('example'))
</script>
```



# 虚拟DOM与DOM diff 算法
diff算法比较不同的最小粒度是节点


```html

<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>key的作用</title>
</head>
<body>
<div id="test"></div>
<!-- 引入react核心库 -->
<script type="text/javascript" src="../js/react.development.js"></script>
<!-- 引入react-dom -->
<script type="text/javascript" src="../js/react-dom.development.js"></script>
<!-- 引入babel -->
<script type="text/javascript" src="../js/babel.min.js"></script>

<script type="text/babel">
	/*
   经典面试题:
      1). react/vue中的key有什么作用？（key的内部原理是什么？）
      2). 为什么遍历列表时，key最好不要用index?

		1. 虚拟DOM中key的作用：
			1). 简单的说: key是虚拟DOM对象的标识, 在更新显示时key起着极其重要的作用。
			2). 详细的说: 当状态中的数据发生变化时，react会根据【新数据】生成【新的虚拟DOM】,
				随后React进行【新虚拟DOM】与【旧虚拟DOM】的diff比较，比较规则如下：

					a. 旧虚拟DOM中找到了与新虚拟DOM相同的key：
						(1).若虚拟DOM中内容没变, 直接使用之前的真实DOM
						(2).若虚拟DOM中内容变了, 则生成新的真实DOM，随后替换掉页面中之前的真实DOM

					b. 旧虚拟DOM中未找到与新虚拟DOM相同的key根据数据创建新的真实DOM，随后渲染到到页面

		2. 用index作为key可能会引发的问题：
			1. 若对数据进行：逆序添加、逆序删除等破坏顺序操作:会产生没有必要的真实DOM更新 ==> 界面效果没问题, 但效率低。

			2. 如果结构中还包含输入类的DOM：会产生错误DOM更新 ==> 界面有问题。

			3. 注意！如果不存在对数据的逆序添加、逆序删除等破坏顺序操作，仅用于渲染列表用于展示，使用index作为key是没有问题的。

		3. 开发中如何选择key?:
			1.最好使用每条数据的唯一标识作为key, 比如id、手机号、身份证号、学号等唯一值。
			2.如果确定只是简单的展示数据，用index也是可以的。
   */

	/*
		慢动作回放----使用index索引值作为key

			初始数据：
					{id:1,name:'小张',age:18},
					{id:2,name:'小李',age:19},
			初始的虚拟DOM：
					<li key=0>小张---18<input type="text"/></li>
					<li key=1>小李---19<input type="text"/></li>

			更新后的数据：
					{id:3,name:'小王',age:20},
					{id:1,name:'小张',age:18},
					{id:2,name:'小李',age:19},
			更新数据后的虚拟DOM：
					<li key=0>小王---20<input type="text"/></li>
					<li key=1>小张---18<input type="text"/></li>
					<li key=2>小李---19<input type="text"/></li>

	-----------------------------------------------------------------

	慢动作回放----使用id唯一标识作为key

			初始数据：
					{id:1,name:'小张',age:18},
					{id:2,name:'小李',age:19},
			初始的虚拟DOM：
					<li key=1>小张---18<input type="text"/></li>
					<li key=2>小李---19<input type="text"/></li>

			更新后的数据：
					{id:3,name:'小王',age:20},
					{id:1,name:'小张',age:18},
					{id:2,name:'小李',age:19},
			更新数据后的虚拟DOM：
					<li key=3>小王---20<input type="text"/></li>
					<li key=1>小张---18<input type="text"/></li>
					<li key=2>小李---19<input type="text"/></li>


	 */
	class Person extends React.Component{

		state = {
			persons:[
				{id:1,name:'小张',age:18},
				{id:2,name:'小李',age:19},
			]
		}

		add = ()=>{
			const {persons} = this.state
			const p = {id:persons.length+1,name:'小王',age:20}
			this.setState({persons:[p,...persons]})
		}

		render(){
			return (
				<div>
					<h2>展示人员信息</h2>
					<button onClick={this.add}>添加一个小王</button>
					<h3>使用index（索引值）作为key</h3>
					<ul>
						{
							this.state.persons.map((personObj,index)=>{
								return <li key={index}>{personObj.name}---{personObj.age}<input type="text"/></li>
							})
						}
					</ul>
					<hr/>
					<hr/>
					<h3>使用id（数据的唯一标识）作为key</h3>
					<ul>
						{
							this.state.persons.map((personObj)=>{
								return <li key={personObj.id}>{personObj.name}---{personObj.age}<input type="text"/></li>
							})
						}
					</ul>
				</div>
			)
		}
	}

	ReactDOM.render(<Person/>,document.getElementById('test'))
</script>
</body>
</html>

```

# React脚手架
webpack搭建的脚手架

作用
	1、用来帮助程序员快速创建一个基于xx库的模板项目
	2、包含了所有需要的配置（语法检查、jsx编译、deserver...）
	3、下载好了所有相关的依赖

开发特点：模块化、组件化、工程化

整体技术架构：es6+webpack+react+eslint

1、全局安装 `npm i -g create-react-app `
2、切换到想要创建文件的目录 使用命令 `create-react-app react_demo`
3、进入项目文件夹 `cd react_demo`
4、开启项目 `npm start`

## 文件架构
node_modules ---- 用于存放各式依赖

public ---- 静态资源文件夹
	favicon.icon ------ 网站页签图标
	**index.html -------- 主页面**
	logo192.png ------- logo图
	logo512.png ------- logo图
	manifest.json ----- 应用加壳的配置文件
	robots.txt -------- 爬虫协议文件

src ---- 源码文件夹
	App.css -------- App组件的样式
	**App.js --------- App组件**
	App.test.js ---- 用于给App做测试
	index.css ------ 样式
	**index.js ------- 入口文件**
	logo.svg ------- logo图
	reportWebVitals.js
		--- 页面性能分析文件(需要web-vitals库的支持)
	setupTests.js
		---- 组件单元测试的文件(需要jest-dom库的支持)


# 案例todoList
## 子组件像父组件传数据
自组件想要把数据传给父组件，
1、父组件给子组件传递一个函数
2、子组件的事件触发时，调用this.props.该函数
3、在该函数中将数据返回给父组件


## 生成唯一的id值的包
`npm i nanoid`(较小)
or
`npm i uuid`(较大)

## 状态与方法
状态在哪里，操作状态的方法就在哪里

## 关于勾选框
defaultChecked只能触发一次
checked可以触发多次


## 总结相关知识点
1、拆分组件、实现静态组件，注意className、style在HTML和react中的不同表达

2、动态初始化列表，考虑将数据放在哪个组件的state中
	某个组件使用，就放在自身的state中
	组件之间使用，就放到共同的父组件state中（官方称之为状态提升）

3、父子组件之间通信
	a.父组件给子组件传递数据，通过props传递
	b.子组件给父组件传递，要求父组件事先给子组件通过props传递一个函数，然后子组件触发函数，父组件得到参数

4、注意defaultChecked和checked区别  类似还有defaultValue和value

5、状态在哪里，修改状态的数据的方法就在哪里

6、细节，大小写问题，this问题

# React网络请求

react本身只关注界面，不包括任何发送ajax请求的代码

前端通过ajax请求与后台进行交互

## axios

轻量级，建议使用
	1、封装XMLHTTPrequest对象的ajax
	2、promise风格
	3、可以用在浏览器端和node服务器端

--------------------

# react脚手架配置代理总结



## 方法一

> 在package.json中追加如下配置

```json
"proxy":"http://localhost:5000"
```

说明：

1. 优点：配置简单，前端请求资源时可以不加任何前缀。
2. 缺点：不能配置多个代理。
3. 工作方式：上述方式配置代理，当请求了3000不存在的资源时，那么该请求会转发给5000 （优先匹配前端资源）



## 方法二

1. 第一步：创建代理配置文件

   ```
   在src下创建配置文件：src/setupProxy.js
   ```

2. 编写setupProxy.js配置具体代理规则：

   ```js
   const proxy = require('http-proxy-middleware')

   module.exports = function(app) {
     app.use(
       proxy('/api1', {  //api1是需要转发的请求(所有带有/api1前缀的请求都会转发给5000)
         target: 'http://localhost:5000', //配置转发目标地址(能返回数据的服务器地址)
         changeOrigin: true, //控制服务器接收到的请求头中host字段的值
         /*
         	changeOrigin设置为true时，服务器收到的请求头中的host为：localhost:5000
         	changeOrigin设置为false时，服务器收到的请求头中的host为：localhost:3000
         	changeOrigin默认值为false，但我们一般将changeOrigin值设为true
         */
         pathRewrite: {'^/api1': ''} //去除请求前缀，保证交给后台服务器的是正常请求地址(必须配置)
       }),
       proxy('/api2', {
         target: 'http://localhost:5001',
         changeOrigin: true,
         pathRewrite: {'^/api2': ''}
       })
     )
   }
   ```

说明：

1. 优点：可以配置多个代理，可以灵活的控制请求是否走代理。
2. 缺点：配置繁琐，前端请求资源时必须加前缀。

--------------------
```
1) GET 请求
axios.get('/user?ID=12345')
    .then(function (response) {
        console.log(response);
    })
    .catch(function (error) {
        console.log(error);`
    });
axios.get('/user', {
    params: {
        ID: 12345
    }
})
    .then(function (response) {
        console.log(response);
    })
    .catch(function (error) {
        console.log(error);
    });

2) POST 请求
axios.post('/user', {
    firstName: 'Fred',
    lastName: 'Flintstone'
})
    .then(function (response) {
        console.log(response);
    })
    .catch(function (error) {
        console.log(error);
    });
```

## fetch

```js
1) GET 请求
fetch(url)
    .then(function(response) {
        return response.json()
    })
    .then(function(data) {
        console.log(data)
    })
    .catch(function(e) {
        console.log(e)
    });

2) POST 请求
fetch(url, {method: "POST", body: JSON.stringify(data), })
    .then(function(data) {
        console.log(data)
    })
    .catch(function(e) {
        console.log(e)
    })
```

# 组件间通信

## 方式一：通过props传递

1.共同的数据放在父组件上，特有的数据放在自己组件内部state

2.通过props可以传递一般数据和函数数据，只能一层一层传递

## 方式二：使用消息订阅subscribe与发布publish机制

1.工具库：PubSubJS

2.npm i pubsub-js

3.
import PubSub from 'pubsub-js' //引入
PubSub.subscribe('delete', function(msg, data){ }); //订阅
PubSub.publish('delete', data) //发布消息

## 方式三：redux

# react-router4

## react-router理解

1.react的一个插件库

2.专门用来实现一个SPA应用

3.基于React的项目基本都会用到此库

## SPA理解

1.单页Web应用（single page web application, SPA）

2.整个应用只有一个完整的页面

3.点击页面中的链接不会刷新页面，本身也不会向服务器发请求

4.当点击链接时，只会做页面的局部更新

5.数据都需要通过ajax请求获取，并在前端异步展现

## 路由的理解

1.什么是路由？
    a.一个路由就是一个映射关系（key:value）
    b.key是路由路径，value可能是function/component

2.路由分类
    a.后台路由：node服务器路由，value是function，用来处理客户端提交的请求并返回一个响应数据
    b.前台路由：浏览器端路由，value是component，当请求的是路由path时，浏览器端没有发送http请求，单界面会更新显示对应的组件

3.后台路由
    a.注册路由：router.get(path,(req, res) => {})
    b.当node接收到一个请求时，根据请求路径找到匹配的路由，调用路由中的函数来处理请求，返回响应数据

4.前台路由
    a.注册路由：`<Route path='/about' component={About}>`
    b.当浏览器的hash变为#about时，当前路由组件就会变为About组件

## react-router使用

`npm i react-router-dom@4`

## react-router相关API

```
1) <BrowserRouter>
2) <HashRouter>
3) <Route>
4) <Redirect>
5) <Link>
6) <NavLink>
7) <Switch>
```

其他
1) props.history 对象
history.push(): 添加一个新的历史记录
history.replace(): 用一个新的历史记录替换当前的记录
history.goBack(): 回退到上一个历史记录
history.goForword(): 前进到下一个历史记录
history.listen(function(location){}): 监视历史记录的变化
2) props.match 对象
3) withRouter 函数

# react-ui

## Ant-Design

### 搭建antd-mobile的基本开发环境

1.npm install antd --save


---------

# redux

## redux是什么?
a predictable state container fo js Apps

1.redux是一个独立专门用于做管理状态的JS库

2.它可以用在react，angular，vue项目中，但基本与react配合使用

3.作用：集中式管理react应用中多个组件共享的状态

## redux工作流程


## redux核心API

### 1.createStore()

作用：创建包含指定reducer的store对象

### 2.store对象

作用：redux库最核心的管理对象
它内部维护着：state与reducer函数

核心方法：getState() dispatch(action) subscribe(listener)

### 3.applyMiddleware()

作用：基于redux的中间件

### 4.combineReducers()

作用：合并多个reducer函数

## redux的三个核心概念

### 1.action

标识要执行行为的对象
包含两个方面的属性
    type：标识属性，值为字符串，唯一，必要属性
    data：数据属性，值类型任意，可选属性

Action Creator(创建Action的工厂函数)

### 2.reducer

根据老的state和action，产生新的state纯函数

注意：返回一个新的状态，不修改原来的状态

### 3.store

将state, action与reducer联系在一起的对象

getState()：得到state

dispatch(action)：分发action，触发reducer调用，产生新的state

subscribe(listener)：注册监听，当产生新的state时，自动调用

## 问题

1.redux与react组件的代码高度耦合
2.编码不够简洁

------


# react-router 6
## 概述
+ 以三个不同的包发布到npm
1. react-router 核心库 提供许多api  hooks
2. react-router-dom 包含react-router的`全部`内容,同时添加一些专门用于DOM的组件, 如<BrowserRouter/>
3. react-router-native 包含react-router的`全部`内容,同时添加一些用于ReactNative的API

+ 和5版本的区别
1. 移除<Switch/>  新增<Router/>
2. 语法变化 component={Comp}, element={<Comp/>}
3. 新增了若干hooks useParams, useNavigate, useMatch等
4. 官方`明确推荐`使用函数式组件


## 流程
1. 安装`yarn add react-router-dom`
2. /src下添加routers.ts
```ts
// key代表路由
// value代表组件路径
{
  "/project": "./pages/ProjectOverview",
  "/project/data": "./pages/DataGovernance",
}
```
3. /src下添加<Router/>组件
```jsx
import React from 'react';
import { Routes, Route, Navigate } from 'react-router-dom';
// import NotFound from './components/NotFound';
import { routes } from './routers'

const Router = () => (
  <Routes>
    {/* 两种写法都可以
      * 写法一  需要Navigate指向的路径必须存在在routes中
      * 写法二  需要element里面的组件引入进来了
      *
    */}
		{/* 重定向 */}
    <Route path="*" element={<Navigate to="/project/error" />} />
    {/* <Route path="*" element={<NotFound />} /> */}

    {/* Object.entries(obj) 返回一个二维数组,数组每个元素是[key, value] */}
    {Object.entries(routes).map(([path, component]) => {
      // 通过懒加载组件
      // 注意 import不能使用变量
      // 必须是一个字符串,否则报错,这是使用模版字符串的原因
      const Comp = React.lazy(() => import(`${component}`));
      return (
        <Route
          key={path}
          path={path}
          element={
            <React.Suspense fallback={<div />}>
              <Comp />
            </React.Suspense>
          }
        />
      );
    })}
  </Routes>
);

export default Router;
```

3. App.tsx里面添加<Router/>, 通过<BrowserRouter/>
```jsx
import { BrowserRouter } from 'react-router-dom';
import Router from '@/Router';

<BrowserRouter>
	<div className={styles.page}>
		<CommonHeader />
		<section>
			<Router />
		</section>
	</div>
</BrowserRouter>
```
## Component

### <BrowserRouter/>
包裹<Routes/>

### <Routes/>
与<Route/>配合使用,必须使用<Routes/>包裹<Route/>

### <Route/>
<Route/>可以嵌套使用,用来表示一级路由,二级路由,三级路由等等
path 开头为 / 的为绝对路径，反之为相对路径。
```jsx
<Route path="/father" element={<Father />}>
  <Route path="child" element={<Child />}></Route>
  <Route path=":name" element={<Another />}></Route>
</Route>
```

caseSensitive属性用来控制是否对url大小写敏感,默认不敏感, true敏感

### <Navigate />
这个组件必须有to属性
如果添加replace属性,为true会清除历史记录,浏览器无法返回


## hooks

### useRoutes
路由表通过 一个数组定义<Routes/>结构
```js
const element = useRoutes([
  {
    path:"/project/first",
    element: <Comp1/>,
  },
  {
    path:"/project/second",
    element: <Comp2/>,
  },
]);
```


### useNavigate
编程式路由导航
使用Link 和Navigate都不如useNavigate
`const navigate = useNavigate();`
使用navigate导航
```ts
/**
 * Describes a location that is the destination of some navigation, either via
 * `history.push` or `history.replace`. May be either a URL or the pieces of a
 * URL path.
 */
export declare type To = string | PartialPath;

/**
 * The interface for the navigate() function returned from useNavigate().
 */
export interface NavigateFunction {
	(to: To, options?: NavigateOptions): void;
	(delta: number): void;
}
export interface NavigateOptions {
	replace?: boolean;
	state?: any;
}
/**
 * Returns an imperative method for changing the location. Used by <Link>s, but
 * may also be used by other elements to change the location.
 *
 * @see https://reactrouter.com/api/useNavigate
 */
export declare function useNavigate(): NavigateFunction;
```

## useParams
从路径中解析出目标key
url的参数格式不同
```js
// 在路由中的路径定义是, 此处用:key指代一个变量
<Route
	path={`/share/:key`}
	exact
	render={() =>
			<RouteConfig.SharePage />
	}
/>

////////////////////////////////////////////////
// 组件中
import { useParams } from 'react-router-dom';
// 例如pathname: /share/wkpqlwIhw1中, wkpqlwIhw1即为分享页的key
const { key: shareKey } = useParams<{ key: string }>();
```

## useMatch

## useSearchParams
`const [searchParams, setSearchSearchParams] = useSearchParams();`
必须通过get方法获取
`const id = searchParams.get('asset_id');`

setSearchSearchParams用于更新url中的search参数
传参要符合searchParams格式


## useResolvedPath
用于解析路径的(没啥用)
类似query-string里面的parse
```ts
interface Path {
	pathname: string;
	search: string;
	hash: string;
}

/**
 * Resolves the pathname of the given `to` value against the current location.
 *
 * @see https://reactrouter.com/api/useResolvedPath
 */
export declare function useResolvedPath(to: To): Path;
```


------

# react-redux

## 概念
+ store是一个普通的 object
  永远不要直接修改store内容

+ 唯一导致store数据更新的是action

+ 然后dispatch发送这个action给store

+ 当store接收到这个action后,会运行root reducer函数,reducer会更新状态

+ store告知所有的subscribers(订阅者)数据已经更新,从而更新渲染


## 使用
1. App从react-redux引入Provider包裹在最外面,可以使得被包裹的组件共享状态,Provider需要接受store的prop 注[A]
```tsx
import { BrowserRouter } from 'react-router-dom'
import { Provider } from 'react-redux';
import Router from './Router'
import store from './stores'

function App() {
  return (
    <Provider store={store}>
      <BrowserRouter>
        <Router />
      </BrowserRouter>
    </Provider>
  )
}

export default App
```

2. 创建stores文件夹,包含单独的slice和index
单独的slice负责具体的功能
```ts
// 注意,从@reduxjs/toolkit引入createSlice, 用于创建slice
import { createSlice } from '@reduxjs/toolkit';

// 初始值
const initialState = {
  value: 0
}

// 具体的slice
// 固定三个属性如下
// name 据我实际测试,叫什么都无所谓(暂时),但为了避免出错,应该与store本身导出的store里面的属性名一致
// initialState 初始值
// reducers 为一个对象, 对象中每个元素都是函数 形如fun(state, action: PayloadAction<T>) {}
const countSlice = createSlice({
  name: 'count',
  initialState,
  reducers: {
    update(state, action) {
      switch (action.payload) {
        case 'add':
          state.value++
          break;
        case 'minus':
          state.value--
          break
        default:
          break;
      }
    },
  }
})

// 从创建的slice中提取actions和reducer
// actions负责导出reducer函数
// reducer默认导出到store里面待使用
const { actions, reducer } = countSlice;

export const { update } = actions
export default reducer
```

主index (store)
这个创建出来的store需要放到第一步里面Provider组件的store prop属性
```ts
// 引入刚刚创建的具体的slice
import countReducer from './countSlice'
// 引入创建store的configureStore
import { configureStore } from '@reduxjs/toolkit';

// 导出默认的store
export default configureStore({
  reducer: {
    //注意这里的key,名字必须和useSelector里面state.xxx名字一致
    countTest: countReducer
  }
})

// 无所谓 直接写any也行
export type RootState = ReturnType<any>;
```


3. 具体业务组件

**更改状态的组件**
```tsx
import React, { FC, memo } from 'react';
import styles from './style.module.scss';
import { useNavigate } from 'react-router-dom'
// useDispatch用来创建dispatch 发送action
import { useDispatch } from 'react-redux'

// 从具体的slice中获取具体的reducer函数,用来作为dispatch的参数
// reducer(args) 整体作为一个action
import { update } from '../../../../stores/countSlice';

const FirstChild: FC = memo(() => {
  const navigate = useNavigate()
  const dispacth = useDispatch()

  const goHome = () => navigate('/home')

  const add = () => {
    dispacth(update('add'))
    goHome()
  }

  const minus = () => {
    dispacth(update('minus'))
    goHome()
  }

  return (
    <div className={styles.container}>
      <p className={styles.btn} onClick={add}>+</p>
      <p className={styles.btn} onClick={minus}>-</p>
    </div>
  )
});

export default FirstChild;
```


**使用状态的组件**
```tsx
import React, { FC, memo } from 'react';
import Second from '../Second';
import styles from './style.module.scss';
import { useNavigate } from 'react-router-dom'
// useSelector用来获取状态
import { useSelector } from 'react-redux'
import { RootState } from '@/stores';

const Home: FC = memo(() => {
  const navigate = useNavigate();
  const goFirst = () => navigate('/first')

  // 写法如下
  // state的类型不写的话会报ts error
  // state.xxxx这个xxxx必须和store里面的reducer的key值一致
  const { value } = useSelector((state: RootState) => state.countTest)

  return (
    <div className={styles.container}>
      <div className={styles.wrapper}>
        <Second count={value} />
      </div>
    </div>
  )
});

export default Home;
```


# hooks
## useEffect
在 React 中，useEffect 钩子用于在组件渲染后执行副作用操作。useEffect 接受两个参数：一个是执行副作用的函数，另一个是可选的依赖项数组。

当你在 useEffect 的第一个参数函数中返回一个函数时，这个返回的函数被称为清理函数

清理函数会在以下情况下执行：
1. 组件卸载时：当组件从 UI 中移除时，清理函数会被调用。这用于清理一些订阅、计时器或其他副作用，以防止内存泄漏。
2. 依赖项变化时：如果 useEffect 提供了一个依赖项数组，当依赖项中的某一项发生变化时，会先执行清理函数，然后再执行新的副作用函数。

注意先执行清理函数 再执行副作用函数
```jsx
// 如果s变化  执行顺序是
// rerender
// 清理函数执行
// 副作用函数执行
const TestSvg: FC<TestSvgProps> = memo(() => {
    const [s, setS] = useState(0)
    console.log('rerender');
    useEffect(() => {
        console.log('副作用函数执行');

        return () => {
            console.log('清理函数执行');
        }
    }, [s])

    return (
        <div>
            <Button onClick={() => setS(s => s + 1)}>click</Button>
        </div>
    )
});

```
3. 重新渲染触发时：如果没有提供依赖项数组，useEffect 中的副作用函数将在每次组件更新时执行，因此清理函数也会在每次组件更新前执行。


## useRef
useRef每次都执行
但是只有mount阶段会创建一个含有current属性的对象
update阶段只会返回这个缓存的对象


# API
## react-dom

### createPortal
createPortal 用于在指定位置插入一个jsx元素

**例1**
```jsx
import { createPortal } from 'react-dom';

// document.body下会多一个p元素
<div>
  <p>这个子节点被放置在父节点 div 中。</p>
  {createPortal(
    <p>这个子节点被放置在 document body 中。</p>,
    document.body
  )}
</div>
```
**例2**
将dom元素渲染到指定的ref元素下
```jsx
 {toolBoxListRef.current &&
	createPortal(
		<>
			<div > Hello </div>
			<div > World </div>
		</>,
		toolBoxListRef.current,
)}
```

**例3**
利用protal创建一个浮动在页面其余部分之上的Modal对话框
```jsx
import { useState } from 'react';
import { createPortal } from 'react-dom';
import ModalContent from './ModalContent.js';

export default function PortalExample() {
  const [showModal, setShowModal] = useState(false);
  return (
    <>
      <button onClick={() => setShowModal(true)}>
        使用 portal 展示模态（motal）
      </button>
      {showModal && createPortal(
        <ModalContent onClose={() => setShowModal(false)} />,
        document.body
      )}
    </>
  );
}
```