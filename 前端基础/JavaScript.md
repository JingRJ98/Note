# 1. JS语法
1、严格区分大小写
2、每一条语句以分好结尾，如果不写分号浏览器会自动添加分号，但是会消耗性能，有时会写错
3、JS中会忽略多个空格和换行，所以可以利用空格和换行对代码进行格式化
# 2.字面量和变量
字面量=常量：不可变的量 1 2 3 4 ……字面量可以直接使用，但是不建议使用
变量可以用来保存字面量 变量更加方便使用，开发中都是通过变量保存数据
```js
// 在js中使用var声明变量
var a;
// 为变量赋值
a=123;
a=456;
// 声明和赋值同时
var b = 7777;
```
# 3.标识符
js中可以自主命名的都是标识符，例如变量名，函数名，属性名
命名规则：
1、标识符中可以含有字母/数字/下划线/$
2、标识符不能以数字开头
3、标识符不能是ES中的关键字或保留字
4、标识符一般采用驼峰命名法，首字母小写，每个单词的字母大写，其余字母小写 xxxYyyZzz
js底层代码保存时实际上采用unicode编码，理论上所有的UTF-8含有的内容都可以作为标识符
# 4.数据类型
数据类型指的就是字面量的类型，在js中有六种数据类型
object属于引用数据类型 其余五个是基本数据类型
## 4.1. String

String字符串
    在JS中字符串需要使用引号
    使用双引号或单引号都可以，但不能混着用
    引号不能嵌套，双引号里不能放双引号，单引号里不能放单引号

在字符串中我们可以使用 \ 作为转义字符，当表示一些特殊符号时可以使用\进行转义
同一个标识符只需要声明一次，下面修改会自动覆盖

```js
    \" 表示 "
    \' 表示 '
    \n 表示换行
    \t 制表符
    \\ 表示\
```
通过.length可以查看字符串的长度
`console.log(str.length);`

字符串+其他类型，结果都是字符串
## 4.2. Number

在JS中所有的数值都是Number类型，包括整数和浮点数

可以使用typeof检查变量类型
```js
var a = "123";
var b = 123;
console.log(a);
console.log(b);
//typeof检查数据类型
console.log(typeof a);
console.log(typeof b);
```

JS中可以表示的数字的最大值
Number.MAX_VALUE
1.7976931348623157e+308

JS中可以表示的大于0的最小值
Number.MIN_VALUE
5e-324

如果使用Number表示的数字超过了最大值，则会返回
    Infinity 正无穷
    -Infinity 负无穷
    使用typeof检查Infinity也会返回number

NaN是一个特殊的数字，表示Not A Number
    使用typeof检查一个NaN也会返回number

注意：如果使用JS进行浮点运算，可能得到一个不精确的结果 如：0.1+0.2
所以千万不要使用JS进行对精确度要求比较高的运算

## 4.3. Boolean

Boolean布尔值
布尔值只有两个，主要用来做逻辑判断
true：表示真
false：表示假
使用typeof检查一个布尔值时，会返回boolean

## 4.4. Null

Null（空值）类型的值只有一个，就是null
null这个值专门用来表示一个为空的对象

注意:使用typeof检查一个null值时，会返回object

## 4.5. Undefined

Undefined（未定义）类型的值只有一个，就undefind
当声明一个变量，但是并不给变量赋值时，它的值就是undefined
使用typeof检测undefined，返回undefined

## 4.6. object
# 5.强制类型转换
主要指将其他数据类型转换成string number boolean
## 5.1 转换成string
方式一
调用被转换数据类型的tostring()方法,不改变原变量
```js
var a = 123;
var b = a.toString();
console.log(typeof a);
console.log(typeof b);
console.log(b);
```
注意：null和undefined这两个值没有toString()方法，如果调用会报错
方式二
调用String()函数，并将被转换的数据作为参数传递给函数
```js
var a = null;
a = String(a);
console.log(a);
console.log(typeof a);
```
对于Number和Boolean实际上就是调用toString()方法 但是对于null和undefined，不会调用toString()方法 它会将null直接转换为"null" 将number直接转换为"number"

方式三
拼接字符串方法（最常用）
隐式的类型转换，浏览器自动完成，实际上也调用String()函数
```
c = c + '';
```
## 5.2 转换成number
字符串转数值
1、纯数字字符串 直接转换为对应数字
2、如果字符串中有非数字，转换成NaN
3、空字符串，转换成数字0
4、布尔值转数值 true转成1 false转成0
5、null转数值，结果为0
6、undefined转数值 结果为NaN

方式一
使用Number()函数
```js
var a = "1234";
a = Number(a);
console.log(a);
console.log(typeof a);
```

方式二（重要）
parseInt函数转换成整数
parseFloat函数转换成浮点数
```js
var a = "123.111px123";
a = parseFloat(a);
console.log(a);
console.log(typeof a);
```
注意 首个字符必须是数字，否则输出NaN
对非string使用parseInt或者parseFloat 会先转换成string再操作

方式三
隐式转换 算术运算中，自动转换成数字型
```
'12' - 0
```

##### 其他进制
```js
// 十六进制 0x
// 八进制 0
// 二进制 0b(但时不时所有的浏览器都支持)
var a = 0xC;
console.log(a);
var b = 0111;
console.log(b);
var c = 0b1111;
console.log(c);
```
 使用parse函数可以指定第二个参数表示进制parseInt(b,10);
 ## 5.3 转换成Boolean
 方式一

 使用Boolean()函数
 除了null和NaN，其余都输出true
 字符串除了空，都输出true
 null和undefined都转换成false
 对象也会转成true

 一言以蔽之，代表空，否定的值都会被转换成false
 ```js
        var a = 123;
        a = Boolean(a);
        console.log(a);
        console.log(typeof a);
 ```
 方式二

 隐式类型转换
 为任意数据类型做两次非运算，即可将其转换成布尔值（Boolean）
 # 6.运算符
 运算符也叫操作符，通过运算符可以对一个或多个值进行运算，并获取运算结果
 比如typeof就是运算符，可以获得一个值的类型
 返回值是字符串
 ## 6.1 算数运算符 + - * / %
 当对非数字类型的值运算时，先转换成数字再计算
 任何数和NaN运算都是NaN
 两个字符串运算会进行拼串操作
 任何值和字符串运算，会先转换成字符串再拼接
 ```js
        var a = 123;
        var result = typeof a;
        console.log(result);
        console.log(typeof result);
        // typeof的返回值是个字符串
        var b = 123;
        b = undefined + "456";
        console.log(b);
        console.log(typeof b);//undefined456  string
 ```
任何值做- * /时，都会转换成number计算
## 6.2 一元运算符
一元运算符，只需要一个操作数
`+` `-` 正号负号，对于非number值，可以先转换成number 再计算
一元运算符优先于算数运算符
### 自增自减
自增可以使变量在自身的基础上加1，原变量的值立即增加1
自增分为两种后++/前++ 无论是a++还是++a，都会立即使原变量的值立即自增1
不同的是，a++和++a的值不同
a++的值等于原变量的值
先赋值后运算

++a的值等于自增后的值
先运算后赋值

自减同理

例1：
```
        var d = 20;
        var a = d++ + ++d +d;
        console.log("a = "+a);
        //a = 20 + 22 +22 = 64
        var b = 20;
        b = b++;
        console.log("b = "+b);
        //b=10
```
例2：
```js
        var n1 = 10,n2 = 20;
        var n = n1++;
        console.log("n = "+n);//10
        console.log("n1 = "+n1);//11
        n = ++n1;
        console.log("n = "+n);//12
        console.log("n1 = "+n1);//12
        n = n2--;
        console.log("n = "+n);//20
        console.log("n2 = "+n2);//19
        n=--n2;
        console.log("n = "+n);//18
        console.log("n2 = "+n2);//18
```
## 6.3逻辑运算符
三种逻辑运算符 对于非布尔值进行与或运算时，先转换成布尔值再运算
! 非
!可以用来对一个值进行非运算 所谓非运算就是值对一个布尔值进行取反操作，true变false，false变true 如果对一个值进行两次取反，它不会变化 如果对非布尔值进行元素，则会将其转换为布尔值，然后再取反 所以我们可以利用该特点，来将一个其他的数据类型转换为布尔值 可以为一个任意数据类型取两次反，来将其转换为布尔值，原理和Boolean()函数一样

&& 与
两个值都为true时才输出true，若果第一个值是false，则不会看第二个值，两个数中有false，返回靠前的false，第一个值时true，返回第二个值

|| 或
两个值都为false时才输出false，若果第一个值是true，则不会看第二个值 ，两个值都是true，返回靠前的true，第一个值是false，返回第二个值

## 6.4 赋值运算符
将符号右侧的值赋值给左侧的变量
```js
a += 5;
a = a + 5;
```
两者等价
## 6.5关系运算符
通过关系运算符可以比较两个值之间的大小关系，如果关系成立它会返回true，如果关系不成立则返回false
非数值比较的情况，会将其转换成数字再比较
任何值和NaN做任何比较都是false
两个字符串比较，不会将其转换成数字比较，而会分别比较字符串中Unicode编码，只比第一位（和按照文件名排序有关）两位一样则比较下一位
如果比较的是字符串型的数字可能会出现不可预期的结果注意一定要转型


相等运算符 ==
用来比较两个值是否相等，相等返回true，不等返回false
如果值的类型不同，则会自动进行类型转换，将其转换为相同的类型然后再比较
NaN不和任何值相等
判断b是否是NaN
isNaN()判断是否是NaN，若是，返回true

!= 不相等运算符

=== 判断两个值是否全等，不会对数值进行类型转换，如果类型不一样，直接返回false
!== 判断两个数值是否不全等，不会自动类型转换，两个值类型不同，直接返回true

## 6.6Unicode编码
```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <script>
        // 使用转义字符和编码显示Unicode
        // \u+四位十六进制编码
        console.log("\u2620");
    </script>
</head>
<body>
    <!--
        在网页中使用Unicode编码
        &#+【十进制】编码
     -->
    <h1 style="font-size: 100px;">&#9760;</h1>
</body>
</html>
```
## 6.7 条件运算符
条件表达式?语句1:语句2;
条件运算符在执行时，首先对条件表达式进行求职，如果该值为true 执行语句1，并返回执行结果，如果该值为false，执行语句2，并返回结果
例：
```js
        // 获取a b c中最大值
        var a=20,b=60,c=40;
        var d = a>b?a:b;
        var max = c>d?c:d;
        console.log("max = "+max);
```
如果条件表达式的结果是非布尔值，会将其转换成布尔值，再运算
## 6.8 运算符的优先级
`,`运算符，可以同时指定多个变量并赋值
和数学中一样，运算符存在优先级
# 7.语句
语句是按照自上向下的顺序一条一条执行的，可以用`{}`为语句进行分组
同一个`{}`中的语句称为一组语句，要么都执行，要么都不执行，他们称为代码块
代码块的结尾不需要分号，js中的代码块只具有分组的作用，没有其他功能，代码块中的内容外部完全可见
## 7.1流程控制语句
流程控制语句使程序可以根据一定的条件选择执行
1、条件判断语句
2、条件分支语句
3、循环语句

1、条件判断可以在执行代码之前进行判断，条件成立才会执行语句，条件不成立就不执行
if语句后的`{}`不是必须的，但是尽量写
语法1
```js
        var a = 3;
        if (a > 5){
            alert("hhhhh");
            alert("wwwwww");
        }
```
语法2
```js
        var a = 3;
        if (a > 5){
            alert("hhhhh");
            alert("wwwwww");
        }
        else{
            alert("cuocuocu");
        }
```
语法3
```js
        var a = 10;
        if (a < 5){
            alert("小于5");
        }
        else if(a < 8){
            alert("小于8");
        }
        else if(a < 15){
            alert("小于15");
        }
        else{
            alert("不对不对不对")
        }
```
只会有一个代码块执行，一旦有一个代码块执行了，整个语句结束了

练习1
```html
<script type="text/javascript">
    /*
        * 	从键盘输入小明的期末成绩:
        *	当成绩为100时，'奖励一辆BMW'
        *	当成绩为[80-99]时，'奖励一台iphone15s'
        *	当成绩为[60-80]时，'奖励一本参考书'
        *	其他时，什么奖励也没有
        */
    var grade = prompt("请输入小明的期末成绩：");
    if(grade>100 || grade<0 || isNaN(grade)){
        alert("错误！");
    }
    else if(grade==100){
        alert("给你一辆BMA");
    }
    else if(grade<100 && grade>=80){
        alert("给你一个iPhone13");
    }
    else if(grade<80 && grade>=60){
        alert("给你一本书");
    }
    else{
        alert("给你一个大嘴巴子！！！");
    }
</script>
```
练习2
```html
	<script type="text/javascript">
		/*
		 * 	大家都知道，男大当婚，女大当嫁。那么女方家长要嫁女儿，当然要提出一定的条件：
		 *	高：180cm以上; 富:1000万以上; 帅:500以上;
		 *	如果这三个条件同时满足，则:'我一定要嫁给他'
		 *	如果三个条件有为真的情况，则:'嫁吧，比上不足，比下有余。'
		 *	如果三个条件都不满足，则:'不嫁！'
		 */
		var a1 = prompt("请输入你的身高（cm）：");
		var a2 = prompt("请输入你的财富（万）：");
		var a3 = prompt("请输入你的颜值：");
		if (isNaN(a1) || isNaN(a2) || isNaN(a3)) {
			alert("请输入数字");
		}
		else {
			if(a1 >= 180 && a2 >= 1000 && a3 >= 500){
			alert("非你不可！！！");
			}
			else if(a1 < 180 && a2 < 1000 && a3 < 500) {
				alert("不嫁");
			}
			else {
				alert("凑活吧");
			}
		}
	</script>
```
练习3
```html
<script type="text/javascript">
    /*
        * 	编写程序，由键盘输入三个整数分别存入变量num1、num2、num3，
        * 	对他们进行排序，并且从小到大输出。
        */
    var num1 = +prompt("请输入第一个数：");
    var num2 = +prompt("请输入第二个数：");
    var num3 = +prompt("请输入第三个数：");
    var max_num,middle_num,min_num;
    if(num1>=num2){
        if(num1>=num3){
            max_num = num1;
            if(num2>=num3){
                middle_num = num2;
                min_num = num3;
            }
            else{
                middle_num = num3;
                min_num = num2;
            }
        }
        else{
            middle_num = num1;
            min_num = num2;
            max_num = num3;
        }
    }
    else{
        if(num2>=num3){
            max_num = num2;
            if(num1>=num3){
            middle_num = num1;
            min_num = num3;
            max_num = num2;
            }
            else{
            middle_num = num3;
            min_num = num1;
            max_num = num2;
            }
        }
        else{
            middle_num = num2;
            max_num = num3;
            min_num = num1;
        }
    }
    alert(min_num +"，"+middle_num+"，"+max_num);
</script>
```
## 7.2 条件分支语句
条件分支语句也叫switch语句
语法：`switch(条件表达式){语句}`
执行时会将case后的值和switch后面的条件表达式的值进行全等比较，如果结果为true，则从当前case处开始执行，如果结果为false，则继续比较，使用break退出switch语句
所有结果都是false，则转到default内容
switch和if语句有重合部分，根据实际使用
```js
var num = 2;
switch(num){
    case 1:console.log("一");
    break;
    case 2:console.log("二");
    break;
    case 3:console.log("三");
    break;
default:
break;
}
```
##### 练习1
```html
<script type="text/javascript">
    /*
        * 对于成绩大于60分的，输出'合格'。低于60分的，输出'不合格'
        */
    var grade = +prompt("请输入成绩：");
    switch(parseInt(grade/10)){
        case 6:
        case 7:
        case 8:
        case 9:
        case 10:
        console.log("成绩合格");
        break;
        default:
            console.log("成绩不合格！");
            break;
    }
</script>
```
## 7.3 while循环
反复执行一段代码多次
创建一个循环1、初始化变量 2、在循环中设置条件表达式 3、定义一个更新表达式，每次更新初始化变量
```js
    var i = 1;
    // while循环
    while(i<1){
        document.write(i++ +"<br/>");
    }
    // do循环
    do{
        document.write(i++ +"<br/>");
    }while(i<1)
```
do循环先执行再判断保证循环体至少执行一次，while先判断再执行
while练习：假设投资的年利率为5%，求1000块增长到5000块需要多少年？
```js
        var money=1000,i = 0;
        while(money<5000){
            money *= 1.05;
            i++;
        }
        console.log("money=:"+money);
        console.log("年=:"+i);
```
## 7.4 for循环
 for(初始化表达式; 条件表达式; 更新表达式) {
    语句...;
}
1、执行初始化表达式
2、执行条件表达式，结果为true执行语句，false终止循环
3、执行更新表达式
4、重复第二步

for循环的三个部分都可以省略，也可写在外部，全部省略为死循环
 #### 练习
 练习1
 ```js
        // 求1到100所有奇数的和
        var sum=0; //定义sum初始为0
        for(i=0;i<100;++i){
            i++;
            sum += i;
        }
        document.write(sum+"<br/>");
 ```
 练习2
 ```js
        // 打印1-100之间所有7的倍数的个数及总和
        var sum = 0;
        for(i=1;i<=40;i++){
            if((i%7) == 0){
                document.write(i+"<br/>");
                sum += i;
            }
        }
        document.write("所有7的倍数总和=："+sum);
 ```
 练习3
 ```js
        // 水仙花数
        // 水仙花数是一个三位数。它每个位上的数字的三次幂之和等于它本身
        // 例如153=1^3+5^3+3^3
        var s=0;
        for(a=1;a<10;a++){
            for(b=0;b<10;b++){
                for(c=0;c<10;c++){
                    if((a*a*a+b*b*b+c*c*c) == a*100+b*10+c){
                        s=a*100+b*10+c;
                        document.write(s+"<br/>")
                    }
                }
            }
        }
 ```
 练习4
 第一版
 ```js
        // 判断质数
        var  s = +prompt("请输入一个大于1的整数：");
        var count =0;
        for(i=1;i<parseInt(s/2);i++){
            if(s % i == 0){
                count++;
            }
            if(count > 1){
                console.log("不是质数");
            }

        }
        // 可以判断不是质数的情况，若是质数，则无输出
 ```
 练习4
 第二版 在第一版的基础上添加了一个标志
 ```js
        // 判断质数
        var  s = +prompt("请输入一个大于1的整数：");
        var count =0;

        var flag = true;//设置一个标志，代表默认输入数是质数
        for(i=2;i<s;i++){
            if(s % i == 0){
                count++;
            }
            if(count == 1){
                console.log("不是质数");
                flag = false;
            }

        }
        if(flag){
            console.log("是质数");
        }
 ```
 练习5
 ```js
        /*
            在页面中输出如下图形
            *
            **
            ***
            ****
            *****
            ******
            ……
        */
       var s=8;
       for(i=1;i<=s;i++){
            for(j=1;j<=i;j++){
                document.write("*");
            }
            document.write("<br/>");//换行
       }
 ```
 练习6
 ```js
        /*
            在页面中输出如下图形
            *****
            ****
            ***
            **
            *
            ……
        */
       var s=8;
       for(i=1;i<=s;i++){
            for(j=s;j>=i;j--){
                document.write("*");
            }
            document.write("<br/>");//换行
       }
 ```
 练习7
 ```js
         // 打印九九乘法表
        for(i=1;i<=9;i++){
            for(j=1;j<=i;j++){
                document.write(j+"*"+i+"="+(i*j)+"&nbsp;&nbsp;&nbsp;&nbsp;");
            }
            document.write("<br/>")
        }
 ```
 练习8
 ```js
        // 输出100以内的质数
        for(i=2;i<100;i++){
            var flag = true;
            for(j=2;j<i;j++){
                if(i%j == 0){
                flag = false;
                }
            }
            if(flag){
                document.write(i+"<br/>");
            }
        }
 ```
 ## 7.5 break和continue
 break关键字可以用来退出switch语句或循环语句 不能用于if语句，会立即终止离他最近的循环
 可以为循环语句创建标签标记循环，使用break时，可以在break后面跟着标签，这样可以终止指定标签的循环，而不是最近的循环

 continue关键字可以用来跳过当次循环，只会对最近的循环起作用，可以设置标签对指定循环进行作用

 # 8.对象
 js中数据类型：`string字符串` `number数值` `boolean布尔值` `null空值` `undefined未定义` 除了这五种基本数据类型，别的都是对象
 基本数据类型都是单一的数值，之间没有关系，不能成为一个整体
 对象属于一种复合的数据类型
 ## 8.1对象分类
 1、内建对象：由ES标准中定义的对象，在任何ES的实现中都可以使用，比如math string number boolean function object
 （详解见16.内置对象）

 2、宿主对象由js的运行环境提供的对象，目前来讲主要是浏览器提供的对象 比如BOM DOM

 3、自定义对象，由开发人员自己创建的对象
 ## 8.2创建对象
 #### 1.用字面量创建对象
 ```js
           // 字面量创建对象
           var obj = {
           name: '孙悟空',
           age: 18,
           gender: '男',
           sayHi: function(){
               console.log('hi');
             }
           };
 ```
 多个属性或者方法用`,`隔开
 调用对象的属性
1、对象["属性名"] = 属性值;
2、对象.属性
使用[]这种形式操作属性，更加灵活
在[]中可以直接传递一个变量，这样变量值是多少就会读取那个属性
js对象的属性值可以使任意类型，甚至也可以是另一个对象
 #### 2.用new Object创建对象
 ```js
        // new创建
        // 用等号赋值的方法添加属性和对象的方法
        var obj = new Object();
        obj.name = '孙悟空';
        obj.age = 18;
        obj.gender = '男';
        obj.sayHi = function(){
            console.log('hi~~~');
        };
 ```
 属性结束需要用分号结束

#### 3.用构造函数创建对象
用构造函数创建对象是因为前两种创建对象的方式一次只能创建一个对象
这个函数封装的是对象，把对象的相同的属性和方法抽象出来封装
```js
        // 构造函数
        function People(name,age,gender){ //构造函数函数名首字母必须大写
            this.name = name;
            this.age = age;
            this.gender = gender;
            // 构造函数不需要return
        }
        var ldh = new People('刘德华',18,'男');
        var zxy = new People('张学友',20,'男');
        console.log(typeof ldh);
        console.log(zxy.age);
```
调用函数返回的类型时object  即ldh是一个对象
调用构造函数必须使用new
构造函数不需要return
我们只要new了调用函数就是创建了一个对象

事例
```js
        // 构造函数首字母必须大写
        function People(name,type,blood){
            this.name = name;
            this.type = type;
            this.blood = blood;
            this.attack = function(attack_){
                console.log(attack_);
            };
        }
        var lp = new People('廉颇','力量',500);
        var hy = new People('后羿','射手',100);
        console.log(lp.type);
        console.log(hy['name']);
        hy.attack('远程');
```
--------------------
构造函数是创建的一类 类class
对象特指一个具体的事物，用构造函数创建对象的过程也叫对象的实例化

# 8.3 引用数据类型保存地址
JS中的变量都是保存在栈内存中，基本数据类型的值直接在栈内存中存储，值与值之间独立存在，修改一个变量不会影响另一个变量
对象是保存在堆内存中，每创建一个新的对象，就会在堆内存中开辟一个新的空间，变量保存的是对象的内存地址，可以进行引用

当比较两个基本数据类型的值时，就是比较值
当比较两个引用数据类型时，需要先看是不是同一个地址
# 8.4对象字面量
`var obj = {};`使用对象字面量创建对象
使用对象字面量可以在创建对象时直接指定对象中的属性
```js
        var obj = {
            name:"孙悟空",
            age:28,
            gneder = "男"
        }
```
属性名和属性值是一组一组的对应结构，名和值之间用`:`连接，属性和属性之间用 `,`隔开，最后一个属性后不需要`,`

# 9.函数
函数也是一个对象，函数中可以封装一些功能（代码），在需要时可以调用
创建一个函数对象
```js
    var fun = new Function();
```
使用typrof检查fun时会返回function
可以将要封装的代码以字符串的形式传递给构造函数，封装到函数中的代码不会立即执行，函数中的代码会在函数调用的时候执行
调用函数：
```js
        var fun = new Function("console.log('hello world!');");
        // console.log(typeof fun);
        // console.log('hello world!');
        fun(); //调用
        fun();
        fun();
```
在实际开发中很少采用构建函数来创建

更多使用函数声明创建函数：
```js
        function fun2(){
            console.log("这还是我的函数！！！")
            alert("hahhahahahah");
            document.write("┭┮﹏┭┮");
        }
        fun2();
```
## 9.1函数的参数
可以在函数的（）指定一个或多个形参，多个形参使用`,`隔开，声明形参就相当于在函数内部声明了对象，但是并不赋值
在调用函数时可以在（）中指定实参，实参会赋值给函数中对应的实参
调用函数时解析器不会检查实参的类型，可能会收到非法参量
也不会检查实参的数量，多余的实参不会赋值（忽略），如果实参数量少于形参数量，未注明的形参是undefined
实参可以是任意数据类型
## 9.2函数返回值
可以使用return设置函数的返回值，return会作为函数的执行结果返回，可以定义一个变量接受该结果
```js
        function sum(a,b,c){
            // console.log(a+b);
            var d = a+b+c;
            return d;
        }
        var result = sum(5,1,6);
        console.log("resukt="+result);
```
return 后可以跟任何值或者不写，相当于返回undefined，可以是对象、函数……
使用return可以结束整个函数
return只能返回后面接近的结果，若要返回多个结果，应当在return后面用`[` `]` 将结果包括进去

```js
        function fun(){
            alert("函数要执行了~~~");
            for(i=0;i<10;i++){
                if(i == 5){
                    // break;退出当前循环
                    // continue; 退出当次循环
                    return;
                    // return直接退出整个函数
                }
                console.log(i);

            }
            alert("完了~~~");
        }
        fun();
```
练习
```js
        // 函数比较两个数中的最大值
        function fun(num1,num2){
            var max;
            if(num1 > num2){
                max = num1;
            }
            else{
                max = num2;
            }
            return max;
        }
        console.log('最大的数是：'+fun(4,Infinity));

	 // 改进版(返回值不同)
        function fun(num1, num2) {
            var max;
            if (num1 > num2) {
                return num1;
            }
            else {
                return num2;
            }
        }
        console.log('最大的数是：'+fun(4,Infinity));


	// 改进版plus
        function fun(num1, num2) {
            return num1 > num2 ? num1 : num2;
        }
        console.log('最大的数是：'+fun(4,Infinity));
```
练习二
```js
        // 求数组中最大的元素
        // 我的思路：冒泡，取最后一个
        function getMaxArr(arr){
            for(var i = 0;i<arr.length;i++){
                for(var j = 0;j<arr.length-i;j++){
                    if(arr[j]>arr[j+1]){
                    var a = arr[j];
                    arr[j] = arr[j+1];
                    arr[j+1] = a;
                    }

                }
            }
            return arr[arr.length-1]
        }
        var result = getMaxArr([1,3,7,12,6,6,2,4]);
        console.log(result);

        // 两年后更新: 只要遍历一遍 维护最大的
```
break结束当前循环体，如for,while
continue 跳出本次过程，继续下一次循环
return 结束整个函数中间的代码
## 9.4立即执行函数
(声明函数)();立即执行函数
```js
        // 立即执行
        // 创建匿名函数
        (function(){
            alert("我是一个匿名函数~~~");
        })();


        (function(a,b){
            alert("我是一个匿名函数~~~");
	    console.log(a+b);
        })(1,2);
```

## 9.5
对象的属性值也可以是函数。
如果一个函数作为对象的属性值，那么这个函数叫做对象的方法（method），调用函数叫做调用对象的方法
只有名称的区别，本质一样
```js
        // 创建一个对象
        var obj1 = new Object();
        // 向对象中添加属性
        obj1.name ="孙悟空";
        // 设置对象的属性值是函数
        // 函数功能为打印name属性值
        obj1.fun = function(){
            console.log(obj1.name);
        }
        // 调用这个函数
        // 例 document.write();
```
## 9.6遍历对象中的属性(也能遍历方法)
for (变量 in 对象){} 可以遍历对象
```js
        // 创建一个对象，有多个属性
        var obj = {
            name:"jrj",
            age:18,
            gender:"男",
            address:"花果山"
        };
        // 枚举对象中的属性
        for(var n in obj){
            console.log(n); //n是变量 输出属性名
            console.log(obj[n]); // 输出属性值 (注意[]里面是变量，不需要'')
        }
```

## 9.7构造函数plus（原版见8.2/3）
构造函数和普通函数的区别就是调用方式的不同，普通函数是直接调用，构造函数需要new关键字调用

new关键字在构造函数过程中的作用：
1、在内存中创建一个空的对象
2、将新建的对象设置为函数中的this，在构造函数中可以用this来引用新的对象
3、执行函数中的代码给空对象添加属性或方法
4、将新建的对象作为返回值返回（所以构造函数里面不需要return）

使用同一个构造函数创建的对象，称为一类对象，也将一个构造函数称为类，通过一个构造函数创建的对象，叫做实例

使用instanceof可以检查一个对象是否是一个类的实例，如果是返回true，否则返回false，所有对象都是object的实例。
```js
        function Person(name,age,gender){
            this.name = name;
            this.age = age;
            this.gender = gender;
            this.sayName = function(){
                alert(this.name);
            };
        }
        var per = new Person("孙悟空",18,"男");
        var per2 = new Person("玉兔精",20,"女");
        console.log(per2);
```
## 9.8构造函数的缺陷
方法被反复创建新的，浪费资源
可以将sayName方法在全局作用域中定义，实现共享

改造前：
```js
        // 方法是在构造函数的内部创建的
        // 每执行一次都会创建一个新的sayName 所有实例的sayName都是唯一的
        // 构造函数执行一次就会创建一个新的方法，但是方法都是一样的，这是没有必要，可以使所有的对象共享同一个方法

        function Person(name,age,gender){
            this.name = name;
            this.age = age;
            this.gender = gender;
            // 向对象中添加一个方法
            this.sayName = function(){
                alert("大家好~~~我是"+this.name);
            };
        }
        //创建person的实例
        var per = new Person("孙悟空",18,"男");
        var per2 = new Person("白骨精",18,"女");
        var per3 = new Person("沙和尚",48,"男");
        per3.sayName();
```
改造后：
```js
        // 创建一个sayName共用方法
        function fun(){
            alert("大家好~~~我是"+this.name);
        };
        function Person(name,age,gender){
            this.name = name;
            this.age = age;
            this.gender = gender;
            // 向对象中添加一个方法
            this.sayName = fun;
            };

        //创建person的实例
        var per = new Person("孙悟空",18,"男");
        var per2 = new Person("白骨精",18,"女");
        var per3 = new Person("沙和尚",48,"男");
        per3.sayName();
```
缺点：将函数定义在全局定义域中，污染了全局作用域的命名空间，而且定义在全局作用域中也很不安全
# 10.作用域
作用域指一个变量作用的范围，js中一共两种作用域

1、全局作用域
直接编写在script标签中的js代码，都在全局作用域
全局作用域在页面打开时创建，关闭时销毁
在全局作用域中，有一个全局对象window，可以直接使用
在全局作用域中创建的变量都会作为window的属性保存，创建的函数都会作为window的方法保存
全局作用域中的所有变量都是全局变量，在页面的任意部分都可以访问到

2、函数作用域
调用函数时创建函数作用域，函数执行完毕，函数作用域销毁，每调用一次函数就会创建一次新的函数作用域
函数作用域中可以访问到全局变量
全局作用域中无法访问函数作用域的变量
当在作用域中操作变量，就近原则，函数中的先找函数作用域中的变量，然后再找上一级。全局中先找全局的
如果在函数中要使用全局变量，可以指定window.变量
在函数中不使用var声明的变量都会成为全局变量
定义形参相当于在函数作用域中声明变量

## 作用域链
内部函数访问外部函数的变量，采取链式查找的方法，决定取哪个值
（就近原则）

# 11.预解析
浏览器运行js代码分为两步 1、预解析2、代码执行
var关键字会在所有代码执行之前声明，但是不会赋值
如果声明变量时不使用var，则声明不会提前。

使用函数的声明形式创建的函数（function 函数名(){}），他会在所有的代码执行之前就被创建，所以我们可以在函数声明前调用
使用函数表达式创建的函数表达式（var 函数名 = function（）{}）不会被声明提前，所以不能再声明之前调用

预解析实例
一
```js
var num = 10;
fun();
function fun() {
    console.log(num);
    num = 20;
}
// 结果为10
// 解释
/*
    var num;

    function fun(){
        console.log(num);
        num = 20;
    }
    num = 10;
    fun();
*/
```
二
```js
var num = 10;
fun();
function fun() {
    console.log(num);
    var num = 20;
}
// 结果为wndefined
// 解释
/*
    var num;

    function fun(){
        var num;
        console.log(num);
        num = 20;
    }
    num = 10;
    fun();
    函数局部作用域，var声明提升到当前作用域的最前面，并没有赋值，就近原则，结果为undefined
*/
```
三
```js
        var num = 10;
        function fun(){
            console.log(num);
            var num = 20;
            console.log(num);
        }
        fun();
        // undefined和20
        // 解释：var声明提到局部作用域的前面，第一个未赋值，第二个赋值了
```
四
```js
        // 经典
        fun();
        console.log(c);
        console.log(b);
        console.log(a);
        function fun(){
            var a=b=c=9;
            console.log(a);
            console.log(b);
            console.log(c);
        }
        // 结果9 9 9 9 9 报错
        // 解释：var a=b=c=9;
        // 相当于
        // var a = 9;
        // b = 9;
        // c = 9;
        // bc未使用var声明，看做全局变量，a作为局部变量
```

# 12.this
解析器在调用函数时每次都会向函数内部传递一个隐含参数，这个隐函参数就是this，this指向的是一个对象，这个对象称为函数执行的上下文对象，根据函数调用方式的不同，this会指向不听的对象
1、以函数的形式调用，this永远都是window（实际上函数调用就相当于window.fun(); 本质上还是方法的对象）

2、方法的形式调用，this就是调用方法的对象
```js
function fun(){
    console.log(this);
}
fun();
// 输出window

// 创建一个对象
var obj = {
    name:"景仁杰",
    sayName:fun
};
console.log(obj.sayName == fun);
// //输出true

obj.sayName();//调用的是局部
fun();//调用的window
```

3.
```js
var name = "全局";

// 创建一个函数
function fun(){
    console.log(this.name);//如果不加this.使用obj.sayName()输出都是全局
}

// 创建两个对象
var obj ={
    name:"孙悟空",
    sayName:fun
};
var obj2 ={
    name:"猪八戒",
    sayName:fun
};
fun();
// 希望调用obj.sayName时输出对象的名字
obj.sayName();
obj2.sayName();
```
4.以构造函数调用，this就是新创建的对象

# 13.使用工厂创建对象
通过该方法可以大量创建对象
```js
function creatPerson(name,age,gender){
    // 创建一个对象
    var obj = new Object();
    // 向对象中添加属性
    obj.name = name;
    obj.age = age;
    obj.gender = gender;
    obj.sayName = function(){
        alert(this.name);
    };
    return obj;
}
var obj2 = creatPerson("孙悟空",18,"男");
var obj3 = creatPerson("猪八戒",28,"男");
var obj4 = creatPerson("沙和尚",38,"男");
obj4.sayName();
```
# 14.原型
prototype 创建的每一个函数都会向函数中添加一个属性prototype（唯一的）这个属性对应的对象，这个对象就是原型对象

# 15. 数组
数组就是一组数据的集合，将一组数据存在单个变量名下，数组中可以存储任意类型的元素

## 数组创建
方式一
```
//  new关键字 创建一个空数组
var arr = new Array();
```
方式二(常用)
```
// 数组字面量创建数组
var arr = [];
```
数组里面的数据用`,`隔开
声明数组并赋值称为数组初始化
## 获取数组元素
数组名[ 索引号 ] 索引号从0开始
```
console.log(arr[3]);
```
没有索引号的元素输出undefined
可以通过给指定索引号的数组元素赋值，会替换掉原来的元素
不要直接给整个数组赋值！！
## 遍历数组
数组索引号从0开始，i从0开始，i<元素个数
```
// 遍历
var arr1 = ['N','M','S','L'];
for(var i = 0;i<4;i++){
    console.log(arr1[i]);
}
```
数组的长度
```
console.log(arr.length);
```
数组的长度是元素个数，不要和索引号混淆
使用长度可以arr.length动态监控数组的长度

## 练习
练习一
```js
// 求数组元素的和，并求平均
var arr = [2,6,1,7,4,10];
var sum = 0;
    var average;
for(var i = 0;i<arr.length;i++){
    sum+=arr[i];
}
average = sum/arr.length;
console.log('和为：'+sum);
console.log('平均数为：'+average);
```
练习二
```js
// 求数组最大值
var arr = [1,6,1,77,52,25,7,99,90];
var max = arr[0];
for(var i = 1;i<arr.length;i++){
    if(arr[i] > max){
        max = arr[i];
    }
}
console.log('数组最大值是：' + max);
```
练习三
```js
// 分割数组变成字符串
var arr = ['N','M','S','L'];
var str = '';
for(var i = 0;i<arr.length;i++){
    str += '|'+arr[i];
}
console.log(str+'|');
```
练习四
```js
// 在数组中依次放入0-100
var arr = [];
for(var i = 0;i<100;i++){
    arr[i] = i+1;
}
console.log(arr);
```
练习五
```JS
// 将数组中大于10的元素筛选出来存入新数组
var arr = [1,23,44,5,66,7,9,10];
var arr1 = [];
var j = 0;
for(var i = 0;i<arr.length;i++){
    if(arr[i] > 10){
        arr1[j] = arr[i];
        j++;
    }
}
console.log(arr1);
```
练习五补充：可以直接使用arr1[arr1.length]来自动设置新数组的长度，不需要j

练习六
```JS
// 将数组中的0删掉，形成新数组
var arr = [0,30,0,6,6,6,6,0,0,1,0];
var arr1 = [];
for(var i = 0;i<arr.length;i++){
    if(arr[i] !== 0){
        arr1[arr1.length] = arr[i];
    }
}
console.log(arr1);
```
练习七
```JS
// 将数组的内容左右翻转
var arr = ['N','M','hhh','S','L','c'];
var arr1 = [];
for(var i = arr.length-1;i>=0;i--){
    arr1[arr1.length] = arr[i];
}
console.log(arr1);
```
## arguments
当形参不确定的时候，可以使用arguments指代，arguments会传递所有的实参
arguments实际上是伪数组
1、具有数组的length属性
2、按照索引的方式进行存储
3、没有真正数组的方法，如push，pop

可以用数组的方式对arguments进行遍历，只有函数具有arguments，且每个函数都内置了arguments

----------------------------------------------------------------

# 16.内置对象
js中对象分为三种：自定义对象，内置对象，浏览器对象
内置对象是js语言自带的对象，供开发者使用，并提供一些最基本或必要的功能/属性/方法

## 16.1Math对象
math数学对象 不是一个构造函数，不需要new调用，可以直接使用里面的属性和方法
```js
console.log(Math.PI);  //3.141592653589793
console.log(Math.max(1,99,3));
console.log(Math.max(1,-1));
console.log(Math.max(1,'hhh'));// NaN
console.log(Math.max());// -Infinity
```
例
```JS
var myMath = {
    PI:3.121592653589,
    max:function(){
        var max = arguments[0];
        for(var i = 1;i<arguments.length;i++){
            if(arguments[i]>max){
                max = arguments[i];
            }
        }
        return max;
    }
}

console.log(myMath.PI);
console.log(myMath.max(1,2,3,4,1));
```
### 16.1.1 绝对值方法
Math.abs();
输入为字符型，会自动隐式转换，若不能转换成数字则输出NaN

### 16.1.2 三个取整方法
#### 1、Math.floor()
向下取整
```JS
console.log(Math.floor(0.9));//0
console.log(Math.floor(1.1));//1
```
#### 2. Math.ceil()
向上取整
#### 3.Math.round()
四舍五入取整
.5在负数时，也是向大取（取绝对值更小的数字）
```js
console.log(Math.round(-1.8));//-2
console.log(Math.round(-1.5));//-1
console.log(Math.round(-1.1));//-1
```

### 16.1.3随机数方法
可以用来生成一个0-1 `[0,1)` 之间的随机数 `console.log(Math.random());`

生成一个0-x之间的随机数`console.log(Math.random()*x)`

生成一个x~y之间的随机数 `console.log(Math.random()*(y-x)+x)`

生成两个数之间的整数（包含这两个数）
```js
// 生成两个数之间的整数（包含这两个数）
function getRandom(min,max){
    return Math.floor(Math.random()*(max-min+1))+min;
}
console.log(getRandom(1,3));
```
随机点名
```js
// 生成两个数之间的整数（包含这两个数）
function getRandom(min,max){
    return Math.floor(Math.random()*(max-min+1))+min;

}
//随机点名
var arr = ['a','b','c']; //输入人名
console.log(arr[getRandom(0,arr.length-1)]);//arr.length是数组中的元素个数，但是数组索引号是从0~arr.length-1
```
猜数字
```js
// 猜数字
    // 生成两个数之间的整数（包含这两个数）
    function getRandom(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
var num = getRandom(1, 10);// 系统生成一个数
while (true) {
    var getnumber = prompt('请输入1~10之间的数：');
    if (num < getnumber) {
        alert('大了');
    }
    else if (num > getnumber) {
        alert('小了');

    }
    else {
        alert('你猜对了！！！');
        break;// 退出整个循环
    }
}
```

## 16.2 Date对象
日期对象是一个构造函数，必须使用new关键字来调用创建日期对象
```
var date = new Date();
console.log(date);
// 不带任何参数输出当前时间

// 字符串型 输出指定的时间
var date1 = new Date('2010-10-1 8:8:8');
console.log(date1);
```
### 16.2.1 Date的方法
##### getFullYear() 年
获取本地时间当年的年份
##### getMonth() 月
获取本地时间当前的月份（0-11）第0个月表示1月，所以输出结果比实际月份少1
##### getDate() 日
获取当前时间的日期
##### getDay() 星期几
获取当前日期对象是周几 会返回一个0-6的值
0 表示周日 1-6表示周一到周六
##### getHours() 时
获取当前时间的小时数
##### getMinutes() 分
获取当前时间的分钟数
##### getSeconds() 秒
获取当前时间的秒数
##### 获得毫秒数
获得距离1970年1月1号0时0分过去了多少毫秒
1秒 = 1000毫秒
```
        var date = new Date();
	console.log(date.getTime());
	// 或者
	console.log(date.valueOf());

	// 简单写法（最常用的用法）
        var date1 = +new Date;
        console.log(date1);

	//H5新增的更更简单的写法
	console.log(Date.now());
```
### 16.2.2 倒计时
```
 // 倒计时
        function countDown(time){
            var nowTime = +new Date();//当前时间
            var inputTime = +new Date(time);// 用户输入的时间
            var times = (inputTime - nowTime)/1000;// 距离目标时间还有多少秒
            var s = parseInt(times % 60);//秒
            s = s < 10 ? '0'+s : s;
            var m = parseInt(times/60%60);//分
            m = m < 10 ? '0'+m : m;
            var h = parseInt(times/3600%24);//时
            h = h < 10 ? '0'+h : h;
            var d = parseInt(times/3600/24);//天
            d = d < 10 ? '0'+d : d;
            return d + '天' + h + '时' + m + '分' + s + '秒';

        }
        console.log(countDown('2021-2-18 8:00:00'));// 时间需要写成字符串格式

```
## 16.3 数组对象
判断是否是数组
Array.isArray
```
        var arr = [];
        var obj = {};
        console.log(arr instanceof Array);
        // 返回true
        console.log(obj instanceof Array);
        // 返回false

        console.log(Array.isArray(arr));
```
### 添加/删除数组元素
push(参数1……)
末尾添加一个或多个元素，修改原数组，返回新数组的长度

pop()
删除数组最后一个元素，把数组长度减1，无参数，修改原数组
返回它删除的元素的值

unshift()
向数组的开头添加一个或多个元素，修改原数组
返回新数组的长度

shift()
删除数组的第一个元素，数组长度减一，无参数，修改原数组
返回第一个元素的值

### 筛选数组
```js
        // 将数组中大于某个值的元素删除，并将剩下的元素存入新数组里面
        var arr = [1500,1200,4000,3000,1000,1800];
        var newArr = []; // 新建数组存放保留的数据
        for(var i = 0;i < arr.length;i++){
            if(arr[i] < 2000){
                // newArr[newArr.length] = arr[i];
                newArr.push(arr[i]);
                // 把符合要求的arr元素向新空数组的末尾添加
            }
        }
        console.log(newArr);
```
### 数组排序
reverse()
颠倒数组中的元素的顺序，无参数
该方法会改变原数组，返回新数组
```js
    var arr = [1,2,3,4,5,6];
    console.log(arr.reverse());
```

sort()
对数组的顺序进行排序
改变原数组，返回新数组
```js
        var arr1 = [1,3,2,5,2];
        arr1.sort();
        console.log(arr1);
```
注意：sort()只能处理个位数的排序，因为只对首位数字比较，两位数的排序
```js
        var arr2 = [1,22,13,4,77,5];
        arr2.sort(function(a,b){
            return a - b;
        });
        console.log(arr2);
        // 输出 [1, 4, 5, 13, 22, 77]
```
` a-b `从小到大
` b-a `从大到小

### 数组索引
indexOf()
数组中查找给定元素的第一个索引位置
如果存在返回第一个出现的指定元素的索引号，如果没有就返回-1

lastIndexOf()
在数组中查找指定元素的最后一个索引位置，没有返回-1
```js
        var arr = [1,2,3,3,3,4,4,45,5,5,56,6,6,7,6,6,6,6,6,6,6,6,6,6,33,4,2,6,];
        console.log(arr.length);//28
        console.log(arr.indexOf(6));// 返回第一个6的索引号11
        console.log(arr.indexOf(100)); // -1
        console.log(arr.lastIndexOf(6)); // 27
```
### 数组去重（重点案例）
```js
        var arr = [1,2,'-1','-1',3,3,3,4,4,45,5,5,56,6,6,7,6,6,6,6,6,6,6,6,6,6,33,4,2,6,];
        // console.log(arr.length);//28
        var arr1 = [];
        for(var i = 0;i<arr.length;i++){
            if(arr1.indexOf(arr[i]) == -1){
                // 在新数组中查找旧数组元素的第一个索引
                // 等于-1说明新数组中没有旧数组的这个元素
                // 则将旧数组中的这个元素放到新数组中
                // 实现去重
                arr1[arr1.length] = arr[i];
            }
        }
        console.log(arr1);
        //  [1, 2, 3, 4, 45, 5, 56, 6, 7, 33]
```
### 数组转换成字符串
toString()
将数组转换成字符串，逗号隔开，返回一个字符串

join('自定义的分隔符')
用于将数组中所有元素转换为一个字符串，返回一个字符串
```js
        var arr = [1,2,'-1','-1',3,3,3,4,4,45,5,5,56,6,6,7,6,6,6,6,6,6,6,6,6,6,33,4,2,6,];
        console.log(arr.toString());
        // 1,2,-1,-1,3,3,3,4,4,45,5,5,56,6,6,7,6,6,6,6,6,6,6,6,6,6,33,4,2,6
        console.log(arr.join('|'));
        // 1|2|-1|-1|3|3|3|4|4|45|5|5|56|6|6|7|6|6|6|6|6|6|6|6|6|6|33|4|2|6
```
## 16.4 字符串对象
### 16.4.1 包装
对象才有属性和方法，复杂数据类型才有属性和方法
但是简单数据类型也可以有属性

基本包装类型就是把简单数据类型包装成复杂数据类,这样简单数据类型就有了属性和方法
在JS中为我们提供了三个包装类，通过这三个包装类可以将基本数据类型的数据转换为对象

new String() 可以将基本数据类型字符串转换为String对象

new Number() 可以将基本数据类型的数字转换为Number对象

new Boolean() 可以将基本数据类型的布尔值转换为Boolean对象

注意：我们在实际应用中不会使用基本数据类型的对象，如果使用基本数据类型的对象，在做一些比较时可能会带来一些不可预期的结果
方法和属性只能添加给对象，不能添加给基本数据类型 当我们对一些基本数据类型的值去调用属性和方法时，浏览器会临时使用包装类将其转换为对象，然后在调用对象的属性和方法 调用完以后，在将其转换为基本数据类型
```js
        var str = 'andy';
        console.log(str.length); // 4

        // 包装过程
        // (1) 把简单数据类型转换成复杂数据类型
        var temp = new String('andy');
        // (2) 把临时变量的值给str
        str = temp;
        // (3) 销毁临时变量
        temp = null;

```
### 16.4.2 字符串不可变
指的是字符串的值内容不可变，只是地址变了，内存中新开辟一个空间存放新的字符串内容
所以不要大量拼接字符串
字符串所有的方法都不会修改字符串本身，都是新开辟空间存放新的字符串（字符串的不可变性）
### 根据字符返回位置
indexOf('查找的字符')
要查找的字符起始位置
查不到返回-1

indexOf('要查找的字符',[起始的索引号位置])
从索引号位置往后查
查不到返回-1
```js
        var str = 'abcdefgccccedf';
        console.log(str.indexOf('c'));// 2
        console.log(str.indexOf('c',[2]));// 2
        console.log(str.indexOf('c',[3]));// 7
```
lastIndexOf()
从后往前找，只找第一个匹配的位置

例子
```js
        // 查找字符串中所有o的位置以及出现的次数
        var str = 'oooabcdefoxyozzopp';
        var index = str.indexOf('o');// 6
        var num = 0;
        while(index !== -1){
            console.log(index);
            num++;
            index = str.indexOf('o',[index+1]);
        }
        console.log('o出现了'+ num +'次');
```
### 根据位置返回字符（重要）
charAt(index)
返回指定位置的字符（index字符串的索引号）
str.charAt(0)
```js
        var str = 'abcde';
        console.log(str.charAt(3));// d
        // 遍历所有的字符
        for(var i = 0;i<str.length;i++){
            console.log(str.charAt(i));
        }
```

charCodeAt(index)
获取指定位置字符的ASCII码
str.charCodeAt(0)
用以判断用户按了哪一个键
```js
        var str = 'abcde';
        // 遍历所有的字符
        for(var i = 0;i<str.length;i++){
            console.log(str.charCodeAt(i));
            // 97 98 99 100 101
        }
```

str[index]
获取指定位置处字符
H5新增,IE8支持 和charAt()等效
```js
        var str = 'abcde';
        console.log(str[3]);// d
        // 遍历所有的字符
        for(var i = 0;i<str.length;i++){
            console.log(str[i]);
            // a b c d e
        }

```

例
```js
        // 查找字符串里面出现最多次的字符以及出现的次数
        var str ='aamjkbcdefooedfoogk';
        var obj = {};// 新建对象保存所有出现过的字符和次数
        for(var i = 0;i<str.length;i++){
            var character = str.charAt(i);
            if(obj[character]){
                // 对象里面已有的属性值加一
                obj[character]++;

            }
            else{
                // 对象里面原先没有的属性赋值为1
                obj[character] = 1;
            }
        }
        console.log(obj);
        var max = 0;
        var ch = '';
        for(var k in obj){
            // k得到的是属性名
            // obj[k]得到属性值
            if(obj[k] > max){
                max = obj[k];
                ch = k;// 因为k属于局部变量，在for循环外无意义
            }

        }

        console.log('出现次数最多的字母是 '+ ch + ' 出现了'+ max + '次');
        // 出现次数最多的字母是 o 出现了4次
```
### 字符串操作方法
concat(str1,st2……)
用于连接多个字符串。等价于+，但是+更常用
```js
        var str = 'jrj';
        console.log(str.concat('SB','JRJ','kkkkk'));
        // jrjSBJRJkkkkk
```

substr(start,length)
从start开始，length是取的个数
```js
        var str1 = 'kkkkkjrjkkkkk';
        console.log(str1.substr(5,3));
        // 从索引号5开始取，取三个字符 jrj
```
### 替换字符
replace()
只会替换第一个字符
```js
        var str = 'kjrjkkjrj';
        console.log(str.replace('j','t'));
        // ktrjkkjrj
```
替换所有
```js
        var str = 'jrjrjrjrjrj';
        while(str.indexOf('j') !== -1){
            str = str.replace('j','*')

        }
        console.log(str);
```
### 字符转换成数组
split('分隔符')
```js
        var str = 'a|b|c|b|d';
        console.log(str.split('|'));
        // 根据字符串的分隔符区分数组里面的分隔符
        //  ["a", "b", "c", "b", "d"]
```

-------------------------
# 17简单数据类型和复杂数据类型
简单数据类型又叫基本数据类型或者值类型，复杂数据类型又叫引用类型
值类型，在变量中存储的是值本身
string number Boolean undefined null
注意：typeof null 会输出object，是一个空的对象

复杂数据类型：在存储变量中存储的仅仅是地址（引用），因此叫做引用数据类型
通过new关键字创建的对象（系统对象，自定义对象）如object array date等

## 17.1堆和栈
栈 存放简单数据类型 直接存放的是值
堆 存放复杂数据类型，首先在栈里面存放十六进制的地址指向堆，一般由程序员分配释放，若程序员不释放，由垃圾回收机制回收

## 17.2传参
### 简单类型传参
函数的形参也可以看作变量，把值类型变量作为参数传递给形参时，其实是把变量在栈空间里面的值复制一份给形参，当局部对形参做任何修改，都不会改变外部变量的值

### 复杂类型传参
函数的形参可以看做变量，当把引用类型的变量传给形参时，其实是把变量在栈空间里保存的堆地址复制给了形参，形参和实参保存的是同一个堆地址，指向同一个堆数据，所以操作的是同一个对象

-----------
# web apis
api application programming interface(应用程序编程接口)预定义的函数，提供应用程序与开发人员基于软件或硬件得以访问的能力，不需要访问源码或者内部机制 `接口`

web api是操作浏览器功能和页面元素的API（DOM和BOM）
web api很多都是方法（函数）
# DOM
DOM document object model 文档对象模型 处理可扩展标记语言的标准编程`接口`
通过接口可以改变网页的内容样式等
## 获取元素
### 1、根据ID获取
getElementById(),返回的是对象
```html
	<body>
	    <!-- <div class="box1"></div> -->
	    <div id = "time">2021-2-20</div>
	    <script>
		// 因为文档页面从上到下，所以script写在标签的下面
		var timer = document.getElementById('time');// 参数是ID
		console.log(timer);
		// 返回的是对象，如果找不到返回null
		console.dir(timer);// dir打印返回的元素对象，更好的查看属性和方法

	    </script>
	</body>
```
### 2、根据标签名获取
getElementByTagName() 返回的对象的集合以伪数组的形式存储
我们想要依次打印元素对象可以采取遍历的形式
返回的元素是动态的
页面只有一个，返回的还是以伪数组的形式
没有元素，返回的是空的伪数组

可以通过父元素获取内部子元素 必须指明是哪一个父元素
### 3、通过类名获取
getElementByClassName()
不兼容IE8及以下浏览器

### 4、querySelector()
返回指定选择器的第一个元素对象
```html
    <div class="box">1</div>
    <div class="box">2</div>

    <script>
        var firstBox = document.querySelector('.box');
        console.log(firstBox);
        // 返回第一个box
        // 参数必须带符号，类，ID等
    </script>
```
### 5、querySelectorAll()
返回指定选择器的所有元素对象集合

## 获取特殊元素
### 获取body元素
```html
    <script>
        var bbb = document.body;
        console.log(bbb);
    </script>
```
### 获取html元素
```js
        var hhh = document.documentElement;
        console.log(hhh);
```

---------
## 事件
事件由三部分组成，事件源，事件类型，事件处理程序

事件源：事件被触发的对象 如创建的按钮

事件类型：如何触发如鼠标点击，鼠标经过，键盘按下

事件处理程序：通过一个函数赋值的方式完成
```html
	<body>
		// 创建事件源
	    <button id="btn">点我 就现在</button>

	    <script>
		// 获取按钮节点
		var btn = document.getElementById('btn');
		// 事件处理程序
		btn.onclick = function(){
		    alert('点我干嘛？？？');
		}
	    </script>
	</body>
```
### 事件基础
执行事件的步骤
1、获取事件源
2、注册事件（绑定事件）
3、添加事件处理程序

## 操作元素
element.innerText
从起始位置到终止位置的内容，但它去除html标签，同时空格和换行也会去掉

element.innerHTML
起始位置到终止位置的全部内容，包括html标签，同时保留空格和换行
```
    <button>显示当前系统时间</button>
    <div>某个时间</div>
    <script>
        // 点击按钮，div里面的文字发生变化
        // 1、获取元素
        var btn = document.querySelector('button');
        var div = document.querySelector('div');
        // 2、注册事件
        btn.onclick = function(){
            div.innerText = getD();// 若果这行代码不在函数里面，相当于不注册事件直接运行
        }
        function getD(){
            var date = new Date();
            var year = date.getFullYear();
            var month = date.getMonth()+1;
            var date111 = date.getDate();// 千万要注意！！！这里的变量不能和一开始的date冲突，否则显示下一行的getDay is not a function
            var day = date.getDay();
            var arr = ['星期日','星期一','星期二','星期三','星期四','星期五','星期六'];

            return'今天是'+year+'年'+month+'月'+date111+'日'+arr[day];

        }
        console.log(getD() );
    </script>
```

.innerText和.innerHTML区别 ：前者不识别HTML标签
```
 	var div = document.querySelector('div');
        div.innerHTML = '今天天气<strong>真不错</strong>';// 识别标签
        // div.innerText = '今天天气<strong>真不错</strong>'; // 不识别，直接当文本输出
```
## 修改元素属性
```html
    <button id="ldh">刘德华</button>
    <button id="lcw">梁朝伟</button>
    <br><br>
    <img src="./img/01.jpg" alt="" title="刘德华">
    <script>
        // 1、获取元素
        var ldh = document.getElementById('ldh');
        var lcw = document.getElementById('lcw');
        var img = document.querySelector('img');

        // 2、注册事件
        ldh.onclick = function(){
            img.src = "./img/01.jpg";
            img.tile = "刘德华";
        }
        lcw.onclick = function(){
            img.src = "./img/02.jpg";
            img.title = "梁朝伟";
        }
    </script>
```
## 操作表单元素
```html
    <button>按钮</button>
    <input type="text" value="输入内容">
    <script>
        // 1、获取元素
        var btn = document.querySelector('button');
        var inp = document.querySelector('input');

        // 2、注册事件
        btn.onclick = function(){
            // input.innerHTML = '点击了';
            // innerHTML是修改普通盒子比如div标签内的内容

            // 表单内容是通过value修改
            inp.value = '点击了';

	    // 如果按钮点击一次后不能再被点击，使用disabled
            this.disabled = true;// this指向的是事件函数的调用者
        }
    </script>
```

##### 京东登录
```html
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        .box {
            width: 400px;
            border-bottom: 2px solid #aaa;
            /* 上边距100px 居中 */
            margin: 200px auto;
            position: relative;
        }

        .box input {
            width: 370px;
            height: 30px;
            border: 0px;
            /* 去除外边框 */
            outline: none;
        }

        .box img {
            width: 24px;
            position: absolute;
            top: 5px;
            right: 0px;

        }
    </style>
</head>

<body>
    <div class="box">
        <label for="">
            <img src="./img/闭眼.png" alt="" id="eye">
        </label>
        <input type="password" name="" id="password">
    </div>
    <script>
        // 1、获取元素
        var eye = document.getElementById('eye');
        var password = document.getElementById('password');

        // 2、注册事件
        // 设置一个flag来表示密码框的状态，通过01变换实现切换
        var flag = 0;
        eye.onclick = function () {
            if (flag == 0) {
                // 密码框改为文本框 显示密码
                password.type = "text";
                eye.src = "./img/睁眼.png";
                flag = 1;
            }
            else{
                // 密码框改为密码框 不显示密码
                password.type = "password";
                eye.src = "./img/闭眼.png";
                flag = 0;
            }
        }
    </script>
</body>

</html>
```
## 操作样式属性
通过JS修改元素的大小颜色位置等样式
Js里面的样式采取驼峰命名法 fontSize，backgroundColor
Js修改style样式操作，产生的是行内样式，比CSS样式的权重更高
##### 淘宝关闭二维码样式
```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>淘宝二维码</title>
    <style>
        .box{
            width: 74px;
            height: 88px;
            border: 1px solid #ccc;
            margin: 100px auto;
            font-size: 12px;
            text-align: center;
            color: #f40;
            position: relative;
        }
        .box img{
            height: 50px;
            margin-top: 5px;
        }
        .box .close-btn{
            position: absolute;
            top: -1px;
            left: -16px;
            width: 14px;
            height: 14px;
            border: 1px solid #ccc;
            font-family:Arial, Helvetica, sans-serif;
            cursor: pointer;
        }
    </style>

</head>
<div class="box">
        <body>
        淘宝二维码
        <img src="./img/02.jpg" alt="">
        <i class="close-btn">×</i>
    </div>
    <script>
        // 1、获取元素
        var btn = document.querySelector('.close-btn');
        var box = document.querySelector('.box');
        // 2、注册事件 程序处理
        btn.onclick = function(){
            box.style.display = 'none';
        }
    </script>
</body>
</html>
```
### 焦点
onfocus 得到焦点
onblur 失去焦点
##### 京东文本框
```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>京东文本框</title>
    <style>
        input{
            color: #ccc;
            margin: 200px 200px;
        }
    </style>
</head>
<body>
    <input type="text" value="手机">
    <script>
        // 1、获取元素
        var text = document.querySelector('input');
        // 2、注册事件 获得焦点事件
        text.onfocus = function(){
            if(this.value == '手机'){
                text.value = "";
            }
            // 获得焦点使文本框字色变深
            this.style.color = '#333';
        }
        // 3、失去焦点事件
        text.onblur = function(){
            if(this.value == ''){
                text.value = '手机';
            }
            // 失去焦点文本框颜色变浅
            this.style.color = '#ccc';
        }
    </script>
</body>
</html>
```
## 通过className修改样式
如果使用element.style.修改元素样式，一次只能改一种
可以在CSS中写好类包含所有样式改变
然后通过给当前元素添加类名
this.className = '类名';
```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        div{
            width: 200px;
            height: 200px;
            background-color: #bfa;
        }
        .change{
            background-color: #aab;
            color: #f60;
            font-size: 25px;
            margin: 200px auto;
        }
    </style>
</head>
<body>
    <div>嗷嗷嗷啊</div>
    <script>
        // 1、获取元素
        var div = document.querySelector('div');
        // 2、注册事件 程序处理
        div.onclick = function(){
            // 修改当前元素的类名
            // 类名已经写在style里面
            this.className = 'change';
        }
    </script>
</body>
</html>
```
注意：className会直接覆盖掉原先的类名！
如果需要保留原先的类名
this.className = '原先的类名 修改的类名';

--------------
## 排他思想（算法）
```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>
<body>
    <button>按钮1</button>
    <button>按钮2</button>
    <button>按钮3</button>
    <button>按钮4</button>
    <button>按钮5</button>
    <script>
        // 1、获取元素
        var btns = document.getElementsByTagName('button');
        // 2、注册事件 程序处理
        for(var i = 0;i<btns.length;i++){
            btns[i].onclick = function(){
                /*
                    1、先把所有按钮的背景颜色去掉
                    2、再把选中的按钮颜色变色
                */
               for(var j=0;j<btns.length;j++){
                   btns[j].style.backgroundColor = '';
               }
                this.style.backgroundColor = '#abf';
            }
        }
    </script>
</body>
</html>
```
### 鼠标经过
onmouseover 鼠标经过
onmouseout 鼠标离开


## 自定义属性操作
#### 获取属性值
element.属性 获取属性值 获取内置属性
element.getAttribute('属性') getAttribute获取的是自己设置的属性

#### 修改属性值
element.属性 = '值'
element.getAttribute('属性','值')

#### 移除属性值
.removeAttribute('属性')

# 节点
获取元素常用的两种形式
1、利用DOM方法获取
逻辑性不强，繁琐

2、节点操作
利用父子兄弟关系获取

页面中所有的内容都是节点 DOM中使用node表示节点
一般来说，节点至少拥有节点类型（nodetype） 节点名称（nodeName） 节点值（nodevalue）三个基本属性
元素节点的节点类型是1
属性节点的节点类型是2
文本节点的节点类型是3（包括文字 空格 换行）
实际开发中，主要操作的是元素节点

## 节点层级
### 父节点
.parentNode 离元素最近的父节点
找不到父节点返回null
### 子节点
通过具体的元素节点调用

1.childNodes
表示当前节点的所有子节点（换行也算文本节点）注意：IE8及以下，不会将空白换行作为节点
可以使用nodeType判断，去除换行
文本节点类型是3，元素节点类型是1
也可以使用children（非标准）

2.firstChild
表示当前节点的第一个子节点
可能包含其他节点
firstElementChild IE9

3.lastChild
表示当前节点的最后一个子节点
可能包含其他节点
lastElementChild IE9

实际开发中可能使用.children[0]表示第一个节点
.children[ul.children.lengtth - 1]表示最后一个节点
### 兄弟节点
1.previousSibling
属性，表示当前节点的前一个兄弟节点
包含元素或文本节点（换行）

2.extSibling
属性，表示当前节点的后一个兄弟节点
包含元素或文本节点（换行）
nextElementSilbling 下一个兄弟元素节点不包括文本（换行）

兼容性问题：自己封装一个函数

## 创建节点
.createElement
可以用于创建一个元素节点对象 它需要一个标签名作为参数，将会根据该标签名创建元素节点对象 并将创建好的对象作为返回值返回
例如： 创建一个li标签 ` var li1 = document.createElement("li"); `

appendChild()
向一个父节点末尾添加一个新的子节点 用法: 父节点.appendChild(子节点);

insertBefore()
可以在指定的子节点前插入新的子节点 语法： 父节点.insertBefore(新节点(前),旧节点(后));

## 删除节点
removeChild()
```js
        // 1、获取元素
        var ul = document.querySelector('ul');
        // 2、删除元素
        ul.removeChild(ul.children[0]);// 删除第一个子节点
```
```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <button>删除</button>
    <ul>
        <li>A</li>
        <li>B</li>
        <li>C</li>
        <li>D</li>
    </ul>
    <script>
        // // 1、获取元素
        var ul = document.querySelector('ul');
        // // 2、删除元素
        // ul.removeChild(ul.children[0]);// 删除第一个子节点

        // 点击按钮依次删除元素
        var btn = document.querySelector('button');
        btn.onclick = function(){

            if(ul.children.length == 0){
                // 禁用删除按钮
                this.disabled = true;
            }
            else{
                ul.removeChild(ul.children[0]);
            }
        }
    </script>
</body>
</html>
```
## 复制节点
```js
        // 1、获取元素
        var ul = document.querySelector('ul');
        // 2、复制节点
        var lili = ul.children[0].cloneNode(true);

        ul.appendChild(lili);
```
如果.cloneNode()括号里面参数为空，或者是false，称为浅拷贝，只复制节点本身，不复制节点内容

括号里为true称为深拷贝，复制标签和内容

##### 动态生成表格
```html
<!DOCTYPE html>
<html lang="zh">

<head>
    <meta charset="UTF-8">
    <title>表格</title>
    <style>
        table {
            width: 500px;
            margin: 100px auto;
            border-collapse: collapse;
            text-align: center;
        }

        td,
        th {
            border: solid 1px #333;
        }

        thead tr {
            /* 设置表头 行 */
            height: 40px;
            background-color: #abf;
        }
    </style>
</head>

<body>
    <table>
        <thead>
            <tr>
                <th>名字</th>
                <th>科目</th>
                <th>成绩</th>
                <th>操作</th>
            </tr>
        </thead>

        <tbody>

        </tbody>
    </table>

    <script>
        // 1、学生数据
        var dates = [
            {
                name: '孙悟空',
                subject: '语文',
                score: 100
            },
            {
                name: '唐僧',
                subject: '语文',
                score: 95
            },
            {
                name: '猪八戒',
                subject: '语文',
                score: 70
            },
            {
                name: '沙和尚',
                subject: '语文',
                score: 90
            }
        ];
        // 2、创建表格的行数（数组元素个数）
        var tbody = document.querySelector('tbody');
        for (var i = 0; i < dates.length; i++) {
            var tr = document.createElement('tr');
            tbody.appendChild(tr);
            // td是数组里面对象的属性个数 dates[i]
            // 遍历对象
            // j 得到属性名
            //  obj[j] 得到属性值
            for (var j in dates[i]) {
                // 创建单元格
                var td = document.createElement('td');
                // 把每个单元格添加元素到行里面
                tr.appendChild(td);
                // dates是数组 dates[i]是数组里面第i个元素（对象） dates[i][j]是数组里面对象的属性值
                // console.log(dates[i][j]);
                // 向单元格中添加元素
                td.innerHTML = dates[i][j];
            }

            // 创建操作里面的 删除单元格
            // 3、创建单元格，数字取决于数组每个元素 对象里面的属性个数
            var td = document.createElement('td');
            tr.appendChild(td);
            // 在最后一个单元格添加内容(删除的链接)
            td.innerHTML = '<a href="#">删除</a>'

        }

        // 4、创建删除操作 获取所有的a 创建事件 删除a所在的行
        var aaa = document.querySelectorAll('a');
        for(var i = 0;i<aaa.length;i++){
            aaa[i].onclick = function(){
                // 点击链接删除a所在的行
                tbody.removeChild(this.parentNode.parentNode);
            }
        }
    </script>
</body>

</html>
```
## 三种动态创建元素的区别
document.write() （很少使用）
直接将内容写入页面的内容流，但是文档流执行完毕，会导致页面全部重绘
```html
<body>
    <button>
        按钮
    </button>
    <script>
        var btn = document.querySelector('button');
        btn.onclick = function(){
            document.write('<div>123</div>');
        }

    </script>
</body>
```

innerHTML创建元素
```html
<body>
    <div class="inn"></div>
    <script>
        var inn = document.querySelector('.inn');
        inn.innerHTML = '<a href="#">百度</a>';
    </script>
</body>
```

createElement()
```html
<body>
    <div class="cre"></div>
    <script>
        var cre = document.querySelector('.cre');
        var a = document.createElement('a');
        cre.appendChild(a);
    </script>
</body>
```
使用innerHTML()创建大量元素时，效率更高
但是前提是不要使用拼接字符串的形式，而要使用数组的形式
结构稍微复杂

## DOM重点核心
为了能够让js操作HTML，js有一套自己的dom接口
学习dom主要是针对元素的操作,增，删，改，查，属性操作，事件操作

增加：
document.write()
innerHTML
creatElement()

删除：
removeChild

改：
1、修改dom元素属性 src href title等
2、修改普通元素内容 innerHTML innerText
3、修改表单元素 value type disabled
4、修改元素样式style className

查：
1、DOM提供的API getElementById getElementByTagName (不推荐)
2、H5新方法 querySelector querySelectorAll (推荐)
3、利用节点获取元素 父子兄弟节点

属性操作：
setAttribute 设置dom属性值
getAttribute 得到属性值
removeAttribute 删除属性

事件操作：
鼠标点击 经过 离开 焦点等

--------------------

# 事件高级

## 注册事件
注册事件有两种方式：传统方式和监听注册事件
### 传统
on开头的例如onclick鼠标点击
特点 注册事件的唯一性，只运行靠后的处理函数
### 方法监听注册事件
addEventListener() 推荐使用

同一个元素同一个事件可以添加多个监听器 不会出现唯一性问题
该方法三个参数
type 事件类型字符串带引号 比如click mouseover 不带on
listener 事件处理函数 事件发生时 会调用该函数
useCapture 可选参数 是一个布尔值默认false

addEventListener()中的this是绑定事件的对象 不支持IE8及以下

attachEvent() 在IE8中及以前可以使用attachEvent()来绑定事件
参数：
1.事件的字符串，要on
2.回调函数 这个方法也可以同时为一个事件绑定多个处理函数
不同的是它是后绑定先执行，执行顺序和addEventListener()相反

注册事件兼容性问题解决
```
        function addEventListener(element ,eventName,fn){
            // 判断当前浏览器是否支持addEventLisener方法
            if(element.addEventListener){
                element.addEventListener(eventName,fn);// 第三个参数默认false

            }
            else if(element.attachEvent){
                element.attachEvent('on'+eventName,fn);

            }
            else{
                element['on'+ eventName] = fn;
            }
        }
```
## 删除事件
### 传统
.onclick = null;
### 方法监听注册方式
.removeEventListener(type,listener,useCapture)

例
```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <style>
        div{
            width: 100px;
            height: 100px;
            background-color: #bfa;
        }
    </style>
</head>
<body>
    <div class="a">1</div>
    <div class="b">2</div>
    <div class="c">3</div>
    <script>
        var divs = document.querySelectorAll('div');

        // 传统绑定事件
        divs[0].onclick = function(){
            alert(111);
            // 传统删除事件
            divs[0].onclick = null;
        }

        // 方法监听注册事件
        // 三个参数，type字符串要加引号不要on
        divs[1].addEventListener('click',fn);
        // 因为要删除函数 所以函数不能使用匿名的
        function fn(){
            alert(222);
            // 方法监听删除
            divs[1].removeEventListener('click',fn);
        }
    </script>
</body>
</html>
```
## DOM事件流
事件发生时会在元素节点之间按照特定的顺序传播，这个传播过程就是DOM事件流
事件流分为三个阶段

1.捕获阶段 在捕获阶段时从最外层的祖先元素，向目标元素进行事件的捕获，但是默认此时不会触发事件

2.当前目标阶段 事件捕获到目标元素，捕获结束开始在目标元素上触发事件

3.冒泡阶段 事件从目标元素向它的祖先元素传递，依次触发祖先元素上的事件 addEventListener 第三个参数是false或省略 处于冒泡阶段

如果希望在捕获阶段就触发事件，可以将addEventListener()的第三个参数设置为true，一般情况下我们不会希望在捕获阶段触发事件，所以这个参数默认都是false

IE8以下的浏览器没有捕获阶段

## 事件对象

event就是事件对象 写到侦听函数的小括号里 可以当成形参看待
事件对象只有事件存在才会存在 是系统自动创建的 不需要传递实参
事件对象是事件一系列相关数据的集合 比如鼠标点击包含鼠标的相关信息 例如鼠标坐标
键盘事件包含键盘按键信息
事件对象可以自己命名 event evt e
事件对象也有兼容性问题 通过window.event 解决
e = e || window.event

### 事件对象常见的属性和方法
e.target 返回触发事件的方法 标准
```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <style>
        div{
            width: 150px;
            height: 150px;
            background-color: #abf;
        }
    </style>
</head>
<body>
    <div>1234</div>
    <ul>
        <li>a</li>
        <li>b</li>
        <li>c</li>
        <li>d</li>
    </ul>
    <script>
        var div = document.querySelector('div');
        div.addEventListener('click',function(e){
            console.log(e.target);// 返回触发事件的对象div1234
            console.log(this);// 返回绑定事件的对象div 1234
        })


        var ul = document.querySelector('ul');
        ul.addEventListener('click',function(e){
            console.log(this);// 给ul绑定事件 this指向ul 返回ul
            console.log(e.target);// 返回点击的li
        })
    </script>
</body>
</html>
```
e.srcElement 返回触发事件的对象 非标准 ie6-8使用

e.type 返回事件的类型 比如click 不带on

e.returnValue 该属性阻止默认事件（默认行为）非标准 ie6-8使用 比如不让链接跳转

e.preventDefault() 该方法阻止默认事件 标准 比如不让链接使用

## 阻止冒泡
所谓的事件的冒泡指的是事件的向上传导，当后代元素上的事件被触发时，其祖先元素的相同事件也会被触发 在开发中大部分情况冒泡都是有用的，如果不希望发生事件冒泡，可以通过事件对象来取消冒泡

e.stopPropagation()阻止冒泡 标准

e.cancelBubble = true 该属性阻止冒泡 非标准 ie6-8使用

## 事件委派
原理：不给每个子节点单独设置事件监听器 而是事件监听器设置在父节点上 然后利用冒泡原理影响设置每个子节点
在父节点上设置事件 然后利用target找到点击li 然后事件冒泡到ul上
```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>
<body>
    <ul>
        <li>a</li>
        <li>b</li>
        <li>c</li>
        <li>d</li>
    </ul>
    <script>
        var ul = document.querySelector('ul');
        ul.addEventListener('click',function(e){
            // alert('aaaaaaa');
            //  点击某个li时，事件会冒泡到Ul上 弹出

            // e.target可以得到当前点击的对象
            e.target.style.backgroundColor = '#b66';
        })
    </script>
</body>
</html>
```

-----------------
# BOM
BOM 是浏览器对象模型 提供了独立于内容而与浏览器窗口进行交互的对象
核心对象是window

BOM由一系列对象组成 每个对象提供很多属性和方法

BOM缺乏标准 各自浏览器自行定义 兼容性较差

window对象是浏览器的顶级对象
1、是js访问浏览器窗口的一个接口
2、是一个全局对象 定义在全局作用域中的变量、函数都会变成window对象的属性和方法
注意window下一个特殊属性window.name

(Browser Object Model) 即浏览器对象模型，主要是指一些浏览器内置对象如：window、history、navigator、location、screen等对象
window指窗口 也是全局对象, 一些全局变量会挂在window上面
navigator浏览器的实例, 一般用于分辨客户端环境
location对象用于获取地址的host, url, search信息等
screen 对象包含有关客户端显示屏幕的信息







## window常见事件
#### 窗口加载事件
文档内容完全加载完才会触发该事件（包括图像 脚本文件 CSS文件等）
```html
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <script>
        // // window.onload 传统的窗口加载事件
        // // 但是只能写一次 写了多次会以最后一次为准
        // window.onload = function () {
        //     var btn = document.querySelector('button');
        //     btn.addEventListener('click', function () {
        //         alert('点我干嘛？？？');
        //     })
        // }

        // 新的窗口加载事件
        // 可以写多个
        window.addEventListener('load', function () {
            var btn = document.querySelector('button');
            btn.addEventListener('click', function () {
                alert('点我干嘛？？？');
            })
        })

        // DOMContentLoaded 是DOM元素加载完毕 不包含图片flash CSS等就可以执行
        // load需要等页面全部元素都加载完才能执行
        document.addEventListener('DOMContentLoaded',function(){
            alert(1234);
        })
    </script>
</head>

<body>
    <button>按钮</button>
</body>

</html>
```
#### 调整窗口事件
` window.addEventListener('resize',function(){}) ` 只要窗口像素发生变化就触发
响应式布局
```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        div{
            width: 300px;
            height: 300px;
            background-color: #abf;
        }
    </style>
    <script>
        // 使script里面代码在所有元素加载完成后执行
        window.addEventListener('load',function(){
            var div = document.querySelector('div');
            window.addEventListener('resize',function(){
                console.log(window.innerWidth);// 获取当前窗口宽度
                if(window.innerWidth < 600){
                    div.style.display = 'none';
                }
                else{
                    div.style.display = 'block';
                }
            })
        })
    </script>
</head>
<body>
    <div></div>
</body>
</html>
```
## 定时器
setTimeout()延时调用 延时调用一个函数不马上执行，而是隔一段时间后再执行，而且只会只会执行一次
` var timer = setTimeout(function(){},3000); `
延时时间单位是毫秒 可以省略 省略默认为0
调用函数可以直接写函数 也可以写函数名。或者'函数名()'单引号不推荐使用
页面中可能有很多定时器 经常给定时器加标识符
setTimeout里面的函数称为回调函数callback

例：五秒后关闭广告
```
    <img src="../黑马pink/img/01.jpg" alt="" class="ad">
    <script>
        var ad = document.querySelector('.ad');
        setTimeout(function(){
            ad.style.display = 'none';
        },5000);
        //  五秒后广告隐藏
    </script>
```

停止定时器 clearTimeout() 关闭一个延时调用
```html
    <img src="../黑马pink/img/01.jpg" alt="" class="ad">
    <br>
    <br>
    <br>
    <button>
        停止定时器
    </button>
    <script>
        var ad = document.querySelector('.ad');
        // 给定时器设置ID
        var timer1 = setTimeout(function(){
            ad.style.display = 'none';
        },3000);
        //  三秒后广告隐藏
        var btn = document.querySelector('button');
        btn.addEventListener('click',function(){
            clearTimeout(timer1);// 参数是定时器标识符
        })
    </script>
```

setInterval()定时调用 可以将一个函数，每隔一段时间被调用依次 ,setTimeout()只会在定时器结束后调用一次
` var timer = setInterval(function(){},1000);  `

clearInterval() 关闭定时器可以接收任意参数，如果参数是一个有效的定时器的标识，则停止对应的定时器，如果参数不是一个有效的标识，则什么也不做可以接收任意参数，如果参数是一个有效的定时器的标识，则停止对应的定时器，如果参数不是一个有效的标识，则什么也不做

### this指向问题
一般this指向的是调用它的对象
1、全局作用域或者普通函数中this指向的是全局对象window 定时器里面的this指向的是window
2、方法中谁调用this this指向谁
3、构造函数中this指向构造函数的实例

## JS执行机制
JS是单线程，同一时间只能做一件事
H5允许js中出现同步和异步

同步任务都在主线程上执行 形成执行栈

异步任务一般通过回调函数实现
分为三类1、普通事件如点击 2、资源加载 3、定时器

先执行栈中同步任务 遇到异步任务放入另一个任务队列中 一旦同步任务栈中执行完毕，将异步任务的任务队列中任务执行栈最下方执行

事件循环：主线程不断获得任务队列中的任务，执行任务，再获取任务，继续执行任务……

## location对象
window对象给我们提供一个location属性用于获取或者设置窗体的URL，并且可以用于解析URL，该属性返回的是一个对象，所以称为location对象

URL：统一资源定位符，互联网上标准资源的地址 互联网上每个资源都有唯一的URL，包含的信息指出资源的位置以及浏览器怎么处理它
` protocal://host[:port]/path/[?query]#fragment `
` 通信协议://主机（域名）[:端口号省略时使用默认端口]/路径/[?参数]#片段（锚点、链接） `

跳转页面
```html
<body>
    <!-- <button>
        跳转
    </button> -->
    <div></div>
    <script>
        // var btn = document.querySelector('button');
        var div = document.querySelector('div');
        // btn.addEventListener('click', function () {
        //     location.href = 'http://www.baidu.com';
        // })

        // 自动跳转
        var time1 = 5;
        setInterval(function () {
            // 时间为0时自动跳转到百度
            if (time1 == 0) {
                location.href = 'http://www.baidu.com';
            }
            // 利用定时器设置页面秒数变化
            else {
                div.innerHTML = '您的页面将在' + time1 + '后跳转';
                time1--;
            }
        }, 1000);
    </script>
</body>
```
assign()方法 用来跳转到其他的页面，作用和.href一样，记录；浏览历史可以实现后退功能 ` location.assign("http://www.baidu.com"); `

replace() 可以使用一个新的页面替换当前页面，调用完毕也会跳转页面，不会生成历史记录，不能使用回退按钮回退

reload()方法 用于重新加载当前页面，作用和刷新按钮一样 如果在方法中传递一个true，作为参数，则会强制清空缓存刷新页面
Ctrl + F5可以强制刷新

## Navigator对象
代表的当前浏览器的信息，通过该对象可以来识别不同的浏览器,由于历史原因，Navigator对象中的大部分属性都已经不能帮助我们识别浏览器了 一般我们只会使用userAgent来判断浏览器的信息， userAgent是一个字符串，这个字符串中包含有用来描述浏览器信息的内容， 不同的浏览器会有不同的userAgent
```js
            var ua = navigator.userAgent;
			console.log(ua);
			if(/firefox/i.test(ua)){
				alert("你是火狐！！！");
			}else if(/chrome/i.test(ua)){
				alert("你是Chrome");
			}else if(/msie/i.test(ua)){
				alert("你是IE浏览器~~~");
			}else if("ActiveXObject" in window){
				alert("你是IE11，枪毙了你~~~");
			}
```
## History对象
对象可以用来操作浏览器向前或向后翻页
length属性 可以获取到当成访问的链接数量
back()方法 可以用来回退到上一个页面，作用和浏览器的回退按钮一样
forward()方法 可以跳转下一个页面，作用和浏览器的前进按钮一样
go()方法 可以用来跳转到指定的页面，它需要一个整数作为参数： 1:表示向前跳转一个页面 相当于forward() 2:表示向前跳转两个页面 -1:表示向后跳转一个页面 -2:表示向后跳转两个页面

history对象在实际开发中使用较少，但是在一些OA系统中可能用到

# document对象
## 1 Document节点概述
document节点是文档的根节点，每张网页都有自己的document节点。
window.document属性就指向这个节点。也就是说，只要浏览器开始载入HTML文档，这个节点对象就存在了，可以直接调用。
`window.document === document`

document节点有不同的办法可以获取：

对于正常的网页，直接使用document或window.document。
对于iframe载入的网页，使用iframe节点的contentDocument属性。
对Ajax操作返回的文档，使用XMLHttpRequest对象的responseXML属性。
对于某个节点包含的文档，使用该节点的ownerDocument属性。

## 2 document节点的属性
document节点有很多属性，用得比较多的是下面这些。

### 2.1 doctype，documentElement，defaultView，body，head，activeElement
以下属性指向文档内部的某个节点

#### 2.1.1 doctype
对于HTML文档来说，document对象一般有两个子节点。第一个子节点是document.doctype，它是一个对象，包含了当前文档类型（Document Type Declaration，简写DTD）信息。对于HTML5文档，该节点就代表<!DOCTYPE html>。如果网页没有声明DTD，该属性返回null。
```js
var doctype = document.doctype;

doctype // "<!DOCTYPE html>"
doctype.name // "html"
```
document.firstChild通常就返回这个节点。

#### 2.1.2 documentElement
document.documentElement属性，表示当前文档的根节点（root）。它通常是document节点的第二个子节点，紧跟在document.doctype节点后面。

对于HTML网页，该属性返回HTML节点，代表<html lang="en">。

#### 2.1.3 defaultView
defaultView属性，在浏览器中返回document对象所在的window对象，否则返回null。
`var win = document.defaultView;`

#### 2.1.4 body
body属性返回当前文档的body或frameset节点，如果不存在这样的节点，就返回null。这个属性是可写的，如果对其写入一个新的节点，会导致原有的所有子节点被移除。

#### 2.1.5 head
head属性返回当前文档的head节点。如果当前文档有多个head，则返回第一个。
```js
document.head === document.querySelector('head')
```

#### 2.1.6 activeElement
activeElement属性返回当前文档中获得焦点的那个元素。用户通常可以使用tab键移动焦点，使用空格键激活焦点，比如如果焦点在一个链接上，此时按一下空格键，就会跳转到该链接。

### 2.2 documentURI，URL，domain，lastModified，location，referrer，title，characterSet
以下属性返回文档信息

#### 2.2.1 documentURI，URL
documentURI属性和URL属性都返回当前文档的网址。不同之处是documentURI属性是所有文档都具备的，URL属性则是HTML文档独有的。

#### 2.2.2 domain
domain属性返回当前文档的域名。比如，某张网页的网址是http://www.example.com/hello.html，domain属性就等于 www.example.com
如果无法获取域名，该属性返回null。

#### 2.2.3 lastModified
lastModified属性返回当前文档最后修改的时间戳，格式为字符串。
注意，lastModified属性的值是字符串，所以不能用来直接比较，两个文档谁的日期更新，需要用Date.parse方法转成时间戳格式，才能进行比较。
```js
if (Date.parse(doc1.lastModified) > Date.parse(doc2.lastModified)) {
  // ...
```

#### 2.2.4 location
location属性返回一个只读对象，提供了当前文档的URL信息。
```js
// 假定当前网址为http://user:passwd@www.example.com:4097/path/a.html?x=111#part1

// protocal : 通信协议 常用的http，ftp，maito等
// host     : 主机(域名)   比如www.test.com
// port     : 端口号 可选，省略时使用方案的默认端口 如http的默认端口为80
// path     : 路径  由零或多个'/'符号隔开的字符串，一般用来表示主机上的一个目录或文件地址
// query    : 参数 以键值对的形式，通过&分开
// fragment : 片段 #后面内容 常见于链接 锚点
document.location.href // "http://user:passwd@www.example.com:4097/path/a.html?x=111#part1"
document.location.protocol // "http:"
document.location.host // "www.example.com:4097"
document.location.hostname // "www.example.com"
document.location.port // "4097"
document.location.pathname // "/path/a.html"
document.location.search // "?x=111"
document.location.hash // "#part1"
document.location.user // "user"
document.location.password // "passed"

// 跳转到另一个网址
document.location.assign('http://www.google.com')
// 优先从服务器重新加载
document.location.reload(true)
// 优先从本地缓存重新加载（默认值）
document.location.reload(false)
// 跳转到另一个网址，但当前文档不保留在history对象中，
// 即无法用后退按钮，回到当前文档
document.location.assign('http://www.google.com')
// 将location对象转为字符串，等价于document.location.href
document.location.toString()
```

虽然location属性返回的对象是只读的，但是可以将URL赋值给这个属性，网页就会自动跳转到指定网址。
```js
document.location = 'http://www.example.com';
// 等价于
document.location.href = 'http://www.example.com';
```

document.location属性与window.location属性等价，历史上，IE曾经不允许对document.location赋值，为了保险起见，建议优先使用window.location。如果只是单纯地获取当前网址，建议使用document.URL。

#### 2.2.5 referrer
referrer属性返回一个字符串，表示前文档的访问来源，如果是无法获取来源或是用户直接键入网址，而不是从其他网页点击，则返回一个空字符串。

#### 2.2.6 title
title属性返回当前文档的标题，该属性是可写的。

document.title = '新标题';
#### 2.2.7 characterSet
characterSet属性返回渲染当前文档的字符集，比如UTF-8、ISO-8859-1。

### 2.3 readyState，designModed
以下属性与文档行为有关

#### 2.3.1 readyState
readyState属性返回当前文档的状态，共有三种可能的值，加载HTML代码阶段（尚未完成解析）是“loading”，加载外部资源阶段是“interactive”，全部加载完成是“complete”。

下面的代码用来检查网页是否加载成功。
```js
// 基本检查
if (document.readyState === 'complete') {
  // ...
}

// 轮询检查
var interval = setInterval(function() {
  if (document.readyState === 'complete') {
    clearInterval(interval);
    // ...
  }
}, 100);
```

#### 2.3.2 designModed
designMode属性控制当前document是否可编辑。通常会打开iframe的designMode属性，将其变为一个所见即所得的编辑器。

iframe_node.contentDocument.designMode = "on";

### 2.4 implementation，compatMode
以下属性返回文档的环境信息

#### 2.4.1 implementation
implementation属性返回一个对象，用来甄别当前环境部署了哪些DOM相关接口。implementation属性的hasFeature方法，可以判断当前环境是否部署了特定版本的特定接口。
```js
document.implementation.hasFeature( 'HTML, 2.0 ')
// true

document.implementation.hasFeature('MutationEvents','2.0')
// true
```
上面代码表示，当前环境部署了DOM HTML 2.0版和MutationEvents的2.0版。

#### 2.4.2 compatMode
compatMode属性返回浏览器处理文档的模式，可能的值为BackCompat（向后兼容模式）和 CSS1Compat（严格模式）。

### 2.5 anchors，embeds，forms，images，links，scripts，styleSheets
以下属性返回文档内部特定元素的集合。这些集合都是动态的，原节点有任何变化，立刻会反映在集合中

#### 2.5.1 anchors
anchors属性返回网页中所有的a节点元素。注意，只有指定了name属性的a元素，才会包含在anchors属性之中。

#### 2.5.2 embeds
embeds属性返回网页中所有嵌入对象，即embed标签，返回的格式为类似数组的对象（nodeList）。

#### 2.5.3 forms
forms属性返回页面中所有表单
```js
var selectForm = document.forms[index];
var selectFormElement = document.forms[index].elements[index];
```
上面代码获取指定表单的指定元素。

#### 2.5.4 images
images属性返回页面所有图片元素（即img标签）
```js
var ilist = document.images;

for(var i = 0; i < ilist.length; i++) {
  if(ilist[i].src == "banner.gif") {
    // ...
  }
}
```
上面代码在所有img标签中，寻找特定图片。

#### 2.5.5 links
links属性返回当前文档所有的链接元素（即a标签，或者说具有href属性的元素）。

#### 2.5.6 scripts
scripts属性返回当前文档的所有脚本（即script标签）。
```js
var scripts = document.scripts;
if (scripts.length !== 0 ) {
  console.log("当前网页有脚本");
}
```
#### 2.5.7 styleSheets
styleSheets属性返回一个类似数组的对象，包含了当前网页的所有样式表。该属性提供了样式表操作的接口。然后，每张样式表对象的cssRules属性，返回该样式表的所有CSS规则。这又方便了操作具体的CSS规则。
```js
var allSheets = [].slice.call(document.styleSheets);
```
上面代码中，使用slice方法将document.styleSheets转为数组，以便于进一步处理

## 3 Document对象的方法
document对象主要有以下一些方法
### 3.1 open()，close()，write()，writeln()
document.open()方法用于新建一个文档，供write方法写入内容。它实际上等于清除当前文档，重新写入内容。不要将此方法与window.open()混淆，后者用来打开一个新窗口，与当前文档无关。

document.close方法用于关闭open方法所新建的文档。一旦关闭，write方法就无法写入内容了。如果再调用write方法，就等同于又调用open方法，新建一个文档，再写入内容。

document.write方法用于向当前文档写入内容。只要当前文档还没有用close方法关闭，它所写入的内容就会追加在已有内容的后面。
```js
// 页面显示“helloworld”
document.open();
document.write("hello");
document.write("world");
document.close();
```
如果页面已经渲染完成（DOMContentLoaded事件发生之后），再调用write方法，它会先调用open方法，擦除当前文档所有内容，然后再写入。
```js
document.addEventListener("DOMContentLoaded", function(event) {
  document.write('<p>Hello World!</p>');
});

// 等同于

document.addEventListener("DOMContentLoaded", function(event) {
  document.open();
  document.write('<p>Hello World!</p>');
  document.close();
});
```
如果在页面渲染过程中调用write方法，并不会调用open方法。（可以理解成，open方法已调用，但close方法还未调用。）
```html
<html>
<body>
hello
<script type="text/javascript">
  document.write("world")
</script>
</body>
</html>
```
在浏览器打开上面网页，将会显示“hello world”。

需要注意的是，虽然调用close方法之后，无法再用write方法写入内容，但这时当前页面的其他DOM节点还是会继续加载。
```html
<html>
<head>
<title>write example</title>
<script type="text/javascript">
  document.open();
  document.write("hello");
  document.close();
</script>
</head>
<body>
world
</body>
</html>
```
在浏览器打开上面网页，将会显示“hello world”。

总之，除了某些特殊情况，应该尽量避免使用document.write这个方法。
document.writeln方法与write方法完全一致，除了会在输出内容的尾部添加换行符。
```js
document.write(1);
document.write(2);
// 12

document.writeln(1);
document.writeln(2);
// 1
// 2
//
```
注意，writeln方法添加的是ASCII码的换行符，渲染成HTML网页时不起作用。

### 3.2 hasFocus()
document.hasFocus方法返回一个布尔值，表示当前文档之中是否有元素被激活或获得焦点。

focused = document.hasFocus();
注意，有焦点的文档必定被激活（active），反之不成立，激活的文档未必有焦点。比如如果用户点击按钮，从当前窗口跳出一个新窗口，该新窗口就是激活的，但是不拥有焦点。

### 3.3 选中当前文档中的元素 重要
#### 3.3.1 querySelector()
querySelector方法返回匹配指定的CSS选择器的元素节点。如果有多个节点满足匹配条件，则返回第一个匹配的节点。如果没有发现匹配的节点，则返回null。
```js
var el1 = document.querySelector('.myclass');
var el2 = document.querySelector('#myParent > [ng-click]');
```
querySelector方法无法选中CSS伪元素

#### 3.3.2 getElementById()
getElementById方法返回匹配指定ID属性的元素节点。如果没有发现匹配的节点，则返回null。

`var elem = document.getElementById("para1");`
注意，在搜索匹配节点时，id属性是大小写敏感的。比如，如果某个节点的id属性是main，那么document.getElementById("Main")将返回null，而不是指定节点。

getElementById方法与querySelector方法都能获取元素节点，不同之处是querySelector方法的参数使用CSS选择器语法，getElementById方法的参数是HTML标签元素的id属性。
```js
document.getElementById('myElement')
document.querySelector('#myElement')
```
上面代码中，两个方法都能选中id为myElement的元素，但是getElementById()比querySelector()效率高得多。

#### 3.3.3 querySelectorAll()
querySelectorAll方法返回匹配指定的CSS选择器的所有节点，返回的是NodeList类型的对象。NodeList对象不是动态集合，所以元素节点的变化无法实时反映在返回结果中。
```js
const elementList = document.querySelectorAll(selectors);
// querySelectorAll方法的参数，可以是逗号分隔的多个CSS选择器，返回所有匹配其中一个选择器的元素。

var matches = document.querySelectorAll('div.note, div.alert');
```
上面代码返回class属性是note或alert的div元素。

querySelectorAll方法支持复杂的CSS选择器。
```js
// 选中data-foo-bar属性等于someval的元素
document.querySelectorAll('[data-foo-bar="someval"]');

// 选中myForm表单中所有不通过验证的元素
document.querySelectorAll('#myForm :invalid');

// 选中div元素，那些class含ignore的除外
document.querySelectorAll('DIV:not(.ignore)');

// 同时选中div，a，script三类元素
document.querySelectorAll('DIV, A, SCRIPT');
```
如果querySelectorAll方法和getElementsByTagName方法的参数是字符串“*”，则会返回文档中的所有HTML元素节点。

与querySelector方法一样，querySelectorAll方法无法选中CSS伪元素。

#### 3.3.4 getElementsByClassName()
getElementsByClassName方法返回一个类似数组的对象（HTMLCollection类型的对象），包括了所有class名字符合指定条件的元素（搜索范围包括本身），元素的变化实时反映在返回结果中。这个方法不仅可以在document对象上调用，也可以在任何元素节点上调用。
```js
// document对象上调用
var elements = document.getElementsByClassName(names);
// 非document对象上调用
var elements = rootElement.getElementsByClassName(names);
// getElementsByClassName方法的参数，可以是多个空格分隔的class名字，返回同时具有这些节点的元素。

document.getElementsByClassName('red test');
```
上面代码返回class同时具有red和test的元素。

#### 3.3.5 getElementsByTagName()
getElementsByTagName方法返回所有指定标签的元素（搜索范围包括本身）。返回值是一个HTMLCollection对象，也就是说，搜索结果是一个动态集合，任何元素的变化都会实时反映在返回的集合中。这个方法不仅可以在document对象上调用，也可以在任何元素节点上调用。
```js
var paras = document.getElementsByTagName("p");
```
上面代码返回当前文档的所有p元素节点。

注意，getElementsByTagName方法会将参数转为小写后，再进行搜索。

#### 3.3.6 getElementsByName()
getElementsByName方法用于选择拥有name属性的HTML元素，比如form、img、frame、embed和object，返回一个NodeList格式的对象，不会实时反映元素的变化。
```js
// 假定有一个表单是<form name="x"></form>
var forms = document.getElementsByName("x");
forms[0].tagName // "FORM"
```
注意，在IE浏览器使用这个方法，会将没有name属性、但有同名id属性的元素也返回，所以name和id属性最好设为不一样的值。

#### 3.3.7 elementFromPoint()
elementFromPoint方法返回位于页面指定位置的元素。

`var element = document.elementFromPoint(x, y);`
上面代码中，elementFromPoint方法的参数x和y，分别是相对于当前窗口左上角的横坐标和纵坐标，单位是CSS像素。elementFromPoint方法返回位于这个位置的DOM元素，如果该元素不可返回（比如文本框的滚动条），则返回它的父元素（比如文本框）。如果坐标值无意义（比如负值），则返回null。

### 3.4 createElement()，createTextNode()，createAttribute()，createDocumentFragment()
以下方法用于生成元素节点

#### 3.4.1 createElement()
createElement方法用来生成HTML元素节点。
```js
var element = document.createElement(tagName);
// 实例
var newDiv = document.createElement("div");
```
createElement方法的参数为元素的标签名，即元素节点的tagName属性。如果传入大写的标签名，会被转为小写。如果参数带有尖括号（即<和>）或者是null，会报错。

#### 3.4.2 createTextNode()
createTextNode方法用来生成文本节点，参数为所要生成的文本节点的内容。
```js
var newDiv = document.createElement("div");
var newContent = document.createTextNode("Hello");
newDiv.appendChild(newContent);
```
上面代码新建一个div节点和一个文本节点，然后将文本节点插入div节点。

#### 3.4.3 createAttribute()
createAttribute方法生成一个新的属性对象节点，并返回它。
```js
attribute = document.createAttribute(name);
createAttribute方法的参数name，是属性的名称。

var node = document.getElementById("div1");
var a = document.createAttribute("my_attrib");
a.value = "newVal";
node.setAttributeNode(a);

// 等同于

var node = document.getElementById("div1");
node.setAttribute("my_attrib", "newVal");
```
#### 3.4.4 createDocumentFragment()
createDocumentFragment方法生成一个DocumentFragment对象。

`var docFragment = document.createDocumentFragment();`
DocumentFragment对象是一个存在于内存的DOM片段，但是不属于当前文档，常常用来生成较复杂的DOM结构，然后插入当前文档。这样做的好处在于，因为DocumentFragment不属于当前文档，对它的任何改动，都不会引发网页的重新渲染，比直接修改当前文档的DOM有更好的性能表现。

```js
var docfrag = document.createDocumentFragment();

[1, 2, 3, 4].forEach(function(e) {
  var li = document.createElement("li");
  li.textContent = e;
  docfrag.appendChild(li);
});

document.body.appendChild(docfrag);
```
### 3.5 createEvent()
createEvent方法生成一个事件对象，该对象可以被element.dispatchEvent方法使用，触发指定事件。

var event = document.createEvent(type);
createEvent方法的参数是事件类型，比如UIEvents、MouseEvents、MutationEvents、HTMLEvents。
```js
var event = document.createEvent('Event');
event.initEvent('build', true, true);
document.addEventListener('build', function (e) {
  // ...
}, false);
document.dispatchEvent(event);
```
### 3.6 createNodeIterator()，createTreeWalker()
以下方法用于遍历元素节点

#### 3.6.1 createNodeIterator()
createNodeIterator方法返回一个DOM的子节点遍历器。
```js
var nodeIterator = document.createNodeIterator(
  document.body,
  NodeFilter.SHOW_ELEMENT
);
```
上面代码返回body元素的遍历器。createNodeIterator方法的第一个参数为遍历器的根节点，第二个参数为所要遍历的节点类型，这里指定为元素节点。其他类型还有所有节点（NodeFilter.SHOW_ALL）、文本节点（NodeFilter.SHOW_TEXT）、评论节点（NodeFilter.SHOW_COMMENT）等。

所谓“遍历器”，在这里指可以用nextNode方法和previousNode方法依次遍历根节点的所有子节点。
```js
var nodeIterator = document.createNodeIterator(document.body);
var pars = [];
var currentNode;

while (currentNode = nodeIterator.nextNode()) {
  pars.push(currentNode);
}
```
上面代码使用遍历器的nextNode方法，将根节点的所有子节点，按照从头部到尾部的顺序，读入一个数组。nextNode方法先返回遍历器的内部指针所在的节点，然后会将指针移向下一个节点。所有成员遍历完成后，返回null。previousNode方法则是先将指针移向上一个节点，然后返回该节点。
```js
var nodeIterator = document.createNodeIterator(
  document.body,
  NodeFilter.SHOW_ELEMENT
);

var currentNode = nodeIterator.nextNode();
var previousNode = nodeIterator.previousNode();

currentNode === previousNode // true
```
上面代码中，currentNode和previousNode都指向同一个的节点。

有一个需要注意的地方，遍历器返回的第一个节点，总是根节点。

#### 3.6.2 createTreeWalker()
createTreeWalker方法返回一个DOM的子树遍历器。它与createNodeIterator方法的区别在于，后者只遍历子节点，而它遍历整个子树。

createTreeWalker方法的第一个参数，是所要遍历的根节点，第二个参数指定所要遍历的节点类型。
```js
var treeWalker = document.createTreeWalker(
  document.body,
  NodeFilter.SHOW_ELEMENT
);

var nodeList = [];

while(treeWalker.nextNode()) nodeList.push(treeWalker.currentNode);
```
上面代码遍历body节点下属的所有元素节点，将它们插入nodeList数组。

### 3.7 adoptNode()，importNode()
以下方法用于获取外部文档的节点

#### 3.7.1 adoptNode()
adoptNode方法将某个节点，从其原来所在的文档移除，插入当前文档，并返回插入后的新节点。

node = document.adoptNode(externalNode);
importNode方法从外部文档拷贝指定节点，插入当前文档。

var node = document.importNode(externalNode, deep);
#### 3.7.2 importNode()
importNode（）方法用于创造一个外部节点的拷贝，然后插入当前文档。它的第一个参数是外部节点，第二个参数是一个布尔值，表示对外部节点是深拷贝还是浅拷贝，默认是浅拷贝（false）。虽然第二个参数是可选的，但是建议总是保留这个参数，并设为true。

另外一个需要注意的地方是，importNode方法只是拷贝外部节点，这时该节点的父节点是null。下一步还必须将这个节点插入当前文档的DOM树。

var iframe = document.getElementsByTagName("iframe")[0];
var oldNode = iframe.contentWindow.document.getElementById("myNode");
var newNode = document.importNode(oldNode, true);
document.getElementById("container").appendChild(newNode);
上面代码从iframe窗口，拷贝一个指定节点myNode，插入当前文档。
————————————————



# JSON
JavaScript Object Notation

JS中的对象只有JS自己认识，其他的语法都不认识 JSON就是一个特殊格式的字符串，这个字符串可以被任意语言所识别，并且可以转换为任意语言中的对象，JSON在开发中主要用于数据的交互

JSON和JS对象的格式一样，只不过JSON字符串中的属性名必须加双引号，其他和JS一致

常用JSON分类： 1.对象{} 2.数组[]
JSON允许的值： 1.字符串 2.数值 3.布尔值 4.null 5.对象 6.数组

```
	var obj = '{
        "name" : "jrj",
        "age" : 18,
        "gender" : "男"
    }';

	var arr = '[1, 2, 3, "hello", true]';
```

## JSON → JS
JSON.parse();
将JSON字符串转换为JS对象,需要一个JSON字符串作为参数，将字符串转为JS对象返回
```
var json = '{"name":"jrj","age":18,"gneder":"男"}';
// 将JSON字符串转换为JS对象
var o = JSON.parse(json);
console.log(o.name);
```
## JS → JSON
JSON.stringify
```js
var obj = {
    name:"jrj",
    age:18,
    gender:"男"
}
var aaa = JSON.stringify(obj);
console.log(aaa);
// {"name":"jrj","age":18,"gender":"男"}
```
JSON这个对象在IE7及以下不支持，若要兼容IE，可引入外部JS文件


-----------------------------------------


# 数据类型
## 基本类型
string
number
boolean
undefined
null
## 引用类型(对象类型)

object
function(特别对象，可执行)
array(特别对象，内部数据有序)

## 判断数据类型
### typeof

返回数据类型的**字符串**
不能判断null与object，都返回object
也不能判断array与object，都返回object

### instanceof(实例)

判断引用类型的具体类型
输出结果是布尔值

`a instanceof Object/Function/Array`
函数、数组也是对象

### ===

可用来判断undefined和null

## undefined和null的区别？

undefined表示定义了未赋值

null表示定义了且赋值，值为null

## 什么时候给变量赋值为null？

初始赋值为null，表明将要赋值为对象

```js
var a = null // 表明将要赋值为对象
a = []
a = null // 释放数组占用的内存
```
最后赋值为null，将指向对象变为垃圾对象使其被回收，释放内存

## 严格区分数据类型和变量类型

变量的类型（变量内存值的类型）
基本类型：保存的值为基本类型
引用类型：保存的值为对象的地址
# 变量参数传递是值传递，值可能是基本类型也可能是对象的地址值

注意区分如下：**保存的地址值和指向的对象**
```js
        var a = {
            // 把对象的地址值给a
            // 对象本身在堆里面，标识对象的地址值保存在栈
            age: 12
        }
        function changeAge(a) {
            a.age = 17;
        }
        changeAge(a);
        // a和函数里面的形参a不同地址 ，指向同一个对象
        // 修改的也是同一个对象
        console.log(a.age);// 17
```

```js
        var a = {
            // 把对象的地址值给a
            // 对象本身在堆里面，标识对象的地址值保存在栈
            age: 12
        }
        function changeAge(a) {
            // 函数的形参原本和最上面的a指向同一个对象 即age=12
            a = {age: 17};
            // 此时函数的形参指向新对象 age=17

            // 真实的a是另一个对象，并没有发生过改变
        }
        changeAge(a);// 把a赋值到形参
        console.log(a.age);// 12
```

**注意：如果使用const声明对象，是不能改成另一个对象的**

```js
        var a = 3
        function add(a) {
            a = a + 1;
            console.log(a);// 4
        }
        add(a);// 函数传递的是数据3
        console.log(a);// 3
        // 按照变量作用域理解吧
```

# JS引擎如何管理内存
## 内存的生命周期
1.分配内存，得到使用权
2.存储数据，可以进行反复操作
3.释放内存空间

## 释放内存

1.局部变量：函数执行完自动释放,注意闭包不同
2.对象：成为垃圾对象>垃圾回收器回收

--------------------

# 对象
方法是一种特殊的属性
属性值是函数的属性是方法
## 什么时候必须使用["属性名"]的方式读取或修改对象属性？
使用`[]`包裹的属性名要加引号，表示是字符串
不加引号表示变量

1、属性名包含特殊字符：- 空格
```js
var p = {}
// p.a-b = 'aaa';// error
p['a-b'] = 'aaa';
console.log(p['a-b']);// aaa
```
2、属性名为变量
```js
var p ={}
var a1 = 'myAge';
var value = 18;
// p.a1 = value;
// console.log(p);
// 输出a1: 18 而不是myAge: 18
// a1应该是保存属性名的变量

p[a1] = value;
console.log(p);
// 输出myAge: 18
```

# 函数

函数是n条语句的封装体，方便复用
只有函数是能够执行的，其他数据不能被执行
提高代码复用，便于阅读交流
```
        fun1();
        // fun2(); // error

        // 函数声明方式
        // 可以提前使用
        function fun1() {
            console.log('你好');
        }
        fun1();

        // 表达式方式定义函数
        // 没法提前使用
        var fun2 = function(){
            console.log('表达式方式');
        }
        fun2();
```

```
        var obj1 = {}
        function fun3(){
            console.log('你妈');
        }
        // 可以让一个函数作为指定任意对象的方法进行调用
        fun3.call(obj1);// 通过函数.call(对象) 让函数称为对象的方法 再调用函数

```

## 回调函数

DOM事件回调函数
```js
        var btn = document.querySelector('button');
        btn.addEventListener('click',function(){
            // 动态获取文本
            alert(this.innerHTML);
        })
```

定时器回调函数
```
        setTimeout(function(){
            alert(123456);
        },2000)
```

网络请求回调函数
后面讲

生命周期回调函数
后面讲

## IIFE
`immediately invoked function expression`
立即执行函数，匿名函数自调用
```js
    (function (){
        var a= 3;
        console.log(a+2);
    })();

    // ES6
    ((a=3) => console.log(a+2))()
```
作用：隐藏实现，不污染全局命名空间，编写JS模块

## this
解析器在调用函数每次都会向函数内部传递进一个隐含的参数,这个隐含的参数就是this，this指向的是一个对象
这个对象我们称为函数执行的 **上下文对象**，根据函数的调用方式的不同，this会指向不同的对象

1.以函数的形式调用时，this永远都是window

2.以方法的形式调用时，this就是调用方法的那个对象

3.当以构造函数的形式调用时，this就是新创建的那个对象（实例）

4.使用call()l和apply()调用时，this是指定的那个对象

5.在事件的响应函数中，响应函数是给谁绑定的，this就是谁

-------------------

# 函数高级

# 原型与原型链（实例对象的隐式原型指向其构造函数的显式原型）

## 函数的prototype属性

每个函数都有prototype属性，它默认指向一个空对象，即（显式）原型对象
而`原型对象`中有一个属性为constructor（构造函数），它指向自己(构造函数对象)

*构造函数和它自己的原型对象 相互引用*

```js
console.log(fun1.prototype.constructor === fun1);
// true
```

给 `原型对象` 添加属性（一般都是方法）
作用：所有实例对象自动拥有原型中的属性（方法）
```js
        function fun1(){

        }
        // 每个函数都有prototype属性
        // 函数名.prototype 是一个对象

        // js可以给对象动态添加属性
        // 添加一个方法
        fun1.prototype.test = function(){
            console.log('test()');
        }
        console.log(fun1.prototype);
```

## 显式原型与隐式原型

每个函数function都有一个prototype属性，即**显式原型**，默认指向空Object(实例对象).在定义函数时自动添加

每个实例对象都有一个__proto__属性，即**隐式原型**，在创建实例对象时自动添加，

**实例对象的隐式原型指向其对应构造函数的显示原型的值**
（↑ 重要！！）
```js
    function Fn(){
    }
    console.log(Fn.prototype);
    var fn = new Fn();
    console.log(fn.__proto__);
    console.log(Fn.prototype === fn__proto__);// true

```
```js
        // 创建函数对象
        // Fn是变量名，保存的是对象的地址值
        function Fn(){
            this.test1 = function(){
                console.log('test1');
            }
        }
        console.log(Fn.prototype);
        // 每个函数function都有一个prototype属性，即显示原型，默认指向空Object（原型对象）

        // 给原型对象添加一个方法test2
        Fn.prototype.test2 = function(){
            console.log('test2');
        }
        var fn = new Fn()
```
**构造函数对象显式原型的值等于实例对象隐式原型的值**

## 原型链（隐式原型链）

访问一个实例对象的属性时
1.先在实例对象自身的属性中查找，找到返回
2.否则，沿着实例对象的__proto__向上查找，找到返回
3.否则，返回undefined
注意：
1.所有函数都是Function(包括Function本身)的实例
2.Object的原型对象是原型链的尽头

### 设置和查找
查找一般是先找自身，没有就沿着原型链溯源
但是设置是直接设在自己身上
```js
    function Fn() {}
    // 在原型对象上添加属性a:'jjj'
    Fn.prototype.a = 'jjj'

    const fn1 = new Fn()
    const fn2 = new Fn()
    fn2.a = 'yyy'

    console.log(fn1.a,fn1);
    // 查找自身有没有a，若没有则沿着原型链（隐式原型-显式原型-原型对象上）找
    // jjj
    console.log(fn2.a,fn2);
    // 给实例设置属性的时候，则直接设置给实例自身
    // yyy
```

## 原型继承

构造函数的实例对象自动拥有构造函数原型对象的属性和方法

## 详解instanceof

A instanceof B
即判断A是不是B的实例
如果B构造函数的显式原型对象在A实例对象的隐式原型链上，则返回true，否则返回false
```js
const arr = []

console.log(arr instanceof Array);// true
console.log(arr instanceof Object);// true

const fn = () => {}
console.log(fn instanceof Function);// true
console.log(fn instanceof Object);// true

// const Ff = () => {}
    // 我们再来回顾下new的执行过程:
    // 1、创建一个空对象
    // 2、将该对象的__proto__指向到构造函数（比如Fn）的原型prototype
    // 3、修改构造函数Fn的上下文到空对象
    // 4、返回空对象（其实就是this）
    // 箭头函数不满足第二步，也就是没有原型prototype，因此无法使用new关键字创建箭头函数的实例对象

function Ff(){}
const ff = new Ff
console.log(ff);
console.log(ff instanceof Ff);// true
console.log(ff instanceof Function);// false
// 因为ff实例对象的原型链上没有Function
// ff.__proto__ === Ff.prototype → object(原型对象)
// object.__proto__ === Object.prototype
// Object.__proto__ === null  (原型链的尽头)
// 可见，中间并不经过Function
console.log(ff instanceof Object);// true

console.log(Ff.prototype instanceof Object);// true
console.log(ff.__proto__ instanceof Object);// true



console.log(Object instanceof Function);// true
console.log(Object instanceof Object);// true
console.log(Function instanceof Function);// true
console.log(Function instanceof Object);// true

function Fun(){}
console.log(Object instanceof Fun);// false
```
### 自己的思考
**所谓的=== 和指向 其实含义相同，因为实例对象的隐式原型也好，构造函数的显示原型也好，都是保存的地址值，地址值全等，也可以说隐式原型指向显示原型，显示原型指向原型对象（一个object）**

## 原型题目
1
```js
    // 创建构造函数
    var A = function(){}
    // 向实例对象中添加属性n 赋值 1
    A.prototype.n = 1;
    // 创建实例对象
    var b = new A();
    // 此时构造函数的显示原型指向新的原型对象（地址改变）
    A.prototype = {
        n:2,
        m:3
    }
    var c = new A();
    console.log(b.n,b.m,c.n,c.m);
    // 1 undefined 2 3
```

2
```js
    // 定义函数
    var F = function(){}
    // 给Object原型添加a方法
    Object.prototype.a = function(){
        console.log('a()');
    }
     // 给Function原型添加b方法
    Function.prototype.b = function(){
        console.log('b()');
    }
    var f = new F();
    f.a() // a()
    f.b()// error
    F.a()// a()
    F.b()// b()
    // 注：此部分理解原型链图
```

## 原型链图
![原型链](./images/js高级/src=http___img2018.cnblogs.com_blog_1762947_201909_1762947-20190926094156327-588943618.jpg&refer=http___img2018.cnblogs.jpg)

# 变量声明提升

var 提前申明，但不赋值，执行到才赋值，执行之前访问为undefined

ES6的const 和let都没有变量提升，声明前存在死区，使用变量即报错

# 函数申明提升

function 提前申明，可直接调用
函数提升必须使用声明的方式
` function fun(){} `

# 全局执行上下文

1.在执行全局代码前将window确定为全局执行上下文

2.对全局数据进行预解析
    var 定义的全局变量为undefined，添加为window的属性
    function声明的全局函数赋值，添加为window的方法
    this赋值为window

3.开始执行全局代码

# 函数执行上下文

1.在调用函数，准备执行函数体之前，创建对应的函数执行上下文对象

2.对局部数据进行预解析
    形参变量→赋值（实参）→添加为执行上下文的属性
    arguments→赋值（所有实参列表 伪数组）→添加为执行上下文的属性
    定义的局部变量→赋值undefined，添加为执行上下文的属性
    function声明的函数→赋值，添加为执行上下文的方法
    this→赋值

3.开始执行函数代码

```js
    var c = 1;
    function C(c) {
        console.log(c);// 输出传递的实参 3
        var c = 2;// 声明提前 值undefined
    }
    C(3);
    // 3
```

# 执行上下文栈

1.在全局代码执行前，JS引擎就会创建一个栈来存储管理所有的执行上下文对象

2.在全局执行上下文（window）确定后，将其添加到栈中，进行压栈操作

3.在函数执行上下文创建后，将其添加到栈中，进行压栈操作

4.在当前函数执行完后，将栈顶对象移除，出栈

5.所有代码执行完后，栈中只剩下window

例题1
```js
        var i = 1;
        fun(1);// 函数声明提前 直接调用
        function fun(i){
            if(i == 4){
                return;
            }
            console.log(i);
            fun(i+1);// 递归调用
            console.log(i);

        }
        console.log(i);
        // 1 2 3 | 3 2 1 1
```
例2
```js
        function a(){}
        var a;
        console.log(typeof a);
        // function
        // 先执行变量提升 再执行函数提升

```
例3
```js
        if (!(b in window)) {
            var b = 1;
        }
        console.log(b);
        // undefined
```
例4
```js
        var c = 1;
        function c(c){
            console.log(c);
            var c = 3;
        }
        c(2);
        // error


        // 相当于

        var c;// 变量提升
        // 函数提升
        function c(c){
            console.log(c);
            var c = 3;
        }
        // 变量赋值
        c = 1;
        // 此时c不是函数 所以报错
        c(2);
```

# 作用域与作用域链

## 作用域

代码段所在的区域，它是静态的，在编写代码时就确定了

分类：全局作用域，函数作用域，ES6块级作用域

作用：隔离变量，不同作用域下同名变量不会有冲突

# 作用域与执行上下文

区别1：
全局作用域之外，每个函数都会创建自己的作用域，作用域在`函数定义时就已经确定`了，而不是在函数调用时

全局执行上下文是在全局作用域确定之后，js代码马上执行前创建
函数执行上下文是在`调用函数时`，函数体代码执行前创建

区别2：
作用域是`静态`的，只要函数定义好就一直存在，且不会再变化
执行上下文是`动态`的，调用函数时创建，函数调用结束时执行上下文出栈，执行上下文释放

联系：
执行上下文从属于所在的作用域

例1
```js
let x = 10
function fn(){
    console.log(x);
}

function show(f){
    let x = 20
    f()
    // 注意此时执行f(),会去fn函数的局部作用域找x
    // 找不到沿着作用域链找到全局x = 10
}
show(fn)
// 10
```

例2
```js
const obj = {
    fn(){
        console.log(fn);
    }
}
obj.fn()
// 报错
// obj的{}里面不形成一层作用域，函数外面直接是全局，全局没有fn
```
# 闭包
```js
        // 实现按钮对应弹出消息框
        var btns = document.querySelectorAll('button');
        // 闭包
        for (var i = 0; i < btns.length; i++) {
            (function (i) {
                var btn = btns[i];
                btn.onclick = function () {
                    alert('第' + (i + 1) + '个');
                }
            })(i);

        }

        // for (var i = 0; i < btns.length; i++) {
        //     var btn = btns[i];
        //     btn.index = i;
        //     btn.onclick = function () {
        //         alert('第' + (this.index + 1) + '个');
        //     }
        // }
```

## 如何产生闭包?

当嵌套的内部子函数引用了外部的父函数的变量（或函数），执行外部父函数就产生了闭包，外部函数执行几次，产生几个闭包

## 闭包到底是什么?

闭包是上文所说嵌套的内部子函数与其对外部函数变量或者函数的引用整体称为闭包
>闭包就是能够读取其他函数内部变量的函数。                       ——阮一峰



## 闭包的作用

1.使用函数内部的变量在函数执行完后，仍然存活在内存中（延长了局部变量的生命周期）

2.让函数外部可以操作（读写）到函数内部的数据（变量/函数）

3.将私有数据和一些功能函数封装到同一的函数里面，然后向外暴露，形成独立的功能模块

## 闭包的缺点及解决：内存溢出与内存泄漏
因为闭包中使用到的局部变量将一直存在，无法释放
及时释放嵌套内函数对象，使之成为垃圾对象被回收

```js
function fun(n, o) {
    console.log(o)
    return {
        fun: function (m) {
            return fun(m, n)
        }
    }
}

var a = fun(0) //undefined
a.fun(1) //0
a.fun(2) //0
a.fun(3) //0

var b = fun(0).fun(1).fun(2).fun(3) // undefined 0 1 2

var c = fun(0).fun(1) // undefined 0
c.fun(2) //1
c.fun(3) //1
```


-----------

# 对象高级
## new Object()构造函数模式

## 对象字面量 使用{}创建

## 工厂模式
缺点：对象没有具体类型，都是object
```js
function Person (name,age) {
    const obj = {
        name:name,
        age:age,
        setName:function(){
            this.name = name
        }
    }
    return obj
}

const p1 = new Person('jrj',16)
const p2 = new Person('Bob',22)
console.log(p1);
```

## 自定义构造函数模式
不同的对象拥有具体的类型
`console.log(p1 instanceof Person);`
```js
function Person(name, age) {
    this.name = name
    this.age = age
    this.setName = function (name) {
        this.name = name
    }
}

const p1 = new Person('jrj', 16)
const p2 = new Person('Bob', 22)
p2.setName('HHH')
console.log(p1)
console.log(p2);;
```

## 构造函数+原型
把方法放到共同的原型上
```js
function Person(name, age) {
    // 构造函数中只初始化一般属性
    this.name = name
    this.age = age
}
Person.prototype.setName = function (name) {
    this.name = name
}

const p1 = new Person('jrj', 16)
const p2 = new Person('Bob', 22)
p2.setName('HHH')
console.log(p1)
console.log(p2)
```

## 继承模式
### 1、原型链继承
**子类型的原型对象应为父类型的实例对象**
```js
function Father() {
    this.fatherProp = 'father'
}
Father.prototype.showFatherProp = function () {
    console.log(this.fatherProp);
}

function Son() {
    this.sonProp = 'son'
}

// 为了实现原型链继承
// 子类型的原型对象应为父类型的实例对象
Son.prototype = new Father()

const s1 = new Son()
s1.showFatherProp()// 'Father'

console.log(s1.__proto__ === Son.prototype);// true
console.log(Son.prototype.__proto__ === Father.prototype);
// true
```

### 2、借用构造函数
即通过js的apply、call实现子类调用父类的属性、方法；
原型链方式可以实现所有属性方法共享，但无法做到属性、方法独享（例如Sub1修改了父类的函数，其他所有的子类Sub2、Sub3...想调用旧的函数就无法实现了）；而借用构造函数除了能独享属性、方法外还能在子类构造函数中传递参数，但代码无法复用。总体而言就是可以实现所有属性方法独享，但无法做到属性、方法共享（例如，Sub1新增了一个函数，然后想让Sub2、Sub3...都可以用的话就无法实现了，只能Sub2、Sub3...各自在构造函数中新增）


### 3、组合继承
结合以上

### 4、最优解：寄生+组合继承

----------

**待续**

--------

# 进程与线程

-----
**不完善**

---

## 进程（process）

程序的一次执行，它占有一片独有的内存空间

## 线程（thread）

是进程内的一个独立执行单元
是程序执行的一个完整流程
是CPU的最小的调度单元

## 相关知识

应用程序必须运行在一个进程的某个线程上

一个进程中至少有一个运行的线程：主线程，进程启动后会自动创建

一个进程中也可以同时运行多个线程，我们就会说程序是多线程运行的

一个进程内的数据可以供其中的线程直接共享

多个进程之间的数据是不能直接共享的

线程池（thread pool）：保存多个线程对象的容器，实现线程对象的反复利用

## JS是单线程运行

但使用H5中的Web Workers可以多线程运行

## 单线程/多线程优缺点
### 多线程
优点：
+ 有效提升CPU效率

缺点：
+ 创建多线程开销
+ 线程切换开销
+ 死锁和状态同步问题

### 单线程
优点：
+ 顺序编码简单易懂

缺点：
+ 效率低

## 浏览器是多线程运行的
但是有的是单进程（Firefox 老IE）
有的是多进程（Chrome 新IE）

# 浏览器内核
浏览器内核由多模块组成

Chrome | Safari ：webkit

firefox：Gecko

IE : Trident

国内：Trident + webkit

## 主线程

### JS引擎线程

负责JS程序的编译和运行

### GUI 渲染线程

负责渲染浏览器界面，解析HTML，CSS，构建DOM树和RenderObject树，布局和绘制等。
当界面需要重绘（Repaint）或由于某种操作引发回流(reflow)时，该线程就会执行
注意，GUI渲染线程与JS引擎线程是互斥的，当JS引擎执行时GUI线程会被挂起（相当于被冻结了），GUI更新会被保存在一个队列中等到JS引擎空闲时立即被执行。

### GUI 渲染线程 与 JavaScript引擎线程互斥！
由于JavaScript是可操纵DOM的，如果在修改这些元素属性同时渲染界面（即JavaScript线程和UI线程同时运行），那么渲染线程前后获得的元素数据就可能不一致了。因此为了防止渲染出现不可预期的结果，浏览器设置GUI渲染线程与JavaScript引擎为互斥的关系，当JavaScript引擎执行时GUI线程会被挂起，GUI更新会被保存在一个队列中等到引擎线程空闲时立即被执行。这也是为什么`<script>`标签要放到body标签的最底部。

## 分线程

### 定时器线程

负责定时器的管理

### DOM事件响应线程

负责DOM事件的管理

### 网络请求线程

如Ajax请求

## JS引擎执行代码的基本流程

先执行初始化代码，包含一些特别的代码如：设置定时器，绑定事件监听，发送AJAX请求

后面在某个时刻才会执行回调代码

# H5 Web Workers多线程

H5规范提供了JS分线程的实现，取名为Web Workers
我们可以将一些大计算量的代码交由Web Workers运行而不冻结用户界面
但是子线程完全受主线程控制，且不得操作DOM，worker的全局对象不是window
所以这个新标准并没有改变JavaScript单线程的本质
不能跨域
不是所有浏览器都支持



##
```js
// 通用处理dom元素的工具对象(包含兼容)
    var EventUtil = {
      // 添加事件监听
      addHandler: function (element, type, handler) {
        if(element.addEventListener) {
          element.addEventListener(type, handler, false);// DOM2级事件
        } else if (element.attachEvent) {
          element.attachEvent('on' + type, handler);// DOM2级IE事件
        } else {
          element['on' + type] = handler;// DOM0级事件
        }
      },

      // 移除事件监听
      removeHandler: function (element, type, handler) {
        if (element.removeEventListener) {
          element.removeEventListener(type, handler, false);
        } else if (element.detachEvent) {
          element.detachEvent('on' + type, handler);
        } else {
          element['on' + type] = null;
        }
      },

      // 获取事件对象
      getEvent: function (event) {
        return event ? event : window.event;
      },

      // 获取事件的目标元素
      getTarget: function (event) {
        return event.target || event.srcElement;
      },

      // 禁止事件默认行为
      preventDefault: function (event) {
        if (event.preventDefault) {
        event.preventDefault();
        } else {
          event.returnValue = false;
        }
      },

      // 禁止事件冒泡
      stopPropagation: function (event) {
        if (event.stopPropagation) {
          event.stopPropagation();
        }else {
          event.cancelBubble = true;
        }
      }
    };
```


# SVG

矢量图 不会随着放大失真
相比于像素图片png jpg等
```xml
  <svg width="100%" height="100%">
    <circle id='myCircle' cx='50' cy='50' r='50'/>
  </svg>
```

## 属性和元素

### fill
表示颜色 fill='orange'

### viewBox
如果只想展示svg的一部分 就要设置viewBox属性
```xml
<svg width="100%" height="100%">
  <circle id='myCircle' cx='50' cy='50' r='50'/>
</svg>
<hr>
<svg width="100" height="100" viewBox="50 50 50 50">
  <circle id='myCircle' cx='50' cy='50' r='50'/>
</svg>
<!-- 表现为切出来上一个圆形的右下角并等比例放大到了和原图一样大 -->
```

视窗 前两个表示视窗的位置坐标(x, y)
后两个表示视窗的宽高
表示在svg中 从(x,y)为左上角的位置 开始切割 宽高的元素 放在svg的width height定义大小的区域中


### <circle></circle>圆形
```xml
  <svg width="100%" height="100%">
    <circle id='myCircle' cx='50' cy='50' r='50'/>
  </svg>
```

cx cy表示圆心 r表示半径

### class
可以设置元素的类名 更精细的指定样式

### stroke
表示边框  strokewidth表示边框宽度

### <g></g>
表示一个组 所有被包裹的元素都共享g的属性 (比如颜色等)

### <rect></rect>长方形
### <line></line>线
```xml
  <svg width="500" height="500">
    <line x1="50" y1="50" x2="350" y2="350" stroke="red" stroke-width="10" ></line>
  </svg>
```
(x1, y1)到(x2, y2)的线段
同时间设置了颜色和宽度

### <polyline></polyline>折线
通过属性的设置绘制一些点之间的 折线


### <path></path>
path包含了很多动作 用来绘制复杂的图案

### <text></text>文本
可以通过css相关的 属性来设置svg中文本的样式


## scg可以控制可编程

```jsx
const Logo = (meta) => (
  <svg width={meta.width} height={meta.height}></svg>
)

<Logo meta={width:200, height: 200}/>
```

### svg中可以通过加入css属性来控制样式的变化


# D3.js
js操作svg的作图库 封装了大量的接口 支持各式各样的样式和动画
[D3]('https://github.com/d3/d3')