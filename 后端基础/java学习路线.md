# Java 学习路线
> 背景: 3年经验大厂前端学习 java 后端路线总结
> isaac 2025/03/01

## 符号表

- 🌕 所有同学必须学习！！！
- 🌖 非常急着找工作，才可不学；目标大厂，必须学习！
- 🌗 急着找工作的话，可不学；目标大厂，建议学习
- 🌘 时间充足的话，再去学


## 阶段 1：编程语言基础

### 🌕 Java 编程基础
- java环境搭建(JDK安装)
- IDEA 安装与使用
- **Java 基础语法**
- **面向对象**
   - 封装(权限修饰符)
   - 继承
   - 多态
- 抽象类
- 接口
- 枚举
- 常用类
- 集合类
- 泛型
- 注解
- 异常处理
- 多线程
- IO 流
- 反射


### 练手项目

- Java 实现简单计算器：[https://www.lanqiao.cn/courses/185](https://www.lanqiao.cn/courses/185)
- Eclipse 实现 Java 编辑器：[https://www.lanqiao.cn/courses/287](https://www.lanqiao.cn/courses/287)
- 一本糊涂账：[https://how2j.cn/module/104.html](https://how2j.cn/module/104.html)
- Java 五子棋：[https://blog.csdn.net/cnlht/article/details/8176130](https://blog.csdn.net/cnlht/article/details/8176130)
- Java 中国象棋：[https://blog.csdn.net/cnlht/article/details/8205733](https://blog.csdn.net/cnlht/article/details/8205733)
- JAVA GUI 图书馆管理系统：[https://github.com/uboger/LibraryManager](https://github.com/uboger/LibraryManager)
- JAVA 坦克大战小游戏：[https://github.com/wangzhengyi/TankWar](https://github.com/wangzhengyi/TankWar)
- Swing 编写的俄罗斯方块：[https://github.com/HelloClyde/Tetris-Swing](https://github.com/HelloClyde/Tetris-Swing)
- 小小记账本：[https://github.com/xenv/SmallAccount](https://github.com/xenv/SmallAccount) （适合了解数据库的同学）


## 阶段 2：巩固基础

建议大家去阅读 《Java 核心技术卷 1》，这本书堪称经典，是帮助你复习巩固 Java 的不二之选，其中图形界面章节可以选择不看。
巩固基础要花至少 1 个月的时间，当你读完《Java 核心技术卷 1》并且不用查询文档也能熟练地用 Java 做题时，就可以接着往下了。

### 🌖 操作系统
- 操作系统的组成
- 进程、线程
- 进程 / 线程间通讯方式
- 进程调度算法
- 进程 / 线程同步方式
- 进程 / 线程状态
- 死锁
- 内存管理
- 局部性原理

### 🌕 计算机网络
- 网络分层模型
- 网络传输过程
- IP、端口
- HTTP / HTTPS 协议
- UDP / TCP 协议
- ARP 地址解析协议
- 网络安全
- DNS 域名解析

## 阶段 3：企业开发基础

学完这个阶段后，你应该已经能独立开发出大多数常见的后台系统了，比如各种管理系统、商城系统等。

### 🌕 MySQL 数据库
- MySQL 环境搭建
- SQL 语句编写
- 约束
- 索引
- 事务
- 锁机制
- 设计数据库表
- 性能优化

其中，**SQL 语句编写** 和 **设计数据库表** 这两个能力一定要有！

### 🌕 Java Web
-  描述：Java 网页应用开发基础
-  XML
-  Servlet
-  Filter
-  Listener
-  JSP
-  JSTL

### 🌕 Spring 5
- 描述：Java 轻量级应用框架
- IOC
- AOP
- 事务

### 🌕 SpringMVC
- 描述：Java 轻量级 web 开发框架
- 什么是 MVC？
- Restful API
- 拦截器
- 配置
- 执行过程

### 🌕 MyBatis
- 描述：数据访问框架，操作数据库进行增删改查等操作
- 全局配置
- 动态 SQL
- 缓存
- 和其他框架的整合
- 逆向工程

### 🌗 MyBatis Plus
- 描述：Mybatis 的增强工具，能够简化开发、提高效率
- 引入
- 条件构造器
- 代码生成器
- 插件扩展
- 自定义全局操作

### 🌕 SpringBoot 2

- 描述：简化 Spring 应用的初始搭建以及开发过程，提高效率
- 常用注解
- 资源整合
- 高级特性
- 本地热部署

### 🌗 Spring Security

- 描述：Spring 的安全管理框架
- 用户认证
- 权限管理
- 相关技术：Shiro

### 🌗 Maven / Gradle

- 描述：项目管理工具
- 构建
- 依赖管理
- 插件
- 配置
- 子父工程
- 多模块打包构建
- Nexus 私服搭建


### 🌕 Linux
- Linux 系统安装
- 环境变量
- 文件管理
- 用户管理
- 内存管理
- 磁盘管理
- 进程管理
- 网络管理
- 软件包管理
- 服务管理
- 日志管理
- Linux 内核
- **常用命令**
- **常用环境搭建**
- **Shell 脚本编程**


## 阶段 4：企业开发进阶

### 🌖 设计模式

-  创建型模式：对象实例化的模式，创建型模式用于解耦对象的实例化过程
   - 单例模式
   - 工厂方法模式
   - 抽象工厂
   - 建造者模式
   - 原型模式
-  结构型模式：把类或对象结合在一起形成一个更大的结构
   - 适配器模式
   - 组合模式
   - 装饰器模式
   - 代理模式
   - 享元模式
   - 外观模式
   - 桥接模式
-  行为型模式：类和对象如何交互，及划分责任和算法
   - 迭代器模式
   - 模板方法模式
   - 策略模式
   - 命令模式
   - 状态模式
   - 责任链模式
   - 备忘录模式
   - 观察者模式
   - 访问者模式
   - 中介者模式
   - 解释器模式

### 🌕 Redis

缓存是高并发系统不可或缺的技术，可以提高系统的性能和并发，而 Redis 是实现缓存的最主流技术，因此它是后台开发必学的知识点，也是面试重点。

- Redis 基础
- 什么是缓存？
- 本地缓存
   - Caffeine 库
- 多级缓存
- Redis 分布式缓存
   - 数据类型
   - 常用操作
   - Java 操作 Redis
      - Spring Boot Redis Template
      - Redisson
   - 主从模型搭建
   - 哨兵集群搭建
   - 日志持久化
- 缓存（Redis）应用场景
   - 数据共享
   - 单点登录
   - 计数器
   - 限流
   - 点赞
   - 实时排行榜
   - 分布式锁
- 缓存常见问题
   - 缓存雪崩
   - 缓存击穿
   - 缓存穿透
   - 缓存更新一致性
- 相关技术：Memcached、Ehcache

### 🌖 消息队列
消息队列是用于传输和保存消息的容器，也是大型分布式系统中常用的技术，主要解决应用耦合、异步消息、流量削锋等问题。后台开发必学，也是面试重点。

- 消息队列的作用
- RabbitMQ 消息队列
   - 生产消费模型
   - 交换机模型
   - 死信队列
   - 延迟队列
   - 消息持久化
   - Java 操作
   - 集群搭建
- 相关技术：Kafka、ActiveMQ、TubeMQ、RocketMQ

### 🌖 Nginx

Nginx 是主流的、开源的、高性能的 HTTP 和反向代理 web 服务器，可以用于挂载网站、请求转发、负载均衡、网关路由等。前后端开发同学都需要学习，在后端开发的面试中有时会考到。

- Nginx 作用
- 正向代理
- 反向代理（负载均衡）
- 常用命令
- 配置
- 动静分离（网站部署）
- 集群搭建
- 相关技术：HAProxy、Apache

### 🌗 Netty 网络编程

开源的 Java 网络编程框架，用于开发高性能（事件驱动、异步非阻塞）、高可靠的网络服务器和客户端程序。

很多网络框架和服务器程序都用到了 Netty 作为底层，学好 Netty 不仅可以让我们自己实现高性能服务器，也能更好地理解其他的框架应用、阅读源码。
- IO 模型（BIO / NIO）
- Channel
- Buffer
- Seletor
- Netty 模型
- WebSocket 编程（动手做个聊天室）
- 相关技术：Vertx（中文文档：[http://vertxchina.github.io/vertx-translation-chinese/](http://vertxchina.github.io/vertx-translation-chinese/) ，比 Netty 简单多了，实在看不懂 Netty 也可以学习下这个）


不同于之前学的 SSM 框架，Netty 还是需要一定学习成本的，一方面是国内资源太缺乏，另一方面很多重要的概念（比如 NIO）还是要多动手写代码调试才能理解。

还是建议先从视频入门，并且不建议在 Netty 上花太多时间，面试的时候一般也就考察一些 Netty 背后的思想（比如 NIO）而非框架本身的语法细节。

### 🌖 Dubbo

- 架构演进
- RPC
- Zookeeper
- 服务提供者
- 服务消费者
- 项目搭建
- 相关技术：DubboX（对 Dubbo 的扩展）

### 🌖 微服务

- 微服务概念
- Spring Cloud 框架
   - 子父工程
   - 服务注册与发现
   - 注册中心 Eureka、Zookeeper、Consul
   - Ribbon 负载均衡
   - Feign 服务调用
   - Hystrix 服务限流、降级、熔断
   - Resilience4j 服务容错
   - Gateway（Zuul）微服务网关
   - Config 分布式配置中心
   - 分布式服务总线
   - Sleuth + Zipkin 分布式链路追踪
- Spring Cloud Alibaba
   - Nacos 注册、配置中心
   - OpenFeign 服务调用
   - Sentinel 流控
   - Seata 分布式事务


时间不急的话，建议先从 Dubbo 学起，对分布式、RPC、微服务有些基本的了解，再去食用 Spring Cloud 全家桶会更香。学完 Spring Cloud 全家桶后，再去学 Spring Cloud Alibaba 就很简单了。

这部分内容的学习，原理 + 实践都很重要，也不要被各种高大上的词汇唬住了，都是上层（应用层）的东西，基本没有什么算法，跟着视频教程学，其实还是很好理解的。

分布式相关知识非常多，但这里不用刻意去背，先通过视频教程实战使用一些微服务框架，也能对其中的概念有基本的了解。

大厂面试的时候很少问 Spring Cloud 框架的细节，更多的是微服务以及各组件的一些思想，比如网关的好处、消息总线的好处等。

###  🌖 Docker
   - 容器概念
   - 镜像
   - 部署服务
   - Dockerfile
   - Docker Compose
   - Docker Machine
   - Docker Swarm
   - 多阶段构建
### 🌘 K8S（Kubernetes）
   - K8S 架构
   - 工作负载
      - 资源类型
      - Pod
      - Pod 生命周期
      - Pod 安全策略
   - K8S 组件
   - K8S 对象
   - 部署应用
   - 服务
      - Ingress
   - Kubectl 命令行
   - 集群管理
- 相关技术：Apache Mesos、Mesosphere


### 🌗 CI / CD


## 阶段 5：项目实战
### 常用类库
#### 工具

- Guava：谷歌开发的 Java 工具库（https://github.com/google/guava）
- Apache Commons：各类工具库，比如 commons-lang、commons-io、commons-collections 等（https://github.com/apache/commons-lang）
- Hutool：Java 工具集库（https://github.com/looly/hutool）
- Lombok：Java 增强库（https://github.com/projectlombok/lombok）
- Apache HttpComponents Client：HTTP 客户端库（https://github.com/apache/httpcomponents-client）
- OkHttp：适用于 JVM、Android 等平台的 Http 客户端（https://github.com/square/okhttp）
- Gson：谷歌的 JSON 处理库（https://github.com/google/gson）
- Jcommander：Java 命令行参数解析框架（https://github.com/cbeust/jcommander）
- Apache PDFBox：PDF 操作库（https://github.com/apache/pdfbox）
- EasyExcel：阿里的 Excel 处理库（https://github.com/alibaba/easyexcel）
- Apache POI：表格文件处理库（https://github.com/apache/poi）

#### 测试

- JUnit：Java 测试框架（https://github.com/junit-team/junit4）
- Mockito：Java 单元测试 Mock 框架（https://github.com/mockito/mockito）
- Selenium：浏览器自动化框架（https://github.com/SeleniumHQ/selenium）
- htmlunit：Java 模拟浏览器（https://github.com/HtmlUnit/htmlunit）
- TestNG：Java 测试框架（https://github.com/cbeust/testng）
- Jacoco：Java 代码覆盖度库（https://github.com/jacoco/jacoco）

#### 其他

- cglib：字节码生成库（https://github.com/cglib/cglib）
- Arthas：Java 诊断工具（https://github.com/alibaba/arthas）
- config：针对 JVM 的配置库（https://github.com/lightbend/config）
- Quasar：Java 纤程库（https://github.com/puniverse/quasar）
- drools：Java 规则引擎（https://github.com/kiegroup/drools）
- Caffeine：Java 高性能缓存库（https://github.com/ben-manes/caffeine）
- Disruptor：高性能线程间消息传递库（https://github.com/LMAX-Exchange/disruptor）
- Knife4j：Swagger 文档增强（https://doc.xiaominfo.com/）
- Thumbnailator：Java 缩略图生成库（https://github.com/coobird/thumbnailator）
- Logback：Java 日志库（https://github.com/qos-ch/logback）
- Apache Camel：消息传输集成框架（https://github.com/apache/camel）
- Quartz：定时任务调度库（https://github.com/quartz-scheduler/quartz）
- Apache Mahout：机器学习库（https://github.com/apache/mahout）
- Apache OpenNLP：NLP 工具库（https://github.com/apache/opennlp）
- RxJava：JVM 反应式编程框架（https://github.com/ReactiveX/RxJava）
- JProfiler：性能分析库（https://www.ej-technologies.com/products/jprofiler/overview.html）
- jsoup：HTML 文档解析库（https://jsoup.org/）
- webmagic：Java 爬虫框架（https://github.com/code4craft/webmagic/）

### IDEA 插件

-  Chinese Language Pack
   - 描述：中文支持
   - 官网：[https://plugins.jetbrains.com/plugin/13710-chinese-simplified-language-pack----](https://plugins.jetbrains.com/plugin/13710-chinese-simplified-language-pack----)
-  Translation
   - 描述：翻译插件，鼠标选中文本，点击右键即可自动翻译成多国语言。
   - 官网：[https://plugins.jetbrains.com/plugin/8579-translation/](https://plugins.jetbrains.com/plugin/8579-translation/)
-  Key Promoter X
   - 描述：快捷键提示插件。当你执行鼠标操作时，如果该操作可被快捷键代替，会给出提示，帮助你自然形成使用快捷键的习惯，告别死记硬背。
   - 官网：[https://plugins.jetbrains.com/plugin/9792-key-promoter-x/](https://plugins.jetbrains.com/plugin/9792-key-promoter-x/)
-  Rainbow Brackets
   - 描述：给括号添加彩虹色，使开发者通过颜色区分括号嵌套层级，便于阅读
   - 官网：[https://plugins.jetbrains.com/plugin/10080-rainbow-brackets/](https://plugins.jetbrains.com/plugin/10080-rainbow-brackets/)
-  CodeGlance
   - 描述：在编辑器右侧生成代码小地图，可以拖拽小地图光标快速定位代码，阅读行数很多的代码文件时非常实用。
   - 官网：[https://plugins.jetbrains.com/plugin/7275-codeglance/](https://plugins.jetbrains.com/plugin/7275-codeglance/)
-  WakaTime
   - 描述：代码统计和跟踪插件
   - 官网：[https://plugins.jetbrains.com/plugin/7425-wakatime](https://plugins.jetbrains.com/plugin/7425-wakatime)
-  Statistic
   - 描述：代码统计
   - 官网：[https://plugins.jetbrains.com/plugin/4509-statistic](https://plugins.jetbrains.com/plugin/4509-statistic)


-  String Manipulation
   - 描述：字符串快捷处理
   - 官网：[https://plugins.jetbrains.com/plugin/2162-string-manipulation](https://plugins.jetbrains.com/plugin/2162-string-manipulation)
-  Tabnine AI Code Completion
   - 描述：使用 AI 去自动提示和补全代码，比 IDEA 自带的代码补全更加智能化
   - 官网：[https://plugins.jetbrains.com/plugin/12798-tabnine-ai-code-completion-js-java-python-ts-rust-go-php--more](https://plugins.jetbrains.com/plugin/12798-tabnine-ai-code-completion-js-java-python-ts-rust-go-php--more)
-  GsonFormatPlus
   - 描述：根据 json 生成对象
   - 官网：[https://plugins.jetbrains.com/plugin/14949-gsonformatplus](https://plugins.jetbrains.com/plugin/14949-gsonformatplus)
-  JUnitGenerator V2.0
   - 描述：自动生成单元测试
   - 官网：[https://plugins.jetbrains.com/plugin/3064-junitgenerator-v2-0](https://plugins.jetbrains.com/plugin/3064-junitgenerator-v2-0)
-  RestfulTool
   - 描述：辅助 web 开发的工具集
   - 官网：[https://plugins.jetbrains.com/plugin/14280-restfultool](https://plugins.jetbrains.com/plugin/14280-restfultool)
-  SequenceDiagram
   - 描述：自动生成方法调用时序图
   - 官网：[https://plugins.jetbrains.com/plugin/8286-sequencediagram](https://plugins.jetbrains.com/plugin/8286-sequencediagram)
-  CheckStyle-IDEA
   - 描述：自动检查 Java 代码规范
   - 官网：[https://plugins.jetbrains.com/plugin/1065-checkstyle-idea](https://plugins.jetbrains.com/plugin/1065-checkstyle-idea)
-  Alibaba Java Coding Guidelines
   - 描述：代码规范检查插件
   - 官网：[https://plugins.jetbrains.com/plugin/10046-alibaba-java-coding-guidelines](https://plugins.jetbrains.com/plugin/10046-alibaba-java-coding-guidelines)
-  SonarLint
   - 描述：帮助你发现和修复代码的错误和漏洞
   - 官网：[https://plugins.jetbrains.com/plugin/7973-sonarlint](https://plugins.jetbrains.com/plugin/7973-sonarlint)
-  MybatisX
   - 描述：MyBatis 增强插件，支持自动生成 entity、mapper、service 等常用操作的代码，优化体验
   - 官网：[https://plugins.jetbrains.com/plugin/10119-mybatisx](https://plugins.jetbrains.com/plugin/10119-mybatisx)
-  RestfulTool
   - 描述：辅助 web 开发的工具集
   - 官网：[https://plugins.jetbrains.com/plugin/14280-restfultool](https://plugins.jetbrains.com/plugin/14280-restfultool)
-  Multirun
   - 描述：同时启动多应用
   - 官网：[https://plugins.jetbrains.com/plugin/7248-multirun](https://plugins.jetbrains.com/plugin/7248-multirun)
- Free Mybatis Plugin
   - 描述：MyBatis 增强插件，支持 mapper => xml 的跳转、代码生成等功能
   - 官网：[https://plugins.jetbrains.com/plugin/8321-free-mybatis-plugin](https://plugins.jetbrains.com/plugin/8321-free-mybatis-plugin)
- MyBatis Log Plugin
   - 描述：MyBatis SQL 提取和格式化输出
   - 官网：[https://plugins.jetbrains.com/plugin/10065-mybatis-log-plugin](https://plugins.jetbrains.com/plugin/10065-mybatis-log-plugin)
- Maven Helper
   - 描述：Maven 辅助插件
   - 官网：[https://plugins.jetbrains.com/plugin/7179-maven-helper](https://plugins.jetbrains.com/plugin/7179-maven-helper)
- Gradle View
   - 描述：Gradle 项目管理工具的扩展
   - 官网：[https://plugins.jetbrains.com/plugin/7150-gradle-view](https://plugins.jetbrains.com/plugin/7150-gradle-view)
- Arthas Idea
   - 描述： Arthas 命令生成插件。Arthas 是阿里开源的 Java 在线诊断工具，该插件可以自动生成 Arthas 在线 Java 代码诊断命令，不用再到官网翻文档拼命令啦！
   - 官网：[https://plugins.jetbrains.com/plugin/13581-arthas-idea/](https://plugins.jetbrains.com/plugin/13581-arthas-idea/)
- GitToolBox
   - 描述： Git 增强插件。在自带的 Git 功能之上，新增了查看 Git 状态、自动拉取代码、提交通知等功能。最好用的是可以查看到每一行代码的最近一次提交信息。
   - 官网：[https://plugins.jetbrains.com/plugin/7499-gittoolbox/](https://plugins.jetbrains.com/plugin/7499-gittoolbox/)
- BashSupport
   - 描述：支持 Bash 脚本文件的高亮和提示等
   - 官网：[https://plugins.jetbrains.com/plugin/4230-bashsupport](https://plugins.jetbrains.com/plugin/4230-bashsupport)
- Git Flow Integration
   - 描述：Git Flow 的图形界面操作
   - 官网：[https://plugins.jetbrains.com/plugin/7315-git-flow-integration](https://plugins.jetbrains.com/plugin/7315-git-flow-integration)
- Gitee
   - 描述：开源中国的码云插件
   - 官网：[https://plugins.jetbrains.com/plugin/8383-gitee](https://plugins.jetbrains.com/plugin/8383-gitee)
- Drools
   - 描述：规则引擎的扩展
   - 官网：[https://plugins.jetbrains.com/plugin/16871-drools](https://plugins.jetbrains.com/plugin/16871-drools)
- EnvFile
   - 描述：对多环境配置文件的支持
   - 官网：[https://plugins.jetbrains.com/plugin/7861-envfile](https://plugins.jetbrains.com/plugin/7861-envfile)
- Kubernetes
   - 描述：容器管理的扩展
   - 官网：[https://plugins.jetbrains.com/plugin/10485-kubernetes](https://plugins.jetbrains.com/plugin/10485-kubernetes)
- Scala
   - 描述：Scala 语言的扩展
   - 官网：[https://plugins.jetbrains.com/plugin/1347-scala](https://plugins.jetbrains.com/plugin/1347-scala)
- Zookeeper
   - 描述：Zookeeper 中间件的扩展
   - 官网：[https://plugins.jetbrains.com/plugin/7364-zookeeper](https://plugins.jetbrains.com/plugin/7364-zookeeper)
- Jms messenger
   - 描述：对 Java 消息服务的扩展
   - 官网：[https://plugins.jetbrains.com/plugin/10949-jms-messenger](https://plugins.jetbrains.com/plugin/10949-jms-messenger)
- MapStruct Support
   - 描述：对 MapStruct 对象转换的支持
   - 官网：[https://plugins.jetbrains.com/plugin/10036-mapstruct-support](https://plugins.jetbrains.com/plugin/10036-mapstruct-support)
- Big Data Tools
   - 描述：提供了 HDFS 等大数据框架的工具集
   - 官网：[https://plugins.jetbrains.com/plugin/12494-big-data-tools](https://plugins.jetbrains.com/plugin/12494-big-data-tools)
- CSV
   - 描述：支持 CSV 文件的高亮和提示等
   - 官网：[https://plugins.jetbrains.com/plugin/10037-csv](https://plugins.jetbrains.com/plugin/10037-csv)
- Ideolog
   - 描述：识别和格式化 log 日志文件
   - 官网：[https://plugins.jetbrains.com/plugin/9746-ideolog](https://plugins.jetbrains.com/plugin/9746-ideolog)
- Toml
   - 描述：支持 toml 文件的高亮和提示等
   - 官网：[https://plugins.jetbrains.com/plugin/8195-toml](https://plugins.jetbrains.com/plugin/8195-toml)
- .env files support
   - 描述：支持 .env 文件的高亮和提示等
   - 官网：[https://plugins.jetbrains.com/plugin/9525--env-files-support](https://plugins.jetbrains.com/plugin/9525--env-files-support)

### 常用软件
#### 开发相关
- Navicat：数据库管理软件（https://www.navicat.com.cn/）
- JMeter：Java 性能测试工具（https://jmeter.apache.org/）
- JVisual VM：Java 运行状态可视化工具（https://visualvm.github.io/）
- Redis Desktop Manager：Redis 可视化管理工具（https://github.com/uglide/RedisDesktopManager）
- Chocolatey：Windows 软件包管理器（https://chocolatey.org/）

## 阶段 6：Java 高级

### 🌖 并发编程
- 线程和进程
- 线程状态
- 并行和并发
- 同步和异步
- Synchronized
- Volatile 关键字
- Lock 锁
- 死锁
- 可重入锁
- 线程安全
- 线程池
- JUC 的使用
- AQS
- Fork Join
- CAS

### 🌖 JVM
- JVM 内存结构
- JVM 生命周期
- 主流虚拟机
- Java 代码执行流程
- 类加载
   - 类加载器
   - 类加载过程
   - 双亲委派机制
- 垃圾回收
   - 垃圾回收器
   - 垃圾回收策略
   - 垃圾回收算法
   - StopTheWorld
- 字节码
- 内存分配和回收
- JVM 性能调优
   - 性能分析方法
   - 常用工具
   - 参数设置
- Java 探针
- 线上故障分析


### 🌖 分布式

- 分布式理论
   - CAP
   - BASE
- 分布式缓存
   - Redis
   - Memcached
   - Etcd
- 一致性算法
   - Raft
   - Paxos
   - 一致性哈希
- 分布式事务
   - 解决方案
      - 2PC
      - 3PC
      - TCC
      - 本地消息表
      - MQ 事务消息
      - 最大努力通知
   - LCN 分布式事务框架：[https://github.com/codingapi/tx-lcn](https://github.com/codingapi/tx-lcn)
- 分布式 id 生成
   - Leaf 分布式 id 生成服务：[https://github.com/Meituan-Dianping/Leaf](https://github.com/Meituan-Dianping/Leaf)
- 分布式任务调度
   - XXL-JOB 调度平台：[https://www.xuxueli.com/xxl-job/](https://www.xuxueli.com/xxl-job/)
   - elastic-job：[https://gitee.com/elasticjob/elastic-job](https://gitee.com/elasticjob/elastic-job)
- 分布式服务调用
   - trpc
- 分布式存储
   - HDFS
   - Ceph
- 分布式数据库
   - TiDB
   - OceanBase
- 分布式文件系统
   - HDFS
- 分布式协调
   - Zookeeper
- 分布式监控
   - Prometheus
   - Zabbix
- 分布式消息队列
   - RabbitMQ
   - Kafka
   - Apache Pulsar
- 分布式日志收集
   - Elastic Stack
   - Loki
- 分布式搜索引擎
   - Elasticsearch
- 分布式链路追踪
   - Apache SkyWalking
- 分布式配置中心
   - Apollo
   - Nacos

### 🌗 高可用

- 限流
- 降级熔断
- 冷备
- 双机热备
- 同城双活
- 异地双活
- 异地多活
- 容灾备份

### 🌗 高并发

- 数据库
   - 分库分表
      - MyCat 中间件
      - Apache ShardingSphere 中间件
   - 读写分离
- 缓存
   - 缓存雪崩
   - 缓存击穿
   - 缓存穿透
- 负载均衡
   - 负载均衡算法
   - 软硬件负载均衡（2、3、4、7 层）

### 🌘 服务网格

服务网格用来描述组成应用程序的微服务网络以及它们之间的交互。服务网格的规模和复杂性不断的增长，它将会变得越来越难以理解和管理，常见的需求包括服务发现、负载均衡、故障恢复、度量和监控等。

- Istio
   - 流量管理
   - 安全性
   - 可观测性
- Envoy（开源的边缘和服务代理）

### 🌘 DDD 领域驱动设计

将数据、业务流程抽象成容易理解的领域模型，通过用代码实现领域模型，来组成完整的业务系统。

- DDD 的优势
- DDD 的适用场景
- DDD 核心概念
   - 领域模型分类：失血、贫血、充血、涨血
   - 子域划分：核心域、通用域、支撑域
   - 限界上下文
   - 实体和值对象
   - 聚合设计
   - 领域事件
- DDD 实践
