> 不属于单独的java学习笔记, 而是有js经验后的java笔记
> 因此会出现很多"和js一致"的说明


# java安装
Oracle搜索java安装
`2025/02/09` 选择安装的 JDK 21 LTS版本

安装完成后, 命令行输入`java -version`查看是否安装成功

# HelloWorld
1. 使用编辑器编写HelloWorld.java文件
```java
class HelloWorld {
    public static void main(String[] args) {
        System.out.println("Hello World!");
    }
}
```

2. 编译
命令行同目录下输入javac编译, 注意时javac
`javac HelloWorld.java`
此时会生成一个HelloWorld.class字节码文件

3. 执行
命令行同目录下使用java指令执行字节码文件
`java HelloWorld`

# 变量和标识符
## 数据类型
基本数据类型：包括 整数类型、浮点数类型、字符类型、布尔类型。

引用数据类型：包括 数组、 类、接口、枚举、注解、记录。

### 整数类型
#### byte
1byte = 8bit
有符号的数字
-128 到 127
10000000 到 11111111  -128 到 -1
负数计算规则是十进制数取反再加1
128 = 10000000
取反 01111111
加一 10000000
10000000 表示 -128
00000000 到 01111111 0到127

#### short
2byte数据

#### int
4byte数据

#### long
8byte数据
数字最后必须加l或者L

### 浮点数
#### float
单精度4byte数据
精确到7位有效数字
数字最后必须加f或者F 否则认为是double

#### double
双精度8byte数据
默认类型

### 字符类型
#### char
占用2byte
使用单引号的单个字符
字母 数字 中文
char a = 'a'
char b = '中'
如果char的值是数字, 会转换成数字ascii码对应的字符
char c = 65;
实际打印是A

### 布尔类型
#### boolean


### String
String +只能做字符串拼接
注意单引号char 双引号String
凡是char + char 一定是先ascii数字运算
```java
class StringTest {
    public static void main(String[] args) {
        String s2 = 4.5f + "";
        System.out.println(s2); // 4.5
        System.out .println(3+4+"Hello!");  // 7Hello!
        System.out.println("Hello!"+3+4); // Hello!34
        System.out.println('a'+1+"Hello!"); // 98Hello! 注意单引号是char
        System.out.println("Hello"+'a'+1); // Hello!a1
    }
}
```
```java
class StringTest {
    public static void main(String[] args) {
        // 凡是char + char 一定是先ascii数字运算
        // 一但靠前的运算中涉及String, 都变成字符串拼接操作
        System.out.println("* *"); // **
        System.out.println("*\t*"); // *    *
        System.out.println("*" + "\t" + "*"); // *    *
        System.out.println('*' + "\t" + "*"); // *    *
        System.out.println('*' + '\t' + "*"); // 51*
        System.out.println('*' + "\t" + '*'); // *    *
        System.out.println("*" + '\t' + '*'); // *    *
        System.out.println('*' + '\t' + '*'); // 93
        System.out.println('*' + 0); // 42
        System.out.println('\t' + 0); // 9
    }
}
```

# 面向对象
属性 等价于js中的class中的属性
方法 等价于js中的class中的方法
构造器 和js不同
```java
// js中的写法
class Person {
    construcor(){
        // 在new关键字创建实例对象时调用
    }
}

// java中的写法
class Person {
    // 和类名相同的一个方法就constructor 构造器函数
    // 并且构造器方法的权限修饰符和类的权限修饰符一致 (例如此处都是缺省的默认修饰符)
    Person(){
        // 在new关键字创建实例对象时调用
    }
}
```
java的构造器支持重载 (多种参数类型)
```java
public class Person {
    private String name;
    private int age;
    private String school;

    // constructor
    public Person(String n, int a) {
        name = n;
        age = a;
    }

    // 重载 适配不同的形参
    public Person(String n, int a, String s) {
        name = n;
        age = a;
        school = s;
    }
}
```
实例对象 等同于js中的实例对象






## 封装
### 权限修饰符
**访问范围**
public:      本类 本包 跨包子类 跨包非继承子类
protected:   本类 本包 跨包子类
缺省:         本类 本包
private:     本类

日常工作中用的相对最多的: public private

### this
基本约等于js中的this, 指向类创建的实例对象
不同的是java可以在类省略this, 默认访问的就是实例对象中的属性, 方法
js的类中不可省略this

java可以直接在构造器方法首行this(), 表示调用空参数的构造器函数
this()中间传参就表示调用参数一致的构造器函数
this方法必须首行

```java
package staticTest;

public class CircleTest {
    public static void main(String[] args) {
        Circle c1 = new Circle();
        Circle c2 = new Circle();

        System.out.println(c1);
        System.out.println(c2);

        System.out.println(Circle.count);
    }
}


class Circle {
    private double radius;
    private int id;
    static int count = 0;
    private static int initId = 1001;

    public Circle() {
        this.id = initId++;
        count++;
    }

    public Circle(double radius) {
        // 通过this调用构造器函数, 执行一些通用的参数
        // 主要是自增id和统计总数
        this();
        this.radius = radius;
    }

    @Override
    public String toString() {
        return "Circle{" +
                "radius=" + radius +
                ", id=" + id +
                '}';
    }
}
```


### 数组增删改查
```JAVA
public class CustomList {
    private Customer[] customers;
    // 客户对象的数量
    private int total;

    public CustomList() {
    }

    public CustomList(int totalCustomer) {
        customers = new Customer[totalCustomer];
    }

    public boolean addCustomer(Customer c) {
        if (total < customers.length) {
            customers[total++] = c;
            return true;
        }
        return false;
    }

    public boolean replaceCustomer(int index, Customer c) {
        if (index >= 0 && index < total) {
            customers[index] = c;
            return true;
        }
        return false;
    }

    public boolean deleteCustomer(int index) {
        if (index >= 0 && index < total) {
            for(int i = index;i< total - 1;i++) {
                // 使用后覆盖前代替删除操作
                customers[i] = customers[i+1];
            }
            customers[total - 1] = null;
            total--;
            return true;
        }
        return false;
    }

    public Customer[] getCustomers() {
        // 拷贝一下
        Customer[] cus = new Customer[total];
        for (int i = 0; i < cus.length; i++) {
            cus[i] = customers[i];
        }
        return cus;
    }

    public Customer getCustomer(int index) {
        if (index >= 0 && index < total) {
            return customers[index];
        }
        return null;
    }

    public int getTotal() {
        return total;
    }

}
```





## 继承
大体和js的继承一致, 使用extends

### super
super 的追溯不仅限于直接父类
super 可用于访问父类属性; 可用于调用父类方法; 可用于在子类构造器中调用父类的构造器(super()必须在构造器方法首行)


### super和this访问顺序
参考js原型链

方法前面没有 super.和 this.
先从子类找匹配方法，如果没有，再从直接父类找，再没有，继续往上追溯

方法前面有 this.
先从子类找匹配方法，如果没有，再从直接父类找，再没有，继续往上追溯

方法前面有 super.
从当前子类的直接父类找，如果没有，继续往上追溯

## 多态
结合ts理解
一句话介绍: 即需要A类型, 那A类的所有子类都能满足要求

子类型可以赋值给父类
`People p1 = new Student()`

但是这里的父类变量, 只能用到父类子类中都有的公共方法, 而不能使用子类的特有方法
理解为取交集


# 类成员
属性 方法 构造器 代码块

属性 方法和js一致
构造器略有不同

## 代码块
只能直接写和用static修饰
一个类中可以写多个代码块

```java
public class SingleTest {
    public static void main(String[] args) {
        Single s1 = new Single();
        Single s2 = new Single();
    }
}

class Single {
    // 非静态代码块在创建实例对象的时候执行
    // 每创建一次实例对象都执行
    {
        System.out.println("我是非静态代码");
    }

    // 静态代码块只在 类加载到内存中执行一次
    // 后面再使用类也不会执行了
    static {
        System.out.println("我是静态代码");
    }
}
```
