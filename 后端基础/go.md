# 官网
https://go.dev/

# Hello World
```go
package main
import "fmt"

func main(){
	fmt.Println("hello, world")
}
```

- 编译为可执行文件再运行
go build hello.go
./hello 执行可执行文件

- 直接运行
go run hello.go

# 数据类型
## 基本数据类型
int
float
string
数组 (和js中的数组不一样)
结构体

## 引用数据类型
指针
切片
管道
map
interface


# 标识符命名
规则和js一致, 尽量小驼峰

英文_数字, 只能英文和_开头, 严格区分大小写

*首字母大写的各种变量可以被其他包访问, 小驼峰的变量只能本包访问*

# 包
包名尽量和所在文件夹命名一致


# 运算符
规则大体和js一致

区别:
- 不允许前置++, ---, 例如++a
- 自增和自减只能单独作为语句, 不返回值, 编译报错
if i++ > 10 会报错
a := i++ 会报错


# 指针
&运算符可以获取变量的地址
*运算符可以获取指针指向的值

# GO没有三目运算符

# 流程控制语句
基本和js一致

区别:
- switch没有break
每个case如果匹配完成 即结束switch
js中不加break的case匹配上还会几乎判断后面的case

- go要实现case的穿透 可以用fallthrough代替break, 只能穿透一层 不管下一层case对不对
注意这个和js的不写break不一样, js不写break会继续判断后面的 case条件是否满足, fallthrough直接默认下一层执行 不管case是否满足

## for循环

方式一
```go
for i:= 0;i< 10;i++ {
  //...
}
```

- go中没有while 和do while
for {} 相当于while(1) {}


方式二 类似js for of
```go
str := "hello world"
for idx, v := range str {
  fmt.Printf("%v, %c \n",idx, v)
}
```

# 函数
```go
func 函数名 (参数) (返回值类型) {

}
```

# 包的基本使用
类似js里面的模块化

# 常用内置包

生产随机数
```go
import (
	"math/rand"
	"time"
)
// 设置随机种子 使用纳秒
rand.Seed(time.Now().UnixNano())

// 生成[0, 100)中间的随机数
rand.Intn(100)

// 生成[a, b]中间的随机数
rand.Intn(b - a + 1) + a
```